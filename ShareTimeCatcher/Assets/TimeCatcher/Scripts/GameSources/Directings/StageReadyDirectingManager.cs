﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public enum StageReadyDirectingStep
{
	TargetTimeStep = 0,
	ReadyStep,
	GoStep,
	DirectingEnd,
}

public class StageReadyDirectingManager : MonoBehaviour 
{
    #region Serialize Variables

#pragma warning disable 649

    [SerializeField] tk2dSprite _stageTextHundredDigitSprite;
    [SerializeField] tk2dSprite _stageTextTenDigitSprite;
	[SerializeField] tk2dSprite _stageTextDigitSprite;

#pragma warning restore 649

    #endregion

    #region Variables

	Action _onFinishedDirecting = null;

	#endregion

	#region Properties

	public Action OnFinishedDirecting
	{
		get{ return _onFinishedDirecting; }
		set{ _onFinishedDirecting = value; }
	}

    #endregion

    #region MonoBehaviour Methods

    #endregion

    #region Methods

    void SetDirectingEnd()
	{
		if (_onFinishedDirecting != null) {
			_onFinishedDirecting ();
		}
	}

	public void SetStageNumber(int stageNum)
	{
		if (stageNum < 10) {string stageSpriteName = string.Format ("{0}", stageNum);
			_stageTextTenDigitSprite.SetSprite (stageSpriteName);
			_stageTextDigitSprite.gameObject.SetActive (false);
		} else if (stageNum >= 10) {
			string stageTenDigit = string.Format ("{0}", stageNum/10);
			string stageDigit = string.Format ("{0}", stageNum%10);
			_stageTextTenDigitSprite.SetSprite (stageTenDigit);
			_stageTextDigitSprite.SetSprite (stageDigit);
			_stageTextDigitSprite.gameObject.SetActive (true);
		} else {
			string stageSpriteName = string.Format ("{0}", stageNum);
			_stageTextTenDigitSprite.SetSprite (stageSpriteName);
		}
	}

	#endregion
}
