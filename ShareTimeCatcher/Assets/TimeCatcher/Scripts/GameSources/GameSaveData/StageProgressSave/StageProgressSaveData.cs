﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LitJson;
using System.Text;
using System.Linq;

public struct StageCurrentInfo
{
	public int stageNum;
	public int starCount;

	public StageCurrentInfo(int num, int count)
	{
		stageNum = num;
		starCount = count;
    }
}

public class StageProgressSaveData : CAGameSaveData 
{
    #region Variables

    float _startStageAngle = -1f;
    int _isGameEvaluate = 0; //0 : Not, 1: Do
    int _evaluatePlayCount;
    int _evaluateAskCount;
    //int _perfectCount;
    //int _isPerfectNotify = 0; // 0 : Not, 1: Do
    Dictionary<int/* Stage Number */, StageCurrentInfo> _stageCurrentInfos = new Dictionary<int, StageCurrentInfo>();

	#endregion

	#region Properties

    public float StartStageAngle
    {
        get { return _startStageAngle; }
        set { _startStageAngle = value; }
    }

    public int IsGameEvaluate
    {
        get { return _isGameEvaluate; }
        set { _isGameEvaluate = value; }
    }

    public int EvaluatePlayCount
    {
        get { return _evaluatePlayCount; }
        set { _evaluatePlayCount = value; }
    }

    public int EvaluateAskCount
    {
        get { return _evaluateAskCount; }
        set { _evaluateAskCount = value; }
    }

    //public int PerfectCount
    //{
    //    get { return _perfectCount; }
    //    set { _perfectCount = value; }
    //}

    //public int IsPerfectNotify
    //{
    //    get { return _isPerfectNotify; }
    //    set { _isPerfectNotify = value; }
    //}

    public override string SaveFileName{
		get{ return "StageProgressSaveData.json"; }
	}

	public Dictionary<int/* Stage Number */, StageCurrentInfo> StageCurrentInfos
	{
		get{ return _stageCurrentInfos; }
	}

	#endregion

	#region Methods

	public void LoadStageProgressData()
	{
		_stageCurrentInfos.Clear ();

        //string readFile = CAFileCtl.Instance.ReadStringFromFile (SaveFileName);
        string readFile = LoadString();

        if (string.IsNullOrEmpty (readFile)) {
			return;
		}

		JsonReader reader = new JsonReader (readFile);
		int objectCount = 0;
		int arrayCount = 0;
		while (reader.Read ()) {
			switch (reader.Token) {
			case JsonToken.ObjectStart:
				objectCount++;
				break;
			case JsonToken.ObjectEnd:
				objectCount--;
				break;
			case JsonToken.ArrayStart:
				arrayCount++;
				break;
			case JsonToken.ArrayEnd:
				arrayCount--;
				break;
			}

            if (reader.Token == JsonToken.PropertyName && (string)reader.Value == "StartStageAngle")
            {
                reader.Read();
                if(reader.Token == JsonToken.Int)
                {
                    _startStageAngle = (float)(int)reader.Value;
                } else if(reader.Token == JsonToken.Double)
                {
                    _startStageAngle = (float)(double)reader.Value;
                }

            } else if (reader.Token == JsonToken.PropertyName && (string)reader.Value == "IsGameEvaluate") {
                reader.Read();
                _isGameEvaluate = (int)reader.Value;

            } else if (reader.Token == JsonToken.PropertyName && (string)reader.Value == "EvaluatePlayCount") {
                reader.Read();
                _evaluatePlayCount = (int)reader.Value;

            } else if (reader.Token == JsonToken.PropertyName && (string)reader.Value == "EvaluateAskCount") {
                reader.Read();
                _evaluateAskCount = (int)reader.Value;

            } 
            //else if (reader.Token == JsonToken.PropertyName && (string)reader.Value == "PerfectCount") {
            //    reader.Read();
            //    _perfectCount = (int)reader.Value;

            //}
            //else if (reader.Token == JsonToken.PropertyName && (string)reader.Value == "IsPerfectNotify")
            //{
            //    reader.Read();
            //    _isPerfectNotify = (int)reader.Value;

            //}
            else if (reader.Token == JsonToken.PropertyName && (string)reader.Value == "StageInfos") {
				StageCurrentInfo stageCurrent = new StageCurrentInfo (0, 0);
				while (reader.Read ()) {
					switch (reader.Token) {
					case JsonToken.ObjectStart:
						objectCount++;
						break;
					case JsonToken.ObjectEnd:
						objectCount--;
						break;
					case JsonToken.ArrayStart:
						arrayCount++;
						break;
					case JsonToken.ArrayEnd:
						arrayCount--;
						break;
					}

					if (reader.Token == JsonToken.ArrayEnd && arrayCount == 0) {
						break;
					} else if (reader.Token == JsonToken.ObjectStart && objectCount == 2) {
						stageCurrent = new StageCurrentInfo (0, 0);
					} else if (reader.Token == JsonToken.ObjectEnd && objectCount == 1) {
						_stageCurrentInfos.Add (stageCurrent.stageNum, stageCurrent);
					} else if (reader.Token == JsonToken.PropertyName && (string)reader.Value == "StageNum") {
						reader.Read ();
						stageCurrent.stageNum = (int)reader.Value;
					} else if (reader.Token == JsonToken.PropertyName && (string)reader.Value == "StarCount") {
						reader.Read ();
						stageCurrent.starCount = (int)reader.Value;
					}
				} // while (reader.Read ()) {
			} // if (reader.Token == JsonToken.PropertyName && (string)reader.Value == "StageInfos") {
			else if (reader.Token == JsonToken.ObjectEnd && objectCount == 0) {
				break;
			}
		}
	}

	public override void SaveData()
	{
        base.SaveData();

		if (_stageCurrentInfos.Count == 0)
			return;

		StringBuilder sb = new StringBuilder();
		JsonWriter writer = new JsonWriter(sb);

		writer.WriteObjectStart ();
		{
            writer.WritePropertyName("StartStageAngle");
            writer.Write(_startStageAngle);

            writer.WritePropertyName("IsGameEvaluate");
            writer.Write(_isGameEvaluate);

            writer.WritePropertyName("EvaluatePlayCount");
            writer.Write(_evaluatePlayCount);

            writer.WritePropertyName("EvaluateAskCount");
            writer.Write(_evaluateAskCount);

            //writer.WritePropertyName("PerfectCount");
            //writer.Write(_perfectCount);

            //writer.WritePropertyName("IsPerfectNotify");
            //writer.Write(_isPerfectNotify);

            writer.WritePropertyName ("StageInfos");
			writer.WriteArrayStart ();
			{
				List<int> stageKeyValues = _stageCurrentInfos.Keys.ToList ();
				for (int i = 0; i < stageKeyValues.Count; i++) {
					writer.WriteObjectStart ();
					{
						writer.WritePropertyName ("StageNum");
						writer.Write (_stageCurrentInfos[stageKeyValues[i]].stageNum);

						writer.WritePropertyName ("StarCount");
						writer.Write (_stageCurrentInfos[stageKeyValues[i]].starCount);
					}
					writer.WriteObjectEnd ();
				}
			}
			writer.WriteArrayEnd ();
		}
		writer.WriteObjectEnd ();

        //CAFileCtl.Instance.WriteStringToFile (sb.ToString (), SaveFileName);
        WriteSaveData(sb.ToString());
    }

	public StageCurrentInfo GetStageCurrentInfo(int stageNum)
	{
		if (!_stageCurrentInfos.ContainsKey (stageNum))
			return new StageCurrentInfo (stageNum, 0);

		return _stageCurrentInfos [stageNum];
	}

	public void SetStageStarCount(int stageNum, int starCount)
	{
		bool saveState = false;
		if (!_stageCurrentInfos.ContainsKey (stageNum)) {
			_stageCurrentInfos.Add (stageNum, new StageCurrentInfo (stageNum, starCount));
			saveState = true;
		} else {
			if (_stageCurrentInfos [stageNum].starCount != starCount && _stageCurrentInfos [stageNum].starCount < starCount) {
				_stageCurrentInfos [stageNum] = new StageCurrentInfo (stageNum, starCount);
				saveState = true;
			}
		}

		if (saveState) {
            SaveData();
		}
	}

    public void SetStartStartAngle(float startAngle)
    {
        if (_startStageAngle == startAngle)
            return;

        _startStageAngle = startAngle;
        SaveData();
    }

    public override void RemoveSaveData()
	{
		CAFileCtl.Instance.DeleteFile (SaveFileName);
		_stageCurrentInfos.Clear ();
	}

	#endregion
}
