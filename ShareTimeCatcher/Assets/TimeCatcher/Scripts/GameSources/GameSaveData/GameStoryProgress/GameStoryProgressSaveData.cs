﻿using System.Collections;
using System.Collections.Generic;
using LitJson;
using UnityEngine;
using System.Text;
using System.Linq;

public struct StoryProgressSaveInfo
{
    public StoryProgressSaveInfo(int index, bool cleared)
    {
        storyIndex = index;
        isCleared = cleared;
    }

    public int storyIndex;
    public bool isCleared;
}

public class GameStoryProgressSaveData : CAGameSaveData
{
    #region Variables

    Dictionary<int /* story Index */, StoryProgressSaveInfo> _storyProgressInfos = new Dictionary<int, StoryProgressSaveInfo>();

    #endregion

    #region Properties

    public override string SaveFileName
    {
        get { return "StoryProgressSaveData.json"; }
    }

    public Dictionary<int /* story Index */, StoryProgressSaveInfo> StoryProgressInfos
    {
        get { return _storyProgressInfos; }
    }

    #endregion

    #region Methods

    public override void LoadSaveData()
    {
        //string readFile = CAFileCtl.Instance.ReadStringFromFile(SaveFileName);
        string readFile = LoadString();

        if (string.IsNullOrEmpty(readFile))
        {
            return;
        }

        JsonReader reader = new JsonReader(readFile);
        int objectCount = 0;
        int arrayCount = 0;
        while (reader.Read())
        {
            switch (reader.Token)
            {
                case JsonToken.ObjectStart:
                    objectCount++;
                    break;
                case JsonToken.ObjectEnd:
                    objectCount--;
                    break;
                case JsonToken.ArrayStart:
                    arrayCount++;
                    break;
                case JsonToken.ArrayEnd:
                    arrayCount--;
                    break;
            }

            if (reader.Token == JsonToken.PropertyName && (string)reader.Value == "progressinfos")
            {
                StoryProgressSaveInfo storyProgress = new StoryProgressSaveInfo(0, false);
                while (reader.Read())
                {
                    switch (reader.Token)
                    {
                        case JsonToken.ObjectStart:
                            objectCount++;
                            break;
                        case JsonToken.ObjectEnd:
                            objectCount--;
                            break;
                        case JsonToken.ArrayStart:
                            arrayCount++;
                            break;
                        case JsonToken.ArrayEnd:
                            arrayCount--;
                            break;
                    }

                    if (reader.Token == JsonToken.ArrayEnd && arrayCount == 0)
                    {
                        break;
                    }
                    else if (reader.Token == JsonToken.ObjectStart && objectCount == 2)
                    {
                        storyProgress = new StoryProgressSaveInfo(0, false);
                    }
                    else if (reader.Token == JsonToken.ObjectEnd && objectCount == 1)
                    {
                        //_stageCurrentInfos.Add(stageCurrent.stageNum, stageCurrent);
                        _storyProgressInfos.Add(storyProgress.storyIndex, storyProgress);
                    }
                    else if (reader.Token == JsonToken.PropertyName && (string)reader.Value == "storyindex")
                    {
                        reader.Read();
                        storyProgress.storyIndex = (int)reader.Value;
                    }
                    else if (reader.Token == JsonToken.PropertyName && (string)reader.Value == "cleared")
                    {
                        reader.Read();
                        storyProgress.isCleared = (bool)reader.Value;
                    }
                } // while (reader.Read ()) {
            } // if (reader.Token == JsonToken.PropertyName && (string)reader.Value == "progressinfos") {
            else if (reader.Token == JsonToken.ObjectEnd && objectCount == 0)
            {
                break;
            }
        }
    }

    public override void SaveData()
    {
        if (_storyProgressInfos.Count == 0)
            return;

        StringBuilder sb = new StringBuilder();
        JsonWriter writer = new JsonWriter(sb);

        writer.WriteObjectStart();
        {
            writer.WritePropertyName("progressinfos");
            writer.WriteArrayStart();
            {
                List<int> storyKeyValues = _storyProgressInfos.Keys.ToList();
                for (int i = 0; i < storyKeyValues.Count; i++)
                {
                    writer.WriteObjectStart();
                    {
                        writer.WritePropertyName("storyindex");
                        writer.Write(_storyProgressInfos[storyKeyValues[i]].storyIndex);

                        writer.WritePropertyName("cleared");
                        writer.Write(_storyProgressInfos[storyKeyValues[i]].isCleared);
                    }
                    writer.WriteObjectEnd();
                }
            }
            writer.WriteArrayEnd();
        }
        writer.WriteObjectEnd();

        //CAFileCtl.Instance.WriteStringToFile(sb.ToString(), SaveFileName);
        WriteSaveData(sb.ToString());
    }

    public void SetStoryProgressState(int stageIndex, bool isClear)
    {
        bool isSave = false;
        if(_storyProgressInfos.ContainsKey(stageIndex)){
            if(_storyProgressInfos[stageIndex].isCleared != isClear){
                _storyProgressInfos[stageIndex] = new StoryProgressSaveInfo(stageIndex, isClear);
                isSave = true;
            }
        } else {
            _storyProgressInfos.Add(stageIndex, new StoryProgressSaveInfo(stageIndex, isClear));
            isSave = true;
        }

        if(isSave)
            SaveData();
    }

    public bool GetStoryProgressState(int stageIndex)
    {
        if (_storyProgressInfos.ContainsKey(stageIndex))
            return _storyProgressInfos[stageIndex].isCleared;

        return false;
    }

    public override void RemoveSaveData()
    {
        CAFileCtl.Instance.DeleteFile(SaveFileName);
        _storyProgressInfos.Clear();
    }

    #endregion
}
