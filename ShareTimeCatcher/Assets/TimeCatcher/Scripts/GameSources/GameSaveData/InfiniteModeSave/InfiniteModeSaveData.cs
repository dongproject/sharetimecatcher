﻿using System.Collections;
using System.Collections.Generic;
using System.Text;
using LitJson;
using UnityEngine;

public class InfiniteModeSaveData : CAGameSaveData
{
    #region Variables

    float _bestRecordSecTime;
    float _cumulativeSecTime = 0f;
    int _selectButtonIndex = -1;
    int _viewTutorialState = 0; // 0 : Not, 1 : View
    List<int> _buyButtons = new List<int>();

    #endregion

    #region Properties

    public override string SaveFileName
    {
        get { return "InfiniteModeSaveData.json"; }
    }

    public float BestRecordSecTime
    {
        get { return _bestRecordSecTime; }
        set { _bestRecordSecTime = value; }
    }

    public float CumulativeSecTime
    {
        get { return _cumulativeSecTime; }
    }

    public int SelectButtonIndex
    {
        get { return _selectButtonIndex; }
    }

    public int ViewTutorialState
    {
        get { return _viewTutorialState; }
    }

    #endregion

    #region Methods

    public override void LoadSaveData()
    {
        base.LoadSaveData();

        _bestRecordSecTime = 0f;
        _cumulativeSecTime = 0f;
        _selectButtonIndex = -1;
        //string readFile = CAFileCtl.Instance.ReadStringFromFile(SaveFileName);
        string readFile = LoadString();

        _buyButtons.Clear();

        if (string.IsNullOrEmpty(readFile))
        {
            return;
        }

        JsonReader reader = new JsonReader(readFile);
        int objectCount = 0;
        int arrayCount = 0;
        while (reader.Read())
        {
            switch (reader.Token)
            {
                case JsonToken.ObjectStart:
                    objectCount++;
                    break;
                case JsonToken.ObjectEnd:
                    objectCount--;
                    break;
                case JsonToken.ArrayStart:
                    arrayCount++;
                    break;
                case JsonToken.ArrayEnd:
                    arrayCount--;
                    break;
            }

            if (reader.Token == JsonToken.PropertyName && (string)reader.Value == "BestRecordSecTime")
            {
                reader.Read();
                if(reader.Token == JsonToken.Int)
                {
                    _bestRecordSecTime = (float)(int)reader.Value;
                }
                else if(reader.Token == JsonToken.Double)
                {
                    _bestRecordSecTime = (float)(double)reader.Value;
                }

            } // if (reader.Token == JsonToken.PropertyName && (string)reader.Value == "BestRecordSecTime") {
            else if (reader.Token == JsonToken.PropertyName && (string)reader.Value == "CumulativeSecTime")
            {
                reader.Read();
                if (reader.Token == JsonToken.Int)
                {
                    _cumulativeSecTime = (float)(int)reader.Value;
                }
                else if (reader.Token == JsonToken.Double)
                {
                    _cumulativeSecTime = (float)(double)reader.Value;
                }

            } // if (reader.Token == JsonToken.PropertyName && (string)reader.Value == "CumulativeSecTime") {
            else if (reader.Token == JsonToken.PropertyName && (string)reader.Value == "SelectButtonIndex")
            {
                reader.Read();
                _selectButtonIndex = (int)reader.Value;
            } // if (reader.Token == JsonToken.PropertyName && (string)reader.Value == "SelectButtonIndex") {
            else if (reader.Token == JsonToken.PropertyName && (string)reader.Value == "ViewTutorialState")
            {
                reader.Read();
                _viewTutorialState = (int)reader.Value;
            } // else if (reader.Token == JsonToken.PropertyName && (string)reader.Value == "ViewTutorialState")
            else if (reader.Token == JsonToken.PropertyName && (string)reader.Value == "BuyButtons")
            {
                while (reader.Read())
                {
                    switch (reader.Token)
                    {
                        case JsonToken.ObjectStart:
                            objectCount++;
                            break;
                        case JsonToken.ObjectEnd:
                            objectCount--;
                            break;
                        case JsonToken.ArrayStart:
                            arrayCount++;
                            break;
                        case JsonToken.ArrayEnd:
                            arrayCount--;
                            break;
                    }

                    if(reader.Token == JsonToken.Int && arrayCount == 1)
                    {
                        _buyButtons.Add((int)reader.Value);
                    } 
                    else if(reader.Token == JsonToken.ArrayEnd && arrayCount == 0)
                    {
                        break;
                    }
                }
            } // if (reader.Token == JsonToken.PropertyName && (string)reader.Value == "BuyButtons") {
            else if (reader.Token == JsonToken.ObjectEnd && objectCount == 0)
            {
                break;
            }
        }
    }

    public override void SaveData()
    {
        base.SaveData();

        StringBuilder sb = new StringBuilder();
        JsonWriter writer = new JsonWriter(sb);

        writer.WriteObjectStart();
        {
            writer.WritePropertyName("BestRecordSecTime");
            writer.Write(System.Math.Round((double)_bestRecordSecTime, 5));

            writer.WritePropertyName("CumulativeSecTime");
            writer.Write(System.Math.Round((double)_cumulativeSecTime, 5));

            writer.WritePropertyName("SelectButtonIndex");
            writer.Write(_selectButtonIndex);

            writer.WritePropertyName("ViewTutorialState");
            writer.Write(_viewTutorialState);

            if (_buyButtons.Count > 0)
            {
                writer.WritePropertyName("BuyButtons");
                writer.WriteArrayStart();
                {
                    for(int i = 0;i< _buyButtons.Count; i++)
                    {
                        writer.Write(_buyButtons[i]);
                    }
                }
                writer.WriteArrayEnd();
            }
           
        }
        writer.WriteObjectEnd();

        //CAFileCtl.Instance.WriteStringToFile(sb.ToString(), SaveFileName);
        WriteSaveData(sb.ToString());
    }

    public override void RemoveSaveData()
    {
        CAFileCtl.Instance.DeleteFile(SaveFileName);
    }

    public void AddCumulativeSecTime(float addSecTime)
    {
        _cumulativeSecTime += addSecTime;
    }

    public void SetViewInfiniteTutorial()
    {
        _viewTutorialState = 1;
        SaveData();
    }

    public bool UseCumulativeSecTime(float useTime)
    {
        if (_cumulativeSecTime - useTime < 0f)
            return false;

        _cumulativeSecTime -= useTime;

        return true;
    }

    public void SetSelectButtonIndex(int buttonIndex)
    {
        _selectButtonIndex = buttonIndex;
    }

    public void AddBuyButton(int buttonIndex)
    {
        if (_buyButtons.Contains(buttonIndex))
            return;

        _buyButtons.Add(buttonIndex);
    }

    public bool IsExistButton(int buttonIndex)
    {
        if (_buyButtons.Contains(buttonIndex))
            return true;

        return false;
    }

    #endregion
}
