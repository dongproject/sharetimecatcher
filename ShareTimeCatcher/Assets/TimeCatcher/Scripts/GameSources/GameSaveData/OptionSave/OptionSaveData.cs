﻿using LitJson;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

public class OptionSaveData : CAGameSaveData
{
    #region Variables

    int _soundFXState; // 0 : off, 1: on
    int _musicState; // 0 : off, 1: on
    int _language;

    #endregion

    #region Properties

    public override string SaveFileName
    {
        get { return "OptionSaveData.json"; }
    }

    public int SoundFXState
    {
        get { return _soundFXState; }
        set { _soundFXState = value; }
    }

    public int MusicState
    {
        get { return _musicState; }
        set { _musicState = value; }
    }

    public int Language
    {
        get { return _language; }
        set { _language = value; }
    }

    #endregion

    #region Methods

    void InitOptionData()
    {
        _soundFXState = 1;
        _musicState = 1;
        _language = (int)GameDefinitions.Language.Kor;
    }

    public override void LoadSaveData()
    {
        base.LoadSaveData();

        string readFile = LoadString();
        if (string.IsNullOrEmpty(readFile)) {
            InitOptionData();
            return;
        }

        JsonReader reader = new JsonReader(readFile);
        int objectCount = 0;
        int arrayCount = 0;
        while (reader.Read()) {
            switch (reader.Token) {
                case JsonToken.ObjectStart:
                    objectCount++;
                    break;
                case JsonToken.ObjectEnd:
                    objectCount--;
                    break;
                case JsonToken.ArrayStart:
                    arrayCount++;
                    break;
                case JsonToken.ArrayEnd:
                    arrayCount--;
                    break;
            }

            if (reader.Token == JsonToken.PropertyName && (string)reader.Value == "SoundFXState") {
                reader.Read();
                _soundFXState = (int)reader.Value;
            } // if (reader.Token == JsonToken.PropertyName && (string)reader.Value == "SoundFXState") {
            else if (reader.Token == JsonToken.PropertyName && (string)reader.Value == "MusicState") {
                reader.Read();
                _musicState = (int)reader.Value;
            } // else if (reader.Token == JsonToken.PropertyName && (string)reader.Value == "MusicState") {
            else if (reader.Token == JsonToken.PropertyName && (string)reader.Value == "Language") {
                reader.Read();
                _language = (int)reader.Value;
            } // else if (reader.Token == JsonToken.PropertyName && (string)reader.Value == "Language") {
            else if (reader.Token == JsonToken.ObjectEnd && objectCount == 0) {
                break;
            }
        }
    }

    public override void SaveData()
    {
        base.SaveData();

        StringBuilder sb = new StringBuilder();
        JsonWriter writer = new JsonWriter(sb);

        writer.WriteObjectStart();
        {
            writer.WritePropertyName("SoundFXState");
            writer.Write(_soundFXState);

            writer.WritePropertyName("MusicState");
            writer.Write(_musicState);

            writer.WritePropertyName("Language");
            writer.Write(_language);
        }
        writer.WriteObjectEnd();

        WriteSaveData(sb.ToString());
    }

    public override void RemoveSaveData()
    {
        CAFileCtl.Instance.DeleteFile(SaveFileName);
    }

    #endregion
}
