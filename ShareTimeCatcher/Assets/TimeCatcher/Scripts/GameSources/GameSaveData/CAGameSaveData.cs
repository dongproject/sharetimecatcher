﻿using UnityEngine;
using System.Collections;

public class CAGameSaveData
{
    #region Variables

    #endregion

    #region Properties

    public virtual string SaveFileName
    {
        get { return ""; }
    }

    #endregion

    #region Methods

    public virtual void LoadSaveData()
    {

    }

    public virtual void SaveData()
    {

    }

    public virtual void RemoveSaveData()
    {

    }

    public virtual void WriteSaveData(string str)
    {
        byte[] encbuf = System.Text.Encoding.UTF8.GetBytes(str);
        string base64Str = System.Convert.ToBase64String(encbuf);

        CAFileCtl.Instance.WriteStringToFile(base64Str, SaveFileName);
    }

    public virtual string LoadString()
    {
        string readFile = CAFileCtl.Instance.ReadStringFromFile(SaveFileName);
        if (string.IsNullOrEmpty(readFile))
            return "";

        byte[] decbuf = System.Convert.FromBase64String(readFile);
        string debufStr = System.Text.Encoding.UTF8.GetString(decbuf);

        return debufStr;
    }

    #endregion
}
