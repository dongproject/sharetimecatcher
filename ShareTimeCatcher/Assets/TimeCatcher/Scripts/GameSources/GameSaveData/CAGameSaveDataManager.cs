﻿using UnityEngine;
using System.Collections;

public class CAGameSaveDataManager
{
	private static CAGameSaveDataManager _instance = null;
	public static CAGameSaveDataManager Instance
	{
		get
		{
			if(_instance == null)
			{
				_instance = new CAGameSaveDataManager();
			}
			return _instance;
		}
	}

	#region Variables

	StageProgressSaveData _stageProgressSave = new StageProgressSaveData();
    GameStoryProgressSaveData _storyProgressSave = new GameStoryProgressSaveData();
    InfiniteModeSaveData _infiniteModeSave = new InfiniteModeSaveData();
    OptionSaveData _optionSaveData = new OptionSaveData();

    #endregion

    #region Properties

    public StageProgressSaveData StageProgressSave
	{
		get{ return _stageProgressSave; }
	}

    public GameStoryProgressSaveData StoryProgressSave
    {
        get { return _storyProgressSave; }
    }

    public InfiniteModeSaveData InfiniteModeSave
    {
        get { return _infiniteModeSave; }
    }

    public OptionSaveData OptionSave
    {
        get { return _optionSaveData; }
    }

    #endregion

    #region Methods

    public void LoadCommonSaveData()
	{
		_stageProgressSave.LoadStageProgressData ();
        _storyProgressSave.LoadSaveData();
        _infiniteModeSave.LoadSaveData();
        _optionSaveData.LoadSaveData();
    }

    public void ResetAllSaveData()
    {
        _stageProgressSave.RemoveSaveData();
        _storyProgressSave.RemoveSaveData();
        _infiniteModeSave.RemoveSaveData();
        _optionSaveData.RemoveSaveData();
    }

    #endregion
}
