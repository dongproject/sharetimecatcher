﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ITouchMoveable
{
	void OnMoveValue(float moveX, float moveY);
}
