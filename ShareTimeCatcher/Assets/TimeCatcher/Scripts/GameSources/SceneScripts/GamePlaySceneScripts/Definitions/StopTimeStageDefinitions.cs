﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum StopTimeResultStep
{
    None = 0,
    ResultDirecting,
    Result,
}

public enum StopTimeJudgeType
{
    Perfect = 0,
    Awesome,
    Good,
    Fail,
}

public class StopTimeStageDefinitions
{
	public enum StopTimeStageType
	{
		Normal 					= 1,
		HitObject 				= 2,
		MatchObject 			= 3,
		FakeMultipleTime		= 4,
		ReflectTime				= 5,
		SushiObject				= 6,
		MosaicStage				= 7,
        ReverseTime             = 8,
        ButtonHPGauge           = 9,
	}
}
