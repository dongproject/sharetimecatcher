﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public enum CommonPopupType
{
    Exit,
    InfiniteMode_1,
    NeedStar,
    Appraise,
    NewRecord,
    LevelUp,
    ContinueGame,
    Tobecontinued,
    ApplicationQuit,
}

public class StageCommonPopupController : MonoBehaviour 
{
    static StageCommonPopupController _instance = null;
    public static StageCommonPopupController Instance
    {
        get{ return _instance; }
    }

    #region Serialize Variables

#pragma warning disable 649

    [Header("Exit Popup Objs")]
    [SerializeField] GameObject _exitRootObj;
	[SerializeField] Text _descText;
	[SerializeField] Text _yesText;
	[SerializeField] Text _noText;

    [Header("InfiniteMode_1 Objs")]
    [SerializeField] GameObject _infiniteMode_1_RootObj;

    [Header("NeedStar Objs")]
    [SerializeField] GameObject _needStarRoot;

    [Header("Appraise Objs")]
    [SerializeField] GameObject _appraiseRoot;

    [Header("Newrecord Objs")]
    [SerializeField] GameObject _newRecordRoot;

    [Header("levelup Objs")]
    [SerializeField] GameObject _levelUpRoot;

    [Header("continueGame Objs")]
    [SerializeField] GameObject _continueRootObj;

    [Header("Tobecontinued Objs")]
    [SerializeField] GameObject _TobecontinuedRoot;

    [Header("Application Quit Objs")]
    [SerializeField] GameObject _applicationQuitRoot;

#pragma warning restore 649

    #endregion

    #region Variables

    CommonPopupType _curPopupType;
    Action _onYesAction = null;
	Action _onNoAction = null;
    Action _onConfirmAction = null;
    Action _onEtcAction = null;
    bool _isEnablePopup = false;

    #endregion

    #region Properties

    public CommonPopupType CurPopupType
    {
        get { return _curPopupType; }
        set { _curPopupType = value; }
    }

    public Action OnYesAction
	{
		get{ return _onYesAction; }
		set{ _onYesAction = value; }
	}

	public Action OnNoAction
	{
		get{ return _onNoAction; }
		set{ _onNoAction = value; }
	}

    public bool IsEnablePopup
    {
        get{ return _isEnablePopup; }
        set { _isEnablePopup = value; }
    }

    #endregion

    #region MonoBehaviour Methods

    private void Awake()
    {
        _exitRootObj.SetActive(false);
        if(_instance != null)
            _instance = this;
    }

    private void OnDisable()
    {
        if (BackKeyManager.Instance != null)
            BackKeyManager.Instance.IsEnableBackKey = true;
    }

    private void OnDestroy()
    {
        _instance = null;
    }

    #endregion

    #region Methods

    public void SetInstance()
    {
        if (_instance != null)
            return;

        _instance = this;
    }

    public void ShowPopup(CommonPopupType popupType, Action onYes, Action onNo, Action onConfirm = null, Action onEtc = null)
    {
        _curPopupType = popupType;
        this.gameObject.SetActive(true);
        _isEnablePopup = true;

        _onYesAction = onYes;
        _onNoAction = onNo;
        _onConfirmAction = onConfirm;
        _onEtcAction = onEtc;

        switch (_curPopupType)
        {
            case CommonPopupType.Exit:
                _exitRootObj.SetActive(true);
                break;
            case CommonPopupType.InfiniteMode_1:
                _infiniteMode_1_RootObj.SetActive(true);
                break;
            case CommonPopupType.NeedStar:
                _needStarRoot.SetActive(true);
                break;
            case CommonPopupType.Appraise:
                _appraiseRoot.SetActive(true);
                break;
            case CommonPopupType.NewRecord:
                _newRecordRoot.SetActive(true);
                break;
            case CommonPopupType.LevelUp:
                _levelUpRoot.SetActive(true);
                break;
            case CommonPopupType.ContinueGame:
                _continueRootObj.SetActive(true);
                break;
            case CommonPopupType.Tobecontinued:
                _TobecontinuedRoot.SetActive(true);
                break;
            case CommonPopupType.ApplicationQuit:
                _applicationQuitRoot.SetActive(true);
                break;
        }

        if (BackKeyManager.Instance != null)             BackKeyManager.Instance.IsEnableBackKey = false;
    }

    public void ClosePopup()
    {
        switch (_curPopupType)
        {
            case CommonPopupType.Exit:
                _exitRootObj.SetActive(false);
                break;
            case CommonPopupType.InfiniteMode_1:
                _infiniteMode_1_RootObj.SetActive(false);
                break;
            case CommonPopupType.NeedStar:
                _needStarRoot.SetActive(false);
                break;
            case CommonPopupType.Appraise:
                _appraiseRoot.SetActive(false);
                break;
            case CommonPopupType.NewRecord:
                _newRecordRoot.SetActive(false);
                break;
            case CommonPopupType.LevelUp:
                _levelUpRoot.SetActive(false);
                break;
            case CommonPopupType.ContinueGame:
                _continueRootObj.SetActive(false);
                break;
            case CommonPopupType.Tobecontinued:
                _TobecontinuedRoot.SetActive(false);
                break;
            case CommonPopupType.ApplicationQuit:
                _applicationQuitRoot.SetActive(false);
                break;
        }

        this.gameObject.SetActive(false);
        _isEnablePopup = false;
    }

    #endregion

    #region CallBack Methods

    public void OnYesFunc()
	{
        ClosePopup();

        Debug.Log ("OnYesFunc");
		if (_onYesAction != null)
			_onYesAction ();
    }

	public void OnNoFunc()
	{
        ClosePopup();

        Debug.Log ("OnNoFunc");
		if (_onNoAction != null)
			_onNoAction ();
    }

    public void OnConfirm()
    {
        ClosePopup();

        if (_onConfirmAction != null)
            _onConfirmAction();
    }

    public void OnETCButton()
    {
        ClosePopup();

        if (_onEtcAction != null)
            _onEtcAction();
    }

    #endregion
}
