﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StopTimeObjectInfo : StageBaseObjectInfo
{
	#region Variables

	Vector3 _objStartPos;
	Vector3 _matchingPos;

	#endregion

	#region Properties

	public Vector3 ObjStartPos
	{
		get{ return _objStartPos; }
		set{ _objStartPos = value; }
	}

	public Vector3 MatchingPos
	{
		get{ return _matchingPos; }
		set{ _matchingPos = value; }
	}

	#endregion
}
