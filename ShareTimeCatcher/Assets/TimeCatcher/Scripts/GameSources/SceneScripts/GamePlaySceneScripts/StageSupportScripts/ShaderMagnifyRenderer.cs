﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public interface IMagnifyRenderInfo
{
	void OnMagnifyPos (float posX, float posY);
	void OnCheckPos (float posX, float posY);
}

public class ShaderMagnifyRenderer : MonoBehaviour 
{
	#region Serialize Variables

	[SerializeField] Shader _shader;
	[SerializeField] Texture _txtScope;

	[SerializeField] MeshRenderer _tempMeshRender;
	[SerializeField] tk2dSpriteFromTexture _tempTexture;

	#endregion

	#region Variables

	IMagnifyRenderInfo _magnifyRenderInfo;

	RenderTexture _renderTexture;
	Camera _renderCam;

	Material _material 	= null;

	float _scopePosX;
	float _scopePosY;

	float _bgImgWidth;
	float _bgImgHeight;
	float _bgImgPosX = 0f;
	float _bgImgPosY = 0f;

	float _scopeRatio = 1f;

	Vector3 _vPosition = Vector3.zero;

	Rect _moveValidRect;

	Transform _uiMagnifyTrans;

	#endregion

	#region Properties

	public IMagnifyRenderInfo MagnifyRenderInfo
	{
		get{ return _magnifyRenderInfo; }
		set{ _magnifyRenderInfo = value; }
	}

	public float ScopePosX
	{
		get{ return _scopePosX; }
		set{ _scopePosX = value; }
	}

	public float ScopePosY
	{
		get{ return _scopePosY; }
		set{ _scopePosY = value; }
	}

	protected RenderTexture RenderTexture {
		get {
			if (_renderTexture == null) {
				_renderTexture = new RenderTexture((int)(Screen.width), (int)(Screen.height), 24, RenderTextureFormat.ARGB32);
				_renderTexture.Create ();
				_renderTexture.name = "RenderTexture";
			}
			return _renderTexture;
		}
	}

	public float ScopeRatio
	{
		get{  return _scopeRatio; }
		set{ _scopeRatio = value; }
	}

	public Shader Shader {
		get { return _shader; }
		set { _shader = value; }
	}

	public Texture TxtScope {
		get { return _txtScope; }
		set { _txtScope = value; }
	}

	public Transform UIMagnifyTrans {
		get { return _uiMagnifyTrans; }
		set { _uiMagnifyTrans = value; }
	}

	#endregion

	#region MonoBehaviour Methods

	void Awake () {
		_renderCam	= this.GetComponent <Camera> ();

		if (CAResolutionCtl.Instance == null) {
			CAResolutionCtl.InitResolutionCtl ();
			CAResolutionCtl.Instance.InitResolution(Screen.width, Screen.height, false);
			Screen.SetResolution((int)CAResolutionCtl.Instance.GetResolutionWidth(), (int)CAResolutionCtl.Instance.GetResolutionHeight(), true);
		}

		_moveValidRect = new Rect (0f, 0f
			, CAResolutionCtl.Instance.GetResolutionWidth(), CAResolutionCtl.Instance.GetResolutionHeight());
	}

	void OnPreRender () {
		if (RenderTexture.width != (int)(Screen.width) || RenderTexture.height != (int)(Screen.height )) 
			_renderTexture = null;

		_renderCam.targetTexture = RenderTexture;
		_tempTexture.texture = RenderTexture;
	}

	#endregion

	#region Methods

	public void SetupScoping () {
		_bgImgWidth = CAResolutionCtl.Instance.GetResolutionWidth();
		_bgImgHeight = CAResolutionCtl.Instance.GetResolutionHeight();
		float fHalfWidth = _bgImgWidth * 0.5f;
		float fHalfHeight = _bgImgHeight * 0.5f;

		float fMaskWidth = _bgImgWidth / (float)_txtScope.width * _scopeRatio;
		float fMaskHeight = _bgImgHeight / (float)_txtScope.height * _scopeRatio;
		float fMaskX = - fMaskWidth * 0.5f + 0.5f;
		float fMaskY = - fMaskHeight * 0.5f + 0.5f;
		_scopePosX = fHalfWidth;
		_scopePosY = fHalfHeight;

		_material = new Material(_shader);

		_tempTexture.Clear ();
		_tempTexture.spriteCollectionSize.pixelsPerMeter = 1f;
		_tempTexture.Create (_tempTexture.spriteCollectionSize, RenderTexture, _tempTexture.anchor);

		_tempTexture.texture = RenderTexture;
		_material.mainTexture = RenderTexture;
		_tempMeshRender.material = _material;

		_material.color	= Color.white;
		_material.SetTextureOffset ("_MainTex", new Vector2(0f, 0f));
		_material.SetTextureScale ("_MainTex", new Vector2 (1f, 1f));	
		_material.SetTexture ("_MaskTex", _txtScope);
		_material.SetTextureOffset ("_MaskTex", new Vector2(fMaskX , fMaskY));
		_material.SetTextureScale ("_MaskTex", new Vector2 (fMaskWidth, fMaskHeight));
	}

	public void SetupNoneMaskTexture()
	{
		_material.SetTexture ("_MaskTex", null);
		_tempTexture.gameObject.SetActive(false);
		_tempTexture.gameObject.SetActive (true);
	}

	public void SetupMaskTexture()
	{
		_material.SetTexture ("_MaskTex", _txtScope);
		_tempTexture.gameObject.SetActive(false);
		_tempTexture.gameObject.SetActive (true);
	}

	void RenewalScoping (float posX, float posY) 
	{
		float calcX = (posX - _bgImgPosX + (_bgImgWidth - Screen.width) * 0.5f) / _bgImgWidth;
		float calcY = (posY - _bgImgPosY  + (_bgImgHeight - Screen.height) * 0.5f) / _bgImgHeight;
		float fMaskX 		= - (float)_bgImgWidth / (float)_txtScope.width * calcX * _scopeRatio + 0.5f;
		float fMaskY 		= - (float)_bgImgHeight / (float)_txtScope.height * calcY * _scopeRatio + 0.5f;

		_material.SetTextureOffset ("_MaskTex", new Vector2(fMaskX , fMaskY));

		_tempTexture.gameObject.SetActive(false);
		_tempTexture.gameObject.SetActive (true);

		float uiPosX = _scopePosX - CAResolutionCtl.Instance.GetResolutionHalfWidth ();
		float uiPosY = _scopePosY - CAResolutionCtl.Instance.GetResolutionHalfHeight ();
		_uiMagnifyTrans.transform.localPosition = new Vector3 (uiPosX, uiPosY, _uiMagnifyTrans.transform.localPosition.z);

		if (_magnifyRenderInfo != null) {
			_magnifyRenderInfo.OnMagnifyPos (uiPosX, uiPosY);
		}
	}

    public void SetScopePos(float posX, float posY)
    {
        posX += _scopePosX;
        posY += _scopePosY;
        float calcX = (posX + (_bgImgWidth - Screen.width) * 0.5f) / _bgImgWidth;
        float calcY = (posY + (_bgImgHeight - Screen.height) * 0.5f) / _bgImgHeight;
        float fMaskX = -(float)_bgImgWidth / (float)_txtScope.width * calcX * _scopeRatio + 0.5f;
        float fMaskY = -(float)_bgImgHeight / (float)_txtScope.height * calcY * _scopeRatio + 0.5f;

        _material.SetTextureOffset("_MaskTex", new Vector2(fMaskX, fMaskY));

        _tempTexture.gameObject.SetActive(false);
        _tempTexture.gameObject.SetActive(true);
    }

	public void ScopeMove(float moveX, float moveY)
	{
		bool bgMoveState = false;

		_scopePosX = Mathf.Clamp(_scopePosX + moveX, _moveValidRect.xMin, _moveValidRect.xMax);
		_scopePosY = Mathf.Clamp(_scopePosY + moveY, _moveValidRect.yMin, _moveValidRect.yMax);

		RenewalScoping(_scopePosX, _scopePosY);

	}

	public void CheckValidButton()
	{
		if (_magnifyRenderInfo != null) {
			float uiPosX = _scopePosX - CAResolutionCtl.Instance.GetResolutionHalfWidth ();
			float uiPosY = _scopePosY - CAResolutionCtl.Instance.GetResolutionHalfHeight ();

			_magnifyRenderInfo.OnCheckPos (uiPosX, uiPosY);
		}
	}

    public Vector2 GetScopeUIPos()
    {
        Vector2 retValue = Vector2.zero;

        if (_magnifyRenderInfo != null)
        {
            float uiPosX = _scopePosX - CAResolutionCtl.Instance.GetResolutionHalfWidth();
            float uiPosY = _scopePosY - CAResolutionCtl.Instance.GetResolutionHalfHeight();

            retValue = new Vector2(uiPosX, uiPosY);
        }

        return retValue;
    }

	public void ScopePosSet(float posX, float posY)
	{
		_scopePosX = posX;
		_scopePosY = posY;
		RenewalScoping(_scopePosX, _scopePosY);
	}

	#endregion
}
