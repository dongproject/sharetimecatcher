﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MakePathDotInfo
{
    #region Variables

    GameObject _dotObject;

    #endregion

    #region Properties

    public GameObject DotObject
    {
        get { return _dotObject; }
        set { _dotObject = value; }
    }

    #endregion
}

public class MakePathDotManager
{
    #region Variables

    //Transform _dotPathParent;
    //GameObject _dotObject;
    float _intervalTime = 0.01f;
    float _curTime = 0f;
    int _prePathDotIndex = 0;
    int _curPathDotIndex = 0;
    int _intervalDotCount = 10;
    List<MakePathDotInfo> _pathDotInfos = new List<MakePathDotInfo>();

    #endregion

    #region Properties

    //public Transform DotPathParent
    //{
    //    get { return _dotPathParent; }
    //    set { _dotPathParent = value; }
    //}



    public List<MakePathDotInfo> PathDotInfos
    {
        get { return _pathDotInfos; }
    }

    #endregion

    #region Methods

    public void InitMakePathDot()
    {
        _curTime = _intervalTime;
        _prePathDotIndex = 0;
        if(_pathDotInfos.Count < _intervalDotCount)
        {
            _curPathDotIndex = _pathDotInfos.Count;
        }
        else
        {
            _curPathDotIndex = _intervalDotCount;
        }

    }

    public int UpdateMakePath()
    {
        int retValue = -1;

        if(_pathDotInfos.Count == 0 || _curPathDotIndex > _pathDotInfos.Count)
        {
            return -1;
        }

        //_curTime += Time.deltaTime;
        //if(_curTime >= _intervalTime)
        //{
        //    _pathDotInfos[_curPathDotIndex].DotObject.SetActive(true);
        //    _curPathDotIndex++;
        //    if (_pathDotInfos.Count <= _curPathDotIndex)
        //    {
        //        retValue = 1;
        //    }
        //    _curTime = 0f;
        //}

        //_pathDotInfos[_curPathDotIndex].DotObject.SetActive(true);
        //_curPathDotIndex++;
        //if (_pathDotInfos.Count <= _curPathDotIndex)
        //{
        //    retValue = 1;
        //}
        _curTime += Time.deltaTime;
        if (_curTime >= _intervalTime)
        {
            for (int i = _prePathDotIndex; i < _curPathDotIndex; i++)
            {
                _pathDotInfos[i].DotObject.SetActive(true);


            }

            if (_pathDotInfos.Count == _curPathDotIndex)
            {
                retValue = 1;
                _curPathDotIndex++;
            }
            else
            {
                _prePathDotIndex = _curPathDotIndex;
                if (_curPathDotIndex + _intervalDotCount > _pathDotInfos.Count)
                {
                    _curPathDotIndex = _pathDotInfos.Count;
                }
                else
                {
                    _curPathDotIndex += _intervalDotCount;
                }
            }

            _curTime = 0f;
        }

        return retValue;
    }

    public void ReleasePathDot()
    {
        for(int i = 0;i< _pathDotInfos.Count; i++)
        {
            GameObject.Destroy(_pathDotInfos[i].DotObject);
        }

        _pathDotInfos.Clear();
    }

    #endregion
}
