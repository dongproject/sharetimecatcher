﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum StageObjectType
{
	CatchMove,
	MotionAni,
	CatchMoveMotionAni,
}

public class StageBaseObjectInfo 
{
	#region Variables

	protected int _objectIndex;
	protected Transform _objTransform;
	protected float _objWidth;
	protected float _objHeight;
	protected float _objLocalPosX = 0f;
	protected float _objLocalPosY = 0f;
    protected bool _isEnable = true;

	#endregion

	#region Properties

	public int ObjectIndex
	{
		get{ return _objectIndex; }
		set{ _objectIndex = value; }
	}

	public Transform ObjTransform
	{
		get{ return _objTransform; }
		set{ _objTransform = value; }
	}

	public float ObjWidth
	{
		get{ return _objWidth; }
		set{ _objWidth = value; }
	}

	public float ObjHeight
	{
		get{ return _objHeight; }
		set{ _objHeight = value; }
	}

	public float XMin
	{
		get{ return (_objTransform.localPosition.x + _objLocalPosX) - (_objWidth * 0.5f); }
	}

	public float XMax
	{
		get{ return (_objTransform.localPosition.x + _objLocalPosX) + _objWidth * 0.5f; }
	}

	public float YMin
	{
		get{ return (_objTransform.localPosition.y + _objLocalPosY) - _objHeight * 0.5f; }
	}

	public float YMax
	{
		get{ return (_objTransform.localPosition.y + _objLocalPosY) + _objHeight * 0.5f; }
	}

	public float ObjLocalPosX
	{
		get{ return _objLocalPosX; }
		set{ _objLocalPosX = value; }
	}

	public float ObjLocalPosY
	{
		get{ return _objLocalPosY; }
		set{ _objLocalPosY = value; }
	}

    public bool IsEnable
    {
        get { return _isEnable; }
        set { _isEnable = value; }
    }

    #endregion

    #region Methods

    public bool CheckValidCollideByScreenPos(float posX, float posY)
	{
		float imgPosX = CAResolutionCtl.Instance.GetScreenToImagePosX(posX);
		float imgPosY = CAResolutionCtl.Instance.GetScreenToImagePosY(posY);

		if ((XMin <= imgPosX && XMax >= imgPosX) &&
		    (YMin <= imgPosY && YMax >= imgPosY)) {
			return true;
		}

		return false;
	}

	public bool CheckValidCollideByImgPos(float imgPosX, float imgPosY)
	{
		if ((XMin <= imgPosX && XMax >= imgPosX) &&
			(YMin <= imgPosY && YMax >= imgPosY)) {
			return true;
		}

		return false;
	}

	#endregion
}
