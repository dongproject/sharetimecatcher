﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StageButtonEffect : MonoBehaviour
{
    #region Serialize Variables

    #endregion

    #region Variables

    UGUITweener[] _buttonEffectTweeners = null;

    #endregion

    #region Properties

    #endregion

    #region MonoBehaviour Methods

    private void Awake()
    {
        if (_buttonEffectTweeners == null)
            SetButtonEffects();
    }

    #endregion

    #region Methods

    void SetButtonEffects()
    {
        _buttonEffectTweeners = this.GetComponentsInChildren<UGUITweener>();
    }

    public void ResetEffectTweeners()
    {
        if (_buttonEffectTweeners == null)
            SetButtonEffects();

        for (int i = 0; i < _buttonEffectTweeners.Length;i++){
            _buttonEffectTweeners[i].ResetToBeginning();
            _buttonEffectTweeners[i].PlayForward();
            _buttonEffectTweeners[i].enabled = true;
            _buttonEffectTweeners[i].gameObject.SetActive(true);
        }
    }

    #endregion
}
