﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StopTimeAssembleButtonStage : StopTimeBaseStage 
{
    #region Definitions

    public enum AssembleButtonType
    {
        None    = -1,
        Button  = 0,
        Snail   = 1,
    }

    #endregion

    #region Serialize Variables

    [SerializeField] GameObject _assembleButtonObj;
    [SerializeField] GameObject _fakeAssembleBtnObj;
    [SerializeField] UGUITweener _snailPosTween;
    [SerializeField] Animator _snailAnimator;
    [SerializeField] GameObject _snailHeadObj;
    [SerializeField] GameObject _snailHeadOffObj;

	#endregion

	#region Variables

	bool _isAssembleButton = false;
    bool _isSnailTouched = false;
    AssembleButtonType _curAssembleBtnType = AssembleButtonType.None;

    #endregion

    #region MonoBehaviour Methods

    protected override void Awake()
	{
		base.Awake ();
        //AddStopTimeObjectInfo (StageObjectType.CatchMove, (int)AssembleButtonType.Button, _assembleButtonObj.GetComponent<tk2dSprite>(), new Vector3(-178.76f, 276.31f, -7f));
        AddStopTimeObjectInfo(StageObjectType.CatchMove, (int)AssembleButtonType.Button, _assembleButtonObj.transform, 180f, 180f, new Vector3(-190f, 223f, -7f));
        AddStopTimeObjectInfo(StageObjectType.CatchMove, (int)AssembleButtonType.Snail, _fakeAssembleBtnObj.transform, 180f, 170f, new Vector3(198f, 224f, -7f));
    }

	#endregion

	#region Methods

	public override int CheckStopTimeButton(int touchID, float posX, float posY)
	{
        //if (!_isAssembleButton)
        //return -1;
        if (_curAssembleBtnType != AssembleButtonType.Button)
            return -1;

        return base.CheckStopTimeButton(touchID, posX, posY);
	}

	public override void OnTouchPress(int touchID, float posX, float posY)
	{
        if (_curAssembleBtnType != AssembleButtonType.None)
			return;

		CheckMatchObject (touchID, posX, posY);
	}

	void CheckMatchObject(int touchID, float posX, float posY)
	{
		if (_curCatchObjectInfo.TouchID != -1) {
			if (_curCatchObjectInfo.TouchID != touchID)
				return;
		}

		float imgPosX = CAResolutionCtl.Instance.GetScreenToImagePosX(posX);
		float imgPosY = CAResolutionCtl.Instance.GetScreenToImagePosY(posY);

		for (int i = 0; i < _catchMoveObjectInfos.Count; i++) {

			if ((_catchMoveObjectInfos [i].XMin <= imgPosX && _catchMoveObjectInfos [i].XMax >= imgPosX) &&
				(_catchMoveObjectInfos [i].YMin <= imgPosY && _catchMoveObjectInfos [i].YMax >= imgPosY)) {

                if(_catchMoveObjectInfos[i].ObjectIndex == (int)AssembleButtonType.Snail){
                    if(!_isSnailTouched){
                        _snailPosTween.enabled = false;
                        _snailAnimator.enabled = false;
                        _snailHeadObj.SetActive(false);
                        _snailHeadOffObj.SetActive(true);
                        _isSnailTouched = true;
                    }
                }

				_curCatchObjectInfo.TouchID = touchID;
				_curCatchObjectInfo.ObjectInfo = _catchMoveObjectInfos [i];

				Debug.Log (string.Format ("CheckMatchObject Matched index : {0}", i));
				break;
			}
		}
	}

	public override void OnTouchRelease(int touchID, float posX, float posY)
	{
		if (_curCatchObjectInfo.TouchID == touchID) {
			CheckMatchAssembleButton (_curCatchObjectInfo.ObjectInfo);
			_curCatchObjectInfo.TouchID = -1;
			_curCatchObjectInfo.ObjectInfo = null;
		}
	}

	public override void OnTouchMoveValues(int touchID, float moveX, float moveY)
	{
        //if (_isAssembleButton)
        //return;

        if (_curAssembleBtnType != AssembleButtonType.None)
            return;

        if (_curCatchObjectInfo.TouchID == touchID) {
			Vector3 objPos = _curCatchObjectInfo.ObjectInfo.ObjTransform.localPosition;
			float objHalfWidth = _curCatchObjectInfo.ObjectInfo.ObjWidth * 0.5f;
			float objHalfHeight = _curCatchObjectInfo.ObjectInfo.ObjHeight * 0.5f;
			float objPosX = Mathf.Clamp (objPos.x + moveX, 
				-CAResolutionCtl.Instance.GetResolutionHalfWidth() + objHalfWidth, 
				CAResolutionCtl.Instance.GetResolutionHalfWidth() - objHalfWidth);

			float objPosY = Mathf.Clamp (objPos.y + moveY, 
				-CAResolutionCtl.Instance.GetResolutionHalfHeight() + objHalfHeight, 
				CAResolutionCtl.Instance.GetResolutionHalfHeight() - objHalfHeight);

			//			Debug.Log (string.Format ("OnTouchMoveValues objPosX : {0}, objPosY : {1}", objPosX, objPosY));

			_curCatchObjectInfo.ObjectInfo.ObjTransform.localPosition = new Vector3 (objPosX, objPosY,
				_curCatchObjectInfo.ObjectInfo.ObjTransform.localPosition.z);
		}
	}

	bool CheckMatchAssembleButton(StopTimeObjectInfo matchObjectInfo)
	{
		float validDis = 70f;

		Vector3 buttonScreenPos = _stageCamera.WorldToScreenPoint (StartButton.transform.position);

		float buttonImgPosX = CAResolutionCtl.Instance.GetScreenToImagePosX (buttonScreenPos.x);
		float buttonImgPosY = CAResolutionCtl.Instance.GetScreenToImagePosY (buttonScreenPos.y);

        //Debug.Log(string.Format("CheckMatchAssembleButton objPosX : {0}, objPosY : {1}, assemX : {2}, assemY : {3}",
                                //matchObjectInfo.ObjTransform.localPosition.x, matchObjectInfo.ObjTransform.localPosition.y,
                                //_assembleButtonObj.transform.localPosition.x, _assembleButtonObj.transform.localPosition.y));

        Vector3 assembleButtonScreenPos = _stageCamera.WorldToScreenPoint (matchObjectInfo.ObjTransform.position);

		float assembleImgPosX = CAResolutionCtl.Instance.GetScreenToImagePosX (assembleButtonScreenPos.x);
		float assembleImgPosY = CAResolutionCtl.Instance.GetScreenToImagePosY (assembleButtonScreenPos.y);

		float rightLineCalcDis = CAMathCtl.Instance.GetPointDistance (assembleImgPosX, 
			assembleImgPosY, buttonImgPosX, buttonImgPosY);

		if (rightLineCalcDis < validDis) {
			matchObjectInfo.ObjTransform.localPosition = new Vector3 (buttonImgPosX,
				buttonImgPosY + 8f,
				matchObjectInfo.ObjTransform.localPosition.z);

            _curAssembleBtnType = (AssembleButtonType)matchObjectInfo.ObjectIndex;
            if(_curAssembleBtnType == AssembleButtonType.Button){
                SetSolveButton();
                _assembleButtonObj.SetActive(false);
                Transform btnImg = _startButton.transform.Find("ButtonImg");
                btnImg.gameObject.SetActive(true);
                SetSolveButton();
            }
            //_isAssembleButton = true;
            return true;
		}

		return false;
	}

	#endregion
}
