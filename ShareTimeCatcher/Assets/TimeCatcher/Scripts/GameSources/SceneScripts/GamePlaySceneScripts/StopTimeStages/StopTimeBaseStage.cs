﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class StopTimeBaseStage : MonoBehaviour 
{
    #region Serialize Variables

#pragma warning disable 649

    [SerializeField] StopTimeStageDefinitions.StopTimeStageType _stageType;

	[SerializeField] GameObject _rootPlayObjects;

	[SerializeField] protected GameObject _startButton;
	[SerializeField] protected GameObject _stopButton;

	[SerializeField] StopTimeViewObject _realTimeViewObject;

	[SerializeField] protected StopTimeButtonInfo[] _stopTimeButtons;

#pragma warning restore 649

    #endregion

    #region Variables

    protected float _buttonStartX;
	protected float _buttonStartY;
	protected float _buttonEndX;
	protected float _buttonEndY;

	ITouchMoveable _touchMoveable;

	protected int _curTouchID = -1;
	protected int _curOtherButtonIndex = -1;

	TargetTimeRootInfo _targetTimeRoot;
	StopTimeViewObject _targetTimeViewObject;

	protected Action _onFailStage = null;

    protected StopTimeButtonType _stageButtonType = StopTimeButtonType.Normal;

    protected List<StopTimeObjectInfo> _catchMoveObjectInfos = new List<StopTimeObjectInfo>();
	protected List<StopTimeObjectInfo> _motionAniObjectInfos = new List<StopTimeObjectInfo>();
	protected List<StopTimeObjectInfo> _catchMotionAniObjectInfos = new List<StopTimeObjectInfo>();

	protected ObjectMotionManager _motionManager = new ObjectMotionManager();

	protected Dictionary<int /* Object Index */, Transform> _motionAniTransforms = new Dictionary<int, Transform> ();

	protected Camera _stageCamera;

	protected StopTimeCatchObjectInfo _curCatchObjectInfo = new StopTimeCatchObjectInfo();

    protected int _stopButtonIndex = 1000;

	Action<float> _onAddRealTimeValue = null;

    protected CAGameTimerEventCtl _eventTimer = new CAGameTimerEventCtl();

    protected bool _isGamePlaying = false;

    protected Action<int, int, float> _onTimePause;
    protected Action _onTimeResume;

    #endregion

    #region Properties

    public int StopButtonIndex
	{
		get{ return _stopButtonIndex; }
	}

	public StopTimeStageDefinitions.StopTimeStageType StageType
	{
		get{ return _stageType; }
	}

	public GameObject RootPlayObjects
	{
		get{ return _rootPlayObjects; }
	}

	public GameObject StartButton
	{
		get{ return _startButton; }
	}

	public GameObject StopButton
	{
		get{ return _stopButton; }
	}

	public StopTimeViewObject TargetTimeViewObject
	{
		get{ return _targetTimeViewObject; }
	}

	public StopTimeViewObject RealTimeViewObject
	{
		get{ return _realTimeViewObject; }
	}

	public ITouchMoveable TouchMoveable
	{
		get{ return _touchMoveable; }
		set{ _touchMoveable = value; }
	}

	public Action OnFailStage
	{
		get{ return _onFailStage; }
		set{ _onFailStage = value; }
	}

	public Camera StageCamera
	{
		get{ return _stageCamera; }
		set{ _stageCamera = value; }
	}

	public Action<float> OnAddRealTimeValue
	{
		get{ return _onAddRealTimeValue; }
		set{ _onAddRealTimeValue = value; }
	}

    public StopTimeButtonType StageButtonType
    {
        get { return _stageButtonType; }
        set { _stageButtonType = value; }
    }

    public Action<int, int, float> OnTimePause
    {
        get { return _onTimePause; }
        set { _onTimePause = value; }
    }

    public Action OnTimeResume
    {
        get { return _onTimeResume; }
        set { _onTimeResume = value; }
    }

    #endregion

    #region MonoBehaviour Methods

    protected virtual void Awake()
	{
		GameObject targetTimeObject = Instantiate(Resources.Load<GameObject> ("Prefabs/Stages/TargetTimeRoot")) as GameObject;
		targetTimeObject.transform.SetParent (this.transform);
		targetTimeObject.transform.localScale = Vector3.one;

		_targetTimeRoot = targetTimeObject.GetComponent<TargetTimeRootInfo> ();
		_targetTimeViewObject = _targetTimeRoot.TargetTimeNumObject;

		_targetTimeRoot.gameObject.SetActive (false);

		_motionManager.OnCompleteAllMotionAni = OnCompleteAllMotionAni;

		_curCatchObjectInfo.TouchID = -1;

        _stageButtonType = StopTimeButtonType.PushImmediate;
    }

	protected virtual void Start()
	{
		_startButton.SetActive (true);
		if(_stopButton != null)
			_stopButton.SetActive (false);
	}

	#endregion

	#region Methods

	public virtual void InitStopTimeStageData()
	{
		RefreshButtonPos ();
		InitStopTimeButtons ();
	}

	protected virtual void InitStopTimeButtons()
	{
		if (_stopTimeButtons == null || _stopTimeButtons.Length == 0)
			return;

		for (int i = 0; i < _stopTimeButtons.Length; i++) {
			_stopTimeButtons [i].RefreshButtonPos (_stageCamera);
		}
	}

	protected void RefreshButtonPos()
	{
		BoxCollider buttonCollider = _startButton.GetComponent<BoxCollider> ();
		float objScaleX = _startButton.transform.localScale.x;
		float objScaleY = _startButton.transform.localScale.y;
		float halfWidth = (buttonCollider.bounds.size.x * objScaleX) * 0.5f;
		float halfHeight = (buttonCollider.bounds.size.y * objScaleY) * 0.5f;
        //Debug.Log(string.Format("RefreshButtonPos buttonCollier Size : {0}", buttonCollider.bounds.size));

//		float screenPosX = CAResolutionCtl.Instance.GetImageToScreenPosX (_startButton.transform.localPosition.x);
//		float screenPosY = CAResolutionCtl.Instance.GetImageToScreenPosY (_startButton.transform.localPosition.y);

		Vector3 buttonScreenPos = _stageCamera.WorldToScreenPoint (_startButton.transform.position);

		_buttonStartX = buttonScreenPos.x - halfWidth;
		_buttonStartY = buttonScreenPos.y - halfHeight;
		_buttonEndX = buttonScreenPos.x + halfWidth;
		_buttonEndY = buttonScreenPos.y + halfHeight;

//		Debug.Log (string.Format ("RefreshButtonPos _buttonStartX : {0}, _buttonStartY : {1}, _buttonEndX : {2}, _buttonEndY : {3}", 
//			_buttonStartX, _buttonStartY, _buttonEndX, _buttonEndY));
	}

	protected void RefreshTouchButtonPos()
	{
		if (_startButton.activeSelf) {
			BoxCollider buttonCollider = _startButton.GetComponent<BoxCollider> ();
			float objScaleX = _startButton.transform.localScale.x;
			float objScaleY = _startButton.transform.localScale.y;
			float halfWidth = (buttonCollider.bounds.size.x * objScaleX) * 0.5f;
			float halfHeight = (buttonCollider.bounds.size.y * objScaleY) * 0.5f;

			Vector3 buttonScreenPos = _stageCamera.WorldToScreenPoint (_startButton.transform.position);

			_buttonStartX = buttonScreenPos.x - halfWidth;
			_buttonStartY = buttonScreenPos.y - halfHeight;
			_buttonEndX = buttonScreenPos.x + halfWidth;
			_buttonEndY = buttonScreenPos.y + halfHeight;

//			Debug.Log (string.Format ("RefreshTouchButtonPos startButton _buttonStartX : {0}, _buttonStartY : {1}, _buttonEndX : {2}, _buttonEndY : {3}", 
//				_buttonStartX, _buttonStartY, _buttonEndX, _buttonEndY));
		} else if (_stopButton != null && _stopButton.activeSelf) {
			BoxCollider buttonCollider = _stopButton.GetComponent<BoxCollider> ();
			float objScaleX = _stopButton.transform.localScale.x;
			float objScaleY = _stopButton.transform.localScale.y;
			float halfWidth = (buttonCollider.bounds.size.x * objScaleX) * 0.5f;
			float halfHeight = (buttonCollider.bounds.size.y * objScaleY) * 0.5f;

			Vector3 buttonScreenPos = _stageCamera.WorldToScreenPoint (_stopButton.transform.position);

			_buttonStartX = buttonScreenPos.x - halfWidth;
			_buttonStartY = buttonScreenPos.y - halfHeight;
			_buttonEndX = buttonScreenPos.x + halfWidth;
			_buttonEndY = buttonScreenPos.y + halfHeight;

//			Debug.Log (string.Format ("RefreshTouchButtonPos stopButton _buttonStartX : {0}, _buttonStartY : {1}, _buttonEndX : {2}, _buttonEndY : {3}", 
//				_buttonStartX, _buttonStartY, _buttonEndX, _buttonEndY));
		}

	}

	protected virtual void FailStage()
	{
		if (_onFailStage != null) {
			_onFailStage ();
		}
	}

	public virtual void SetStartPlayData()
	{
		_targetTimeRoot.gameObject.SetActive (true);
		_rootPlayObjects.SetActive (true);

        _isGamePlaying = true;
    }

	public virtual void HideTimeObject()
	{
		_targetTimeRoot.gameObject.SetActive (false);
		_rootPlayObjects.SetActive (false);
	}

	public virtual int CheckStopTimeButton(int touchID, float posX, float posY)
	{
		if ((posX >= _buttonStartX && posX <= _buttonEndX) &&
		    (posY >= _buttonStartY && posY <= _buttonEndY)) {
			if (_curTouchID == -1) {
				if (_startButton != null && _stopButton != null) {
					_startButton.SetActive (false);
					_stopButton.SetActive (true);
				}
				_curTouchID = touchID;
			}
			return _stopButtonIndex;
		} else {
			if (_curTouchID == touchID) {
				if (_startButton != null && _stopButton != null) {
					_startButton.SetActive (true);
					_stopButton.SetActive (false);
				}
				_curTouchID = -1;
			}
		}

		return -1;
	}

    protected void SetCatchMoveObjEnable(int objIndex, bool enable)
    {
        for(int i =0;i<_catchMoveObjectInfos.Count;i++)
        {
            if(_catchMoveObjectInfos[i].ObjectIndex == objIndex)
            {
                _catchMoveObjectInfos[i].ObjTransform.gameObject.SetActive(enable);
                _catchMoveObjectInfos[i].IsEnable = enable;
                break;
            }
        }
    }

    //	public virtual int CheckStopTimeOtherButtons(int touchID, float posX, float posY)
    //	{
    //		if ((posX >= _buttonStartX && posX <= _buttonEndX) &&
    //			(posY >= _buttonStartY && posY <= _buttonEndY)) {
    //			if (_curTouchID == -1) {
    //				_startButton.SetActive (false);
    //				_stopButton.SetActive (true);
    //				_curTouchID = touchID;
    //			}
    //			return _buttonIndex;
    //		} else {
    //			if (_curTouchID == touchID) {
    //				_startButton.SetActive (true);
    //				_stopButton.SetActive (false);
    //				_curTouchID = -1;
    //			}
    //		}
    //
    //		return -1;
    //	}

    public virtual CACurGamePlayTouchData OnGetTouchData(int touchID, float posX, float posY)
	{
		return null;
	}

	public virtual void OnTouchPress(int touchID, float posX, float posY)
	{
		
	}

	public virtual void OnTouchRelease(int touchID, float posX, float posY)
	{
		
	}

	public virtual void OnTouchDrag(int touchID, float posX, float posY)
	{
		
	}

	public virtual void OnTouchMoveValues(int touchID, float moveX, float moveY)
	{

	}

	public virtual void OnFinishedPlay()
	{
		
	}

    public void AddStopTimeObjectInfo(StageObjectType objectType, int objectIndex, tk2dSprite objectSprite, Vector3 matchingPos)
    {
        StopTimeObjectInfo inputObjectInfo = new StopTimeObjectInfo();
        inputObjectInfo.ObjectIndex = objectIndex;

        inputObjectInfo.ObjStartPos = objectSprite.transform.localPosition;

        inputObjectInfo.ObjWidth = objectSprite.GetBounds().size.x;
        inputObjectInfo.ObjHeight = objectSprite.GetBounds().size.y;

        inputObjectInfo.ObjTransform = objectSprite.transform;
        inputObjectInfo.MatchingPos = matchingPos;

        switch (objectType)
        {
            case StageObjectType.CatchMove:
                _catchMoveObjectInfos.Add(inputObjectInfo);
                break;
            case StageObjectType.MotionAni:
                _motionAniObjectInfos.Add(inputObjectInfo);
                break;
            case StageObjectType.CatchMoveMotionAni:
                _catchMotionAniObjectInfos.Add(inputObjectInfo);
                break;
        }

    }

    public void AddStopTimeObjectInfo(StageObjectType objectType, int objectIndex, Transform objTrans, tk2dSprite objectSprite, Vector3 matchingPos)
	{
		StopTimeObjectInfo inputObjectInfo = new StopTimeObjectInfo ();
		inputObjectInfo.ObjectIndex = objectIndex;

		inputObjectInfo.ObjStartPos = objTrans.localPosition;

		inputObjectInfo.ObjWidth = objectSprite.GetBounds ().size.x;
		inputObjectInfo.ObjHeight = objectSprite.GetBounds ().size.y;

		inputObjectInfo.ObjTransform = objTrans;
		inputObjectInfo.MatchingPos = matchingPos;

		switch (objectType) {
		case StageObjectType.CatchMove:
			_catchMoveObjectInfos.Add (inputObjectInfo);
			break;
		case StageObjectType.MotionAni:
			_motionAniObjectInfos.Add (inputObjectInfo);
			break;
		case StageObjectType.CatchMoveMotionAni:
			_catchMotionAniObjectInfos.Add (inputObjectInfo);
			break;
		}

	}

	public void AddStopTimeObjectInfo(StageObjectType objectType, int objectIndex, Transform objTransform, float width, float height, Vector3 matchingPos, float localPosX = 0f, float localPosY = 0f)
	{
		StopTimeObjectInfo inputObjectInfo = new StopTimeObjectInfo ();
		inputObjectInfo.ObjectIndex = objectIndex;

		inputObjectInfo.ObjStartPos = objTransform.localPosition;

		inputObjectInfo.ObjWidth = width;
		inputObjectInfo.ObjHeight = height;

		inputObjectInfo.ObjTransform = objTransform;
		inputObjectInfo.MatchingPos = matchingPos;

		inputObjectInfo.ObjLocalPosX = localPosX;
		inputObjectInfo.ObjLocalPosY = localPosY;

		switch (objectType) {
		case StageObjectType.CatchMove:
			_catchMoveObjectInfos.Add (inputObjectInfo);
			break;
		case StageObjectType.MotionAni:
			_motionAniObjectInfos.Add (inputObjectInfo);
			break;
		case StageObjectType.CatchMoveMotionAni:
			_catchMotionAniObjectInfos.Add (inputObjectInfo);
			break;
		}

	}

	public virtual void TouchButtonObj(int buttonIndex)
	{
		Debug.Log (string.Format ("TouchButtonObj buttonIndex : {0}", buttonIndex));
	}

	protected virtual void SetPenaltyTime(float penaltyTime)
	{
		if (_onAddRealTimeValue != null) {
			_onAddRealTimeValue (penaltyTime);
		}
	}

    public virtual void SetGameEnd()
    {
        _isGamePlaying = false;
        SoundManager.Instance.StopSubMusic(true);
    }

    public virtual void SetSolveButton()
    {
        SoundManager.Instance.PlayEffectFX(SoundDefinitions.effectSound_10);
    }

    #endregion

    #region CallBack Methods

    protected virtual void OnChangeMoveValue(int motionID, ObjectMotionManager.MotionType motionType, Vector2 moveValue)
	{
		Vector3 curPosition = _motionAniTransforms [motionID].localPosition;
		_motionAniTransforms [motionID].localPosition = new Vector3 (curPosition.x + moveValue.x, curPosition.y + moveValue.y, curPosition.z);
	}

	protected virtual void OnCompleteMotionAni(int motionID, ObjectMotionAni objMotionAni)
	{
		//Debug.Log (string.Format ("OnCompleteMotionAni motionID : {0}", motionID));
		if (_motionAniTransforms.ContainsKey (motionID)) {
			//Debug.Log(string.Format ("OnCompleteMotionAni Remove objIndex : {0}", motionID));
			_motionAniTransforms.Remove (motionID);
		}

    }

	protected virtual void OnCompleteAllMotionAni()
	{
		
	}

	public virtual void OnButtonTouched(int buttonID)
	{

	}

	#endregion
}
