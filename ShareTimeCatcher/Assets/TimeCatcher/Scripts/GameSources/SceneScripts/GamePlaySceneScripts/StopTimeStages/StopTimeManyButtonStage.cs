﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StopTimeManyButtonStage : StopTimeBaseStage 
{
	#region Serialize Variables

	[SerializeField] GameObject _realButtonObj;

	#endregion

	#region Variables

	int _swapCount = 5;

	#endregion

	#region MonoBehaviour Methods

	protected override void Awake()
	{
		base.Awake ();
	}

	#endregion

	#region Methods

	public override void InitStopTimeStageData()
	{
		InitRandomTimeButtonSwap ();

		base.InitStopTimeStageData ();
	}

	void InitRandomTimeButtonSwap()
	{
		for (int i = 0; i < _swapCount; i++) {
			int ranValue = Random.Range (0, _stopTimeButtons.Length);

			Vector3 preRealButtonPos = _realButtonObj.transform.localPosition;

			_realButtonObj.transform.localPosition = _stopTimeButtons [ranValue].transform.localPosition;
			_stopTimeButtons [ranValue].transform.localPosition = preRealButtonPos;
		}
	}

	public override int CheckStopTimeButton(int touchID, float posX, float posY)
	{
		if ((posX >= _buttonStartX && posX <= _buttonEndX) &&
			(posY >= _buttonStartY && posY <= _buttonEndY)) {
			if (_curTouchID == -1) {
				StartButton.SetActive (false);
				StopButton.SetActive (true);
				_curTouchID = touchID;
			}

			return StopButtonIndex;
		} else {
			for (int i = 0; i < _stopTimeButtons.Length; i++) {
				if (_stopTimeButtons [i].CheckValidTouchArea (posX, posY)) {
					
					if (_curTouchID == -1) {
						_stopTimeButtons [i].StandButtonObj.SetActive (false);
						_stopTimeButtons [i].PushedButtonObj.SetActive (true);
						_curTouchID = touchID;
					}
					_curOtherButtonIndex = i;
					return _stopTimeButtons [i].ButtonIndex;
				}
			}

			if (_curTouchID == touchID) {
				if (_curOtherButtonIndex != -1) {
					_stopTimeButtons [_curOtherButtonIndex].StandButtonObj.SetActive (true);
					_stopTimeButtons [_curOtherButtonIndex].PushedButtonObj.SetActive (false);
				} else {
					StartButton.SetActive (true);
					StopButton.SetActive (false);
				}

				_curOtherButtonIndex = -1;
				_curTouchID = -1;
			}
		}

		return -1;
	}

	public override void TouchButtonObj(int buttonIndex)
	{
		base.TouchButtonObj (buttonIndex);

		SetPenaltyTime (1f);
	}

	public override void OnTouchRelease(int touchID, float posX, float posY)
	{
		if (_curTouchID == touchID) {
			_curTouchID = -1;
		}
	}

	#endregion
}
