﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardMatchInfo : MonoBehaviour 
{
	#region Serialize Variables

	[SerializeField] int _matchIndex;
	[SerializeField] GameObject _cardOpenObject;
	[SerializeField] GameObject _cardCloseObject;

	#endregion

	#region Variables

	bool _isCloseCard;

	#endregion

	#region Properties

	public int MatchIndex
	{
		get{ return _matchIndex; }
	}

	public GameObject CardOpenObject
	{
		get{ return _cardOpenObject; }
	}

	public GameObject CardCloseObject
	{
		get{ return _cardCloseObject; }
	}

	public bool IsCloseCard
	{
		get{ return _isCloseCard; }
	}

	#endregion

	#region Methods

	public void SetCloseCard()
	{
		_isCloseCard = true;
		_cardCloseObject.SetActive (true);
	}

	public void SetOpenCard()
	{
		_isCloseCard = false;
		_cardCloseObject.SetActive (false);
	}

	#endregion
}
