﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StopTimeViewObject : MonoBehaviour 
{
	#region Serialize Variables

	[SerializeField] tk2dSprite _hourImage_1;
	[SerializeField] tk2dSprite _hourImage_2;

	[SerializeField] tk2dSprite _minuteImage_1;
	[SerializeField] tk2dSprite _minuteImage_2;

	[SerializeField] tk2dSprite _secondImage_1;
	[SerializeField] tk2dSprite _secondImage_2;

	[SerializeField] tk2dSprite _millisecondImage_1;
	[SerializeField] tk2dSprite _millisecondImage_2;

	[SerializeField] tk2dSprite _mosaSecondImage_1;
	[SerializeField] tk2dSprite _mosaSecondImage_2;

	[SerializeField] tk2dSprite _mosaMillisecondImage_1;
	[SerializeField] tk2dSprite _mosaMillisecondImage_2;

	#endregion

	#region Properties

	public tk2dSprite HourImage_1
	{
		get{ return _hourImage_1; }
	}

	public tk2dSprite HourImage_2
	{
		get{ return _hourImage_2; }
	}

	public tk2dSprite MinuteImage_1
	{
		get{ return _minuteImage_1; }
	}

	public tk2dSprite MinuteImage_2
	{
		get{ return _minuteImage_2; }
	}

	public tk2dSprite SecondImage_1
	{
		get{ return _secondImage_1; }
	}

	public tk2dSprite SecondImage_2
	{
		get{ return _secondImage_2; }
	}

	public tk2dSprite MillisecondImage_1
	{
		get{ return _millisecondImage_1; }
	}

	public tk2dSprite MillisecondImage_2
	{
		get{ return _millisecondImage_2; }
	}

	public tk2dSprite MosaSecondImage_1
	{
		get{ return _mosaSecondImage_1; }
	}

	public tk2dSprite MosaSecondImage_2
	{
		get{ return _mosaSecondImage_2; }
	}

	public tk2dSprite MosaMillisecondImage_1
	{
		get{ return _mosaMillisecondImage_1; }
	}

	public tk2dSprite MosaMillisecondImage_2
	{
		get{ return _mosaMillisecondImage_2; }
	}

	#endregion
}
