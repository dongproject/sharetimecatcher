﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class StopTimeMoveObjectHitButtonStage : StopTimeBaseStage 
{
	#region Definitions

	public enum HitButtonObjIndex
	{
		Throwing_1 	= 0,
		Throwing_2 	= 1,
		Throwing_3 	= 2,
		Throwing_4 	= 3,
		Throwing_5 	= 4,
		Max			= 5,
	}

    #endregion

    #region Serialize Variables

#pragma warning disable 649

    [SerializeField] GameObject _momObject;

	[Header("Bomb Objects")]
	[SerializeField] tk2dSpriteAnimator _bombSpriteAniEffect_1;
	[SerializeField] tk2dSpriteAnimator _bombSpriteAniEffect_2;

	[Header("Throwing Objects")]
	[SerializeField] tk2dSprite _throwing_1;
	[SerializeField] tk2dSprite _throwing_2;
	[SerializeField] tk2dSprite _throwing_3;
	[SerializeField] tk2dSprite _throwing_4;
	[SerializeField] tk2dSprite _throwing_5;

	[Header("Button Object")]
	[SerializeField] GameObject _buttonObject;

	[Header("Start Positions")]
	[SerializeField] Transform[] _startPositions;

    [Header("Gauge Objects")]
    [SerializeField] tk2dSlicedSprite _gaugeSlicedBG;
    [SerializeField] tk2dSlicedSprite _gaugeSlicedSprite;

#pragma warning restore 649

    #endregion

    #region Variables

    float _appearGapTime = 1f;
	float _curAppearTime = 0f;
	Dictionary<int, int> _startPositionDic = new Dictionary<int, int>();
	int _curStartPosIndex = -1;

	Dictionary<int, int> _moveObjectDic = new Dictionary<int, int>();
	int _curMoveIndex = -1;

	int _moveButtonID = 100;

	int _collideIndex = -1;

    float _buttonTotalHP;
    float _buttonPerHPValue;
    float _curButtonHP;

    float _gaugeMaxWidth;
    bool _isUpate = false;

    #endregion

    #region MonoBehaviour Methods

    protected override void Awake()
	{
		base.Awake ();

        _momObject.SetActive(false);

        AddStopTimeObjectInfo (StageObjectType.MotionAni, (int)HitButtonObjIndex.Throwing_1, _throwing_1, new Vector3(479f, 32f, -10f));
		AddStopTimeObjectInfo (StageObjectType.MotionAni, (int)HitButtonObjIndex.Throwing_2, _throwing_2, new Vector3(495f, -170f, -10f));
		AddStopTimeObjectInfo (StageObjectType.MotionAni, (int)HitButtonObjIndex.Throwing_3, _throwing_3, new Vector3(-477f, -331f, -10f));
		AddStopTimeObjectInfo (StageObjectType.MotionAni, (int)HitButtonObjIndex.Throwing_4, _throwing_4, new Vector3(-489f, -156f, -10f));
		AddStopTimeObjectInfo (StageObjectType.MotionAni, (int)HitButtonObjIndex.Throwing_5, _throwing_5, new Vector3(-485f, -11f, -10f));

		AddStopTimeObjectInfo (StageObjectType.MotionAni, _moveButtonID, _buttonObject.transform, 200f, 200f, Vector3.zero);

		for (int i = 0; i < _startPositions.Length; i++) {
			_startPositionDic.Add (i, i);
		}

		for (int i = 0; i < (int)HitButtonObjIndex.Max; i++) {
			_moveObjectDic.Add (i, i);
		}
	}

    protected override void Start()
    {
        base.Start();
        _gaugeMaxWidth = _gaugeSlicedBG.dimensions.x;
        SetHitObjectGaugeValue(1f);
    }

    //private void Update()
    //{
    //    if (_isUpate)
    //    {
    //        _motionManager.Update();
    //        CheckAppearObject();
    //        _eventTimer.UpdateGameTimer();
    //    }
    //}

    #endregion

    #region Methods

    public override void SetStartPlayData()
	{
		base.SetStartPlayData ();

        _momObject.SetActive(true);
        _isUpate = true;

        StartCoroutine (OnUpdate ());
	}

    public void SetButtonHPValues(float totalHP, float perHPValue)
    {
        _buttonTotalHP = totalHP;
        _buttonPerHPValue = perHPValue;
        _curButtonHP = _buttonTotalHP;
    }

    void SetHitObjectGaugeValue(float gaugeValue)
    {
        _gaugeSlicedSprite.dimensions = new Vector2(_gaugeMaxWidth * gaugeValue, _gaugeSlicedSprite.dimensions.y);
    }

    public override int CheckStopTimeButton(int touchID, float posX, float posY)
	{
		return base.CheckStopTimeButton(touchID, posX, posY);
	}

	void AddObjectMoveAni(int objIndex, Vector3 startPos, Vector2 destValue, int motionID, float aniTime = 1f)
	{
		Transform moveTransform = null;
		for (int i = 0; i < _motionAniObjectInfos.Count; i++) {
			if (_motionAniObjectInfos [i].ObjectIndex == objIndex) {
				moveTransform = _motionAniObjectInfos [i].ObjTransform;
				if (moveTransform.localPosition.x == destValue.x && moveTransform.localPosition.y == destValue.y) {
					Debug.Log (string.Format ("AddObjectMoveAni Same Position!!!"));
					return;
				}
				break;
			}
		}

		if (moveTransform == null) {
			Debug.Log (string.Format ("AddObjectMoveAni Not Exist MoveAni Object!!!"));
			return;
		}

		moveTransform.localPosition = startPos;
		moveTransform.gameObject.SetActive (true);

		Vector3 destPos = new Vector3 (destValue.x, 
			destValue.y, moveTransform.localPosition.z);
		_motionManager.AddObjTimeMovement (aniTime, startPos, destPos, OnChangeMoveValue, motionID, OnCompleteMotionAni); 

		_motionAniTransforms.Add (motionID, moveTransform);
	}

	void AddButtonObjectMoveAni(int objIndex, int motionID, ObjectMotionAni objMotionAni, float aniTime)
	{
		Transform moveTransform = null;
		for (int i = 0; i < _motionAniObjectInfos.Count; i++) {
			if (_motionAniObjectInfos [i].ObjectIndex == objIndex) {
				moveTransform = _motionAniObjectInfos [i].ObjTransform;
				break;
			}
		}

		if (moveTransform == null) {
			Debug.Log (string.Format ("AddObjectMoveAni Not Exist MoveAni Object!!!"));
			return;
		}

		moveTransform.gameObject.SetActive (true);

		_motionManager.AddObjMotionAniTimeMovement (aniTime, objMotionAni, OnChangeMoveValue, motionID, OnCompleteMotionAni); 

		_motionAniTransforms.Add (motionID, moveTransform);
	}

	void CheckAppearObject()
	{
		_curAppearTime += Time.deltaTime;
		if (_curAppearTime >= _appearGapTime && _moveObjectDic.Count > 0) {
			_curAppearTime = 0f;
			List<int> moveObjKeys = _moveObjectDic.Keys.ToList ();
			int ranValue = Random.Range (0, moveObjKeys.Count);
			if (_curMoveIndex != -1) {
				_moveObjectDic.Add (_curMoveIndex, _curMoveIndex);
			}
			_curMoveIndex = _moveObjectDic [moveObjKeys [ranValue]];
			_moveObjectDic.Remove (_curMoveIndex);

			if(_motionAniTransforms.ContainsKey(_curMoveIndex))
				_motionAniTransforms.Remove (_curMoveIndex);

			List<int> postionKeys = _startPositionDic.Keys.ToList ();
			int ranPosIndex = Random.Range (0, postionKeys.Count);
			if (_curStartPosIndex != -1)
				_startPositionDic.Add (_curStartPosIndex, _curStartPosIndex);
			_curStartPosIndex = _startPositionDic [postionKeys [ranPosIndex]];
			_startPositionDic.Remove (_curStartPosIndex);

//			Debug.Log (string.Format ("CheckAppearObject _curMoveIndex : {0}, _curStartPosIndex : {1}, _moveObjectDic Count {2}", 
//				_curMoveIndex, _curStartPosIndex, _moveObjectDic.Count));
			AddObjectMoveAni (_curMoveIndex, _startPositions[_curStartPosIndex].localPosition, _buttonObject.transform.localPosition, _curMoveIndex, 1.0f);
		}
	}

    void ChangeButtonPerHP()
    {
        _curButtonHP -= _buttonPerHPValue;

        if (_curButtonHP <= 0f) {
            _curButtonHP = 0f;
            FailStage();
            //_motionManager.ReleaseAllObjMovement();
            _isUpate = false;
        }

        SetHitObjectGaugeValue(_curButtonHP/_buttonTotalHP);
    }

    public override void OnTouchPress(int touchID, float posX, float posY)
	{
		//Debug.Log (string.Format ("OnTouchPress touchID : {0}, posX : {1}, posY : {2}", touchID, posX, posY));

		float imgPosX = CAResolutionCtl.Instance.GetScreenToImagePosX(posX);
		float imgPosY = CAResolutionCtl.Instance.GetScreenToImagePosY(posY);

        float revisionValue = 5f;

		for (int i = 0; i < _motionAniObjectInfos.Count; i++) {
			int motionID = _motionAniObjectInfos [i].ObjectIndex;
			if (motionID == _moveButtonID)
				continue;

            if (!_motionAniTransforms.ContainsKey(motionID))
                continue;

			if ((_motionAniObjectInfos [i].XMin - revisionValue <= imgPosX && _motionAniObjectInfos [i].XMax + revisionValue >= imgPosX) &&
				(_motionAniObjectInfos [i].YMin - revisionValue <= imgPosY && _motionAniObjectInfos [i].YMax + revisionValue >= imgPosY)) {

                //Debug.Log (string.Format ("CheckMatchObject Matched index : {0}, Object Index : {1}", i, _motionAniObjectInfos[i].ObjectIndex));

                SoundManager.Instance.PlayEffectFX(SoundDefinitions.effectSound_17);

                tk2dSpriteAnimator bombAnimator = _bombSpriteAniEffect_2;
				bombAnimator.transform.localPosition = new Vector3(_motionAniTransforms [motionID].localPosition.x, 
					_motionAniTransforms [motionID].localPosition.y, -10f);
				bombAnimator.gameObject.SetActive (true);
				_eventTimer.SetGameTimerData (OnDisableBombEffect, 0.5f, bombAnimator);

				_motionAniTransforms [motionID].gameObject.SetActive (false);
                _motionAniTransforms.Remove(motionID);

                _motionManager.ReleaseObjMovement (motionID);
                break;
			}
		}
	}

	#endregion

	#region Coroutine Methods

	public IEnumerator OnUpdate()
	{
		while(_isUpate)
		{
            if (_isGamePlaying)
            {
                _motionManager.Update();
                CheckAppearObject();
                _eventTimer.UpdateGameTimer();
            }

			yield return null;
		}
	}

	#endregion

	#region CallBack Methods

	protected override void OnCompleteMotionAni(int motionID, ObjectMotionAni objMotionAni)
	{
		if (_collideIndex == motionID && motionID != _moveButtonID && objMotionAni != null) {
            //AddButtonObjectMoveAni (_moveButtonID, _moveButtonID, objMotionAni, 0.2f);
            ChangeButtonPerHP();
            _collideIndex = -1;
		}

		base.OnCompleteMotionAni (motionID, objMotionAni);

//		_moveObjectDic.Add (motionID, motionID);
	}

	protected override void OnChangeMoveValue(int motionID, ObjectMotionManager.MotionType motionType, Vector2 moveValue)
	{
        if (!_motionAniTransforms.ContainsKey(motionID))
            return;

		if (motionID == _moveButtonID) {
//			base.OnChangeMoveValue(motionID, motionType, moveValue);
			Vector3 curPosition = _motionAniTransforms [motionID].localPosition;

			float objHalfWidth = 50f;
			float objHalfHeight = 50f;
			float objPosX = Mathf.Clamp (curPosition.x + moveValue.x, 
				-CAResolutionCtl.Instance.GetResolutionHalfWidth() + objHalfWidth, 
				CAResolutionCtl.Instance.GetResolutionHalfWidth() - objHalfWidth);

			float objPosY = Mathf.Clamp (curPosition.y + moveValue.y, 
				-CAResolutionCtl.Instance.GetResolutionHalfHeight() + objHalfHeight, 
				CAResolutionCtl.Instance.GetResolutionHalfHeight() - objHalfHeight);

			_motionAniTransforms [motionID].localPosition = new Vector3 (objPosX, objPosY, curPosition.z);
			RefreshButtonPos ();
		} else {
			base.OnChangeMoveValue(motionID, motionType, moveValue);
			float calcDis = CAMathCtl.Instance.CalcDistance (_buttonObject.transform.localPosition.x, _buttonObject.transform.localPosition.y,
				_motionAniTransforms [motionID].localPosition.x, _motionAniTransforms [motionID].localPosition.y);

			if (calcDis < 100f) {
				_collideIndex = motionID;
//				int ranValue = Random.Range (0, 2);
				tk2dSpriteAnimator bombAnimator = _bombSpriteAniEffect_1;
//				if (ranValue == 0) {
//					bombAnimator = _bombSpriteAniEffect_1;
//				} else {
//					bombAnimator = _bombSpriteAniEffect_2;
//				}
				bombAnimator.transform.localPosition = new Vector3(_motionAniTransforms [motionID].localPosition.x, 
					_motionAniTransforms [motionID].localPosition.y, -10f);
				bombAnimator.gameObject.SetActive (true);
				_eventTimer.SetGameTimerData (OnDisableBombEffect, 0.5f, bombAnimator);
				_motionAniTransforms [motionID].gameObject.SetActive (false);
                _motionAniTransforms.Remove(motionID);

                _motionManager.ReleaseObjMovement (motionID);
			}
		}


//		Debug.Log (string.Format ("OnChangeMoveValue motionID : {0}, calcDis: {1}", motionID, calcDis));
	}

	void OnDisableBombEffect(object objData)
	{
		tk2dSpriteAnimator bombAnimator = objData as tk2dSpriteAnimator;
		bombAnimator.gameObject.SetActive (false);
	}

	#endregion
}
