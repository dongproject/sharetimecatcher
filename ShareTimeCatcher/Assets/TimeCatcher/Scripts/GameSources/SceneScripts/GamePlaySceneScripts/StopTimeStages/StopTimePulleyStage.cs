﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum PulleyHangType
{
	NotHang,
	RightOneTon,
    RightPulley,
	RightHalfTon,
	RightHalfTonOneTon,
	RightHalfTonPulley,
	RightHalfPulleyOne, // Success
}

public class StopTimePulleyStage : StopTimeBaseStage 
{
	public enum PulleyObjIndex
	{
		HalfTon 		= 0,
		OneTon			= 1,
		Pulley			= 2,
		LeftLine		= 3,
		CenterLine		= 4,
		RightLine		= 5,
	}

	#region Serialize Variables

	[SerializeField] GameObject _pulleyObj;
	[SerializeField] GameObject[] _weightObjs;
	[SerializeField] GameObject[] _railObjs;

	[SerializeField] Transform _leftPulleyMatchTrans;
	[SerializeField] Transform _leftLineHookTrans;
	[SerializeField] Transform _rightLineHookTrans;

	[SerializeField] Transform _halfTonHookTrans;
	[SerializeField] Transform _oneTonHookTrans;
	[SerializeField] Transform _pulleyHookTrans;

	#endregion

	#region Variables

	PulleyHangType _pulleyHangType = PulleyHangType.NotHang;

	Rect _moveAreaRect;

	#endregion

	#region Properties

	#endregion

	#region MonoBehaviour Methods

	protected override void Awake()
	{
		base.Awake ();
		AddStopTimeObjectInfo (StageObjectType.CatchMove, (int)PulleyObjIndex.HalfTon, _weightObjs[0].GetComponent<tk2dSprite>(), new Vector3(95f, -302f, -17f)); // HalfTon
		AddStopTimeObjectInfo (StageObjectType.CatchMove, (int)PulleyObjIndex.OneTon, _weightObjs[1].GetComponent<tk2dSprite>(), new Vector3(98f, -466f, -17f)); // OneTon
       //AddStopTimeObjectInfo (StageObjectType.CatchMove, (int)PulleyObjIndex.Pulley, _pulleyObj.GetComponent<tk2dSprite>(), new Vector3(93f, -108f, -16f)); // Pulley
        AddStopTimeObjectInfo(StageObjectType.CatchMove, (int)PulleyObjIndex.Pulley, _pulleyObj.transform, 130f, 220f, new Vector3(77f, -142f, -16f)); // Pulley
        AddStopTimeObjectInfo (StageObjectType.MotionAni, (int)PulleyObjIndex.LeftLine, _railObjs[0].transform, 110f, 1200f, new Vector3(0f, 0f, -15f));
		AddStopTimeObjectInfo (StageObjectType.MotionAni, (int)PulleyObjIndex.CenterLine, _railObjs[1].transform, 30f, 1160f, new Vector3(0f, 0f, -15f));
		AddStopTimeObjectInfo (StageObjectType.MotionAni, (int)PulleyObjIndex.RightLine, _railObjs[2].transform, 60f, 1240f, new Vector3(0f, 0f, -15f));
//		AddStopTimeObjectInfo (StageObjectType.CatchMove, (int)PulleyObjIndex.Pulley, _trapSalmonSprite, new Vector3(-101f, -315f, -10f));

		float screenWidth = CAResolutionCtl.Instance.GetResolutionWidth ();
		float screenHeight = CAResolutionCtl.Instance.GetResolutionHeight ();

		_moveAreaRect = new Rect (-screenWidth * 0.5f, -screenHeight * 0.5f, screenWidth, screenHeight);

		StartCoroutine (OnUpdate ());
	}

	#endregion

	#region Methods

	public override void OnTouchPress(int touchID, float posX, float posY)
	{
		if(_pulleyHangType != PulleyHangType.RightHalfPulleyOne)
			CheckMatchObject (touchID, posX, posY);
	}

	void CheckMatchObject(int touchID, float posX, float posY)
	{
		if (_curCatchObjectInfo.TouchID != -1) {
			if (_curCatchObjectInfo.TouchID != touchID)
				return;
		}

		float imgPosX = CAResolutionCtl.Instance.GetScreenToImagePosX(posX);
		float imgPosY = CAResolutionCtl.Instance.GetScreenToImagePosY(posY);

		for (int i = 0; i < _catchMoveObjectInfos.Count; i++) {
			if ((_catchMoveObjectInfos [i].XMin <= imgPosX && _catchMoveObjectInfos [i].XMax >= imgPosX) &&
				(_catchMoveObjectInfos [i].YMin <= imgPosY && _catchMoveObjectInfos [i].YMax >= imgPosY)) {

				if (_pulleyHangType == PulleyHangType.RightHalfTonPulley) {
					if (_catchMoveObjectInfos [i].ObjectIndex == (int)PulleyObjIndex.HalfTon)
						continue;
				} else if (_pulleyHangType == PulleyHangType.RightHalfPulleyOne) {
					if (_catchMoveObjectInfos [i].ObjectIndex == (int)PulleyObjIndex.Pulley)
						continue;
				}

				_curCatchObjectInfo.TouchID = touchID;
				_curCatchObjectInfo.ObjectInfo = _catchMoveObjectInfos [i];

//				TestObjectMoveAni (_curCatchObjectInfo.ObjectInfo.ObjectIndex, _curCatchObjectInfo.ObjectInfo.ObjTransform);
				Debug.Log (string.Format ("CheckMatchObject Matched index : {0}", i));
				break;
			}
		}
	}

	void AddObjectMoveAni(int objIndex, Vector2 destValue, float aniTime = 1f)
	{
		Transform moveTransform = null;
		for (int i = 0; i < _motionAniObjectInfos.Count; i++) {
			if (_motionAniObjectInfos [i].ObjectIndex == objIndex) {
				moveTransform = _motionAniObjectInfos [i].ObjTransform;
				if (moveTransform.localPosition.x == destValue.x && moveTransform.localPosition.y == destValue.y) {
					Debug.Log (string.Format ("AddObjectMoveAni Same Position!!!"));
					return;
				}
				break;
			}
		}

		if (moveTransform == null) {
			Debug.Log (string.Format ("AddObjectMoveAni Not Exist MoveAni Object!!!"));
			return;
		}

		Vector3 destPos = new Vector3 (destValue.x, 
			destValue.y, moveTransform.localPosition.z);
		_motionManager.AddObjTimeMovement (aniTime, moveTransform.localPosition, destPos, OnChangeMoveValue, objIndex, OnCompleteMotionAni); 
	
		_motionAniTransforms.Add (objIndex, moveTransform);
	}

	public override void OnTouchRelease(int touchID, float posX, float posY)
	{
		if (_curCatchObjectInfo.TouchID == touchID) {
			CheckMatchLineObject (_curCatchObjectInfo.ObjectInfo);
			_curCatchObjectInfo.TouchID = -1;
			_curCatchObjectInfo.ObjectInfo = null;
		}
	}

	public override void OnTouchMoveValues(int touchID, float moveX, float moveY)
	{
		if (_curCatchObjectInfo.TouchID == touchID) {

			Vector3 objPos = _curCatchObjectInfo.ObjectInfo.ObjTransform.localPosition;
			float objHalfWidth = _curCatchObjectInfo.ObjectInfo.ObjWidth * 0.5f;
			float objHalfHeight = _curCatchObjectInfo.ObjectInfo.ObjHeight * 0.5f;
			float objPosX = Mathf.Clamp (objPos.x + moveX, 
				-CAResolutionCtl.Instance.GetResolutionHalfWidth() + objHalfWidth, 
				CAResolutionCtl.Instance.GetResolutionHalfWidth() - objHalfWidth);

			float objPosY = Mathf.Clamp (objPos.y + moveY, 
				-CAResolutionCtl.Instance.GetResolutionHalfHeight() + objHalfHeight, 
				CAResolutionCtl.Instance.GetResolutionHalfHeight() - objHalfHeight);

			//			Debug.Log (string.Format ("OnTouchMoveValues objPosX : {0}, objPosY : {1}", objPosX, objPosY));

			_curCatchObjectInfo.ObjectInfo.ObjTransform.localPosition = new Vector3 (objPosX, objPosY,
				_curCatchObjectInfo.ObjectInfo.ObjTransform.localPosition.z);

			if (_pulleyHangType == PulleyHangType.RightOneTon) {
				if (_curCatchObjectInfo.ObjectInfo.ObjectIndex == (int)PulleyObjIndex.OneTon) {
					_curCatchObjectInfo.ObjectInfo.ObjTransform.localScale = Vector3.one;

					SetPulleyHangType (PulleyHangType.NotHang);
				}
            } else if (_pulleyHangType == PulleyHangType.RightPulley) {
                if (_curCatchObjectInfo.ObjectInfo.ObjectIndex == (int)PulleyObjIndex.Pulley) {
                    _curCatchObjectInfo.ObjectInfo.ObjTransform.localScale = Vector3.one;

                    SetPulleyHangType(PulleyHangType.NotHang);
                }
            } else if (_pulleyHangType == PulleyHangType.RightHalfTon) {
				if (_curCatchObjectInfo.ObjectInfo.ObjectIndex == (int)PulleyObjIndex.HalfTon) {
					_curCatchObjectInfo.ObjectInfo.ObjTransform.localScale = Vector3.one;

					SetPulleyHangType (PulleyHangType.NotHang);
				}
            } else if (_pulleyHangType == PulleyHangType.RightHalfTonOneTon) {
                if (_curCatchObjectInfo.ObjectInfo.ObjectIndex == (int)PulleyObjIndex.OneTon) {
					_curCatchObjectInfo.ObjectInfo.ObjTransform.localScale = Vector3.one;

					SetPulleyHangType (PulleyHangType.RightHalfTon);
				}
            } else if (_pulleyHangType == PulleyHangType.RightHalfTonPulley) {
                if (_curCatchObjectInfo.ObjectInfo.ObjectIndex == (int)PulleyObjIndex.Pulley)
                {
                    _curCatchObjectInfo.ObjectInfo.ObjTransform.localScale = Vector3.one;

                    SetPulleyHangType(PulleyHangType.RightHalfTon);
                }
            }
        }
	}

	public override int CheckStopTimeButton(int touchID, float posX, float posY)
	{
		if (_motionManager.CheckMotionState() || _pulleyHangType != PulleyHangType.RightHalfPulleyOne)
			return -1;

		return base.CheckStopTimeButton(touchID, posX, posY);
	}

	void SetPulleyHangType(PulleyHangType hangType)
	{
		_pulleyHangType = hangType;
        switch (_pulleyHangType)
        {
            case PulleyHangType.NotHang:
                {
                    AddObjectMoveAni((int)PulleyObjIndex.LeftLine, new Vector2(0f, 0f));
                    AddObjectMoveAni((int)PulleyObjIndex.RightLine, new Vector2(0f, 0f));
                }
                break;
            case PulleyHangType.RightOneTon:
                {
                    float leftLineMovePos = 1128f + 150f;
                    float rightLineMovePos = -376f;
                    AddObjectMoveAni((int)PulleyObjIndex.LeftLine, new Vector2(0f, leftLineMovePos));
                    AddObjectMoveAni((int)PulleyObjIndex.RightLine, new Vector2(0f, rightLineMovePos));
                }
                break;
            case PulleyHangType.RightPulley:
                {
                    float leftLineMovePos = 1128f + 150f;
                    float rightLineMovePos = -376f;
                    AddObjectMoveAni((int)PulleyObjIndex.LeftLine, new Vector2(0f, leftLineMovePos));
                    AddObjectMoveAni((int)PulleyObjIndex.RightLine, new Vector2(0f, rightLineMovePos));
                }
                break;
            case PulleyHangType.RightHalfTon:
                {
                    float leftLineMovePos = 752f;
                    float centerLineMovePos = 68f;
                    float rightLineMovePos = -188f;
                    AddObjectMoveAni((int)PulleyObjIndex.LeftLine, new Vector2(0f, leftLineMovePos));
                    AddObjectMoveAni((int)PulleyObjIndex.CenterLine, new Vector2(0f, centerLineMovePos));
                    AddObjectMoveAni((int)PulleyObjIndex.RightLine, new Vector2(0f, rightLineMovePos));
                }
                break;
            case PulleyHangType.RightHalfTonOneTon:
                {
                    float leftLineMovePos = 280f; //174f;
                    float centerLineMovePos = 1500f;//68f;
                    //float rightLineMovePos = -188f;
                    AddObjectMoveAni((int)PulleyObjIndex.LeftLine, new Vector2(0f, leftLineMovePos));
                    AddObjectMoveAni((int)PulleyObjIndex.CenterLine, new Vector2(0f, centerLineMovePos));
                    //AddObjectMoveAni((int)PulleyObjIndex.RightLine, new Vector2(0f, rightLineMovePos));
                }
                break;
            case PulleyHangType.RightHalfTonPulley:
                {
                    float leftLineMovePos = 567f;
                    float rightLineMovePos = -188f;
                    AddObjectMoveAni((int)PulleyObjIndex.LeftLine, new Vector2(0f, leftLineMovePos));
                    AddObjectMoveAni((int)PulleyObjIndex.RightLine, new Vector2(0f, rightLineMovePos));
                }
                break;
            case PulleyHangType.RightHalfPulleyOne:
                {
                    float leftLineMovePos = 409f;
                    float centerLineMovePos = 300f + 60f;
                    float rightLineMovePos = -188f;
                    AddObjectMoveAni((int)PulleyObjIndex.LeftLine, new Vector2(0f, leftLineMovePos));
                    AddObjectMoveAni((int)PulleyObjIndex.CenterLine, new Vector2(0f, centerLineMovePos));
                    AddObjectMoveAni((int)PulleyObjIndex.RightLine, new Vector2(0f, rightLineMovePos));

                    SetSolveButton();
                }
                break;
        }
    }
		
	bool CheckMatchLineObject(StopTimeObjectInfo matchObjectInfo)
	{
		if (_motionManager.CheckMotionState ())
			return false;

		if (matchObjectInfo.ObjectIndex == (int)PulleyObjIndex.HalfTon) {
			return CheckMatchHalfTon (matchObjectInfo);
		} else if (matchObjectInfo.ObjectIndex == (int)PulleyObjIndex.OneTon) {
			return CheckMatchOneTon (matchObjectInfo);
		} else if (matchObjectInfo.ObjectIndex == (int)PulleyObjIndex.Pulley) {
			return CheckMatchPulleyObject (matchObjectInfo);
		}

		return false;
	}

	bool CheckMatchHalfTon(StopTimeObjectInfo matchObjectInfo)
	{
		float validDis = 70f;

		if (_pulleyHangType == PulleyHangType.NotHang) {
			Vector3 rightLineHookScreenPos = _stageCamera.WorldToScreenPoint (_rightLineHookTrans.position);

			float rightLineHookImgPosX = CAResolutionCtl.Instance.GetScreenToImagePosX (rightLineHookScreenPos.x);
			float rightLineHookImgPosY = CAResolutionCtl.Instance.GetScreenToImagePosY (rightLineHookScreenPos.y);

			Vector3 halfTonHookScreenPos = _stageCamera.WorldToScreenPoint (_halfTonHookTrans.position);

			float halfTonHookImgPosX = CAResolutionCtl.Instance.GetScreenToImagePosX (halfTonHookScreenPos.x);
			float halfTonHookImgPosY = CAResolutionCtl.Instance.GetScreenToImagePosY (halfTonHookScreenPos.y);

			float rightLineCalcDis = CAMathCtl.Instance.GetPointDistance (halfTonHookImgPosX, 
				halfTonHookImgPosY, rightLineHookImgPosX, rightLineHookImgPosY);

			if (rightLineCalcDis < validDis) {
				matchObjectInfo.ObjTransform.localPosition = new Vector3 (rightLineHookImgPosX,
					rightLineHookImgPosY - 100f,
					matchObjectInfo.ObjTransform.localPosition.z);

				SetPulleyHangType (PulleyHangType.RightHalfTon);
				Debug.Log (string.Format ("CheckMatchHalfTon rightLine Matching Success!!!!"));
				return true;
			}
		}

		return false;
	}

	bool CheckMatchOneTon(StopTimeObjectInfo matchObjectInfo)
	{
		float validDis = 70f;

		if (_pulleyHangType == PulleyHangType.NotHang) {
			Vector3 rightLineHookScreenPos = _stageCamera.WorldToScreenPoint (_rightLineHookTrans.position);

			float rightLineHookImgPosX = CAResolutionCtl.Instance.GetScreenToImagePosX (rightLineHookScreenPos.x);
			float rightLineHookImgPosY = CAResolutionCtl.Instance.GetScreenToImagePosY (rightLineHookScreenPos.y);

			Vector3 oneTonHookScreenPos = _stageCamera.WorldToScreenPoint (_oneTonHookTrans.position);

			float oneTonHookImgPosX = CAResolutionCtl.Instance.GetScreenToImagePosX (oneTonHookScreenPos.x);
			float oneTonHookImgPosY = CAResolutionCtl.Instance.GetScreenToImagePosY (oneTonHookScreenPos.y);

			float rightLineCalcDis = CAMathCtl.Instance.GetPointDistance (oneTonHookImgPosX,
                                         oneTonHookImgPosY, rightLineHookImgPosX, rightLineHookImgPosY);

			if (rightLineCalcDis < validDis) {
				matchObjectInfo.ObjTransform.localPosition = new Vector3 (rightLineHookImgPosX,
					rightLineHookImgPosY - 100f,
					matchObjectInfo.ObjTransform.localPosition.z);

                SetPulleyHangType (PulleyHangType.RightOneTon);
				Debug.Log (string.Format ("CheckMatchOneTon rightLine Matching Success!!!!"));
				return true;
			}
        } else if(_pulleyHangType == PulleyHangType.RightHalfTon){
            Vector3 leftHookScreenPos = _stageCamera.WorldToScreenPoint(_leftLineHookTrans.position);

            float leftHookImgPosX = CAResolutionCtl.Instance.GetScreenToImagePosX(leftHookScreenPos.x);
            float leftHookImgPosY = CAResolutionCtl.Instance.GetScreenToImagePosY(leftHookScreenPos.y);

            Vector3 oneTonHookScreenPos = _stageCamera.WorldToScreenPoint(_oneTonHookTrans.position);

            float oneTonHookImgPosX = CAResolutionCtl.Instance.GetScreenToImagePosX(oneTonHookScreenPos.x);
            float oneTonHookImgPosY = CAResolutionCtl.Instance.GetScreenToImagePosY(oneTonHookScreenPos.y);

            float pulleyCalcDis = CAMathCtl.Instance.GetPointDistance(oneTonHookImgPosX,
                oneTonHookImgPosY, leftHookImgPosX, leftHookImgPosY);

            if (pulleyCalcDis < validDis)
            {
                matchObjectInfo.ObjTransform.localPosition = new Vector3(oneTonHookImgPosX,
                    oneTonHookImgPosY - 100f,
                    matchObjectInfo.ObjTransform.localPosition.z);

                SetPulleyHangType(PulleyHangType.RightHalfTonOneTon);
                return true;
            }
        } else if (_pulleyHangType == PulleyHangType.RightHalfTonPulley) {
			Vector3 pulleyHookScreenPos = _stageCamera.WorldToScreenPoint (_pulleyHookTrans.position);

			float pulleyHookImgPosX = CAResolutionCtl.Instance.GetScreenToImagePosX (pulleyHookScreenPos.x);
			float pulleyHookImgPosY = CAResolutionCtl.Instance.GetScreenToImagePosY (pulleyHookScreenPos.y);

			Vector3 oneTonHookScreenPos = _stageCamera.WorldToScreenPoint (_oneTonHookTrans.position);

			float oneTonHookImgPosX = CAResolutionCtl.Instance.GetScreenToImagePosX (oneTonHookScreenPos.x);
			float oneTonHookImgPosY = CAResolutionCtl.Instance.GetScreenToImagePosY (oneTonHookScreenPos.y);

			float pulleyCalcDis = CAMathCtl.Instance.GetPointDistance (oneTonHookImgPosX, 
				oneTonHookImgPosY, pulleyHookImgPosX, pulleyHookImgPosY);

			if (pulleyCalcDis < validDis) {
				matchObjectInfo.ObjTransform.localPosition = new Vector3 (oneTonHookImgPosX,
					oneTonHookImgPosY - 100f,
					matchObjectInfo.ObjTransform.localPosition.z);

				SetPulleyHangType (PulleyHangType.RightHalfPulleyOne);
				Debug.Log (string.Format ("CheckMatchOneTon Pulley Matching Success!!!!"));
				return true;
			}
		}

		return false;
	}

	bool CheckMatchPulleyObject(StopTimeObjectInfo matchObjectInfo)
	{
		float validDis = 70f;

        if (_pulleyHangType == PulleyHangType.NotHang)
        {
            Vector3 rightLineHookScreenPos = _stageCamera.WorldToScreenPoint(_rightLineHookTrans.position);

            float rightLineHookImgPosX = CAResolutionCtl.Instance.GetScreenToImagePosX(rightLineHookScreenPos.x);
            float rightLineHookImgPosY = CAResolutionCtl.Instance.GetScreenToImagePosY(rightLineHookScreenPos.y);

            Vector3 pulleyHookScreenPos = _stageCamera.WorldToScreenPoint(_pulleyHookTrans.position);

            float pulleyHookImgPosX = CAResolutionCtl.Instance.GetScreenToImagePosX(pulleyHookScreenPos.x);
            float pulleyHookImgPosY = CAResolutionCtl.Instance.GetScreenToImagePosY(pulleyHookScreenPos.y);

            float rightLineCalcDis = CAMathCtl.Instance.GetPointDistance(pulleyHookImgPosX,
                                         pulleyHookImgPosY, rightLineHookImgPosX, rightLineHookImgPosY);

            if (rightLineCalcDis < validDis)
            {
                matchObjectInfo.ObjTransform.localPosition = new Vector3(rightLineHookImgPosX,
                    rightLineHookImgPosY - 100f,
                    matchObjectInfo.ObjTransform.localPosition.z);

                SetPulleyHangType(PulleyHangType.RightPulley);
                Debug.Log(string.Format("CheckMatchPulleyObject rightLine Matching Success!!!!"));
                return true;
            }
        } else {
            Vector3 leftPulleyScreenPos = _stageCamera.WorldToScreenPoint(_leftPulleyMatchTrans.position);
            //      Debug.Log (string.Format ("LeftPulleyMatch leftPulleyScreenPos : {0}", leftPulleyScreenPos));
            float leftLinePulleyImgPosX = CAResolutionCtl.Instance.GetScreenToImagePosX(leftPulleyScreenPos.x);
            float leftLinePulleyImgPosY = CAResolutionCtl.Instance.GetScreenToImagePosY(leftPulleyScreenPos.y);

            float leftPulleyCalcDis = CAMathCtl.Instance.GetPointDistance(matchObjectInfo.ObjTransform.localPosition.x,
                matchObjectInfo.ObjTransform.localPosition.y, leftLinePulleyImgPosX, leftLinePulleyImgPosY);

            if (leftPulleyCalcDis < validDis)
            {
                matchObjectInfo.ObjTransform.localScale = new Vector3(1f, -1f, 1f);
                matchObjectInfo.ObjTransform.localPosition = new Vector3(leftLinePulleyImgPosX,
                    leftLinePulleyImgPosY - 12f - 58f,
                    matchObjectInfo.ObjTransform.localPosition.z);
                Debug.Log(string.Format("LeftPulleyMatch Matching Success!!!!"));
                SetPulleyHangType(PulleyHangType.RightHalfTonPulley);

                //          _hangObjects.Add ((int)PulleyObjIndex.Pulley, matchObjectInfo.ObjTransform);
                return true;
            }
        }

		return false;
	}

	#endregion

	#region Coroutine Methods

	public IEnumerator OnUpdate()
	{
		while(true)
		{
			_motionManager.Update ();
			yield return null;
		}
	}

	#endregion

	#region CallBack Methods

	protected override void OnChangeMoveValue(int motionID, ObjectMotionManager.MotionType motionType, Vector2 moveValue)
	{
		base.OnChangeMoveValue (motionID, motionType, moveValue);

		List<Transform> hangTrans = new List<Transform> ();

		if (motionID == (int)PulleyObjIndex.RightLine) {
            switch (_pulleyHangType)
            {
                case PulleyHangType.RightOneTon:
                    hangTrans.Add(_weightObjs[1].transform);
                    break;
                case PulleyHangType.RightPulley:
                    hangTrans.Add(_pulleyObj.transform);
                    break;
                case PulleyHangType.RightHalfTon:
                    hangTrans.Add(_weightObjs[0].transform);
                    break;
                case PulleyHangType.RightHalfTonOneTon:
                    hangTrans.Add(_weightObjs[0].transform);
                    break;
                case PulleyHangType.RightHalfTonPulley:
                    hangTrans.Add(_weightObjs[0].transform);
                    break;
                case PulleyHangType.RightHalfPulleyOne:
                    hangTrans.Add(_weightObjs[0].transform);
                    break;
            }
        } else if(motionID == (int)PulleyObjIndex.LeftLine) {
			switch (_pulleyHangType) {
			case PulleyHangType.RightHalfTonOneTon:
				hangTrans.Add (_weightObjs [1].transform);
				break;
			case PulleyHangType.RightHalfTonPulley:
				hangTrans.Add (_pulleyObj.transform);
				break;
			case PulleyHangType.RightHalfPulleyOne:
				hangTrans.Add (_pulleyObj.transform);
				hangTrans.Add (_weightObjs [1].transform);
				break;
			}
		}

		for (int i = 0; i < hangTrans.Count; i++) {
			hangTrans [i].localPosition = new Vector3 (hangTrans [i].localPosition.x + moveValue.x,
				hangTrans [i].localPosition.y + moveValue.y, hangTrans [i].localPosition.z);
		}
	}

	protected override void OnCompleteMotionAni(int motionID, ObjectMotionAni objMotionAni)
	{
		base.OnCompleteMotionAni (motionID, objMotionAni);

		if (_pulleyHangType == PulleyHangType.RightHalfPulleyOne) {
			RefreshButtonPos ();
		}
	}

	#endregion
}
