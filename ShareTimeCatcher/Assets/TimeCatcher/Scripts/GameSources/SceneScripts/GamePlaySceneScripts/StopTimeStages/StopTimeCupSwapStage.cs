﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum CupSwapStep
{
	CupDownAni,
	CupSwap,
	CupSwapComplete,
	CupMatchSuccessChoice,
    CupMatchFailDelayTime,
	CupMatchFailChoice,

}

public enum AniCupSwapTriggerType
{
    CupStartDownAni = 0,
    CupLeftSmallUpAni = 1,
    CupLeftLargeUpAni = 2,
    CupRightSmallDownAni = 3,
    CupRightLargeDownAni = 4,
    CupIdle = 5,
}

public class StopTimeCupSwapStage : StopTimeBaseStage 
{
	#region Serialize Variables

	[SerializeField] Animator[] _cupAnimators;
	[SerializeField] GameObject[] _cupImgObjs;
    [SerializeField] Animator _catAnimator;

	#endregion

	#region Variables

	float _delayStartTime = 1.5f;
	float _curDelayTime = 0f;

	int _leftSmallUpAniHash = Animator.StringToHash("CupLeftSmallUpAni");
	int _leftLargeUpAniHash = Animator.StringToHash("CupLeftLargeUpAni");
	int _rightSmallDownAniHash = Animator.StringToHash("CupSmallRightAni");
	int _rightLargeDownAniHash = Animator.StringToHash("CupRightLargeDownAni");

	string _isStartDown = "IsStartDown";
	string _isLeftLargeUp = "IsLeftLargeUp";
	string _isRightLargeDown = "IsRightLargeDown";
	string _isLeftSmallUp = "IsLeftSmallUp";
	string _isRightSmallDown = "IsRightSmallDown";

	string _curAniBoolString = "";

	int _swapCount = 12;
	int _curSwapCount = 0;

	CupSwapStep _curCupSwapStep = CupSwapStep.CupDownAni;
	int _swapAniCount = 0;

	float _gapSwapDelayTime = 0.1f;
	float _curSwapDelayTime = 0f;

	bool _isSwapCup = false;

	int[] _curCupIndexes = new int[3];

	int _preRanValue = -1;
	int _curButtonIndex = 1;

    float _failDelayTime = 0.5f;
    float _curFailDelayTime = 0f;

	#endregion

	#region Properties

	#endregion

	#region MonoBehaviour Methods

	protected override void Awake()
	{
		base.Awake ();
		for (int i = 0; i < _cupAnimators.Length; i++) {
			_curCupIndexes [i] = i;
			_cupAnimators [i].gameObject.SetActive (false);
			int aniIndex = i;
			AnimationTriggerEvent triggerEvent = _cupAnimators [i].gameObject.GetComponent<AnimationTriggerEvent> (); 
			triggerEvent.OnTriggerEvent = (x) => OnFinishCupAni (aniIndex, x);
		}
		StartCoroutine (OnCheckStartDelayTime());
		_swapAniCount = 0;

        _catAnimator.enabled = false;
    }

	#endregion

	#region Methods

	void SetCupSwap()
	{
		List<int> _cupIndexInfos = new List<int> ();
		for (int i = 0; i < 3; i++) {
			_cupIndexInfos.Add (i);
		}

		int firstRanValue = -1;

		while (true) {
			firstRanValue = Random.Range (0, 3);
			if (firstRanValue != _preRanValue) {
				break;
			}
		}

		_preRanValue = firstRanValue;
		_cupIndexInfos.Remove (firstRanValue);
		int secondRanValue = _cupIndexInfos[Random.Range (0, 2)];

		int gapValue = Mathf.Abs (secondRanValue - firstRanValue);

		Debug.Log (string.Format ("SetCupSwap firstRanValue : {0}, secondRanValue : {1}", firstRanValue, secondRanValue));

		if (firstRanValue < secondRanValue) {
			if (gapValue == 1) {
				_cupAnimators [firstRanValue].SetBool (_isRightSmallDown, true);
				_cupAnimators [secondRanValue].SetBool (_isLeftSmallUp, true);
			} else {
				_cupAnimators [firstRanValue].SetBool (_isRightLargeDown, true);
				_cupAnimators [secondRanValue].SetBool (_isLeftLargeUp, true);
			}
		} else {
			if (gapValue == 1) {
				_cupAnimators [secondRanValue].SetBool (_isRightSmallDown, true);
				_cupAnimators [firstRanValue].SetBool (_isLeftSmallUp, true);
			} else {
				_cupAnimators [secondRanValue].SetBool (_isRightLargeDown, true);
				_cupAnimators [firstRanValue].SetBool (_isLeftLargeUp, true);
			}
		}

		SetSwapCupIndex (firstRanValue, secondRanValue);

		_curSwapCount++;
		if (_curSwapCount == _swapCount) {
			SetCupSwapComplete ();
		}
	}

	void SetSwapCupIndex(int firstIndex, int secondIndex)
	{
		if (_curButtonIndex == firstIndex) {
			_curButtonIndex = secondIndex;
		} else if (_curButtonIndex == secondIndex) {
			_curButtonIndex = firstIndex;
		}

//		Animator tempAni = _cupAnimators[firstIndex];
//
//		_cupAnimators [firstIndex] = _cupAnimators [secondIndex];
//		_cupAnimators [secondIndex] = tempAni;
//
//		AnimationTriggerEvent triggerEvent = _cupAnimators [firstIndex].gameObject.GetComponent<AnimationTriggerEvent> ();
//
//		triggerEvent.OnTriggerEvent = (x) => OnFinishCupAni (firstIndex, x);
//
//		triggerEvent = _cupAnimators [secondIndex].gameObject.GetComponent<AnimationTriggerEvent> (); 
//
//		triggerEvent.OnTriggerEvent = (x) => OnFinishCupAni (secondIndex, x);
	}

	void SetCupSwapComplete()
	{
		_curCupSwapStep = CupSwapStep.CupSwapComplete;
        _catAnimator.enabled = false;
	}

	void SuccessMatchCup()
	{
        SetSolveButton();
		_curCupSwapStep = CupSwapStep.CupMatchSuccessChoice;
		RefreshButtonPos ();
	}

    void FailDelayTime()
    {
        _curCupSwapStep = CupSwapStep.CupMatchFailDelayTime;
    }

	protected override void FailStage()
	{
		_curCupSwapStep = CupSwapStep.CupMatchFailChoice;
		base.FailStage ();
	}

	#endregion

	#region Coroutine Methods

	public IEnumerator OnCheckStartDelayTime()
	{
		while(_curDelayTime < _delayStartTime)
		{
			_curDelayTime += Time.deltaTime;
			if (_curDelayTime >= _delayStartTime) {
				for (int i = 0; i < _cupAnimators.Length; i++) {
					_cupAnimators [i].gameObject.SetActive (true);
					_cupAnimators [i].enabled = true;
				}
			}
			yield return null;
		}
	}

	public IEnumerator OnUpdateCupSwap()
	{
        while(_curCupSwapStep == CupSwapStep.CupSwap || _curCupSwapStep == CupSwapStep.CupSwapComplete
              || _curCupSwapStep == CupSwapStep.CupMatchFailDelayTime)
		{
            switch(_curCupSwapStep){
                case CupSwapStep.CupSwap:
                    if (_isSwapCup)
                    {
                        _curSwapDelayTime += Time.deltaTime;
                        if (_curSwapDelayTime >= _gapSwapDelayTime)
                        {
                            SetCupSwap();
                            _curSwapDelayTime = 0f;
                            _isSwapCup = false;
                        }
                    }
                    break;
                case CupSwapStep.CupMatchFailDelayTime:
                    _curFailDelayTime += Time.deltaTime;
                    if(_curFailDelayTime >= _failDelayTime){
                        FailStage();
                    }
                    break;
            }
			
			yield return null;
		}
	}

	#endregion

	#region CallBack Methods

	void OnFinishCupAni(int aniIndex, int aniType)
	{
		Debug.Log (string.Format ("OnFinishCupAni aniIndex : {0}, aniType : {1}, _cupImgObjs pos : {2}", 
			aniIndex, aniType, _cupImgObjs[aniIndex].transform.localPosition));

		if (aniType != (int)AniCupSwapTriggerType.CupStartDownAni) {
//			_cupAnimators [aniIndex].transform.localPosition = _cupImgObjs [aniIndex].transform.localPosition;
//			_cupImgObjs [aniIndex].transform.localPosition = Vector3.zero;
		}

		float gapXValue = 0f;

        switch ((AniCupSwapTriggerType)aniType)
        {
            case AniCupSwapTriggerType.CupStartDownAni:
                _swapAniCount++;
                if (_swapAniCount == 3)
                {
                    StartButton.SetActive(false);
                    StopButton.SetActive(false);
                    _curCupSwapStep = CupSwapStep.CupSwap;
                    _isSwapCup = true;
                    _swapAniCount = 0;
                    StartCoroutine(OnUpdateCupSwap());

                    _catAnimator.enabled = true;
                }
                break;
            case AniCupSwapTriggerType.CupLeftSmallUpAni:
                gapXValue = -215f;
                _cupAnimators[aniIndex].SetBool(_isLeftSmallUp, false);
                break;
            case AniCupSwapTriggerType.CupLeftLargeUpAni:
                gapXValue = -430f;
                _cupAnimators[aniIndex].SetBool(_isLeftLargeUp, false);
                break;
            case AniCupSwapTriggerType.CupRightSmallDownAni:
                gapXValue = 215f;
                _cupAnimators[aniIndex].SetBool(_isRightSmallDown, false);
                break;
            case AniCupSwapTriggerType.CupRightLargeDownAni:
                gapXValue = 430f;
                _cupAnimators[aniIndex].SetBool(_isRightLargeDown, false);
                break;
        }

        if (gapXValue != 0f) {
//			Vector3 cupAniPos = _cupAnimators [aniIndex].transform.localPosition;
//			_cupAnimators [aniIndex].transform.localPosition = new Vector3 (cupAniPos.x + gapXValue, cupAniPos.y, cupAniPos.z);
		}

		if (aniType != (int)AniCupSwapTriggerType.CupStartDownAni) {
			if (_curCupSwapStep == CupSwapStep.CupSwap) {
				_swapAniCount++;
				if (_swapAniCount == 2) {
					_isSwapCup = true;
					_swapAniCount = 0;
				}
			}
		}
	}

	void OnCupButton_1()
	{
		if (_curCupSwapStep != CupSwapStep.CupSwapComplete) {
			return;
		}
			
		Debug.Log (string.Format ("OnCupButton_1"));
		_cupImgObjs [0].SetActive (false);

		SetButtonInfo (0);

		if (_curButtonIndex == 0) {
			SuccessMatchCup ();
		} else {
            FailDelayTime();
		}
	}

	void OnCupButton_2()
	{
		if (_curCupSwapStep != CupSwapStep.CupSwapComplete) {
			return;
		}

		Debug.Log (string.Format ("OnCupButton_2"));
		_cupImgObjs [1].SetActive (false);

		SetButtonInfo (1);

		if (_curButtonIndex == 1) {
			SuccessMatchCup ();
		} else {
            FailDelayTime();
		}
	}

	void OnCupButton_3()
	{
		if (_curCupSwapStep != CupSwapStep.CupSwapComplete) {
			return;
		}

		Debug.Log (string.Format ("OnCupButton_3"));
		_cupImgObjs [2].SetActive (false);

		SetButtonInfo (2);

		if (_curButtonIndex == 2) {
			SuccessMatchCup ();
		} else {
            FailDelayTime();
		}
	}

	public override int CheckStopTimeButton(int touchID, float posX, float posY)
	{
		if (_curCupSwapStep != CupSwapStep.CupMatchSuccessChoice)
			return -1;

		return base.CheckStopTimeButton(touchID, posX, posY);
	}

	void SetButtonInfo(int clickButton)
	{
		if (clickButton != _curButtonIndex) {
			
			return;
		}
		Vector3 buttonCurPos = StartButton.transform.localPosition;
		if (_curButtonIndex == 0) {
			StartButton.transform.localPosition = new Vector3 (buttonCurPos.x - 215f, buttonCurPos.y, buttonCurPos.z);
		} else if (_curButtonIndex == 1) {

		} else if (_curButtonIndex == 2) {
			StartButton.transform.localPosition = new Vector3 (buttonCurPos.x + 215f, buttonCurPos.y, buttonCurPos.z);
		}
		StopButton.transform.localPosition = StartButton.transform.localPosition;

		StartButton.gameObject.SetActive (true);
//		_curCupSwapStep = CupSwapStep.CupMatchChoice;
	}

	#endregion
}
