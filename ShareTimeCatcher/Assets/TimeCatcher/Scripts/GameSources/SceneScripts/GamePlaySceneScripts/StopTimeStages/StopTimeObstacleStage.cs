﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StopTimeObstacleStage : StopTimeBaseStage
{
	#region Serialize Variables

	[SerializeField] GameObject[] _obstacleObjects;

	#endregion

	#region Variables

//	float[] _obstacleChangeTimes = new float[3];
//	float[] _curObstacleTimes = new float[3];

	bool[] _obstacleReverseState = new bool[3];

	#endregion

	#region MonoBenoBehaviour Methods

	protected override void Start ()
	{
//		_obstacleChangeTimes [0] = 1.2f;
//		_obstacleChangeTimes [1] = 1.3f;
//		_obstacleChangeTimes [2] = 1.3f;
//
//		for (int i = 0; i < _curObstacleTimes.Length; i++) {
//			_curObstacleTimes [i] = 0f;
//		}

		base.Start ();

		for (int i = 0; i < _obstacleReverseState.Length; i++) {
			_obstacleReverseState [i] = false;
		}
	}

	// Update is called once per frame
	void Update ()
	{
//		for (int i = 0; i < _curObstacleTimes.Length; i++) {
//			_curObstacleTimes [i] += Time.deltaTime;
//
//
//		}
	}

	#endregion

	#region Methods

	public void OnFinishedTweenObstacle_1()
	{
		Vector3 obstacleLocalScale = _obstacleObjects [0].transform.localScale;
		_obstacleObjects [0].transform.localScale = new Vector3 (obstacleLocalScale.x, obstacleLocalScale.y * -1f, obstacleLocalScale.z);

		UGUITweener objectTweener = _obstacleObjects [0].GetComponent<UGUITweener> ();
		if (_obstacleReverseState [0]) {
			objectTweener.PlayForward ();
			_obstacleReverseState [0] = false;
		} else {
			_obstacleReverseState [0] = true;
			objectTweener.PlayReverse ();
		}
	}

	public void OnFinishedTweenObstacle_2()
	{
		Vector3 obstacleLocalScale = _obstacleObjects [1].transform.localScale;
		_obstacleObjects [1].transform.localScale = new Vector3 (obstacleLocalScale.x, obstacleLocalScale.y * -1f, obstacleLocalScale.z); 

		UGUITweener objectTweener = _obstacleObjects [1].GetComponent<UGUITweener> ();
		if (_obstacleReverseState [1]) {
			objectTweener.PlayForward ();
			_obstacleReverseState [1] = false;
		} else {
			_obstacleReverseState [1] = true;
			objectTweener.PlayReverse ();
		}
	}

	public void OnFinishedTweenObstacle_3()
	{
		Vector3 obstacleLocalScale = _obstacleObjects [2].transform.localScale;
		_obstacleObjects [2].transform.localScale = new Vector3 (obstacleLocalScale.x, obstacleLocalScale.y * -1f, obstacleLocalScale.z); 

		UGUITweener objectTweener = _obstacleObjects [2].GetComponent<UGUITweener> ();
		if (_obstacleReverseState [2]) {
			objectTweener.PlayForward ();
			_obstacleReverseState [2] = false;
		} else {
			_obstacleReverseState [2] = true;
			objectTweener.PlayReverse ();
		}
	}

	#endregion
}
