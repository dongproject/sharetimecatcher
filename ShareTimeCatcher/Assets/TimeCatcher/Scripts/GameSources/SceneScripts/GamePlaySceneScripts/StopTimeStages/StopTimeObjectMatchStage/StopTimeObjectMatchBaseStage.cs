﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum StopTimeMatchStep
{
	None = 0,
	Matching,
	CheckValid,
	MatchSuccess,
	MatchFailed,
	MatchCompleted,
}

public class StopTimeCatchObjectInfo
{
	#region Variables

	int _touchID;
	StopTimeObjectInfo _objectInfo;

	#endregion

	#region Properties

	public int TouchID
	{
		get{ return _touchID; }
		set{ _touchID = value; }
	}

	public StopTimeObjectInfo ObjectInfo
	{
		get{ return _objectInfo; }
		set{ _objectInfo = value; }
	}

	#endregion
}

public class StopTimeObjectMatchBaseStage : StopTimeBaseStage 
{
    #region Serialize Variables

#pragma warning disable 649

    [SerializeField] protected int _matchIndex;

#pragma warning restore 649

    #endregion

    #region Variables

    protected StopTimeMatchStep _curMatchingStep = StopTimeMatchStep.None;

	protected Rect _moveAreaRect;

	protected float _successDelayTime = 0.1f;
	protected float _failedDelayTime = 1f;
	protected float _curStepTime = 0f;

	#endregion

	#region MonoBehaviour Methods

	protected override void Awake()
	{
		base.Awake ();

        if (CAResolutionCtl.Instance == null)
        {
            CAResolutionCtl.InitResolutionCtl();
            CAResolutionCtl.Instance.InitResolution(Screen.width, Screen.height, false);
        }

        float screenWidth = CAResolutionCtl.Instance.GetResolutionWidth ();
		float screenHeight = CAResolutionCtl.Instance.GetResolutionHeight ();

		_moveAreaRect = new Rect (-screenWidth * 0.5f, -screenHeight * 0.5f, screenWidth, screenHeight);
	}

	void Update()
	{
		switch (_curMatchingStep) {
		case StopTimeMatchStep.Matching:

			break;
		case StopTimeMatchStep.CheckValid:

			break;
		case StopTimeMatchStep.MatchSuccess:
			UpdateMatchSuccess ();
			break;
		case StopTimeMatchStep.MatchFailed:
			UpdateMatchFailed ();
			break;
		}
	}

	#endregion

	#region Methods

	protected virtual void SetCurMatchingStep(StopTimeMatchStep matchStep)
	{
		_curMatchingStep = matchStep;
		switch (_curMatchingStep) {
		case StopTimeMatchStep.Matching:
			SetMatchingStep ();
			break;
		case StopTimeMatchStep.CheckValid:
			SetCheckValidStep ();
			break;
		case StopTimeMatchStep.MatchSuccess:
			SetMatchSuccessStep ();
			break;
		case StopTimeMatchStep.MatchFailed:
			SetMatchFailedStep ();
			break;
		case StopTimeMatchStep.MatchCompleted:
			SetMatchCompletedStep ();
			break;
		}
	}

	void SetMatchingStep()
	{
		_curCatchObjectInfo.ObjectInfo.ObjTransform.localPosition = _curCatchObjectInfo.ObjectInfo.MatchingPos;

		SetCurMatchingStep (StopTimeMatchStep.CheckValid);
	}

	protected virtual void SetCheckValidStep()
	{
		if (_curCatchObjectInfo.ObjectInfo.ObjectIndex == _matchIndex) {
			SetCurMatchingStep (StopTimeMatchStep.MatchSuccess);
		} else {
			SetCurMatchingStep (StopTimeMatchStep.MatchFailed);
		}
	}

	protected virtual void SetMatchSuccessStep()
	{
	}

	protected virtual void SetMatchFailedStep()
	{
	}

	protected virtual void SetMatchCompletedStep()
	{
        SetSolveButton();
	}

	void UpdateMatchSuccess()
	{
		_curStepTime += Time.deltaTime;
		if (_curStepTime >= _successDelayTime) {
			_curStepTime = 0f;
			SetCurMatchingStep (StopTimeMatchStep.MatchCompleted);
		}
	}

	void UpdateMatchFailed()
	{
		_curStepTime += Time.deltaTime;
		if (_curStepTime >= _failedDelayTime) {
			_curStepTime = 0f;
            if (_curCatchObjectInfo.ObjectInfo != null) {
                _curCatchObjectInfo.ObjectInfo.ObjTransform.localPosition = _curCatchObjectInfo.ObjectInfo.ObjStartPos;
                _curCatchObjectInfo.ObjectInfo = null;
            }
			_curCatchObjectInfo.TouchID = -1;
		    
			SetMatchFailedData ();

			SetCurMatchingStep (StopTimeMatchStep.None);
		}
	}

	protected virtual void SetMatchFailedData()
	{
		
	}

	public override void OnTouchMoveValues(int touchID, float moveX, float moveY)
	{
		if (_curMatchingStep != StopTimeMatchStep.None)
			return;

		if (_curCatchObjectInfo.TouchID == touchID) {
			Vector3 objPos = _curCatchObjectInfo.ObjectInfo.ObjTransform.localPosition;
			float objHalfWidth = _curCatchObjectInfo.ObjectInfo.ObjWidth * 0.5f;
			float objHalfHeight = _curCatchObjectInfo.ObjectInfo.ObjHeight * 0.5f;
			float objPosX = Mathf.Clamp (objPos.x + moveX, 
				-CAResolutionCtl.Instance.GetResolutionHalfWidth() + objHalfWidth, 
				CAResolutionCtl.Instance.GetResolutionHalfWidth() - objHalfWidth);

			float objPosY = Mathf.Clamp (objPos.y + moveY, 
				-CAResolutionCtl.Instance.GetResolutionHalfHeight() + objHalfHeight, 
				CAResolutionCtl.Instance.GetResolutionHalfHeight() - objHalfHeight);

			//			Debug.Log (string.Format ("OnTouchMoveValues objPosX : {0}, objPosY : {1}", objPosX, objPosY));

			_curCatchObjectInfo.ObjectInfo.ObjTransform.localPosition = new Vector3 (objPosX, objPosY,
				_curCatchObjectInfo.ObjectInfo.ObjTransform.localPosition.z);
		}
	}

	public override int CheckStopTimeButton(int touchID, float posX, float posY)
	{
        if (_curMatchingStep != StopTimeMatchStep.MatchCompleted)
        {
            if(base.CheckStopTimeButton(touchID, posX, posY) == _stopButtonIndex){
                SetCurMatchingStep(StopTimeMatchStep.MatchFailed);
            }

            return -1;
        }

		return base.CheckStopTimeButton(touchID, posX, posY);
	}

	public override CACurGamePlayTouchData OnGetTouchData(int touchID, float posX, float posY)
	{
		return null;
	}

	public override void OnTouchPress(int touchID, float posX, float posY)
	{
		CheckMatchObject (touchID, posX, posY);
	}

	void CheckMatchObject(int touchID, float posX, float posY)
	{
		if (_curMatchingStep != StopTimeMatchStep.None)
			return;

		if (_curCatchObjectInfo.TouchID != -1) {
			if (_curCatchObjectInfo.TouchID != touchID)
				return;
		}

		float imgPosX = CAResolutionCtl.Instance.GetScreenToImagePosX(posX);
		float imgPosY = CAResolutionCtl.Instance.GetScreenToImagePosY(posY);

		for (int i = 0; i < _catchMoveObjectInfos.Count; i++) {
            if (!_catchMoveObjectInfos[i].IsEnable)
                continue;

            if ((_catchMoveObjectInfos [i].XMin <= imgPosX && _catchMoveObjectInfos [i].XMax >= imgPosX) &&
				(_catchMoveObjectInfos [i].YMin <= imgPosY && _catchMoveObjectInfos [i].YMax >= imgPosY)) {

				_curCatchObjectInfo.TouchID = touchID;
				_curCatchObjectInfo.ObjectInfo = _catchMoveObjectInfos [i];

				Debug.Log (string.Format ("CheckMatchObject Matched index : {0}", i));
				break;
			}
		}
	}

	public override void OnTouchRelease(int touchID, float posX, float posY)
	{
		if (_curCatchObjectInfo.TouchID == touchID) {
			if (!CheckObjectDisMatching (_curCatchObjectInfo.ObjectInfo)) {
				_curCatchObjectInfo.TouchID = -1;
				_curCatchObjectInfo.ObjectInfo = null;
			}
		}
	}

	protected virtual bool CheckObjectDisMatching(StopTimeObjectInfo matchObjectInfo)
	{

		return false;
	}

	#endregion
}
