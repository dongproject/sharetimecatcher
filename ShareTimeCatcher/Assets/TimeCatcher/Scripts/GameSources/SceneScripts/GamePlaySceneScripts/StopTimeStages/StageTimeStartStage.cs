﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum StartStageStep
{
    None,
    StartDelay,
    FirstNotice,
    FirstNoticeDelay,
    SecondNotice,
    Completed,
}

public class StageTimeStartStage : StopTimeBaseStage
{
    #region Serialize Variables

#pragma warning disable 649

    [SerializeField] GameObject _instructionArrow1;
    [SerializeField] GameObject _instructionArrow2;
    [SerializeField] GameObject _desText1;
    [SerializeField] GameObject _desText2;

#pragma warning restore 649

    #endregion

    #region Variables

    StartStageStep _stageStep = StartStageStep.None;

    #endregion

    #region Properties

    #endregion

    #region Methods

    public override void SetStartPlayData()
    {
        base.SetStartPlayData();

        SetStageStep(StartStageStep.StartDelay);

        StartCoroutine(OnUpdate());
    }

    void SetStageStep(StartStageStep step)
    {
        _stageStep = step;
        switch (step)
        {
            case StartStageStep.StartDelay:
                _eventTimer.SetGameTimerData(OnFinishStageStepTime, 1f);
                break;
            case StartStageStep.FirstNotice:
                _instructionArrow1.SetActive(true);
                _desText1.SetActive(true);
                SetStageStep(StartStageStep.FirstNoticeDelay);
                break;
            case StartStageStep.FirstNoticeDelay:
                _eventTimer.SetGameTimerData(OnFinishStageStepTime, 1f);
                break;
            case StartStageStep.SecondNotice:
                _instructionArrow2.SetActive(true);
                _desText2.SetActive(true);
                SetStageStep(StartStageStep.Completed);
                break;
        }
    }

    #endregion

    #region Call Back Methods

    void OnFinishStageStepTime(object objData)
    {
        switch(_stageStep){
            case StartStageStep.StartDelay:
                SetStageStep(StartStageStep.FirstNotice);
                break;
            case StartStageStep.FirstNoticeDelay:
                SetStageStep(StartStageStep.SecondNotice);
                break;
        }
    }

    #endregion

    #region Coroutine Methods

    public IEnumerator OnUpdate()
    {
        while (_stageStep != StartStageStep.Completed)
        {
            _eventTimer.UpdateGameTimer();
            yield return null;
        }
    }

    #endregion
}
