﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StopTimeMosaicNumStage : StopTimeBaseStage 
{
	#region Serialize Variables

	[SerializeField] float _leftMosaTime;
    [SerializeField] Transform _fatherTransform;

	#endregion

	#region Variables

	TimeCatcherSpriteManager _timeManager;

	#endregion

	#region Properties

	public TimeCatcherSpriteManager TimeManager
	{
		get{ return _timeManager; }
		set{ 
			_timeManager = value;
			_timeManager.OnChangeStageState = OnChangeTime;
			_timeManager.LeftStageChangeTime = _leftMosaTime;
		}
	}

	#endregion

	#region CallBack Methods

	void OnChangeTime()
	{
		_timeManager.ImgRealTimeNum.IsMosaSecondNum = true;
		_timeManager.ImgRealTimeNum.IsMosaMillisecondNum = true;
	}

    public void OnFinishTweenFather()
    {
        //Debug.Log(string.Format("OnFinishTweenFather"));
        _fatherTransform.localScale = new Vector3(_fatherTransform.localScale.x * -1f, 1f, 1f);
    }

	#endregion
}
