﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StopTimeCoverCupStage : StopTimeBaseStage
{
    #region Definitions

    public enum CoverCupID
    {
        Cup_1 = 0,
        Cup_2 = 1,
    }

    #endregion

    #region Serialize Variables

    //[SerializeField] GameObject[] _coverCupObjects;
    //[SerializeField] GameObject[] _cupDestinations;
    [SerializeField] UGUITweener[] _cupPosTweeners;

    #endregion

    #region MonoBehaviour Methods

    protected override void Awake()
    {
        base.Awake();

        //AddStopTimeObjectInfo(StageObjectType.MotionAni, (int)CoverCupID.Cup_1, _coverCupObjects[0].transform, 100f, 100f, new Vector3(222f, 697f, -20f));
        //AddStopTimeObjectInfo(StageObjectType.MotionAni, (int)CoverCupID.Cup_2, _coverCupObjects[1].transform, 100f, 100f, new Vector3(222f, 729f, -21f));
    }

    #endregion

    #region Methods

    public override void SetStartPlayData()
    {
        base.SetStartPlayData();

        StartCoroutine(OnUpdate());
    }

    void AddObjectMoveAni(int objIndex, Vector2 destValue, int motionID, float aniTime = 1f)
    {
        Transform moveTransform = null;
        for (int i = 0; i < _motionAniObjectInfos.Count; i++)
        {
            if (_motionAniObjectInfos[i].ObjectIndex == objIndex)
            {
                moveTransform = _motionAniObjectInfos[i].ObjTransform;
                if (moveTransform.localPosition.x == destValue.x && moveTransform.localPosition.y == destValue.y)
                {
                    Debug.Log(string.Format("AddObjectMoveAni Same Position!!!"));
                    return;
                }
                break;
            }
        }

        if (moveTransform == null)
        {
            Debug.Log(string.Format("AddObjectMoveAni Not Exist MoveAni Object!!!"));
            return;
        }

        Vector3 destPos = new Vector3(destValue.x,
            destValue.y, moveTransform.localPosition.z);
        _motionManager.AddObjTimeMovement(aniTime, moveTransform.localPosition, destPos, OnChangeMoveValue, motionID, OnCompleteMotionAni);

        _motionAniTransforms.Add(motionID, moveTransform);
    }

    public override int CheckStopTimeButton(int touchID, float posX, float posY)
    {
        for (int i = 0; i < _stopTimeButtons.Length; i++)
        {
            if (!_stopTimeButtons[i].IsPushedButton && _stopTimeButtons[i].CheckValidTouchArea(posX, posY))
            {
                if (_curTouchID == -1)
                {
                    _curTouchID = touchID;
                }
                return _stopTimeButtons[i].ButtonIndex;
            }
        }

        if (_curTouchID == touchID)
        {
            _curTouchID = -1;
        }

        return base.CheckStopTimeButton(touchID, posX, posY);
    }

    public override void TouchButtonObj(int buttonIndex)
    {
        base.TouchButtonObj(buttonIndex);

        if (!_stopTimeButtons[buttonIndex].IsPushedButton)
        {
            _stopTimeButtons[buttonIndex].StandButtonObj.SetActive(false);
            _stopTimeButtons[buttonIndex].PushedButtonObj.SetActive(true);
            _stopTimeButtons[buttonIndex].IsPushedButton = true;
            //AddObjectMoveAni(buttonIndex, _cupDestinations[buttonIndex].transform.localPosition, 
            //buttonIndex, 0.7f);
            _cupPosTweeners[buttonIndex].enabled = true;
        }
        //else
        //{
        //    _stopTimeButtons[buttonIndex].StandButtonObj.SetActive(true);
        //    _stopTimeButtons[buttonIndex].PushedButtonObj.SetActive(false);
        //    _stopTimeButtons[buttonIndex].IsPushedButton = false;
        //}
    }

    #endregion

    #region Coroutine Methods

    public IEnumerator OnUpdate()
    {
        while (true)
        {
            _motionManager.Update();
            yield return null;
        }
    }

    #endregion
}
