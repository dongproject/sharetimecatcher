﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StopTimeButtonSumColorStage : StopTimeBaseStage 
{
	public enum ColorButtonType
	{
		Red = 0,
		Yellow = 1,
		Blue = 2,
	}


    #region Serialize Variables

#pragma warning disable 649

    [SerializeField] GameObject _redSelect;
	[SerializeField] GameObject _redYellowSelect;
	[SerializeField] GameObject _redBlueSelect;
	[SerializeField] GameObject _redBlueYellowSelect;
	[SerializeField] GameObject _yellowSelect;
	[SerializeField] GameObject _blueSelect;

	[SerializeField] GameObject _whiteButtonObj;

	[SerializeField] GameObject _redLineObj;
	[SerializeField] GameObject _yellowLineObj;
	[SerializeField] GameObject _blueLineObj;

    [SerializeField] GameObject _realButtonObj;

#pragma warning restore 649

    #endregion

    #region Variables

    bool _isCompleteButtonSum = false;
	bool _isRedSelect = false;
	bool _isYellowSelect = false;
	bool _isBlueSelect = false;

	#endregion

	#region MonoBehaviour Methods

	protected override void Awake()
	{
		base.Awake ();
	}

	#endregion

	#region Methods

	public override int CheckStopTimeButton(int touchID, float posX, float posY)
	{
        //base.CheckStopTimeButton(touchID, posX, posY);

		if (!_isCompleteButtonSum) {
			for (int i = 0; i < _stopTimeButtons.Length; i++) {
				if (_stopTimeButtons [i].CheckValidTouchArea (posX, posY)) {
					//if (_curTouchID == -1) {
					//	_curTouchID = touchID;
					//}
					return _stopTimeButtons [i].ButtonIndex;
				}
			}

			//if (_curTouchID == touchID) {
			//	_curTouchID = -1;
			//}
		} else {
            for (int i = 0; i < _stopTimeButtons.Length; i++)
            {
                if (_stopTimeButtons[i].CheckValidTouchArea(posX, posY))
                {
                    //if (_curTouchID == -1)
                    //{
                    //    _curTouchID = touchID;
                    //}
                    return _stopTimeButtons[i].ButtonIndex;
                }
            }

            return base.CheckStopTimeButton(touchID, posX, posY);

   //         if (_curTouchID == touchID)
   //         {
   //             _curTouchID = -1;
   //         }

   //         if ((posX >= _buttonStartX && posX <= _buttonEndX) &&
			//	(posY >= _buttonStartY && posY <= _buttonEndY)) {
			//	if (_curTouchID == -1) {
			//		StartButton.SetActive (false);
			//		StopButton.SetActive (true);
			//		_curTouchID = touchID;
			//	}
			//	return StopButtonIndex;
			//} else {
			//	if (_curTouchID == touchID) {
			//		StartButton.SetActive (true);
			//		StopButton.SetActive (false);
			//		_curTouchID = -1;
			//	}
			//}
		}

		return -1;
	}

	public override void TouchButtonObj(int buttonIndex)
	{
		base.TouchButtonObj (buttonIndex);

		SetColorButtonType ((ColorButtonType)buttonIndex);

		if (!_stopTimeButtons [buttonIndex].IsPushedButton) {
			_stopTimeButtons [buttonIndex].StandButtonObj.SetActive (false);
			_stopTimeButtons [buttonIndex].PushedButtonObj.SetActive (true);
			_stopTimeButtons [buttonIndex].IsPushedButton = true;
		} else {
			_stopTimeButtons [buttonIndex].StandButtonObj.SetActive (true);
			_stopTimeButtons [buttonIndex].PushedButtonObj.SetActive (false);
			_stopTimeButtons [buttonIndex].IsPushedButton = false;
		}
	}

	void SetColorButtonType(ColorButtonType buttonType)
	{
		switch (buttonType) {
		case ColorButtonType.Red:
			if (_isRedSelect) {
				_isRedSelect = false;
				_redLineObj.SetActive (false);
			} else {
				_redLineObj.SetActive (true);
				_isRedSelect = true;
			}
			break;
		case ColorButtonType.Yellow:
			if (_isYellowSelect) {
				_isYellowSelect = false;
				_yellowLineObj.SetActive (false);
			} else {
				_isYellowSelect = true;
				_yellowLineObj.SetActive (true);
			}
			break;
		case ColorButtonType.Blue:
			if (_isBlueSelect) {
				_isBlueSelect = false;
				_blueLineObj.SetActive (false);
			} else {
				_isBlueSelect = true;
				_blueLineObj.SetActive (true);
			}
			break;
		}

		_redSelect.SetActive (false);
		_redYellowSelect.SetActive (false);
		_redBlueSelect.SetActive (false);
		_redBlueYellowSelect.SetActive (false);
		_yellowSelect.SetActive (false);
		_blueSelect.SetActive (false);

        _isCompleteButtonSum = false;
        _realButtonObj.SetActive(false);
        _whiteButtonObj.SetActive(false);
        if (!_isRedSelect && _isYellowSelect && _isBlueSelect) {
            //_whiteButtonObj.SetActive (false);
            _realButtonObj.SetActive(true);
            _isCompleteButtonSum = true;
            SetSolveButton();
            //_curTouchID = -1;
        } else if (_isRedSelect && !_isYellowSelect && !_isBlueSelect) {
			_redSelect.SetActive (true);
		} else if (_isRedSelect && _isYellowSelect && !_isBlueSelect) {
			_redYellowSelect.SetActive (true);
		} else if (_isRedSelect && _isYellowSelect && _isBlueSelect) {
			_redBlueYellowSelect.SetActive (true);
		} else if (_isRedSelect && !_isYellowSelect && _isBlueSelect) {
			_redBlueSelect.SetActive (true);
		} else if (!_isRedSelect && _isYellowSelect && !_isBlueSelect) {
			_yellowSelect.SetActive (true);
		} else if (!_isRedSelect && !_isYellowSelect && _isBlueSelect) {
			_blueSelect.SetActive (true);
        }
        else
        {
            _whiteButtonObj.SetActive(true);
        }
    }

	#endregion
}
