﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StopTimeObjectMatchCatStage : StopTimeObjectMatchBaseStage 
{
    #region Serialize Variables

#pragma warning disable 649

    [Header("Object Match Objects")]
	[SerializeField] tk2dSprite _catBodySprite;
    [SerializeField] GameObject _catNormalHeadObject;
	[SerializeField] GameObject _catRefuseHeadObject;

    [SerializeField] Transform _trapCanTrans; // 0
    [SerializeField] tk2dSprite _trapCanSprite; // 0

    [SerializeField] Transform _trapFlyTrans; // 1
    [SerializeField] tk2dSprite _trapFlySprite; // 1

    [SerializeField] Transform _trapWoolTrans; // 2
    [SerializeField] tk2dSprite _trapWoolSprite; // 2

    [SerializeField] Transform _trapMouseTrans; // 3
    [SerializeField] tk2dSprite _trapMouseSprite; // 3

    [SerializeField] Transform _trapSalmonTrans; // 4
    [SerializeField] tk2dSprite _trapSalmonSprite; // 4

	[SerializeField] tk2dSprite _feedBowlSprite;
	[SerializeField] Transform _bowlObjPosition;

	[SerializeField] GameObject _successCatObj;

#pragma warning restore 649

    #endregion

    #region Variables

    float _bowlStartX;
    float _bowlStartY;
    float _bowlEndX;
    float _bowlEndY;

    #endregion

    #region MonoBehaviour Methods

    protected override void Awake()
	{
		base.Awake ();

        SwapPosition();

        AddStopTimeObjectInfo(StageObjectType.CatchMove, 0, _trapCanTrans, _trapCanSprite, _trapCanTrans.localPosition);
        AddStopTimeObjectInfo(StageObjectType.CatchMove, 1, _trapFlyTrans, _trapFlySprite, _trapFlyTrans.localPosition);
        AddStopTimeObjectInfo(StageObjectType.CatchMove, 2, _trapWoolTrans, _trapWoolSprite, _trapWoolTrans.localPosition);
        AddStopTimeObjectInfo(StageObjectType.CatchMove, 3, _trapMouseTrans, _trapMouseSprite, _trapMouseTrans.localPosition);
        AddStopTimeObjectInfo(StageObjectType.CatchMove, 4, _trapSalmonTrans, _trapSalmonSprite, _trapSalmonTrans.localPosition);

        //AddStopTimeObjectInfo (StageObjectType.CatchMove, 0, _trapCanTrans, _trapCanSprite, new Vector3(-110f, -318f, -10f));
        //AddStopTimeObjectInfo (StageObjectType.CatchMove, 1, _trapFlyTrans, _trapFlySprite, new Vector3(-107f, -349f, -10f));
        //AddStopTimeObjectInfo (StageObjectType.CatchMove, 2, _trapWoolTrans, _trapWoolSprite, new Vector3(-87f, -314f, -10f));
        //AddStopTimeObjectInfo (StageObjectType.CatchMove, 3, _trapMouseTrans, _trapMouseSprite, new Vector3(-107f, -318f, -10f));
        //AddStopTimeObjectInfo (StageObjectType.CatchMove, 4, _trapSalmonTrans, _trapSalmonSprite, new Vector3(-101f, -315f, -10f));
    }

    #endregion

    #region Methods

    public override void SetStartPlayData()
    {
        base.SetStartPlayData();

        SetBowlTouchAreaPos();
    }

    void SwapPosition()
    {
        Vector3[] trapObjPos = new Vector3[4];

        trapObjPos[0] = _trapCanTrans.localPosition;
        trapObjPos[1] = _trapWoolTrans.localPosition;
        trapObjPos[2] = _trapMouseTrans.localPosition;
        trapObjPos[3] = _trapSalmonTrans.localPosition;

        for (int i = 0; i < 20;i++){
            int ranValue = Random.Range(0, 4);
            int secRanValue = 0;
            for (int j = 0; j < 100;j++){
                secRanValue = Random.Range(0, 4);
                if (secRanValue != ranValue)
                    break;
            }

            if(secRanValue == ranValue){
                if (ranValue == 3)
                    secRanValue = 0;
                else
                    secRanValue = ranValue + 1;
            }

            Vector3 tempPos = trapObjPos[ranValue];
            trapObjPos[ranValue] = trapObjPos[secRanValue];
            trapObjPos[secRanValue] = tempPos;
        }

        _trapCanTrans.localPosition = trapObjPos[0];
        _trapWoolTrans.localPosition = trapObjPos[1];
        _trapMouseTrans.localPosition = trapObjPos[2];
        _trapSalmonTrans.localPosition = trapObjPos[3];
    }

	protected override bool CheckObjectDisMatching(StopTimeObjectInfo matchObjectInfo)
	{
		float validDis = 85f; // 70f
		Vector3 bowlPosition = _bowlObjPosition.localPosition;

		float calcDis = CAMathCtl.Instance.GetPointDistance (matchObjectInfo.ObjTransform.localPosition.x, 
			matchObjectInfo.ObjTransform.localPosition.y, bowlPosition.x, bowlPosition.y);

		Debug.Log (string.Format ("CheckBowlMatching calcDis : {0}", calcDis));

		if (calcDis < validDis) {
			Debug.Log (string.Format ("CheckBowlMatching Matching Success!!!!"));
			SetCurMatchingStep (StopTimeMatchStep.Matching);

			return true;
		}

        float catValidDis = 150f;
        Vector3 catButtonPos = _catBodySprite.transform.localPosition;

        float calcCatDis = CAMathCtl.Instance.GetPointDistance(matchObjectInfo.ObjTransform.localPosition.x,
            matchObjectInfo.ObjTransform.localPosition.y, catButtonPos.x, catButtonPos.y);

        if(calcCatDis < catValidDis)
        {
            Debug.Log(string.Format("CheckObjectDisMatching Cat DisMatching!!!!"));
            //SetCurMatchingStep(StopTimeMatchStep.MatchFailed);
            SetCurMatchingStep(StopTimeMatchStep.Matching);
            return true;
        }

        return false;
	}

    void SetBowlTouchAreaPos()
    {
        BoxCollider bowlCollider = _feedBowlSprite.gameObject.GetComponent<BoxCollider>();         float objScaleX = _feedBowlSprite.transform.localScale.x;
        float objScaleY = _feedBowlSprite.transform.localScale.y;
        float halfWidth = (bowlCollider.bounds.size.x * objScaleX) * 0.5f;
        float halfHeight = (bowlCollider.bounds.size.y * objScaleY) * 0.5f;

        Vector3 bowlScreenPos = _stageCamera.WorldToScreenPoint(_feedBowlSprite.transform.position);

        _bowlStartX = bowlScreenPos.x - halfWidth;
        _bowlStartY = bowlScreenPos.y - halfHeight;         _bowlEndX = bowlScreenPos.x + halfWidth;
        _bowlEndY = bowlScreenPos.y + halfHeight;
    }

    public override int CheckStopTimeButton(int touchID, float posX, float posY)
    {
        if (_curMatchingStep != StopTimeMatchStep.MatchCompleted)
        {
            if (CheckTouchAreaBowl(posX, posY))
            {
                SetCurMatchingStep(StopTimeMatchStep.MatchFailed);
                return -1;
            }
        }

        return base.CheckStopTimeButton(touchID, posX, posY);
    }

    bool CheckTouchAreaBowl(float posX, float posY)
    {
        if ((posX >= _bowlStartX && posX <= _bowlEndX) &&           (posY >= _bowlStartY && posY <= _bowlEndY))
        {
            return true;
        }

        return false;
    }

    protected override void SetMatchSuccessStep()
	{
        _catNormalHeadObject.SetActive (false);
        _catRefuseHeadObject.SetActive (false);
		_catBodySprite.gameObject.SetActive (false);
		_successCatObj.gameObject.SetActive (true);
        _trapWoolTrans.gameObject.SetActive(false);
        _curStepTime = 0f;
	}

	protected override void SetMatchFailedStep()
	{
        _catNormalHeadObject.SetActive (false);
        _catRefuseHeadObject.SetActive (true);
		_curStepTime = 0f;
	}

	protected override void SetMatchCompletedStep()
	{
        base.SetMatchCompletedStep();
		_trapCanSprite.gameObject.SetActive(false);
		_trapFlySprite.gameObject.SetActive (false);
		_trapMouseSprite.gameObject.SetActive(false);
		_trapSalmonSprite.gameObject.SetActive (false);
        _trapWoolTrans.gameObject.SetActive(false);

    }

	protected override void SetMatchFailedData()
	{
        _catNormalHeadObject.SetActive (true);
        _catRefuseHeadObject.SetActive (false);
	}

	#endregion
}
