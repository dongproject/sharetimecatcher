﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct RemoveIceTouchInfo
{
	public RemoveIceTouchInfo(int id, float touchX, float touchY)
	{
		touchID = id;
		posX = touchX;
		posY = touchY;
	}

	public int touchID;
	public float posX;
	public float posY;
}

public class StopTimeRemoveIceStage : StopTimeBaseStage
{
    #region Definitions

    public enum AppearObj
    {
        DoliObj,
        CatObj,
        FatherObj,
    }


    #endregion

    #region Serialize Variables

#pragma warning disable 649

    [SerializeField] GameObject _dewAniObj;
    [SerializeField] GameObject[] _leftIceObjects;
    [SerializeField] GameObject[] _rightIceObjects;
    [SerializeField] GameObject[] _iceCharacterObjs;

    [SerializeField] UGUITweenPosition _doliTweenPos;
    [SerializeField] UGUITweenPosition _catTweenPos;
    [SerializeField] UGUITweenPosition _fatherTweenPos;

    [SerializeField] GameObject _iceStartObjPosY;
    [SerializeField] GameObject _iceEndObjPosY;

#pragma warning restore 649

    #endregion

    #region Variables

    AppearObj _curAppearObj;

    bool _isIced = true;
    RemoveIceTouchInfo _iceTouchInfo;
    RemoveIceTouchInfo _moveTouchInfo;
    float _moveGapValue = 50f;

    float _validStartPosY;
    float _validEndPosY;

    int _curLeftIceIndex;
    int _curRightIceIndex;

    float _iceAlpha = 1f;
    float _minusAlpha = 0.4f;
    float _iceRemoveCount = 2;
    float _curIceRemoveCount = 0;

    bool _isDoliLeftDir = true;
    bool _isCatRightDir = true;
    bool _isFatherLeftDir = true;

    List<tk2dSprite> _leftIceSprites = new List<tk2dSprite>();
    List<tk2dSprite> _rightIceSprites = new List<tk2dSprite>();

    bool _isLastIce = false;

    #endregion

    #region Properties

    #endregion

    #region MonoBehaviour

    protected override void Awake()
    {
        base.Awake();

        _leftIceSprites.Clear();

        for (int i = 0; i < _leftIceObjects.Length; i++)
        {
            tk2dSprite iceSprite = _leftIceObjects[i].GetComponent<tk2dSprite>();
            _leftIceSprites.Add(iceSprite);
        }

        _rightIceSprites.Clear();

        for (int i = 0; i < _rightIceObjects.Length; i++)
        {
            tk2dSprite iceSprite = _rightIceObjects[i].GetComponent<tk2dSprite>();
            _rightIceSprites.Add(iceSprite);
        }
    }

    #endregion

    #region Methods

    public override void InitStopTimeStageData()
    {
        base.InitStopTimeStageData();
        _iceTouchInfo = new RemoveIceTouchInfo(-1, 0f, 0f);
        _moveTouchInfo = new RemoveIceTouchInfo(-1, 0f, 0f);

        _isIced = true;
        _curLeftIceIndex = 0;
        _curRightIceIndex = 0;

        InitValidTouchArea();
    }

    public override void SetStartPlayData()
    {
        base.SetStartPlayData();

        StartCoroutine(OnUpdate());
        SetAppearObj(AppearObj.DoliObj);
    }

    void SetAppearObj(AppearObj appearObj)
    {
        _curAppearObj = appearObj;
        switch(_curAppearObj){
            case AppearObj.DoliObj:
                if (_isDoliLeftDir) {
                    _doliTweenPos.transform.localScale = new Vector3(1f, 1f, 1f);
                    //_doliTweenPos.ResetToBeginning();
                    _doliTweenPos.PlayForward();
                    _doliTweenPos.enabled = true;
                    _isDoliLeftDir = false;
                } else {
                    _doliTweenPos.transform.localScale = new Vector3(-1f, 1f, 1f);
                    _doliTweenPos.PlayReverse();
                    _doliTweenPos.enabled = true;
                    _isDoliLeftDir = true;
                }
                break;
            case AppearObj.CatObj:
                if (_isCatRightDir) {
                    _catTweenPos.transform.localScale = new Vector3(-1f, 1f, 1f);
                    //_catTweenPos.ResetToBeginning();
                    _catTweenPos.PlayForward();
                    _catTweenPos.enabled = true;
                    _isCatRightDir = false;
                } else {
                    _catTweenPos.transform.localScale = new Vector3(1f, 1f, 1f);
                    _catTweenPos.PlayReverse();
                    _catTweenPos.enabled = true;
                    _isCatRightDir = true;
                }
                break;
            case AppearObj.FatherObj:
                if (_isFatherLeftDir) {
                    _fatherTweenPos.transform.localScale = new Vector3(1f, 1f, 1f);
                    //_fatherTweenPos.ResetToBeginning();
                    _fatherTweenPos.PlayForward();
                    _fatherTweenPos.enabled = true;
                    _isFatherLeftDir = false;
                } else {
                    _fatherTweenPos.transform.localScale = new Vector3(-1f, 1f, 1f);
                    _fatherTweenPos.PlayReverse();
                    _fatherTweenPos.enabled = true;
                    _isFatherLeftDir = true;
                }
                break;
        }
    }

    public override void OnTouchPress(int touchID, float posX, float posY)
    {
        if (!_isIced)
            return;

        if (_iceTouchInfo.touchID == -1 && CheckValidTouchArea(posX, posY))
        {
            _iceTouchInfo = new RemoveIceTouchInfo(touchID, posX, posY);
            _moveTouchInfo = new RemoveIceTouchInfo(touchID, posX, posY);
        }
    }

    public override void OnTouchRelease(int touchID, float posX, float posY)
    {
        if (_iceTouchInfo.touchID == touchID)
        {
            _iceTouchInfo = new RemoveIceTouchInfo(-1, 0f, 0f);
        }

        if (_moveTouchInfo.touchID == touchID)
        {
            _moveTouchInfo = new RemoveIceTouchInfo(-1, 0f, 0f);
        }
    }

    public override void OnTouchDrag(int touchID, float posX, float posY)
    {
        if (!_isIced)
            return;

        if (_iceTouchInfo.touchID == -1 && CheckValidTouchArea(posX, posY))
        {
            _iceTouchInfo = new RemoveIceTouchInfo(touchID, posX, posY);
            _moveTouchInfo = new RemoveIceTouchInfo(touchID, posX, posY);
        }
    }

    public override void OnTouchMoveValues(int touchID, float moveX, float moveY)
    {
        if (!_isIced)
            return;

        if (_moveTouchInfo.touchID == touchID)
        {
            _moveTouchInfo.posX += moveX;
            _moveTouchInfo.posY += moveY;

            float gapX = Mathf.Abs(_moveTouchInfo.posX - _iceTouchInfo.posX);
            if (gapX >= _moveGapValue)
            {
                RemoveIce(_moveTouchInfo.posX, _moveTouchInfo.posY);
            }
            else
            {
                float gapY = Mathf.Abs(_moveTouchInfo.posY - _iceTouchInfo.posY);
                if (gapY >= _moveGapValue)
                {
                    RemoveIce(_moveTouchInfo.posX, _moveTouchInfo.posY);
                }
            }
        }
    }

    void RemoveIce(float posX, float posY)
    {
        _curIceRemoveCount++;
        if(_curIceRemoveCount > _iceRemoveCount){
            _curIceRemoveCount = 0;
        } else {
            return;
        }

        _iceTouchInfo.posX = posX;
        _iceTouchInfo.posY = posY;
        float imgPosX = CAResolutionCtl.Instance.GetScreenToImagePosX(posX);
        float imgPosY = CAResolutionCtl.Instance.GetScreenToImagePosY(posY);

        Debug.Log(string.Format("RemoveIce posX : {0}, posY : {1}, imgPosX : {2}, imgPosY : {3}",
            posX, posY, imgPosX, imgPosY));

        GameObject dewAniObject = Instantiate<GameObject>(_dewAniObj);
        dewAniObject.transform.SetParent(this.transform);
        dewAniObject.transform.localScale = Vector3.one;
        dewAniObject.transform.localPosition = new Vector3(imgPosX, imgPosY, -20f);
        dewAniObject.SetActive(true);
        _eventTimer.SetGameTimerData(OnDestroyDewAniObj, 2f, dewAniObject);

        if (_iceAlpha >= 1f)
        {
            _iceAlpha -= _minusAlpha;
        }

        bool leftAllRemoveIce = false;
        bool rightAllRemoveIce = false;

        if (_curLeftIceIndex < _leftIceSprites.Count)
        {
            _leftIceSprites[_curLeftIceIndex].color = new Color(_leftIceSprites[_curLeftIceIndex].color.r,
                                                                _leftIceSprites[_curLeftIceIndex].color.g,
                                                                _leftIceSprites[_curLeftIceIndex].color.b, _iceAlpha);
            _curLeftIceIndex++;
        }
        else
        {
            leftAllRemoveIce = true;
        }

        if (_curRightIceIndex < _rightIceSprites.Count)
        {
            _rightIceSprites[_curRightIceIndex].color = new Color(_rightIceSprites[_curRightIceIndex].color.r,
                                                                _rightIceSprites[_curRightIceIndex].color.g,
                                                                _rightIceSprites[_curRightIceIndex].color.b, _iceAlpha);
            _curRightIceIndex++;
        }
        else
        {
            rightAllRemoveIce = true;
        }

        if (leftAllRemoveIce && rightAllRemoveIce)
        {
            //Debug.Log(string.Format("RemoveIce _iceAlpha : {0}", _iceAlpha));
            if (_iceAlpha > 0f)
            {
                _curLeftIceIndex = 0;
                _curRightIceIndex = 0;
                _iceAlpha -= _minusAlpha;
                if (_iceAlpha <= 0.01f)
                {
                    _iceAlpha = 0f;
                    _isLastIce = true;
                }
            }
            else
            {
                CompleteStage();
            }
        }
    }

    void CompleteStage()
    {
        SetSolveButton();
        _isIced = false;
        for (int i = 0; i < _iceCharacterObjs.Length; i++)
        {
            _iceCharacterObjs[i].SetActive(false);
        }
    }

    public override int CheckStopTimeButton(int touchID, float posX, float posY)
    {
        if (_isIced)
            return -1;

        return base.CheckStopTimeButton(touchID, posX, posY);
    }

    void InitValidTouchArea()
    {
        //float gapY = 200f;
        //_validStartPosY = StartButton.transform.localPosition.y - 200f;
        //_validEndPosY = StartButton.transform.localPosition.y + 200f;

        //_validStartPosY = CAResolutionCtl.Instance.GetImageToScreenPosY(_validStartPosY);
        //_validEndPosY = CAResolutionCtl.Instance.GetImageToScreenPosY(_validEndPosY);

        float imgStartPosY = _iceStartObjPosY.transform.localPosition.y;
        float imgEndPosY = _iceEndObjPosY.transform.localPosition.y;

        _validStartPosY = CAResolutionCtl.Instance.GetImageToScreenPosY(imgStartPosY);
        _validEndPosY = CAResolutionCtl.Instance.GetImageToScreenPosY(imgEndPosY);
    }

    bool CheckValidTouchArea(float posX, float posY)
    {
        if (posY >= _validStartPosY && posY <= _validEndPosY)
            return true;

        return false;
    }

    #endregion

    #region Coroutine Methods

    public IEnumerator OnUpdate()
    {
        while (_isIced)
        {
            _eventTimer.UpdateGameTimer();
            yield return null;
        }
    }

    #endregion

    #region CallBack Methods

    void OnDestroyDewAniObj(object objData)
    {
        GameObject dewObject = objData as GameObject;
        if(dewObject != null){
            Destroy(dewObject);
        }
    }

    public void OnFinishDoliTweenPos()
    {
        if(!_isLastIce)
            SetAppearObj(AppearObj.CatObj);
    }

    public void OnFinishCatTweenPos()
    {
        if (!_isLastIce)
            SetAppearObj(AppearObj.FatherObj);
    }

    public void OnFinishFatherTweenPos()
    {
        if (!_isLastIce)
            SetAppearObj(AppearObj.DoliObj);
    }

    #endregion
}
