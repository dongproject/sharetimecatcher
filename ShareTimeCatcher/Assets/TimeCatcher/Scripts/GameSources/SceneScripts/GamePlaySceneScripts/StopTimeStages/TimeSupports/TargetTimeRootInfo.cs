﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetTimeRootInfo : MonoBehaviour 
{
	#region Serialize Variables

	[SerializeField] StopTimeViewObject _targetTimeNumObject;
    [SerializeField] tk2dBaseSprite[] _timeObjSprites;

	#endregion

	#region Properties

	public StopTimeViewObject TargetTimeNumObject
	{
		get{ return _targetTimeNumObject; }
	}

    public tk2dBaseSprite[] TimeObjSprites
    {
        get { return _timeObjSprites; }
    }

    #endregion
}
