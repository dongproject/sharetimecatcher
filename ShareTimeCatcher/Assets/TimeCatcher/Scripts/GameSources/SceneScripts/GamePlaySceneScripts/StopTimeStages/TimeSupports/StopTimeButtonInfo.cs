﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StopTimeButtonInfo : MonoBehaviour 
{
	#region Serialize Variables

	[SerializeField] int _buttonIndex;
	[SerializeField] GameObject _standButtonObj;
	[SerializeField] GameObject _pushedButtonObj;

	#endregion

	#region Variables

	protected float _buttonStartX;
	protected float _buttonStartY;
	protected float _buttonEndX;
	protected float _buttonEndY;

	protected bool _isPushedButton = false;

	#endregion

	#region Properties

	public int ButtonIndex
	{
		get{ return _buttonIndex; }
	}

	public GameObject StandButtonObj
	{
		get{ return _standButtonObj; }
	}

	public GameObject PushedButtonObj
	{
		get{ return _pushedButtonObj; }
	}

	public float ButtonStartX
	{
		get{ return _buttonStartX; }
	}

	public float ButtonStartY
	{
		get{ return _buttonStartY; }
	}

	public float ButtonEndX
	{
		get{ return _buttonEndX; }
	}

	public float ButtonEndY
	{
		get{ return _buttonEndY; }
	}

	public bool IsPushedButton
	{
		get{ return _isPushedButton; }
		set{ _isPushedButton = value; }
	}

	#endregion

	#region Methods

	public void RefreshButtonPos(Camera stageCam)
	{
		BoxCollider buttonCollider = _standButtonObj.GetComponent<BoxCollider> ();
		float objScaleX = this.transform.localScale.x;
		float objScaleY = this.transform.localScale.y;

		objScaleX *= _standButtonObj.transform.localScale.x;
		objScaleY *= _standButtonObj.transform.localScale.y;
		float halfWidth = (buttonCollider.bounds.size.x * objScaleX) * 0.5f;
		float halfHeight = (buttonCollider.bounds.size.y * objScaleY) * 0.5f;

		Vector3 buttonScreenPos = stageCam.WorldToScreenPoint (_standButtonObj.transform.position);

		_buttonStartX = buttonScreenPos.x - halfWidth;
		_buttonStartY = buttonScreenPos.y - halfHeight;
		_buttonEndX = buttonScreenPos.x + halfWidth;
		_buttonEndY = buttonScreenPos.y + halfHeight;

		Debug.Log (string.Format ("_buttonStartX : {0}, _buttonStartY : {1}, _buttonEndX : {2}, _buttonEndY : {3}", 
			_buttonStartX, _buttonStartY, _buttonEndX, _buttonEndY));
	}

	public bool CheckValidTouchArea(float posX, float posY)
	{
		if ((posX >= _buttonStartX && posX <= _buttonEndX) &&
		   (posY >= _buttonStartY && posY <= _buttonEndY))
			return true;

		return false;
	}

	#endregion
}
