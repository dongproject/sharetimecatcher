﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StopTimeRailroadStage : StopTimeBaseStage 
{
	#region Definitions

	public enum StageButtonID
	{
		LeverImgID		= 1,
		RailImgID		= 2,
		MoveButtonID	= 3,
	}

	public enum MoveButtonAniStep
	{
		ButtonAni_1         = 0,
		ButtonAni_2         = 1,
		ButtonAni_3         = 2,
		ButtonAni_4         = 3,
		ButtonAni_5         = 4,
		ButtonAni_6         = 5,
		ButtonAni_7         = 6,
		ButtonAni_8         = 7,
		ButtonAni_9         = 8,
        ButtonReverseAni_1  = 9,
        ButtonReverseAni_2  = 10,
        ButtonReverseAni_3  = 11,
        ButtonReverseAni_4  = 12,
        ButtonReverseAni_5  = 13,
    }

	public enum LeverPosState
	{
		LeverLeft = 0,
		LeverRight = 1,
	}

	public enum RailPivotState
	{
		LeftUpRightDown = 0,
		LeftDownRightUp = 1,
	}

	#endregion

	#region Serialize Variables

	[SerializeField] UGUITweener[] _railPivotTweens;
	[SerializeField] UGUITweener _leverTween;
	[SerializeField] Transform[] _movePointTrans;
	[SerializeField] Transform _moveButtonTrans;

	#if UNITY_EDITOR
	[SerializeField] RFButtonManager _buttonManager = null;
	#endif

	#endregion

	#region Variables

	protected bool _isCompleteStage = false;

	int _buttonAniStep;

	int _leverTweenState = (int)LeverPosState.LeverLeft;

	RailPivotState _railPivotState = RailPivotState.LeftDownRightUp;

	#endregion

	#region MonoBehaviour Methods

	protected override void Awake()
	{
		base.Awake ();

		AddStopTimeObjectInfo (StageObjectType.MotionAni, (int)StageButtonID.MoveButtonID, _moveButtonTrans, 100f, 100f, new Vector3(-501f, -85f, 0f));
	}

	protected override void Start()
	{
		base.Start ();

		#if UNITY_EDITOR
		if(_buttonManager.ButtonCamera != null){
			_buttonManager.InitRFButtonManager();
		}
		#endif

		_leverTween.AddOnFinished (() => OnFinishLeverTween());
	}

	#endregion

	#region Methods

	public override void SetStartPlayData()
	{
		base.SetStartPlayData ();

		SetButtonMoveAni (MoveButtonAniStep.ButtonAni_1);

		StartCoroutine (OnUpdate ());
	}

	void SetButtonMoveAni(MoveButtonAniStep aniStep)
	{
		_buttonAniStep = (int)aniStep;
        switch (aniStep)
        {
            case MoveButtonAniStep.ButtonAni_1:
                AddObjectMoveAni((int)StageButtonID.MoveButtonID, _movePointTrans[1].localPosition, _buttonAniStep, 2f);
                break;
            case MoveButtonAniStep.ButtonAni_2:
                AddObjectMoveAni((int)StageButtonID.MoveButtonID, _movePointTrans[2].localPosition, _buttonAniStep, 0.7f);
                break;
            case MoveButtonAniStep.ButtonAni_3:
                AddObjectMoveAni((int)StageButtonID.MoveButtonID, _movePointTrans[3].localPosition, _buttonAniStep, 1.5f);
                break;
            case MoveButtonAniStep.ButtonAni_4:
                AddObjectMoveAni((int)StageButtonID.MoveButtonID, _movePointTrans[4].localPosition, _buttonAniStep, 0.5f);
                break;
            case MoveButtonAniStep.ButtonAni_5:
                AddObjectMoveAni((int)StageButtonID.MoveButtonID, _movePointTrans[5].localPosition, _buttonAniStep, 1.5f);
                break;
            case MoveButtonAniStep.ButtonAni_6:
                AddObjectMoveAni((int)StageButtonID.MoveButtonID, _movePointTrans[6].localPosition, _buttonAniStep, 0.7f);
                break;
            case MoveButtonAniStep.ButtonAni_7:
                AddObjectMoveAni((int)StageButtonID.MoveButtonID, _movePointTrans[7].localPosition, _buttonAniStep, 1.5f);
                break;
            case MoveButtonAniStep.ButtonAni_8:
                AddObjectMoveAni((int)StageButtonID.MoveButtonID, _movePointTrans[8].localPosition, _buttonAniStep, 0.5f);
                break;
            case MoveButtonAniStep.ButtonAni_9:
                AddObjectMoveAni((int)StageButtonID.MoveButtonID, _movePointTrans[9].localPosition, _buttonAniStep, 1f);
                break;
            case MoveButtonAniStep.ButtonReverseAni_1:
                AddObjectMoveAni((int)StageButtonID.MoveButtonID, _movePointTrans[0].localPosition, _buttonAniStep, 1f);
                break;
            case MoveButtonAniStep.ButtonReverseAni_2:
                AddObjectMoveAni((int)StageButtonID.MoveButtonID, _movePointTrans[1].localPosition, _buttonAniStep, 1f);
                break;
            case MoveButtonAniStep.ButtonReverseAni_3:
                AddObjectMoveAni((int)StageButtonID.MoveButtonID, _movePointTrans[2].localPosition, _buttonAniStep, 1f);
                break;
            case MoveButtonAniStep.ButtonReverseAni_4:
                AddObjectMoveAni((int)StageButtonID.MoveButtonID, _movePointTrans[3].localPosition, _buttonAniStep, 1f);
                break;
            case MoveButtonAniStep.ButtonReverseAni_5:
                AddObjectMoveAni((int)StageButtonID.MoveButtonID, _movePointTrans[4].localPosition, _buttonAniStep, 1f);
                break;
        }
    }

	public override int CheckStopTimeButton(int touchID, float posX, float posY)
	{
		if (!_isCompleteStage)
			return -1;

		return base.CheckStopTimeButton(touchID, posX, posY);
	}

	void AddObjectMoveAni(int objIndex, Vector2 destValue, int motionID, float aniTime = 1f)
	{
		Transform moveTransform = null;
		for (int i = 0; i < _motionAniObjectInfos.Count; i++) {
			if (_motionAniObjectInfos [i].ObjectIndex == objIndex) {
				moveTransform = _motionAniObjectInfos [i].ObjTransform;
				if (moveTransform.localPosition.x == destValue.x && moveTransform.localPosition.y == destValue.y) {
					Debug.Log (string.Format ("AddObjectMoveAni Same Position!!!"));
					return;
				}
				break;
			}
		}

		if (moveTransform == null) {
			Debug.Log (string.Format ("AddObjectMoveAni Not Exist MoveAni Object!!!"));
			return;
		}

		Vector3 destPos = new Vector3 (destValue.x, 
			destValue.y, moveTransform.localPosition.z);
		_motionManager.AddObjTimeMovement (aniTime, moveTransform.localPosition, destPos, OnChangeMoveValue, motionID, OnCompleteMotionAni); 

		_motionAniTransforms.Add (motionID, moveTransform);
	}

	void SetLeverPos(LeverPosState leverPos)
	{
		_leverTweenState = (int)leverPos;
		switch (leverPos) {
		case LeverPosState.LeverLeft:
			_leverTween.PlayReverse ();
//			SetRailPivotTween (RailPivotState.LeftDownRightUp);
			break;
		case LeverPosState.LeverRight:
			_leverTween.PlayForward ();
//			SetRailPivotTween (RailPivotState.LeftUpRightDown);
			break;
		}

		_leverTween.enabled = true;
	}

	void SetRailPivotTween(RailPivotState pivotState)
	{
		_railPivotState = pivotState;

		switch (_railPivotState) {
		case RailPivotState.LeftDownRightUp:
			for (int i = 0; i < _railPivotTweens.Length; i++) {
				_railPivotTweens [i].PlayReverse ();
				_railPivotTweens [i].enabled = true;
			}
			break;
		case RailPivotState.LeftUpRightDown:
			for (int i = 0; i < _railPivotTweens.Length; i++) {
				_railPivotTweens [i].PlayForward ();
				_railPivotTweens [i].enabled = true;
			}
			break;
		}
	}

	#endregion

	#region Coroutine Methods

	public IEnumerator OnUpdate()
	{
		while(true)
		{
			_motionManager.Update ();
			yield return null;
		}
	}

	#endregion

	#region CallBack Methods

	public override void OnButtonTouched(int buttonID)
	{
		if (buttonID == (int)StageButtonID.LeverImgID) {
            SoundManager.Instance.PlayEffectFX(SoundDefinitions.effectSound_16);
            if (_leverTweenState == (int)LeverPosState.LeverLeft) {
				SetLeverPos (LeverPosState.LeverRight);
			} else {
				SetLeverPos (LeverPosState.LeverLeft);
			}
		}
	}

	protected override void OnChangeMoveValue(int motionID, ObjectMotionManager.MotionType motionType, Vector2 moveValue)
	{
		base.OnChangeMoveValue (motionID, motionType, moveValue);
	}

	protected override void OnCompleteMotionAni(int motionID, ObjectMotionAni objMotionAni)
	{
		base.OnCompleteMotionAni (motionID, objMotionAni);

        switch ((MoveButtonAniStep)motionID)
        {
            case MoveButtonAniStep.ButtonAni_1:
                if (_railPivotState != RailPivotState.LeftUpRightDown)
                {
                    //FailStage ();
                    SetButtonMoveAni(MoveButtonAniStep.ButtonReverseAni_1);
                }
                else
                {
                    SetButtonMoveAni(MoveButtonAniStep.ButtonAni_2);
                }
                break;
            case MoveButtonAniStep.ButtonAni_2:
                SetButtonMoveAni(MoveButtonAniStep.ButtonAni_3);
                break;
            case MoveButtonAniStep.ButtonAni_3:
                SetButtonMoveAni(MoveButtonAniStep.ButtonAni_4);
                break;
            case MoveButtonAniStep.ButtonAni_4:
                SetButtonMoveAni(MoveButtonAniStep.ButtonAni_5);
                break;
            case MoveButtonAniStep.ButtonAni_5:
                if (_railPivotState != RailPivotState.LeftDownRightUp)
                {
                    //FailStage();
                    SetButtonMoveAni(MoveButtonAniStep.ButtonReverseAni_5);
                }
                else
                {
                    SetButtonMoveAni(MoveButtonAniStep.ButtonAni_6);
                }
                break;
            case MoveButtonAniStep.ButtonAni_6:
                SetButtonMoveAni(MoveButtonAniStep.ButtonAni_7);
                break;
            case MoveButtonAniStep.ButtonAni_7:
                SetButtonMoveAni(MoveButtonAniStep.ButtonAni_8);
                break;
            case MoveButtonAniStep.ButtonAni_8:
                SetButtonMoveAni(MoveButtonAniStep.ButtonAni_9);
                break;
            case MoveButtonAniStep.ButtonAni_9:
                SetSolveButton();
                _isCompleteStage = true;
                RefreshButtonPos();
                break;
            case MoveButtonAniStep.ButtonReverseAni_1:
                SetButtonMoveAni(MoveButtonAniStep.ButtonAni_1);
                break;
            case MoveButtonAniStep.ButtonReverseAni_2:
                SetButtonMoveAni(MoveButtonAniStep.ButtonReverseAni_1);
                break;
            case MoveButtonAniStep.ButtonReverseAni_3:
                if (_railPivotState != RailPivotState.LeftUpRightDown)
                {
                    SetButtonMoveAni(MoveButtonAniStep.ButtonAni_3);
                }
                else
                {
                    SetButtonMoveAni(MoveButtonAniStep.ButtonReverseAni_2);
                }
                break;
            case MoveButtonAniStep.ButtonReverseAni_4:
                SetButtonMoveAni(MoveButtonAniStep.ButtonReverseAni_3);
                break;
            case MoveButtonAniStep.ButtonReverseAni_5:
                SetButtonMoveAni(MoveButtonAniStep.ButtonReverseAni_4);
                break;
        }
    }

	void OnFinishLeverTween()
	{
		switch ((LeverPosState)_leverTweenState) {
		case LeverPosState.LeverLeft:
			SetRailPivotTween (RailPivotState.LeftDownRightUp);
			break;
		case LeverPosState.LeverRight:
			SetRailPivotTween (RailPivotState.LeftUpRightDown);
			break;
		}
	}

	#endregion
}
