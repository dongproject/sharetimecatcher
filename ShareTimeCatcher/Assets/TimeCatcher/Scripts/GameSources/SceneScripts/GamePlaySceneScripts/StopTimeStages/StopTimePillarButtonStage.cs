﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StopTimePillarButtonStage : StopTimeBaseStage
{
    #region Definitions

    public enum ButtonAniID
    {
        MoveButtonID = 0,
    }

    public enum MoveButtonAniType
    {
        None = 0,
        MoveAniUp = 1,
        MoveAniDown = 2,
    }

    #endregion

    #region Serialize Variables

#pragma warning disable 649

    [SerializeField] Transform _moveButtonTrans;
    [SerializeField] ShaderMagnifyRenderer _maskRenderer;
    [SerializeField] Transform _startButtonPosTrans;
    [SerializeField] Transform _destButtonPosTrans;
    [SerializeField] Transform _validButtonPosTrans;
    [SerializeField] UGUITweenPosition _upTweener;
    [SerializeField] UGUITweenPosition _downTweener;

#pragma warning restore 649

    #endregion

    #region Variables

    MoveButtonAniType _curMoveAniType = MoveButtonAniType.None;

    float _buttonTweenBottom = -345f;
    float _buttonTweenUp = 86f;

    float _buttonValidTouchStartY = -280f;// -264f;
    float _buttonValidTouchEndY = -210f;//-221f;

    #endregion

    #region Properties

    #endregion

    #region MonoBehaviour Methods

    protected override void Awake()
    {
        base.Awake();

        //_stageButtonType = StopTimeButtonType.PushImmediate;
    }

    // Use this for initialization
    protected override void Start()
    {
        base.Start();

        AddStopTimeObjectInfo(StageObjectType.MotionAni, (int)ButtonAniID.MoveButtonID, _moveButtonTrans, 100f, 100f, Vector3.zero);

        SetMagnifyScopeData();
    }

    #endregion

    #region Methods

    public override void SetStartPlayData()
    {
        base.SetStartPlayData();

        StartCoroutine(OnUpdate());
    }

    public void SetMagnifyScopeData()
    {
        _maskRenderer.ScopeRatio = 1f;
        _maskRenderer.SetupScoping();
        _maskRenderer.SetScopePos(150f, 0f);
    }

    public override int CheckStopTimeButton(int touchID, float posX, float posY)
    {
        for (int i = 0; i < _stopTimeButtons.Length; i++)
        {
            if (_stopTimeButtons[i].CheckValidTouchArea(posX, posY))
            {
                //if (_curTouchID == -1)
                //{
                //    _curTouchID = touchID;
                //}

                if (_curTouchID == -1)
                {
                    _stopTimeButtons[0].StandButtonObj.SetActive(false);
                    _stopTimeButtons[0].PushedButtonObj.SetActive(true);
                    _stopTimeButtons[0].IsPushedButton = true;
                    _curTouchID = touchID;
                }
                else if (_curTouchID == touchID && _stopTimeButtons[0].IsPushedButton)
                {
                    _stopTimeButtons[0].StandButtonObj.SetActive(true);
                    _stopTimeButtons[0].PushedButtonObj.SetActive(false);
                    _stopTimeButtons[0].IsPushedButton = false;
                    _curTouchID = -1;
                }

                return _stopTimeButtons[i].ButtonIndex;
            }
        }

        //if (_curTouchID == touchID)
        //{
        //    _curTouchID = -1;
        //}

        if (_curTouchID == touchID && _stopTimeButtons[0].IsPushedButton)
        {
            _stopTimeButtons[0].StandButtonObj.SetActive(true);
            _stopTimeButtons[0].PushedButtonObj.SetActive(false);
            _stopTimeButtons[0].IsPushedButton = false;
            _curTouchID = -1;

            return -1;
        }

        //if (_moveButtonTrans.localPosition.y < _validButtonPosTrans.localPosition.y)
        //    return -1;

        if (_buttonValidTouchStartY > _moveButtonTrans.localPosition.y)
            return -1;

        if (_buttonValidTouchEndY < _moveButtonTrans.localPosition.y)
            return -1;

        int buttonIndex = base.CheckStopTimeButton(touchID, posX, posY);

        if(buttonIndex == _stopButtonIndex)
        {
            _downTweener.enabled = false;
        }

        return buttonIndex;
    }

    public override void TouchButtonObj(int buttonIndex)
    {
        base.TouchButtonObj(buttonIndex);

        SetSolveButton();

        if(_curMoveAniType == MoveButtonAniType.MoveAniDown){
            _downTweener.enabled = false;
        }

        _curMoveAniType = MoveButtonAniType.MoveAniUp;
        _upTweener.from = new Vector3(_moveButtonTrans.localPosition.x, _moveButtonTrans.localPosition.y,
                                  _moveButtonTrans.localPosition.z);
        
        _upTweener.ResetToBeginning();
        _upTweener.PlayForward();
        _upTweener.enabled = true;
    }

    #endregion

    #region Coroutine Methods

    public IEnumerator OnUpdate()
    {
        while (true)
        {
            _motionManager.Update();
            yield return null;
        }
    }

    #endregion

    #region Callback Methods

    public void OnCompleteUpTweener()
    {
        _curMoveAniType = MoveButtonAniType.MoveAniDown;
        _downTweener.ResetToBeginning();
        _downTweener.PlayForward();
        _downTweener.enabled = true;
    }

    public void OnCompleteDownTweener()
    {
        _curMoveAniType = MoveButtonAniType.None;
    }

    #endregion
}
