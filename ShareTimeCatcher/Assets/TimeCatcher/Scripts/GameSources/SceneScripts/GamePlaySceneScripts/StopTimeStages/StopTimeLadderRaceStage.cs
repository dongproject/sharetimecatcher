﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum LadderRaceType
{
    ladderType_0 = 0,
    ladderType_1,
    ladderType_2,
    ladderType_3,
    ladderType_4,
    ladderType_5,
    ladderType_6,
    ladderType_7,
    ladderType_8,
    ladderType_9,
    ladderType_10,
    ladderType_11,
}

public class StopTimeLadderRaceStage : StopTimeBaseStage 
{
    #region Definitions

    public enum LadderMatchObjectKind
    {
        DoliObj     = 0,
        ButtonObj   = 1,
        FatherObj   = 2,
        CatObj      = 3,
    }

    public enum LadderGameStep
    {
        ButtonSelect,
        MatchingAni,
        Fail,
        DelayTime,
        FindButtonObjAni,
        Complete,
    }

    public enum CollideLineDir
    {
        LeftDir,
        RightDir,
    }

    #endregion

    #region Sub Class

    public class CollideLineInfo
    {
        #region Variables

        GameObject _collideLineObj;
        CollideLineDir _lineDir;

        #endregion

        #region Properties

        public GameObject CollideLineObj
        {
            get { return _collideLineObj; }
            set { _collideLineObj = value; }
        }

        public CollideLineDir LineDir
        {
            get { return _lineDir; }
            set { _lineDir = value; }
        }

        #endregion
    }

    #endregion

    #region Serialize Variables

#pragma warning disable 649

    [SerializeField] GameObject[] _ladderTypeObjects;

    [SerializeField] GameObject[] _ladderNormalObjects;
    [SerializeField] GameObject[] _ladderMatchObjects;
    [SerializeField] UGUITweener[] _buttonTweeners;

    [SerializeField] GameObject _findButtonEffect;

    [SerializeField] GameObject _verticalDotObj;
    [SerializeField] GameObject _horizonDotObj;
    [SerializeField] GameObject _selectDotObj;

    [SerializeField] Transform[] _buttonStartPositions;
    [SerializeField] Transform[] _matchObjPositions;
    [SerializeField] Transform _foundPathRoot;

#pragma warning restore 649

    #endregion

    #region Variables

    float _gapLineX = 154f;
    float _gapDotY = 16f;
    float _gapDotX = 22f;

    LadderGameStep _ladderStep = LadderGameStep.ButtonSelect;
    LadderRaceType _ladderType = LadderRaceType.ladderType_1;
//    int _correctButtonIndex;
    List<int[]> _ladderMatchObjInfos = new List<int[]>();
    List<Vector3> _pathPositions = new List<Vector3>();
    List<GameObject> _checkHorizonLines = new List<GameObject>();

    MakePathDotManager _makePathManager = new MakePathDotManager();

    int _curSelectObjectKind;
    int _curButtonIndex;

    #endregion

    #region MonoBehaviour Methods

    protected override void Awake ()
    {
        base.Awake ();
        InitLadderRaceData ();
        SetRandomLadderRaceType ();
    }

    void Update()
    {
        _eventTimer.UpdateGameTimer ();
        if(_makePathManager.UpdateMakePath() == 1)
        {
            SetMatchObject();
            //_eventTimer.SetGameTimerData(OnDelayPathView, 0.5f);
        }
    }

    #endregion

    #region Methods

    void InitLadderRaceData()
    {
        _ladderMatchObjInfos.Clear ();
        for (int i = 0; i < 12; i++) {
            int[] inputLadderMatch = GetLadderMatchInfo ((LadderRaceType)i);
            _ladderMatchObjInfos.Add (inputLadderMatch);
        }
    }

    void SetRandomLadderRaceType()
    {
        int ranLadderTypeValue = Random.Range (0, 12);
        SetLadderRaceType ((LadderRaceType)ranLadderTypeValue);
    }

    int[] GetLadderMatchInfo(LadderRaceType type)
    {
        int[] retValue = new int[4];

        switch (type) {
        case LadderRaceType.ladderType_0:
            retValue [0] = (int)LadderMatchObjectKind.DoliObj;
            retValue [1] = (int)LadderMatchObjectKind.CatObj;
            retValue [2] = (int)LadderMatchObjectKind.ButtonObj;
            retValue [3] = (int)LadderMatchObjectKind.FatherObj;
            break;
        case LadderRaceType.ladderType_1:
            retValue [0] = (int)LadderMatchObjectKind.FatherObj;
            retValue [1] = (int)LadderMatchObjectKind.CatObj;
            retValue [2] = (int)LadderMatchObjectKind.DoliObj;
            retValue [3] = (int)LadderMatchObjectKind.ButtonObj;
            break;
        case LadderRaceType.ladderType_2:
            retValue [0] = (int)LadderMatchObjectKind.ButtonObj;
            retValue [1] = (int)LadderMatchObjectKind.DoliObj;
            retValue [2] = (int)LadderMatchObjectKind.FatherObj;
            retValue [3] = (int)LadderMatchObjectKind.CatObj;
            break;
        case LadderRaceType.ladderType_3:
            retValue [0] = (int)LadderMatchObjectKind.DoliObj;
            retValue [1] = (int)LadderMatchObjectKind.ButtonObj;
            retValue [2] = (int)LadderMatchObjectKind.FatherObj;
            retValue [3] = (int)LadderMatchObjectKind.CatObj;
            break;
        case LadderRaceType.ladderType_4:
            retValue [0] = (int)LadderMatchObjectKind.FatherObj;
            retValue [1] = (int)LadderMatchObjectKind.ButtonObj;
            retValue [2] = (int)LadderMatchObjectKind.CatObj;
            retValue [3] = (int)LadderMatchObjectKind.DoliObj;
            break;
        case LadderRaceType.ladderType_5:
            retValue [0] = (int)LadderMatchObjectKind.ButtonObj;
            retValue [1] = (int)LadderMatchObjectKind.DoliObj;
            retValue [2] = (int)LadderMatchObjectKind.FatherObj;
            retValue [3] = (int)LadderMatchObjectKind.CatObj;
            break;
        case LadderRaceType.ladderType_6:
            retValue [0] = (int)LadderMatchObjectKind.DoliObj;
            retValue [1] = (int)LadderMatchObjectKind.CatObj;
            retValue [2] = (int)LadderMatchObjectKind.ButtonObj;
            retValue [3] = (int)LadderMatchObjectKind.FatherObj;
            break;
        case LadderRaceType.ladderType_7:
            retValue [0] = (int)LadderMatchObjectKind.DoliObj;
            retValue [1] = (int)LadderMatchObjectKind.FatherObj;
            retValue [2] = (int)LadderMatchObjectKind.CatObj;
            retValue [3] = (int)LadderMatchObjectKind.ButtonObj;
            break;
        case LadderRaceType.ladderType_8:
            retValue [0] = (int)LadderMatchObjectKind.ButtonObj;
            retValue [1] = (int)LadderMatchObjectKind.FatherObj;
            retValue [2] = (int)LadderMatchObjectKind.CatObj;
            retValue [3] = (int)LadderMatchObjectKind.DoliObj;
            break;
        case LadderRaceType.ladderType_9:
            retValue [0] = (int)LadderMatchObjectKind.DoliObj;
            retValue [1] = (int)LadderMatchObjectKind.FatherObj;
            retValue [2] = (int)LadderMatchObjectKind.ButtonObj;
            retValue [3] = (int)LadderMatchObjectKind.CatObj;
            break;
        case LadderRaceType.ladderType_10:
            retValue [0] = (int)LadderMatchObjectKind.DoliObj;
            retValue [1] = (int)LadderMatchObjectKind.ButtonObj;
            retValue [2] = (int)LadderMatchObjectKind.CatObj;
            retValue [3] = (int)LadderMatchObjectKind.FatherObj;
            break;
        case LadderRaceType.ladderType_11:
            retValue [0] = (int)LadderMatchObjectKind.DoliObj;
            retValue [1] = (int)LadderMatchObjectKind.FatherObj;
            retValue [2] = (int)LadderMatchObjectKind.CatObj;
            retValue [3] = (int)LadderMatchObjectKind.ButtonObj;
            break;
        }

        return retValue;
    }

    void SetLadderRaceType(LadderRaceType type)
    {
        _ladderType = type;

        _ladderTypeObjects [(int)_ladderType].SetActive (true);

//        switch (_ladderType) {
//        case LadderRaceType.ladderType_0:
//            _correctButtonIndex = 2;
//            break;
//        case LadderRaceType.ladderType_1:
//            _correctButtonIndex = 3;
//            break;
//        case LadderRaceType.ladderType_2:
//            _correctButtonIndex = 0;
//            break;
//        case LadderRaceType.ladderType_3:
//            _correctButtonIndex = 1;
//            break;
//        case LadderRaceType.ladderType_4:
//            _correctButtonIndex = 1;
//            break;
//        case LadderRaceType.ladderType_5:
//            _correctButtonIndex = 0;
//            break;
//        case LadderRaceType.ladderType_6:
//            _correctButtonIndex = 2;
//            break;
//        case LadderRaceType.ladderType_7:
//            _correctButtonIndex = 3;
//            break;
//        case LadderRaceType.ladderType_8:
//            _correctButtonIndex = 0;
//            break;
//        case LadderRaceType.ladderType_9:
//            _correctButtonIndex = 2;
//            break;
//        case LadderRaceType.ladderType_10:
//            _correctButtonIndex = 1;
//            break;
//        case LadderRaceType.ladderType_11:
//            _correctButtonIndex = 3;
//            break;
//        }
    }

    void SetButtonIndex(int buttonIndex)
    {
        //_buttonTweeners [buttonIndex].enabled = true;

        int[] buttonKinds = _ladderMatchObjInfos[(int)_ladderType];
        int selectObjKind = buttonKinds[buttonIndex];

        //_ladderNormalObjects[selectObjKind].gameObject.SetActive(false);

        _curSelectObjectKind = selectObjKind;
        _curButtonIndex = buttonIndex;
        switch ((LadderMatchObjectKind)selectObjKind)
        {
            case LadderMatchObjectKind.DoliObj:
            case LadderMatchObjectKind.FatherObj:
            case LadderMatchObjectKind.CatObj:
                {
                    //_ladderMatchObjects[selectObjKind].gameObject.SetActive(true);

                    ////_ladderStep = LadderGameStep.Fail;

                    //_ladderStep = LadderGameStep.DelayTime;
                    ////_eventTimer.SetGameTimerData (OnWaitJudegeFail, 2f);
                    //_eventTimer.SetGameTimerData(OnWaitDelayTime, 2f, buttonIndex);
                    FoundPath(buttonIndex);
                }
                break;
            case LadderMatchObjectKind.ButtonObj:
                {
                    //SetFindButtonObj(selectObjKind);
                    FoundPath(buttonIndex);
                    //_ladderStep = LadderGameStep.Complete;
                    //RefreshButtonPos();
                }
                break;
        }

        _stopTimeButtons[buttonIndex].StandButtonObj.SetActive(false);
        _stopTimeButtons[buttonIndex].PushedButtonObj.SetActive(true);
        _stopTimeButtons[buttonIndex].IsPushedButton = true;
    }

    void SetMatchObject()
    {
        _ladderNormalObjects[_curSelectObjectKind].gameObject.SetActive(false);

        switch ((LadderMatchObjectKind)_curSelectObjectKind)
        {
            case LadderMatchObjectKind.DoliObj:
            case LadderMatchObjectKind.FatherObj:
            case LadderMatchObjectKind.CatObj:
                {
                    _ladderMatchObjects[_curSelectObjectKind].gameObject.SetActive(true);

                    _ladderStep = LadderGameStep.DelayTime;
                    _eventTimer.SetGameTimerData(OnWaitDelayTime, 2f, _curButtonIndex);
                }
                break;
            case LadderMatchObjectKind.ButtonObj:
                {
                    SetFindButtonObj(_curSelectObjectKind);
                }
                break;
        }
    }

    void SetFindButtonObj(int selectObjKind)
    {
        _ladderStep = LadderGameStep.FindButtonObjAni;
        _findButtonEffect.SetActive(true);

        _eventTimer.SetGameTimerData(OnFindButton, 0.5f, selectObjKind);
    }

    public override int CheckStopTimeButton(int touchID, float posX, float posY)
    {
        if (_ladderStep == LadderGameStep.ButtonSelect){
            for (int i = 0; i < _stopTimeButtons.Length; i++)
            {
                if (_stopTimeButtons[i].CheckValidTouchArea(posX, posY))
                {
                    if (_curTouchID == -1)
                    {
                        _stopTimeButtons[i].StandButtonObj.SetActive(false);
                        _stopTimeButtons[i].PushedButtonObj.SetActive(true);
                        _stopTimeButtons[i].IsPushedButton = true;
                        _curTouchID = touchID;
                        _curOtherButtonIndex = i;
                    }
                    else if (_curTouchID == touchID && _stopTimeButtons[0].IsPushedButton)
                    {
                        _stopTimeButtons[i].StandButtonObj.SetActive(true);
                        _stopTimeButtons[i].PushedButtonObj.SetActive(false);
                        _stopTimeButtons[i].IsPushedButton = false;
                        _curTouchID = -1;
                        _curOtherButtonIndex = -1;
                    }

                    return _stopTimeButtons[i].ButtonIndex;
                }
            }

            if (_curTouchID == touchID && _curOtherButtonIndex != -1 && _stopTimeButtons[_curOtherButtonIndex].IsPushedButton)
            {
                _stopTimeButtons[_curOtherButtonIndex].StandButtonObj.SetActive(true);
                _stopTimeButtons[_curOtherButtonIndex].PushedButtonObj.SetActive(false);
                _stopTimeButtons[_curOtherButtonIndex].IsPushedButton = false;
                _curTouchID = -1;
                _curOtherButtonIndex = -1;

                return -1;
            }
        }

        if (_ladderStep != LadderGameStep.Complete)
            return -1;

        return base.CheckStopTimeButton(touchID, posX, posY);
    }

    public override void TouchButtonObj(int buttonIndex)
    {
        base.TouchButtonObj(buttonIndex);

        if (_ladderStep != LadderGameStep.ButtonSelect)
            return;

        SetButtonIndex(buttonIndex);
    }

    void FoundPath(int selectObjKind)
    {
        GameObject foundPathObj = new GameObject("foundPathObj");
        foundPathObj.transform.SetParent(this.transform);
        foundPathObj.transform.localScale = Vector3.one;
        Vector3 startPosition = _buttonStartPositions[selectObjKind].transform.localPosition;

        foundPathObj.transform.localPosition = startPosition;

        _checkHorizonLines.Clear();

        _pathPositions.Clear();
        _pathPositions.Add(startPosition);

        int exceptCount = 0;
        float checkGapY = 5f;
        while(true)
        {
            foundPathObj.transform.localPosition = new Vector3(foundPathObj.transform.localPosition.x,
                foundPathObj.transform.localPosition.y - checkGapY,
                foundPathObj.transform.localPosition.z);

            CollideLineInfo lineInfo = GetCollideLineInfo(foundPathObj.transform);
            if(lineInfo != null)
            {
                _checkHorizonLines.Add(lineInfo.CollideLineObj);
                float curGapLineX = 0f;
                if(lineInfo.LineDir == CollideLineDir.LeftDir)
                {
                    curGapLineX = -_gapLineX;
                }
                else
                {
                    curGapLineX = _gapLineX;
                }

                foundPathObj.transform.position = new Vector3(foundPathObj.transform.position.x + curGapLineX,
                    lineInfo.CollideLineObj.transform.position.y,
                    foundPathObj.transform.position.z);

                _pathPositions.Add(new Vector3(foundPathObj.transform.localPosition.x - curGapLineX,
                    foundPathObj.transform.localPosition.y,
                    foundPathObj.transform.localPosition.z));
                _pathPositions.Add(foundPathObj.transform.localPosition);
            }

            if (foundPathObj.transform.localPosition.y <= _matchObjPositions[0].transform.localPosition.y)
            {
                for(int i = 0;i< _matchObjPositions.Length; i++)
                {
                    float gapMatchX = Mathf.Abs(foundPathObj.transform.localPosition.x - _matchObjPositions[i].transform.localPosition.x);
                    if(gapMatchX <= 10f)
                    {
                        _pathPositions.Add(_matchObjPositions[i].transform.localPosition);
                    }
                }
                break;
            }

            exceptCount++;
            if(exceptCount > 10000)
            {
                Debug.Log(string.Format("!!!!!!FoundPath exceptCount > 10000 selectObjKind : {0}", selectObjKind));
                break;
            }
        }

        MakePathDot();

        Destroy(foundPathObj);
    }

    void MakePathDot()
    {
        _makePathManager.ReleasePathDot();

        Debug.Log(string.Format("MakePathDot _pathPositions Count : {0}", _pathPositions.Count));

        Vector3[] pairPaths = new Vector3[2];
        int pathIndex = 0;
        for(int i = 0;i< _pathPositions.Count; i++)
        {
            Debug.Log(string.Format("MakePathDot pathPositions : {0}", _pathPositions[i]));

            pairPaths[pathIndex] = _pathPositions[i];
            pathIndex++;

            if (pathIndex == 2)
            {
                MakePairPath(pairPaths);
                //pathIndex = 0;
                pairPaths[0] = pairPaths[1];
                pathIndex = 1;
            }
        }

        _makePathManager.InitMakePathDot();
    }

    void MakePairPath(Vector3[] pairPaths)
    {
        float dotZ = -5f;
        if (Mathf.Abs(pairPaths[0].x - pairPaths[1].x) <= 1f)
        {
            float curDotPathY = pairPaths[0].y;
            int exceptCount = 0;
            while (true)
            {
                //MakePathDotInfo addPathDotInfo = new MakePathDotInfo();
                //addPathDotInfo.DotObject = Instantiate(_selectDotObj);
                //addPathDotInfo.DotObject.SetActive(false);
                //addPathDotInfo.DotObject.transform.SetParent(_foundPathRoot);
                //addPathDotInfo.DotObject.transform.localPosition = new Vector3(pairPaths[0].x, curDotPathY,
                //    dotZ);

                //_makePathManager.PathDotInfos.Add(addPathDotInfo);
                AddPathDotInfo(new Vector3(pairPaths[0].x, curDotPathY, dotZ));

                curDotPathY -= _gapDotY;
                if(curDotPathY <= pairPaths[1].y)
                {
                    //addPathDotInfo = new MakePathDotInfo();
                    //addPathDotInfo.DotObject = Instantiate(_selectDotObj);
                    //addPathDotInfo.DotObject.SetActive(false);
                    //addPathDotInfo.DotObject.transform.SetParent(_foundPathRoot);
                    //addPathDotInfo.DotObject.transform.localPosition = new Vector3(pairPaths[0].x, pairPaths[1].y,
                    //    dotZ);

                    //_makePathManager.PathDotInfos.Add(addPathDotInfo);
                    Vector3 pathDotPosition = new Vector3(pairPaths[0].x, pairPaths[1].y, dotZ);
                    AddPathDotInfo(pathDotPosition);

                    break;
                }

                exceptCount++;
                if (exceptCount > 1000)
                {
                    Debug.Log(string.Format("MakePairPath exceptCount > 1000"));
                    break;
                }
            }
        }
        else
        {
            bool isLeftDir = true;

            if(pairPaths[0].x - pairPaths[1].x < 0f)
            {
                isLeftDir = false;
            }

            float curDotPosX = pairPaths[0].x;

            int exceptCount = 0;
            while (true)
            {
                Vector3 pathHorizonDotPos = Vector3.zero;
                if (isLeftDir)
                {
                    pathHorizonDotPos = new Vector3(curDotPosX, pairPaths[0].y, dotZ);
                    AddPathDotInfo(pathHorizonDotPos);

                    curDotPosX -= _gapDotX;
                    if(curDotPosX <= pairPaths[1].x){
                        pathHorizonDotPos = new Vector3(pairPaths[1].x, pairPaths[0].y, dotZ);
                        AddPathDotInfo(pathHorizonDotPos);
                        break;
                    }
                }
                else
                {
                    pathHorizonDotPos = new Vector3(curDotPosX, pairPaths[0].y, dotZ);
                    AddPathDotInfo(pathHorizonDotPos);

                    curDotPosX += _gapDotX;
                    if (curDotPosX >= pairPaths[1].x)
                    {
                        pathHorizonDotPos = new Vector3(pairPaths[1].x, pairPaths[0].y, dotZ);
                        AddPathDotInfo(pathHorizonDotPos);
                        break;
                    }
                }

                exceptCount++;
                if(exceptCount > 1000)
                {
                    Debug.Log(string.Format("MakePairPath exceptCount > 1000"));
                    break;
                }
            }

        }
    }

    void AddPathDotInfo(Vector3 dotPosition)
    {
        MakePathDotInfo addPathDotInfo = new MakePathDotInfo();
        addPathDotInfo.DotObject = Instantiate(_selectDotObj);
        addPathDotInfo.DotObject.SetActive(false);
        addPathDotInfo.DotObject.transform.SetParent(_foundPathRoot);
        addPathDotInfo.DotObject.transform.localPosition = dotPosition;

        _makePathManager.PathDotInfos.Add(addPathDotInfo);
    }

    CollideLineInfo GetCollideLineInfo(Transform pathCheckObj)
    {
        CollideLineInfo retValue = null;

        Collider2D collider = Physics2D.OverlapPoint(pathCheckObj.position);
        if (collider != null && collider.gameObject.activeSelf)
        {
            if (_checkHorizonLines.Contains(collider.gameObject))
                return null;

            retValue = new CollideLineInfo();
            retValue.CollideLineObj = collider.gameObject;
            if(pathCheckObj.position.x > collider.transform.position.x)
            {
                retValue.LineDir = CollideLineDir.LeftDir;
            }
            else
            {
                retValue.LineDir = CollideLineDir.RightDir;
            }
        }

        return retValue;
    }


    #endregion

    #region Button Methods

    void OnLadderButton_1()
    {
        if (_ladderStep != LadderGameStep.ButtonSelect)
            return;
        
        Debug.Log (string.Format ("OnLadderButton_1"));
        SetButtonIndex (0);
    }

    void OnLadderButton_2()
    {
        if (_ladderStep != LadderGameStep.ButtonSelect)
            return;

        Debug.Log (string.Format ("OnLadderButton_2"));
        SetButtonIndex (1);
    }

    void OnLadderButton_3()
    {
        if (_ladderStep != LadderGameStep.ButtonSelect)
            return;

        Debug.Log (string.Format ("OnLadderButton_3"));
        SetButtonIndex (2);
    }

    void OnLadderButton_4()
    {
        if (_ladderStep != LadderGameStep.ButtonSelect)
            return;

        Debug.Log (string.Format ("OnLadderButton_4"));
        SetButtonIndex (3);
    }

    #endregion

    #region CallBack Methods

    void OnWaitJudegeFail(object objData)
    {
        FailStage ();
    }

    void OnWaitDelayTime(object objData)
    {
        int buttonIndex = (int)objData;

        _stopTimeButtons[buttonIndex].StandButtonObj.SetActive(true);
        _stopTimeButtons[buttonIndex].PushedButtonObj.SetActive(false);
        _stopTimeButtons[buttonIndex].IsPushedButton = false;

        _ladderStep = LadderGameStep.ButtonSelect;

        int[] buttonKinds = _ladderMatchObjInfos[(int)_ladderType];
        int selectObjKind = buttonKinds[buttonIndex];

        _ladderMatchObjects[selectObjKind].gameObject.SetActive(false);
        _ladderNormalObjects[selectObjKind].gameObject.SetActive(true);

        _makePathManager.ReleasePathDot();
    }

    void OnFindButton(object objData)
    {
        SetSolveButton();
        int selectObjKind = (int)objData;

        _ladderStep = LadderGameStep.Complete;
        _findButtonEffect.SetActive(false);

        _ladderMatchObjects[selectObjKind].gameObject.SetActive(true);

        RefreshButtonPos();
    }

    void OnDelayPathView(object objData)
    {
        _makePathManager.ReleasePathDot();
    }

    #endregion
}
