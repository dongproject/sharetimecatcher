﻿using UnityEngine;

public class StopTimeTutorialStage : StopTimeBaseStage
{
    #region Definitions

    public enum TutoStep
    {
        None,
        startDelay,
        TutoStep_1,
        TutoStep_1_Delay,
        TutoStep_1_Touch,
        TutoStep_2_preDelay,
        TutoStep_2,
        TutoStep_2_Delay,
        TutoStep_2_Touch,
        TutoStep_3_preDelay,
        TutoStep_3,
    }

    #endregion

    #region Sub Class

    [System.Serializable]
    public class TutorialStepInfo
    {
        public GameObject blackBoardObj;
        public GameObject tutorialTextObj;
    }

    #endregion

    #region Serialize Variables

#pragma warning disable 649

    [SerializeField] TutorialStepInfo[] _tutoSteps;
    [SerializeField] GameObject _tutoNextObj;

#pragma warning restore 649

    #endregion

    #region Variables

    TutoStep _curTutoStep = TutoStep.None;
    UGUITweener _tutoNextTweener;
    float _delayTime = 0f;
    float _curDelayTime = 0f;

    #endregion

    #region MonoBehaviour Methods

    protected override void Awake()
    {
        base.Awake();

        _tutoNextTweener = _tutoNextObj.GetComponent<UGUITweener>();
    }

    private void Update()
    {
        switch(_curTutoStep)
        {
            case TutoStep.startDelay:
                _curDelayTime += Time.deltaTime;
                if(_delayTime <= _curDelayTime)
                {
                    SetTutorialStep(TutoStep.TutoStep_1);
                }
                break;
            case TutoStep.TutoStep_1_Delay:
                _curDelayTime += Time.deltaTime;
                if (_delayTime <= _curDelayTime)
                {
                    SetTutorialStep(TutoStep.TutoStep_1_Touch);
                }
                break;
            case TutoStep.TutoStep_2_preDelay:
                _curDelayTime += Time.deltaTime;
                if (_delayTime <= _curDelayTime)
                {
                    SetTutorialStep(TutoStep.TutoStep_2);
                }
                break;
            case TutoStep.TutoStep_2_Delay:
                _curDelayTime += Time.deltaTime;
                if (_delayTime <= _curDelayTime)
                {
                    SetTutorialStep(TutoStep.TutoStep_2_Touch);
                }
                break;
            case TutoStep.TutoStep_3_preDelay:
                _curDelayTime += Time.deltaTime;
                if (_delayTime <= _curDelayTime)
                {
                    SetTutorialStep(TutoStep.TutoStep_3);
                }
                break;
        }
    }

    #endregion

    #region Methods

    public override void SetStartPlayData()
    {
        base.SetStartPlayData();

        SetTutorialStep(TutoStep.startDelay);
    }

    void SetTutorialStep(TutoStep tutoStep)
    {
        _curTutoStep = tutoStep;
        switch (_curTutoStep)
        {
            case TutoStep.startDelay:
                _delayTime = 1f;
                _curDelayTime = 0f;
                break;
            case TutoStep.TutoStep_1:
                if (_onTimePause != null)
                    _onTimePause(0, 0, 56f);

                _tutoSteps[0].blackBoardObj.SetActive(true);
                _tutoSteps[0].tutorialTextObj.SetActive(true);
                SetTouchNotiObject();
                SetTutorialStep(TutoStep.TutoStep_1_Delay);
                break;
            case TutoStep.TutoStep_1_Delay:
                _delayTime = 1f;
                _curDelayTime = 0f;
                break;
            case TutoStep.TutoStep_2_preDelay:
                _delayTime = 1f;
                _curDelayTime = 0f;
                if (_onTimeResume != null)
                    _onTimeResume();

                _tutoSteps[0].blackBoardObj.SetActive(false);
                _tutoSteps[0].tutorialTextObj.SetActive(false);
                _tutoNextTweener.gameObject.SetActive(false);
                break;
            case TutoStep.TutoStep_2:
                if (_onTimePause != null)
                    _onTimePause(0, 0, 57f);

                _tutoSteps[1].blackBoardObj.SetActive(true);
                _tutoSteps[1].tutorialTextObj.SetActive(true);
                SetTouchNotiObject();
                SetTutorialStep(TutoStep.TutoStep_2_Delay);
                break;
            case TutoStep.TutoStep_2_Delay:
                _delayTime = 1f;
                _curDelayTime = 0f;
                break;
            case TutoStep.TutoStep_3_preDelay:
                _delayTime = 3f;
                _curDelayTime = 0f;
                if (_onTimeResume != null)
                    _onTimeResume();

                _tutoSteps[1].blackBoardObj.SetActive(false);
                _tutoSteps[1].tutorialTextObj.SetActive(false);
                _tutoNextTweener.gameObject.SetActive(false);
                break;
            case TutoStep.TutoStep_3:
                if (_onTimePause != null)
                    _onTimePause(0, 1, 0f);

                _tutoSteps[2].blackBoardObj.SetActive(true);
                _tutoSteps[2].tutorialTextObj.SetActive(true);
                break;
        }
    }

    void SetTouchNotiObject()
    {
        _tutoNextTweener.ResetToBeginning();
        _tutoNextTweener.PlayForward();
        _tutoNextTweener.enabled = true;
        _tutoNextTweener.gameObject.SetActive(true);
    }

    public override int CheckStopTimeButton(int touchID, float posX, float posY)
    {
        if (_curTutoStep != TutoStep.TutoStep_3)
            return -1;

        return base.CheckStopTimeButton(touchID, posX, posY);
    }

    public override void OnTouchPress(int touchID, float posX, float posY)
    {
        switch (_curTutoStep)
        {
            case TutoStep.TutoStep_1_Touch:
                SetTutorialStep(TutoStep.TutoStep_2_preDelay);
                break;
            case TutoStep.TutoStep_2_Touch:
                SetTutorialStep(TutoStep.TutoStep_3_preDelay);
                break;
        }
    }

    #endregion
}
