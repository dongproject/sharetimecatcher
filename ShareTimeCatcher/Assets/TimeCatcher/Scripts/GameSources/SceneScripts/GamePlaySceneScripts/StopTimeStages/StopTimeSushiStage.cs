﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StopTimeSushiStage : StopTimeBaseStage 
{
	#region Definitions

	public enum SushiButtonID
	{
		LeverID 		= 0,
		FakeButton_1 	= 1,
		FakeButton_2 	= 2,
	}

	public enum MoveObjectID
	{
		RailMoveObj 	= 1,
		MoveButtonObj	= 2,
	}

	public enum LeverPosState
	{
		LeverLeft = 0,
		LeverRight = 1,
	}

	#endregion

	#region Serialize Variables

	[SerializeField] UGUITweener _leverTween;
	[SerializeField] Transform _railMoveObjTrans;
	[SerializeField] Transform _buttonObjTrans;
    [SerializeField] Transform _buttonPosTrans;

    [Header("RailMoveObjects")]
    [SerializeField] UGUITweener[] _dogUpDownTweeners;

    [Header("RailStopObjects")]
    [SerializeField] UGUITweener _dogArmTween;
    [SerializeField] UGUITweener _dogLegTween;
    [SerializeField] UGUITweener _dogLeftRightTween;
    [SerializeField] Transform[] _sushiObjectTrans;

	#if UNITY_EDITOR
	[SerializeField] RFButtonManager _buttonManager = null;
	#endif

	#endregion

	#region Variables

	RFButtonManager _rfButtonManager = null;

	int _leverTweenState = (int)LeverPosState.LeverLeft;
	bool _isRailMove = false;

    bool _buttonMoveState = true;

    float _railTime = 7f;
    float _destSpeedValue = 350f;

    float _railDecreaseValue = -3f;
    float _railIncreaseValue = 3f;

    float _restartPosX = 3110f;

    #endregion

    #region Properties

    public RFButtonManager RFButtonManager
	{
		get{ return _rfButtonManager; }
		set{ _rfButtonManager = value; }
	}

	#endregion

	#region MonoBehaviour Methods

	protected override void Awake()
	{
		base.Awake ();

		#if UNITY_EDITOR
		if (CAGameGlobal.Instance == null) {
			_rfButtonManager = _buttonManager;
		}
        #endif

        SwapSushiObjects();

        AddStopTimeObjectInfo (StageObjectType.MotionAni, (int)MoveObjectID.RailMoveObj, _railMoveObjTrans, 100f, 100f, _railMoveObjTrans.localPosition);
		//AddStopTimeObjectInfo (StageObjectType.MotionAni, (int)MoveObjectID.MoveButtonObj, _buttonObjTrans, 100f, 100f, _buttonObjTrans.localPosition);
	}

	protected override void Start()
	{
		base.Start ();

		#if UNITY_EDITOR
		if(_buttonManager.ButtonCamera != null){
			_buttonManager.InitRFButtonManager();
		}
		#endif

		_leverTween.AddOnFinished (() => OnFinishLeverTween());

        for (int i = 0; i < _dogUpDownTweeners.Length; i++)
        {
            _dogUpDownTweeners[i].enabled = true;
        }

        _dogLeftRightTween.enabled = false;
    }

	#endregion

	#region Methods

	public override void SetStartPlayData()
	{
		base.SetStartPlayData ();

//		SetButtonMoveAni (MoveButtonAniStep.ButtonAni_1);

		_isRailMove = true;

		AddObjectMoveAni ((int)MoveObjectID.RailMoveObj, new Vector2(3200f, 0f), (int)MoveObjectID.RailMoveObj, _railTime);
		//AddObjectMoveAni ((int)MoveObjectID.MoveButtonObj, new Vector2(3200f, 0f), (int)MoveObjectID.MoveButtonObj, _railTime);

		StartCoroutine (OnUpdate ());
	}

    void SwapSushiObjects()
    {
        int swapCount = 10;

        for (int i = 0; i < swapCount;i++){
            int ranIndex = Random.Range(0, _sushiObjectTrans.Length);
            Vector3 savePosition = _sushiObjectTrans[ranIndex].localPosition;
            _sushiObjectTrans[ranIndex].localPosition = new Vector3(_buttonPosTrans.localPosition.x,
                                                                    _sushiObjectTrans[ranIndex].localPosition.y,
                                                                    _sushiObjectTrans[ranIndex].localPosition.z);
            _buttonPosTrans.localPosition = new Vector3(savePosition.x, _buttonPosTrans.localPosition.y,
                                                        _buttonPosTrans.localPosition.z);
        }
    }

	void AddObjectMoveAni(int objIndex, Vector2 destValue, int motionID, float aniTime = 1f)
	{
		Transform moveTransform = null;
		for (int i = 0; i < _motionAniObjectInfos.Count; i++) {
			if (_motionAniObjectInfos [i].ObjectIndex == objIndex) {
				moveTransform = _motionAniObjectInfos [i].ObjTransform;
				if (moveTransform.localPosition.x == destValue.x && moveTransform.localPosition.y == destValue.y) {
					Debug.Log (string.Format ("AddObjectMoveAni Same Position!!!"));
					return;
				}
				break;
			}
		}

		if (moveTransform == null) {
			Debug.Log (string.Format ("AddObjectMoveAni Not Exist MoveAni Object!!!"));
			return;
		}

		Vector3 destPos = new Vector3 (destValue.x, 
			destValue.y, moveTransform.localPosition.z);
        //ObjectMotionAni motionAni = _motionManager.AddObjSpeedMovement(aniTime, moveTransform.localPosition, destPos, OnChangeMoveValue, motionID, OnCompleteMotionAni);
        ObjectMotionAni motionAni = _motionManager.AddObjUniformVelocityMovement(moveTransform.localPosition, destPos, _destSpeedValue, OnChangeMoveValue, motionID, OnCompleteMotionAni);

        //_destSpeedValue = motionAni.Velocity;
        //_destSpeedValue = 100f;

        _motionAniTransforms.Add (motionID, moveTransform);
	}

    ObjectMotionAni AddObjectMoveUpAni(int objIndex, Vector2 destValue, int motionID, float speedValue, float speedChangeValue, float speedDestValue)
    {
        ObjectMotionAni retValue = null;

        Transform moveTransform = null;
        for (int i = 0; i < _motionAniObjectInfos.Count; i++)
        {
            if (_motionAniObjectInfos[i].ObjectIndex == objIndex)
            {
                moveTransform = _motionAniObjectInfos[i].ObjTransform;
                if (moveTransform.localPosition.x == destValue.x && moveTransform.localPosition.y == destValue.y)
                {
                    Debug.Log(string.Format("AddObjectMoveAni Same Position!!!"));
                    return retValue;
                }
                break;
            }
        }

        if (moveTransform == null)
        {
            Debug.Log(string.Format("AddObjectMoveAni Not Exist MoveAni Object!!!"));
            return retValue;
        }

        Vector3 destPos = new Vector3(destValue.x,
            destValue.y, moveTransform.localPosition.z);
        retValue = _motionManager.AddObjSpeedUpMovement(moveTransform.localPosition, destPos, speedValue, speedChangeValue, speedDestValue, OnChangeMoveValue, motionID, OnCompleteMotionAni);

        _motionAniTransforms.Add(motionID, moveTransform);

        _buttonMoveState = true;

        return retValue;
    }

    ObjectMotionAni AddObjectMoveDownAni(int objIndex, Vector2 destValue, int motionID, float speedValue, float speedChangeValue, float speedDestValue)
    {
        ObjectMotionAni retValue = null;
        Transform moveTransform = null;
        for (int i = 0; i < _motionAniObjectInfos.Count; i++)
        {
            if (_motionAniObjectInfos[i].ObjectIndex == objIndex)
            {
                moveTransform = _motionAniObjectInfos[i].ObjTransform;
                if (moveTransform.localPosition.x == destValue.x && moveTransform.localPosition.y == destValue.y)
                {
                    Debug.Log(string.Format("AddObjectMoveAni Same Position!!!"));
                    return retValue;
                }
                break;
            }
        }

        if (moveTransform == null)
        {
            Debug.Log(string.Format("AddObjectMoveAni Not Exist MoveAni Object!!!"));
            return retValue;
        }

        Vector3 destPos = new Vector3(destValue.x,
            destValue.y, moveTransform.localPosition.z);
        retValue = _motionManager.AddObjSpeedDownMovement(moveTransform.localPosition, destPos, speedValue, speedChangeValue, speedDestValue, OnChangeMoveValue, motionID, OnCompleteMotionAni);

        _motionAniTransforms.Add(motionID, moveTransform);

        _buttonMoveState = true;

        return retValue;
    }

    void SetLeverPos(LeverPosState leverPos)
	{
		_leverTweenState = (int)leverPos;
		switch (leverPos) {
		case LeverPosState.LeverLeft:
			_leverTween.PlayReverse ();
			break;
		case LeverPosState.LeverRight:
			_leverTween.PlayForward ();
			break;
		}

		_leverTween.enabled = true;

        switch ((LeverPosState)_leverTweenState)
        {
            case LeverPosState.LeverLeft:
                SetRailMoveState(true);
                break;
            case LeverPosState.LeverRight:
                SetRailMoveState(false);
                break;
        }
    }

	void SetRailMoveState(bool moveState)
	{
		_isRailMove = moveState;

		if (!_isRailMove) {
			if (_rfButtonManager != null) {
				_rfButtonManager.SetCacheButtonRect ();
			}

			RefreshButtonPos ();

            _dogArmTween.ResetToBeginning();
            _dogArmTween.PlayForward();
            _dogArmTween.enabled = true;
            _dogArmTween.gameObject.SetActive(true);

            _dogLegTween.ResetToBeginning();
            _dogLegTween.PlayForward();
            _dogLegTween.enabled = true;

            _dogLeftRightTween.enabled = true;

            for (int i = 0; i < _dogUpDownTweeners.Length;i++){
                _dogUpDownTweeners[i].enabled = false;
            }

            ObjectMotionAni railAni = _motionManager.GetObjMovement((int)MoveObjectID.RailMoveObj);
            if(railAni != null){
                float curSpeedValue = railAni.Velocity;
                //float curDistance = railAni.Distance;

                float sinA = railAni.SinA;
                float cosA = railAni.CosA;
                _motionManager.ReleaseObjMovement((int)MoveObjectID.RailMoveObj);
                railAni = AddObjectMoveDownAni((int)MoveObjectID.RailMoveObj, new Vector2(3200f, 0f), (int)MoveObjectID.RailMoveObj, curSpeedValue, _railDecreaseValue, 30f);

                railAni.SinA = sinA;
                railAni.CosA = cosA;
            }

            //ObjectMotionAni railButtonAni = _motionManager.GetObjMovement((int)MoveObjectID.MoveButtonObj);
            //if (railButtonAni != null)
            //{
            //    float curSpeedValue = railButtonAni.Velocity;
            //    float sinA = railButtonAni.SinA;
            //    float cosA = railButtonAni.CosA;
            //    //float curDistance = railAni.Distance;

            //    _motionManager.ReleaseObjMovement((int)MoveObjectID.MoveButtonObj);
            //    railButtonAni = AddObjectMoveDownAni((int)MoveObjectID.MoveButtonObj, new Vector2(3200f, 0f), (int)MoveObjectID.MoveButtonObj, curSpeedValue, _railDecreaseValue, 30f);
            //    railButtonAni.SinA = sinA;
            //    railButtonAni.CosA = cosA;
            //}
        } else{
            _dogArmTween.gameObject.SetActive(false);

            _dogLegTween.PlayReverse();
            _dogLegTween.enabled = true;

            for (int i = 0; i < _dogUpDownTweeners.Length; i++)
            {
                _dogUpDownTweeners[i].enabled = true;
            }

            _dogLeftRightTween.enabled = false;

            ObjectMotionAni railAni = _motionManager.GetObjMovement((int)MoveObjectID.RailMoveObj);
            float curSpeedValue = 0f;
            float sinA = 0f;
            float cosA = 1f;
            if (railAni != null)
            {
                curSpeedValue = railAni.Velocity;
                //float curDistance = railAni.Distance;

                sinA = railAni.SinA;
                cosA = railAni.CosA;

                _motionManager.ReleaseObjMovement((int)MoveObjectID.RailMoveObj);

            }

            railAni = AddObjectMoveUpAni((int)MoveObjectID.RailMoveObj, new Vector2(3200f, 0f), (int)MoveObjectID.RailMoveObj, curSpeedValue, _railIncreaseValue, _destSpeedValue);
            railAni.SinA = sinA;
            railAni.CosA = cosA;

            //ObjectMotionAni railButtonAni = _motionManager.GetObjMovement((int)MoveObjectID.MoveButtonObj);
            //if (railButtonAni != null)
            //{
            //    curSpeedValue = railButtonAni.Velocity;
            //    //float curDistance = railAni.Distance;

            //    sinA = railButtonAni.SinA;
            //    cosA = railButtonAni.CosA;

            //    _motionManager.ReleaseObjMovement((int)MoveObjectID.MoveButtonObj);
               
            //}

            //railButtonAni = AddObjectMoveUpAni((int)MoveObjectID.MoveButtonObj, new Vector2(3200f, 0f), (int)MoveObjectID.MoveButtonObj, curSpeedValue, _railIncreaseValue, _destSpeedValue);

            //railButtonAni.SinA = sinA;
            //railButtonAni.CosA = cosA;
        }
	}

	public override int CheckStopTimeButton(int touchID, float posX, float posY)
	{
		if (_buttonMoveState)
			return -1;

		return base.CheckStopTimeButton(touchID, posX, posY);
	}

    void SetRestartRail()
    {
        _railMoveObjTrans.localPosition = new Vector3(0f, _railMoveObjTrans.localPosition.y,
                                                      _railMoveObjTrans.localPosition.z);

        _motionManager.ReleaseObjMovement((int)MoveObjectID.RailMoveObj);

        AddObjectMoveAni((int)MoveObjectID.RailMoveObj, new Vector2(3200f, 0f), (int)MoveObjectID.RailMoveObj, _railTime);
    }

	#endregion

	#region Coroutine Methods

	public IEnumerator OnUpdate()
	{
		while(true)
		{
			//if(_isRailMove)
			_motionManager.Update ();
			yield return null;
		}
	}

	#endregion

	#region CallBack Methods

	public override void OnButtonTouched(int buttonID)
	{
        if (!_isGamePlaying)
            return;

		Debug.Log (string.Format ("OnButtonTouched buttonID : {0}", buttonID));
		if (buttonID == (int)SushiButtonID.LeverID) {
            SoundManager.Instance.PlayEffectFX(SoundDefinitions.effectSound_16);
            if (_leverTweenState == (int)LeverPosState.LeverLeft) {
				SetLeverPos (LeverPosState.LeverRight);
			} else {
				SetLeverPos (LeverPosState.LeverLeft);
			}
		} else {
			//FailStage ();
		}
	}

	void OnFinishLeverTween()
	{
        //switch ((LeverPosState)_leverTweenState) {
        //case LeverPosState.LeverLeft:
        //	SetRailMoveState (true);
        //	break;
        //case LeverPosState.LeverRight:
        //	SetRailMoveState (false);
        //	break;
        //}_buttonMoveState
    }

    protected override void OnCompleteMotionAni(int motionID, ObjectMotionAni objMotionAni)
    {
        base.OnCompleteMotionAni(motionID, objMotionAni);

        RefreshButtonPos();

        _buttonMoveState = false;
    }

    protected override void OnChangeMoveValue(int motionID, ObjectMotionManager.MotionType motionType, Vector2 moveValue)
    {
        base.OnChangeMoveValue(motionID, motionType, moveValue);

        Vector3 curPosition = _motionAniTransforms[motionID].localPosition;
        if(curPosition.x >= _restartPosX){
            SetRestartRail();
        }
    }

    #endregion
}
