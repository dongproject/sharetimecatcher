﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StopTimeMovingButtonStage : StopTimeBaseStage 
{
    #region Serialize Variables

    [SerializeField] UGUITweenRotation _pendulumTween;
    [SerializeField] float[] _pendulumDurations;

    #endregion

    #region MonoBehaviour Methods

    protected override void Awake()
    {
        base.Awake();

        //_stageButtonType = StopTimeButtonType.PushImmediate;

        //float ranDuration = Random.Range(1f, 2.2f);
        //_pendulumTween.duration = ranDuration;
        int ranIndex = Random.Range(0, _pendulumDurations.Length);
        _pendulumTween.duration = _pendulumDurations[ranIndex];
    }

    #endregion

    #region Methods

    public override int CheckStopTimeButton(int touchID, float posX, float posY)
	{
		RefreshTouchButtonPos ();
		Debug.Log (string.Format ("CheckStopTimeButton posX : {0}, posY : {1}", posX, posY));

		return base.CheckStopTimeButton(touchID, posX, posY);
	}

	#endregion
}
