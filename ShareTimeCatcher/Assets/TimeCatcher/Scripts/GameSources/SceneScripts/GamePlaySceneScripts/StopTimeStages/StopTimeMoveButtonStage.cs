﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class StopTimeMoveButtonStage : StopTimeBaseStage 
{
    #region Serialize Variabless

#pragma warning disable 649

    [SerializeField] tk2dSpriteAnimator _buttonSpriteAnimator;
	[SerializeField] Transform _moveButtonTrans;
	[SerializeField] Transform[] _movePointTrans;
    [SerializeField] Animator _movingBeeAnimator;

#pragma warning restore 649

    #endregion

    #region Variables

    int _moveButtonID = 1;
	int _curPointIndex = -1;
	Dictionary<int /* pointIndex */, Transform> _pointTransDic = new Dictionary<int, Transform>();

	#endregion

	#region MonoBehaviour Methods

	protected override void Awake()
	{
		base.Awake ();

		_buttonSpriteAnimator.AnimationCompleted = OnCompleteButtonAnimation;
		_buttonSpriteAnimator.AnimationEventTriggered = OnTriggerButtonAnimation;
//		_buttonSpriteAnimator.playAutomatically = false;

		AddStopTimeObjectInfo (StageObjectType.MotionAni, _moveButtonID, _moveButtonTrans, 100f, 100f, new Vector3(0f, -327f, -1f));
	
		for (int i = 0; i < _movePointTrans.Length; i++) {
			_pointTransDic.Add (i, _movePointTrans [i]);
		}

        //_movingBeeAnimator.SetFloat("Offset", (float)Random.Range(0, 11));
        _movingBeeAnimator.gameObject.SetActive(false);
    }

	#endregion

	#region Methods

	public override void SetStartPlayData()
	{
		base.SetStartPlayData ();

        //AnimationState beeMoveAniState = _movingBeeAnimator.GetCurrentAnimatorStateInfo(0);

        _movingBeeAnimator.gameObject.SetActive(true);
        //_movingBeeAnimator.GetCurrentAnimatorStateInfo(0).
        float randomValue = Random.Range(0f, _movingBeeAnimator.GetCurrentAnimatorStateInfo(0).length);

        _movingBeeAnimator.Play("MovingBeeAni", 0, randomValue);

        AudioClip musicClip = Resources.Load<AudioClip>(SoundFilePaths.MusicPath_7);
        SoundManager.Instance.PlaySubMusic(musicClip, true);

        StartCoroutine (OnUpdate ());
	}

	void AddObjectMoveAni(int objIndex, Vector2 destValue, int motionID, float aniTime = 1f)
	{
		Transform moveTransform = null;
		for (int i = 0; i < _motionAniObjectInfos.Count; i++) {
			if (_motionAniObjectInfos [i].ObjectIndex == objIndex) {
				moveTransform = _motionAniObjectInfos [i].ObjTransform;
				if (moveTransform.localPosition.x == destValue.x && moveTransform.localPosition.y == destValue.y) {
					Debug.Log (string.Format ("AddObjectMoveAni Same Position!!!"));
					return;
				}
				break;
			}
		}

		if (moveTransform == null) {
			Debug.Log (string.Format ("AddObjectMoveAni Not Exist MoveAni Object!!!"));
			return;
		}

		Vector3 destPos = new Vector3 (destValue.x, 
			destValue.y, moveTransform.localPosition.z);
		_motionManager.AddObjTimeMovement (aniTime, moveTransform.localPosition, destPos, OnChangeMoveValue, motionID, OnCompleteMotionAni); 

		_motionAniTransforms.Add (motionID, moveTransform);
	}

	#endregion

	#region Coroutine Methods

	public IEnumerator OnUpdate()
	{
		while(true)
		{
			_motionManager.Update ();
			yield return null;
		}
	}

    #endregion

    #region Callback Methods

    void OnCompleteButtonAnimation(tk2dSpriteAnimator tk2dAnimator, tk2dSpriteAnimationClip tk2dAnimCip)
	{
		Debug.Log (string.Format ("OnCompleteButtonAnimation"));
	}

	void OnTriggerButtonAnimation(tk2dSpriteAnimator tk2dAnimator, tk2dSpriteAnimationClip tk2dAnimCip, int triggerIndex)
	{
		Debug.Log (string.Format ("OnTriggerButtonAnimation triggerIndex : {0}", triggerIndex));

		//List<int> pointTranKeys = _pointTransDic.Keys.ToList ();

		//int ranValue = Random.Range (0, pointTranKeys.Count);

		//if (_curPointIndex != -1) {
		//	_pointTransDic.Add (_curPointIndex, _movePointTrans [_curPointIndex]);
		//}

		//_curPointIndex = pointTranKeys [ranValue];
		//Transform targetTrans = _pointTransDic [_curPointIndex];
		//_pointTransDic.Remove (_curPointIndex);

		_motionManager.ReleaseObjMovement (_moveButtonID);
        //		Debug.Log (string.Format ("targetTrans.localPosition : {0}", targetTrans.localPosition));

        int ranAngle = 0;
        float disValue = 150f;
        
        float halfWidth = CAResolutionCtl.Instance.GetResolutionHalfWidth();
        float halfHeight = CAResolutionCtl.Instance.GetResolutionHalfHeight();

        Vector2 targetPos = Vector2.zero;

        float upGap = 300f;
        float downGap = 300f;
        float xGap = 100f;

        if(_moveButtonTrans.localPosition.x - disValue <= -halfWidth + xGap)
        {
            int areaRan = Random.Range(0, 2);
            if(areaRan == 0){
                ranAngle = Random.Range(0, 90);
            } else {
                ranAngle = Random.Range(271, 360);
            }
            //Debug.Log(string.Format("OnTriggerButtonAnimation 1, objX : {0}, disValue : {1}, halfWidth : {2}, ranAngle : {3}", 
                                    //_moveButtonTrans.localPosition.x, disValue, halfWidth, ranAngle));
        } else if(_moveButtonTrans.localPosition.y + disValue >= halfHeight - upGap)
        {
            ranAngle = Random.Range(181, 360);
            //Debug.Log(string.Format("OnTriggerButtonAnimation 2, objY : {0}, disValue : {1}, halfHeight : {2}, ranAngle : {3}",
                                    //_moveButtonTrans.localPosition.y, disValue, halfHeight, ranAngle));

        }
        else if (_moveButtonTrans.localPosition.x + disValue >= halfWidth - xGap)
        {
            ranAngle = Random.Range(91, 270);
            //Debug.Log(string.Format("OnTriggerButtonAnimation 3, objX : {0}, disValue : {1}, halfWidth : {2}, ranAngle : {3}",
                                    //_moveButtonTrans.localPosition.x, disValue, halfWidth, ranAngle));
        }
        else if (_moveButtonTrans.localPosition.y - disValue <= -halfHeight + downGap)
        {
            ranAngle = Random.Range(1, 180);
            //Debug.Log(string.Format("OnTriggerButtonAnimation 4, objY : {0}, disValue : {1}, halfHeight : {2}, ranAngle : {3}",
                                    //_moveButtonTrans.localPosition.y, disValue, halfHeight, ranAngle));
        } else {
            ranAngle = Random.Range(0, 360);
        }

        //Debug.Log(string.Format("ranAngle : {0}", ranAngle));
        targetPos = CAMathCtl.Instance.GetPosByAngle(_moveButtonTrans.localPosition, disValue, ranAngle);

        AddObjectMoveAni(_moveButtonID, targetPos, _moveButtonID, 1f);

        //AddObjectMoveAni (_moveButtonID, targetTrans.localPosition, _moveButtonID, 1f);
    }

	protected override void OnChangeMoveValue(int motionID, ObjectMotionManager.MotionType motionType, Vector2 moveValue)
	{
//		Debug.Log (string.Format ("OnChangeMoveValue motionID : {0}, moveValue : {1}", motionID, moveValue));
		RefreshButtonPos();
		base.OnChangeMoveValue (motionID, motionType, moveValue);
	}

	#endregion
}
