﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StopTimeObjectMatchCatDogStage : StopTimeObjectMatchBaseStage
{
    #region Definitions

    public enum MatchStep
    {
        Normal = 0,
        MeatKitty,
        FeedKitty,
        Completed,
    }

    public enum CatchMoveObjIndex
    {
        Can         = 0,
        Doll        = 1,
        Meat        = 2,
        Feed        = 3,
    }

    public enum TouchedObjectIndex
    {
        None        = -1,
        Cat         = 0,
        Dog         = 1,
        KittyCat    = 2,
    }

    #endregion

    #region Serialize Variables

#pragma warning disable 649

    [Header("Move Objects")]
    [SerializeField] tk2dSprite _canSprite;
    [SerializeField] Transform _canTrans;

    [SerializeField] tk2dSprite _dollSprite;
    [SerializeField] Transform _dollTrans;

    [SerializeField] tk2dSprite _meatSprite;
    [SerializeField] Transform _meatTrans;

    [SerializeField] tk2dSprite _feedNormalSprite;
    [SerializeField] Transform _feedNormalTrans;

    [Header("Dog Objects")]
    [SerializeField] GameObject _dogNormalObj;
    [SerializeField] GameObject _dogRefuseObj;

    [Header("Cat Objects")]
    [SerializeField] GameObject _catNormalObj;
    [SerializeField] GameObject _catHeadNormalObj;
    [SerializeField] GameObject _catRefuseObj;

    [Header("Kitty Cat Objects")]
    [SerializeField] GameObject _kittyNormalObj;
    [SerializeField] GameObject _kittyRefuseObj;
    [SerializeField] GameObject _kittyAniObj_1;
    [SerializeField] GameObject _kittyAniObj_2;

    [Header("Success Objects")]
    [SerializeField] GameObject _catDogDancingObj;
    [SerializeField] GameObject _kittyCatSuccessObj;

    [Header("Matching Objects")]
    [SerializeField] Transform _catMatchingTrans;
    [SerializeField] Transform _dogMatchingTrans;
    [SerializeField] Transform _kittyMatchingTrans;
#pragma warning restore 649

    #endregion

    #region Variables

    MatchStep _curMatchStep = MatchStep.Normal;
    TouchedObjectIndex _curTouchedObjIndex = TouchedObjectIndex.None;
    //Dictionary<int /* Move Obj Index */, Vector3> _moveObjStartPositions = new Dictionary<int, Vector3>(); 

    #endregion

    #region MonoBehaviour Methods

    protected override void Awake()
    {
        base.Awake();

        AddStopTimeObjectInfo(StageObjectType.CatchMove, (int)CatchMoveObjIndex.Can, _canTrans, _canSprite, _canTrans.localPosition);
        AddStopTimeObjectInfo(StageObjectType.CatchMove, (int)CatchMoveObjIndex.Doll, _dollTrans, _dollSprite, _dollTrans.localPosition);
        AddStopTimeObjectInfo(StageObjectType.CatchMove, (int)CatchMoveObjIndex.Meat, _meatTrans, _meatSprite, _meatTrans.localPosition);
        AddStopTimeObjectInfo(StageObjectType.CatchMove, (int)CatchMoveObjIndex.Feed, _feedNormalTrans, _feedNormalSprite, _feedNormalTrans.localPosition);

        //_moveObjStartPositions.Add(0, _canTrans.localPosition));
    }

    #endregion

    #region Methods

    public override void SetStartPlayData()
    {
        base.SetStartPlayData();

        StartCoroutine(OnUpdate());
    }

    protected override void SetCheckValidStep()
    {

        //if (_curCatchObjectInfo.ObjectInfo.ObjectIndex == _matchIndex)
        //{
        //    SetCurMatchingStep(StopTimeMatchStep.MatchSuccess);
        //}
        //else
        //{
        //    SetCurMatchingStep(StopTimeMatchStep.MatchFailed);
        //}
    }

    public override void TouchButtonObj(int buttonIndex)
    {
        if (_curMatchStep == MatchStep.Completed)
            return;

        base.TouchButtonObj(buttonIndex);
        Debug.Log(string.Format("TouchButtonObj buttonIndex : {0}", buttonIndex));

        SetRefuseObj(buttonIndex);

        //_eventTimer.

        //if (!_stopTimeButtons[buttonIndex].IsPushedButton)
        //{
        //    _stopTimeButtons[buttonIndex].StandButtonObj.SetActive(false);
        //    _stopTimeButtons[buttonIndex].PushedButtonObj.SetActive(true);
        //    _stopTimeButtons[buttonIndex].IsPushedButton = true;
        //}
        //else
        //{
        //    _stopTimeButtons[buttonIndex].StandButtonObj.SetActive(true);
        //    _stopTimeButtons[buttonIndex].PushedButtonObj.SetActive(false);
        //    _stopTimeButtons[buttonIndex].IsPushedButton = false;
        //}
    }

    public override int CheckStopTimeButton(int touchID, float posX, float posY)
    {
        if (_curMatchStep == MatchStep.Completed)
        {
            return base.CheckStopTimeButton(touchID, posX, posY);
        }
        else
        {
            for (int i = 0; i < _stopTimeButtons.Length; i++)
            {
                if (_stopTimeButtons[i].CheckValidTouchArea(posX, posY))
                {
                    return _stopTimeButtons[i].ButtonIndex;
                }
            }
        }

        return -1;
    }

    void SetRefuseObj(int buttonIndex)
    {
        bool isRefuse = true;
        switch ((TouchedObjectIndex)buttonIndex)
        {
            case TouchedObjectIndex.Cat:
                _catHeadNormalObj.SetActive(false);
                _catRefuseObj.SetActive(true);
                break;
            case TouchedObjectIndex.Dog:
                _dogNormalObj.SetActive(false);
                _dogRefuseObj.SetActive(true);
                break;
            case TouchedObjectIndex.KittyCat:
                if (_curMatchStep == MatchStep.Normal)
                {
                    _kittyNormalObj.SetActive(false);
                    _kittyRefuseObj.SetActive(true);
                }
                else
                {
                    isRefuse = false;
                }
                break;
        }

        _curCatchObjectInfo.TouchID = -1;
        _curCatchObjectInfo.ObjectInfo = null;

        if (isRefuse)
        {
            _eventTimer.SetGameTimerData(OnRefuseObj, 0.5f, buttonIndex);
        }
    }

    protected override bool CheckObjectDisMatching(StopTimeObjectInfo matchObjectInfo)
    {
        float validDis = 150f;
        Vector3 catPosition = _catMatchingTrans.localPosition;

        float calcDis = CAMathCtl.Instance.GetPointDistance(matchObjectInfo.ObjTransform.localPosition.x,
            matchObjectInfo.ObjTransform.localPosition.y, catPosition.x, catPosition.y);

        Debug.Log(string.Format("CheckCatMatching calcDis : {0}", calcDis));

        if (calcDis < validDis)
        {
            Debug.Log(string.Format("CheckCatMatching Matching Success!!!!"));
            //SetCurMatchingStep(StopTimeMatchStep.Matching);
            SetRefuseObj((int)TouchedObjectIndex.Cat);
            matchObjectInfo.ObjTransform.localPosition = matchObjectInfo.ObjStartPos;
            return true;
        }

        float catValidDis = 150f;
        Vector3 dogObjPos = _dogMatchingTrans.localPosition;

        float calcDogDis = CAMathCtl.Instance.GetPointDistance(matchObjectInfo.ObjTransform.localPosition.x,
            matchObjectInfo.ObjTransform.localPosition.y, dogObjPos.x, dogObjPos.y);

        if (calcDogDis < catValidDis)
        {
            Debug.Log(string.Format("CheckObjectDisMatching Dog DisMatching!!!!"));
            //SetCurMatchingStep(StopTimeMatchStep.MatchFailed);
            SetRefuseObj((int)TouchedObjectIndex.Dog);
            matchObjectInfo.ObjTransform.localPosition = matchObjectInfo.ObjStartPos;
            return true;
        }

        float kittyValidDis = 120f;
        Vector3 kittyObjPos = _kittyMatchingTrans.localPosition;

        float calcKittyDis = CAMathCtl.Instance.GetPointDistance(matchObjectInfo.ObjTransform.localPosition.x,
            matchObjectInfo.ObjTransform.localPosition.y, kittyObjPos.x, kittyObjPos.y);

        if (calcKittyDis < kittyValidDis)
        {
            Debug.Log(string.Format("CheckObjectDisMatching Kitty DisMatching!!!!"));
            //SetCurMatchingStep(StopTimeMatchStep.MatchFailed);
            if(matchObjectInfo.ObjectIndex == (int)CatchMoveObjIndex.Meat)
            {
                SetCatchMoveObjEnable(matchObjectInfo.ObjectIndex, false);
                if(_curMatchStep == MatchStep.Normal)
                {
                    SetCurMatchStep(MatchStep.MeatKitty);
                }
                else
                {
                    SetCurMatchStep(MatchStep.Completed);
                }
            } 
            else if(matchObjectInfo.ObjectIndex == (int)CatchMoveObjIndex.Feed)
            {
                SetCatchMoveObjEnable(matchObjectInfo.ObjectIndex, false);
                if (_curMatchStep == MatchStep.Normal)
                {
                    SetCurMatchStep(MatchStep.FeedKitty);
                }
                else
                {
                    SetCurMatchStep(MatchStep.Completed);
                }
            }
            else
            {
                SetRefuseObj((int)TouchedObjectIndex.KittyCat);
                matchObjectInfo.ObjTransform.localPosition = matchObjectInfo.ObjStartPos;
            }
            return true;
        }

        _curCatchObjectInfo.TouchID = -1;
        _curCatchObjectInfo.ObjectInfo = null;

        return false;
    }

    void SetCurMatchStep(MatchStep curStep)
    {
        _curMatchStep = curStep;
        switch (_curMatchStep)
        {
            case MatchStep.MeatKitty:
                _kittyNormalObj.SetActive(false);
                _kittyAniObj_1.SetActive(true);
                break;
            case MatchStep.FeedKitty:
                _kittyNormalObj.SetActive(false);
                _kittyAniObj_2.SetActive(true);
                break;
            case MatchStep.Completed:
                _kittyAniObj_1.SetActive(false);
                _kittyAniObj_2.SetActive(false);
                _kittyCatSuccessObj.SetActive(true);

                _dogNormalObj.SetActive(false);
                _catNormalObj.SetActive(false);
                _catDogDancingObj.SetActive(true);

                _canTrans.gameObject.SetActive(false);
                _dollTrans.gameObject.SetActive(false);

                _curMatchingStep = StopTimeMatchStep.MatchCompleted;
                SetSolveButton();
                break;
        }

        _curCatchObjectInfo.TouchID = -1;
        _curCatchObjectInfo.ObjectInfo = null;
    }

    #endregion

    #region Coroutine Methods

    public IEnumerator OnUpdate()
    {
        while (true)
        {
            _eventTimer.UpdateGameTimer();
            yield return null;
        }
    }

    #endregion

    #region CallBack Methods

    void OnRefuseObj(object objData)
    {
        int buttonIndex = (int)objData;

        switch ((TouchedObjectIndex)buttonIndex)
        {
            case TouchedObjectIndex.Cat:
                if (_curMatchStep != MatchStep.Completed)
                    _catHeadNormalObj.SetActive(true);
                _catRefuseObj.SetActive(false);
                break;
            case TouchedObjectIndex.Dog:
                if(_curMatchStep != MatchStep.Completed)
                    _dogNormalObj.SetActive(true);
                _dogRefuseObj.SetActive(false);
                break;
            case TouchedObjectIndex.KittyCat:
                if(_curMatchStep == MatchStep.Normal)
                    _kittyNormalObj.SetActive(true);
                _kittyRefuseObj.SetActive(false);
                break;
        }
    }

    #endregion
}
