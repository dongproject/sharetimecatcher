﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StopTimeRanTimePosStage : StopTimeBaseStage
{
    #region Definitions

    public enum TimeRanPosStep
    {
        None,
        Start,
        StartDelay,
        Hide,
        HideDelay,
        Show,
        ShowDelay,
        Finish,
    }

    #endregion

    #region Serialize Variables

#pragma warning disable 649

    [SerializeField] GameObject _drawingSmokeObj;
    [SerializeField] Transform[] _timeObjTrans;
    [SerializeField] Transform _ninjaTrans;

#pragma warning restore 649

    #endregion

    #region Variables

    TimeRanPosStep _ranPosStep = TimeRanPosStep.None;
    List<List<GameObject>> _hideGroupObjects = new List<List<GameObject>>();
    List<GameObject> _smokeObjects = new List<GameObject>();

    #endregion

    #region Properties

    #endregion

    #region MonoBehaviour Methods

    #endregion

    #region Methods

    public override void SetStartPlayData()
    {
        base.SetStartPlayData();

        SetTimeRanPosStep(TimeRanPosStep.Start);

        StartCoroutine(OnUpdate());
    }

    void SetTimeRanPosStep(TimeRanPosStep posStep)
    {
        _ranPosStep = posStep;

        switch (_ranPosStep)
        {
            case TimeRanPosStep.Start:
                _eventTimer.SetGameTimerData(OnFinishDelayTime, 2f, TimeRanPosStep.StartDelay);
                SetTimeRanPosStep(TimeRanPosStep.StartDelay);
                break;
            case TimeRanPosStep.Hide:
                SetHideObjects();
                break;
            case TimeRanPosStep.Show:
                SetShowObjects();
                break;
            case TimeRanPosStep.Finish:
                _eventTimer.ReleaseGameTimerList();
                break;
        }
    }

    void SetHideObjects()
    {
        int swapCount = 3;
        List<Transform> timeTrans = new List<Transform>();
        for (int i = 0; i < _timeObjTrans.Length;i++){
            timeTrans.Add(_timeObjTrans[i]);
        }

        _hideGroupObjects.Clear();
        for (int i = 0; i < swapCount;i++){
            if(i == 0){
                int ranPosIndex = Random.Range(0, timeTrans.Count);
                //Vector3 tempPosition = timeTrans[ranPosIndex].localPosition;
                //timeTrans[ranPosIndex].localPosition = new Vector3(_ninjaTrans.localPosition.x,
                //                                                   _ninjaTrans.localPosition.y,
                //                                                   timeTrans[ranPosIndex].localPosition.z);

                //_ninjaTrans.localPosition = new Vector3(tempPosition.x, tempPosition.y,
                //_ninjaTrans.localPosition.z);

                //_hideObjects.Add(_ninjaTrans.gameObject);
                //_hideObjects.Add(timeTrans[ranPosIndex].gameObject);

                List<GameObject> inputGroupObject = new List<GameObject>();
                inputGroupObject.Add(_ninjaTrans.gameObject);
                inputGroupObject.Add(timeTrans[ranPosIndex].gameObject);

                _hideGroupObjects.Add(inputGroupObject);

                timeTrans.RemoveAt(ranPosIndex);
            } else {
                int ranFirstPosIndex = Random.Range(0, timeTrans.Count);
                int ranSecondPosIndex = 0;
                int exceptCount = 0;
                while(true){
                    ranSecondPosIndex = Random.Range(0, timeTrans.Count);
                    if (ranSecondPosIndex != ranFirstPosIndex){
                        break;
                    }

                    exceptCount++;
                    if(exceptCount > 1000){
                        Debug.Log(string.Format("ExceptCount exceptCount > 1000"));
                        break;
                    }
                }

                //Vector3 tempPosition = timeTrans[ranFirstPosIndex].localPosition;
                //timeTrans[ranFirstPosIndex].localPosition = new Vector3(timeTrans[ranSecondPosIndex].localPosition.x,
                //                                                   timeTrans[ranSecondPosIndex].localPosition.y,
                //                                                   timeTrans[ranFirstPosIndex].localPosition.z);

                //timeTrans[ranSecondPosIndex].localPosition = new Vector3(tempPosition.x, tempPosition.y,
                //timeTrans[ranSecondPosIndex].localPosition.z);

                //_hideObjects.Add(timeTrans[ranFirstPosIndex].gameObject);
                //_hideObjects.Add(timeTrans[ranSecondPosIndex].gameObject);

                List<GameObject> inputGroupObject = new List<GameObject>();
                inputGroupObject.Add(timeTrans[ranFirstPosIndex].gameObject);
                inputGroupObject.Add(timeTrans[ranSecondPosIndex].gameObject);

                List<Transform> delTimeTrans = new List<Transform>();
                delTimeTrans.Add(timeTrans[ranFirstPosIndex]);
                delTimeTrans.Add(timeTrans[ranSecondPosIndex]);

                _hideGroupObjects.Add(inputGroupObject);

                for (int j = 0; j < delTimeTrans.Count;j++){
                    timeTrans.Remove(delTimeTrans[j]);
                }
            }
        }

        float ninjaGapY = 40f;
        float timeGapY = 58f;
        for (int i = 0; i < _hideGroupObjects.Count;i++){
            List<GameObject> groupObj = _hideGroupObjects[i];
            if (i == 0) {
                //groupObj[0]
                Vector3 ninjaSmokePos = new Vector3(groupObj[0].transform.localPosition.x,
                                                   groupObj[0].transform.localPosition.y + ninjaGapY,
                                                    groupObj[0].transform.localPosition.z);

                SetDrawingSmoke(ninjaSmokePos);
                Vector3 timeSmokePos = new Vector3(groupObj[1].transform.localPosition.x,
                                                  groupObj[1].transform.localPosition.y + timeGapY,
                                                  groupObj[1].transform.localPosition.z);
                SetDrawingSmoke(timeSmokePos);

            } else {
                for (int j = 0; j < groupObj.Count;j++){
                    Vector3 timeSmokePos = new Vector3(groupObj[j].transform.localPosition.x,
                                                  groupObj[j].transform.localPosition.y + timeGapY,
                                                  groupObj[j].transform.localPosition.z);
                    SetDrawingSmoke(timeSmokePos);
                }
            }
        }

        _eventTimer.SetGameTimerData(OnAppearSmoke, 0.3f);
        _eventTimer.SetGameTimerData(OnFinishDelayTime, 0.5f, TimeRanPosStep.HideDelay);
        SetTimeRanPosStep(TimeRanPosStep.HideDelay);
    }

    void SetDrawingSmoke(Vector3 appearPosition)
    {
        GameObject smokeObj = Instantiate<GameObject>(_drawingSmokeObj);
        smokeObj.SetActive(true);
        smokeObj.transform.SetParent(this.transform);
        smokeObj.transform.localScale = new Vector3(1.2f, 1.2f, 1.2f);
        smokeObj.transform.localPosition = new Vector3(appearPosition.x, appearPosition.y, -10f);

        _smokeObjects.Add(smokeObj);
    }

    void SetShowObjects()
    {
        for (int i = 0; i < _hideGroupObjects.Count; i++)
        {
            List<GameObject> groupObj = _hideGroupObjects[i];
            for (int j = 0; j < groupObj.Count; j++)
            {
                groupObj[j].SetActive(true);
            }
        }

        _eventTimer.SetGameTimerData(OnFinishDelayTime, 2f, TimeRanPosStep.ShowDelay);
        SetTimeRanPosStep(TimeRanPosStep.ShowDelay);
    }

    public override void SetGameEnd()
    {
        base.SetGameEnd();

        SetTimeRanPosStep(TimeRanPosStep.Finish);
    }

    #endregion

    #region Coroutine Methods

    public IEnumerator OnUpdate()
    {
        while (true)
        {
            _eventTimer.UpdateGameTimer();
            yield return null;
        }
    }

    #endregion

    #region CallBack Methods

    void OnFinishDelayTime(object objData)
    {
        TimeRanPosStep posStep = (TimeRanPosStep)objData;

        switch(posStep){
            case TimeRanPosStep.StartDelay:
                SetTimeRanPosStep(TimeRanPosStep.Hide);
                break;
            case TimeRanPosStep.HideDelay:
                SetTimeRanPosStep(TimeRanPosStep.Show);
                break;
            case TimeRanPosStep.ShowDelay:
                SetTimeRanPosStep(TimeRanPosStep.Hide);
                break;
        }
    }

    void OnAppearSmoke(object objData)
    {
        for (int i = 0; i < _hideGroupObjects.Count; i++) {
            List<GameObject> groupObj = _hideGroupObjects[i];
            for (int j = 0; j < groupObj.Count;j++){
                groupObj[j].SetActive(false);
            }
            Vector3 tempPos = groupObj[0].transform.localPosition;
            groupObj[0].transform.localPosition = new Vector3(groupObj[1].transform.localPosition.x,
                                                              groupObj[1].transform.localPosition.y,
                                                              groupObj[0].transform.localPosition.z);

            groupObj[1].transform.localPosition = new Vector3(tempPos.x, tempPos.y,
                                                              groupObj[1].transform.localPosition.z);
        }

        for (int i = 0; i < _smokeObjects.Count;i++){
            Destroy(_smokeObjects[i]);
        }

        _smokeObjects.Clear();


    }

    #endregion
}
