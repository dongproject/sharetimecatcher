﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StopTimeFakeMultipleTimeStage : StopTimeBaseStage 
{
	#region Serialize Variables

	[SerializeField] StopTimeViewObject[] _fakeTimeViewObjects;
	[SerializeField] int _realTimePosIndex;
	[SerializeField] bool _isRandomPos;
    [SerializeField] Transform _cheerTrans;

	#endregion

	#region Variables

	float _gapTimeY = 90f;
	int _realTimeCount;
    float _startCheerPosY = 392f;
    int _curRealTargetIndex;

	#endregion

	#region Properties

	public StopTimeViewObject[] FakeTimeViewObjects
	{
		get{ return _fakeTimeViewObjects; }
	}

	#endregion

	#region MonoBehaviour Methods

	protected override void Awake()
	{
		base.Awake ();
		_realTimeCount = _fakeTimeViewObjects.Length + 1;
        if (_isRandomPos){
            RandomFakeTimePos();
            SetCheerObjPos();
        }
	}

	#endregion

	#region Methods

	void RandomFakeTimePos()
	{
		List<int> timePosIndex = new List<int> ();
		int exceptionCount = 0;

		while (timePosIndex.Count < _realTimeCount) {
			int ranPosValue = Random.Range (0, _realTimeCount);
			if (!timePosIndex.Contains (ranPosValue)) {
				timePosIndex.Add (ranPosValue);
			}

			exceptionCount++;
			if (exceptionCount > 10000) {
				Debug.Log (string.Format ("RandomFakeTimePos exceptionCount over!!!"));
				return;
			}
		}

		float startPosY = 270f;
		for (int i = 0; i < timePosIndex.Count; i++) {
			if (timePosIndex [i] == _realTimeCount - 1) {
                _curRealTargetIndex = i;
                RealTimeViewObject.transform.localPosition = new Vector3 (0f, startPosY, 0f);
			} else {
				_fakeTimeViewObjects [timePosIndex [i]].transform.localPosition = new Vector3 (0f, startPosY, 0f);
			}

			startPosY -= _gapTimeY;
		}
	}

    void SetCheerObjPos()
    {
        int ranCheerIndex = -1;
        for (int i = 0; i < 100;i++){
            int ranValue = Random.Range(0, _realTimeCount);
            if(ranValue != _curRealTargetIndex){
                ranCheerIndex = ranValue;
                break;
            }
        }

        if(ranCheerIndex == -1){
            if(_curRealTargetIndex == 0){
                ranCheerIndex = 1;
            } else {
                ranCheerIndex = _curRealTargetIndex - 1;
            }
        }

        float gapCheerY = _gapTimeY * ranCheerIndex;
        float cheerObjPosY = _startCheerPosY - gapCheerY;

        _cheerTrans.localPosition = new Vector3(_cheerTrans.localPosition.x, cheerObjPosY,
                                               _cheerTrans.localPosition.z);
    }

	#endregion
}
