﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StopTimeMathStage : StopTimeBaseStage
{
    #region Serialize Variables

    [SerializeField] Transform _angelTrans;
    [SerializeField] GameObject _X_Text_Obj;
    [SerializeField] GameObject _Y_Text_Obj;

    #endregion

    #region Variables

    #endregion

    #region MonoBehaviour Methods

    protected override void Awake()
    {
        base.Awake();
    }

    #endregion

    #region Methods

    #endregion
}
