﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum StopTimeHitObjectStep
{
	HitObjectStand,
	HitObjectLeftHit,
	HitObjectRightHit,
	HitObjectCrying,
	HitObjectClear,
}

public class StopTimeHitObjectStage : StopTimeBaseStage 
{
    #region Serialize Variables

#pragma warning restore 649

    [Header("Hit Target Objects")]
	[SerializeField] GameObject _hitObjectStandObject;
	[SerializeField] GameObject _hitObjectLeftHitObject;
	[SerializeField] GameObject _hitObjectRightHitObject;
	[SerializeField] GameObject _hitObjectCryObject;
	[SerializeField] GameObject _hitClearObject;

	[Header("Gauge Objects")]
	[SerializeField] tk2dSlicedSprite _gaugeSlicedBG;
	[SerializeField] tk2dSlicedSprite _gaugeSlicedSprite;

    [Header("Hit Objects")]
    [SerializeField] GameObject[] _hitObjects;
    [SerializeField] BoxCollider _hitObjTouchArea;

#pragma warning restore 649

    #endregion

    #region Variables

    StopTimeHitObjectStep _curHitObjectStep = StopTimeHitObjectStep.HitObjectStand;
	StopTimeHitObjectStep _hitObjectLastHit = StopTimeHitObjectStep.HitObjectRightHit;

	int _hitObjectTotalHP = 50;
	int _hitCount = 1;
	int _curHitObjectHP = 50;

	float _hitStandTime = 0.2f;
	float _curHitStandTime = 0f;

	float _gaugeMaxWidth;

	float _cryingTime = 2f;
	float _curCryingTime = 0f;

    int _curHitObjIndex = 0;

    float _gangStartX;
    float _gangEndX;
    float _gangStartY;
    float _gangEndY;

	#endregion

	#region Properties

	public int HitObjectTotalHP
	{
		get{ return _hitObjectTotalHP; }
		set{ _hitObjectTotalHP = value; }
	}

	public int HitCount
	{
		get{ return _hitCount; }
		set{ _hitCount = value; }
	}

	public int CurHitObjectHP
	{
		get{ return _curHitObjectHP; }
		set{ _curHitObjectHP = value; }
	}

	#endregion

	#region MonoBehaviour Methods

	// Use this for initialization
	protected override void Start ()
	{
		base.Start ();
		_gaugeMaxWidth = _gaugeSlicedBG.dimensions.x;
		SetHitObjectGaugeValue (1f);
		StartCoroutine (UpdateHitObjectStage ());

        SetGangTouchArea();

    }

    void SetGangTouchArea()
    {
        _gangStartX = _hitObjTouchArea.transform.localPosition.x - (_hitObjTouchArea.size.x * 0.5f);
        _gangEndX = _hitObjTouchArea.transform.localPosition.x + (_hitObjTouchArea.size.x * 0.5f); ;
        _gangStartY = _hitObjTouchArea.transform.localPosition.y - (_hitObjTouchArea.size.y * 0.5f);
        _gangEndY = _hitObjTouchArea.transform.localPosition.y + (_hitObjTouchArea.size.y * 0.5f);

        _gangStartX = CAResolutionCtl.Instance.GetImageToScreenPosX(_gangStartX);
        _gangEndX = CAResolutionCtl.Instance.GetImageToScreenPosX(_gangEndX);
        _gangStartY = CAResolutionCtl.Instance.GetImageToScreenPosY(_gangStartY);
        _gangEndY = CAResolutionCtl.Instance.GetImageToScreenPosY(_gangEndY);
    }

    // Update is called once per frame
    //	void Update ()
    //	{
    //		switch (_curHitObjectStep) {
    //		case StopTimeHitObjectStep.HitObjectLeftHit:
    //		case StopTimeHitObjectStep.HitObjectRightHit:
    //			CalcHitBoxerStayTime ();
    //			break;
    //		case StopTimeHitObjectStep.HitObjectCrying:
    //			_curCryingTime += Time.deltaTime;
    //			if (_curCryingTime >= _cryingTime) {
    //				SetCurrentHitObjectStep (StopTimeHitObjectStep.HitObjectClear);
    //			}
    //			break;
    //		}
    //	}

    #endregion

    #region Coroutine Methods

    IEnumerator UpdateHitObjectStage()
	{
		while(true)
		{
			switch (_curHitObjectStep) {
			case StopTimeHitObjectStep.HitObjectLeftHit:
			case StopTimeHitObjectStep.HitObjectRightHit:
				CalcHitBoxerStayTime ();
				break;
			case StopTimeHitObjectStep.HitObjectCrying:
				_curCryingTime += Time.deltaTime;
				if (_curCryingTime >= _cryingTime) {
					SetCurrentHitObjectStep (StopTimeHitObjectStep.HitObjectClear);
				}
				break;
			}

			yield return null;
		}
	}

	#endregion

	#region Methods

	public override int CheckStopTimeButton(int touchID, float posX, float posY)
	{
		//if (_curHitObjectStep != StopTimeHitObjectStep.HitObjectClear)
			//return -1;

		return base.CheckStopTimeButton(touchID, posX, posY);
	}

	public override void OnTouchPress(int touchID, float posX, float posY)
	{
        if(CheckValidObject(posX, posY))
        {
            switch (_curHitObjectStep)
            {
                case StopTimeHitObjectStep.HitObjectStand:
                case StopTimeHitObjectStep.HitObjectLeftHit:
                case StopTimeHitObjectStep.HitObjectRightHit:
                    HitBoxer();
                    break;
            }
        }
	}

    bool CheckValidObject(float posX, float posY)
    {
        if((_gangStartX <= posX && _gangEndX >= posX) && 
            (_gangStartY <= posY && _gangEndY >= posY))
        {
            return true;
        }

        return false;
    }

    void HitBoxer()
	{
		_curHitStandTime = 0f;
		if (_hitObjectLastHit == StopTimeHitObjectStep.HitObjectLeftHit) {
			SetCurrentHitObjectStep (StopTimeHitObjectStep.HitObjectRightHit);
		} else {
			SetCurrentHitObjectStep (StopTimeHitObjectStep.HitObjectLeftHit);
		}

		_curHitObjectHP -= _hitCount;
		if (_curHitObjectHP < 0) {
			_curHitObjectHP = 0;
			SetCurrentHitObjectStep (StopTimeHitObjectStep.HitObjectCrying);
		}

		SetHitObjectGaugeValue ((float)_curHitObjectHP / (float)_hitObjectTotalHP);

        for (int i = 0; i < _hitObjects.Length;i++){
            if(_curHitObjIndex == i){
                SetHitSound(i);
                _hitObjects[i].SetActive(true);
            } else {
                _hitObjects[i].SetActive(false);
            }
        }

        _curHitObjIndex++;
        if (_curHitObjIndex >= _hitObjects.Length)
            _curHitObjIndex = 0;
    }

    void SetHitSound(int index)
    {
        if(index == 0)
        {
            SoundManager.Instance.PlayEffectFX(SoundDefinitions.effectSound_11);
        } 
        else if(index == 1)
        {
            SoundManager.Instance.PlayEffectFX(SoundDefinitions.effectSound_12);
        }
        else if (index == 2)
        {
            SoundManager.Instance.PlayEffectFX(SoundDefinitions.effectSound_13);
        }
        else if (index == 3)
        {
            SoundManager.Instance.PlayEffectFX(SoundDefinitions.effectSound_14);
        }
    }

    public void SetCurrentHitObjectStep(StopTimeHitObjectStep boxingStep)
	{
		_curHitObjectStep = boxingStep;
        switch (_curHitObjectStep)
        {
            case StopTimeHitObjectStep.HitObjectStand:
                _hitObjectStandObject.SetActive(true);
                if (_hitObjectLeftHitObject != null)
                    _hitObjectLeftHitObject.SetActive(false);
                if (_hitObjectRightHitObject != null)
                    _hitObjectRightHitObject.SetActive(false);
                _hitObjectCryObject.SetActive(false);
                break;
            case StopTimeHitObjectStep.HitObjectLeftHit:
                _hitObjectLastHit = _curHitObjectStep;

                if (_hitObjectLeftHitObject != null)
                {
                    _hitObjectStandObject.SetActive(false);

                    _hitObjectRightHitObject.SetActive(false);
                    _hitObjectLeftHitObject.SetActive(true);
                    //              _hitObjectCryObject.SetActive (false);
                }
                break;
            case StopTimeHitObjectStep.HitObjectRightHit:
                _hitObjectLastHit = _curHitObjectStep;

                if (_hitObjectRightHitObject != null)
                {
                    _hitObjectStandObject.SetActive(false);
                    _hitObjectLeftHitObject.SetActive(false);
                    _hitObjectRightHitObject.SetActive(true);
                    //_hitObjectCryObject.SetActive(false);
                }
                break;
            case StopTimeHitObjectStep.HitObjectCrying:
                SetSolveButton();
                _hitObjectStandObject.SetActive(false);
                if (_hitObjectLeftHitObject != null)
                    _hitObjectLeftHitObject.SetActive(false);
                if (_hitObjectRightHitObject != null)
                    _hitObjectRightHitObject.SetActive(false);
                _hitObjectCryObject.SetActive(true);

                _gaugeSlicedBG.gameObject.SetActive(false);
                _gaugeSlicedSprite.gameObject.SetActive(false);
                break;
            case StopTimeHitObjectStep.HitObjectClear:
                _hitObjectStandObject.SetActive(false);
                if (_hitObjectLeftHitObject != null)
                    _hitObjectLeftHitObject.SetActive(false);
                if (_hitObjectRightHitObject != null)
                    _hitObjectRightHitObject.SetActive(false);
                _hitObjectCryObject.SetActive(false);

                _hitClearObject.SetActive(true);
                break;
        }
    }

	void CalcHitBoxerStayTime()
	{
		_curHitStandTime += Time.deltaTime;
		if (_hitStandTime <= _curHitStandTime) {
			SetCurrentHitObjectStep (StopTimeHitObjectStep.HitObjectStand);
			_curHitStandTime = 0f;
		}
	}

	void SetHitObjectGaugeValue(float gaugeValue)
	{
		_gaugeSlicedSprite.dimensions = new Vector2 (_gaugeMaxWidth * gaugeValue, _gaugeSlicedSprite.dimensions.y); 
	}

	#endregion
}
