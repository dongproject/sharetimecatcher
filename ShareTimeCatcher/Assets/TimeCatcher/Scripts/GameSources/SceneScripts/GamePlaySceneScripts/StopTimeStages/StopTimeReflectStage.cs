﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StopTimeReflectStage : StopTimeBaseStage 
{
	#region Serialize Variables

	[SerializeField] StopTimeViewObject _reflectRealTimeViewObject;

	#endregion

	#region Properties

	public StopTimeViewObject ReflectRealTimeViewObject
	{
		get{ return _reflectRealTimeViewObject; }
	}

	#endregion
}
