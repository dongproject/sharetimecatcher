﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StopTimeCurtainStage : StopTimeBaseStage
{
    #region Definitions

    public enum CurtainStep
    {
        None,
        Start,
        ButtonCurtainTouch,
        DisapperButtonOtherObj,
        Completed,
    }

    #endregion

    #region Serialize Variables

    [SerializeField] UGUITweener[] _curtainTweens;
    [SerializeField] GameObject[] _curtainObjects;
    [SerializeField] UGUITweener[] _buttonOtherTweens;

    #endregion

    #region Variables

    int _buttonIndex = 2;
    bool _isTouchCurtain = true;
    CurtainStep _curStep = CurtainStep.None;

    #endregion

    #region MonoBehaviour Methods

    protected override void Awake()
    {
        base.Awake();
    }

    #endregion

    #region Methods

    public override void InitStopTimeStageData()
    {
        SwapCurtainPosition();
        base.InitStopTimeStageData();
    }

    public override void SetStartPlayData()
    {
        base.SetStartPlayData();

        _curStep = CurtainStep.Start;

        StartCoroutine(OnUpdate());
    }

    void SwapCurtainPosition()
    {
        int swapCount = 10;

        for (int i = 0; i < swapCount; i++)
        {
            int firstIndex = Random.Range(0, 4);
            int secondIndex = Random.Range(4, 9);

            Vector3 tempPosition = _curtainObjects[firstIndex].transform.localPosition;
            _curtainObjects[firstIndex].transform.localPosition = _curtainObjects[secondIndex].transform.localPosition;
            _curtainObjects[secondIndex].transform.localPosition = tempPosition;
        }
    }

    public override int CheckStopTimeButton(int touchID, float posX, float posY)
    {
        if (_curStep != CurtainStep.Completed)
            return -1;

        return base.CheckStopTimeButton(touchID, posX, posY);
    }

    #endregion

    #region CallBack Methods

    public override void OnButtonTouched(int buttonID)
    {
        Debug.Log(string.Format("OnButtonTouched buttonID : {0}", buttonID));

        if (!_isTouchCurtain)
            return;

        if (_curStep != CurtainStep.Start)
            return;

        if(buttonID == _buttonIndex)
        {
            _curStep = CurtainStep.ButtonCurtainTouch;

        }

        SoundManager.Instance.PlayEffectFX(SoundDefinitions.effectSound_15);
        UGUITweener touchTween = _curtainTweens[buttonID];
        _eventTimer.SetGameTimerData(OnTouchCurtainDelayTime, 1f, touchTween);
        touchTween.PlayForward();
        touchTween.enabled = true;
        _isTouchCurtain = false;
    }

    void OnTouchCurtainDelayTime(object objData)
    {
        UGUITweener touchTween = objData as UGUITweener;
        if(touchTween != null){
            _isTouchCurtain = true;

            if(_curStep == CurtainStep.Start) {
                touchTween.PlayReverse();
                touchTween.enabled = true;
            } else if(_curStep == CurtainStep.ButtonCurtainTouch){
                _eventTimer.SetGameTimerData(OnDelayTime, 0.1f);
            }
        }
    }

    void OnDelayTime(object objData)
    {
        _curStep = CurtainStep.DisapperButtonOtherObj;
        for (int i = 0; i < _buttonOtherTweens.Length;i++){
            _buttonOtherTweens[i].enabled = true;
        }
    }

    public void OnDisappearButtonOtherObj()
    {
        SetSolveButton();
        _curStep = CurtainStep.Completed;
        RefreshButtonPos();
    }

    #endregion

    #region Coroutine Methods

    public IEnumerator OnUpdate()
    {
        while (true)
        {
            _eventTimer.UpdateGameTimer();
            yield return null;
        }
    }

    #endregion


}
