﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum StopTimeMagnifyStep
{
	None = 0,
	SearchCompleted,
}

public class StopTimeMagnifyStage : StopTimeBaseStage, IMagnifyRenderInfo
{
	#region Serialize Variables

	[SerializeField] ShaderMagnifyRenderer _magnifyRenderer;

	[SerializeField] MeshRenderer _tempMeshRender;
	[SerializeField] tk2dSpriteFromTexture _tempTexture;

	[SerializeField] GameObject _uiMagnifyObject;

	[SerializeField] GameObject _magnifyObjectRoot;
    [SerializeField] Transform[] _targetObjTrans;

	#endregion

	#region Variables

	float _searchButtonPosX = -134f;
	float _searchButtonPosY = 256f;

	StopTimeMagnifyStep _curMagnifyStep = StopTimeMagnifyStep.None;

    #endregion

    #region Properties

    #endregion

    #region MonoBehaviour Methods

    protected override void Awake()
    {
        base.Awake();
        SwapPosition();

    }

    protected override void Start()
	{
		base.Start ();
        SetMagnifyScopeData ();
	}

    #endregion

    #region Methods

    public override void SetStartPlayData()
    {
        base.SetStartPlayData();

        RefreshStopButtonObj();
    }

    void SwapPosition()
    {
        int objLength = _targetObjTrans.Length;
        Vector3[] _trapObjPos = new Vector3[objLength];

        for (int i = 0; i < _targetObjTrans.Length;i++){
            _trapObjPos[i] = _targetObjTrans[i].localPosition;
        }

        for (int i = 0; i < 20; i++)
        {
            int ranValue = Random.Range(0, objLength);
            int secRanValue = 0;
            for (int j = 0; j < 100; j++)
            {
                secRanValue = Random.Range(0, objLength);
                if (secRanValue != ranValue)
                    break;
            }

            if (secRanValue == ranValue)
            {
                if (ranValue == objLength - 1)
                    secRanValue = 0;
                else
                    secRanValue = ranValue + 1;
            }

            Vector3 tempPos = _trapObjPos[ranValue];
            _trapObjPos[ranValue] = _trapObjPos[secRanValue];
            _trapObjPos[secRanValue] = tempPos;
        }

        for (int i = 0; i < _targetObjTrans.Length; i++)
        {
            _targetObjTrans[i].localPosition = _trapObjPos[i];
        }
    }

    void RefreshStopButtonObj()
    {
        _searchButtonPosX = _targetObjTrans[0].localPosition.x;
        _searchButtonPosY = _targetObjTrans[0].localPosition.y;

        RefreshButtonPos();
    }

    public void SetMagnifyScopeData()
	{
		_magnifyRenderer.ScopeRatio = 1f;
		_magnifyRenderer.SetupScoping();
		_magnifyRenderer.UIMagnifyTrans = _uiMagnifyObject.transform;

		_magnifyRenderer.MagnifyRenderInfo = this as IMagnifyRenderInfo;
	}

	public override void OnTouchMoveValues(int touchID, float moveX, float moveY)
	{
		//if (_curMagnifyStep != StopTimeMagnifyStep.None)
			//return;

		_magnifyRenderer.ScopeMove (moveX, moveY);
	}

	public override void OnTouchRelease(int touchID, float posX, float posY)
	{
		//if (_curMagnifyStep != StopTimeMagnifyStep.None)
			//return;

		_magnifyRenderer.CheckValidButton ();
	}

	public override int CheckStopTimeButton(int touchID, float posX, float posY)
	{
        if (_curMagnifyStep == StopTimeMagnifyStep.None)
        {
            if (_curTouchID == touchID)
            {
                if (_startButton != null && _stopButton != null)
                {
                    _startButton.SetActive(true);
                    _stopButton.SetActive(false);
                }
                _curTouchID = -1;
            }
            return -1;
        }

        Vector2 scopePos = _magnifyRenderer.GetScopeUIPos();
        if (!CheckValidButtonTouchArea(scopePos.x, scopePos.y))
            return -1;

		return base.CheckStopTimeButton(touchID, posX, posY);
	}

	public override void OnFinishedPlay()
	{
		_magnifyObjectRoot.SetActive (false);
	}

	#endregion

	#region IMagnifyRenderInfo

	void IMagnifyRenderInfo.OnMagnifyPos (float posX, float posY)
	{
		if (_curMagnifyStep == StopTimeMagnifyStep.None)
			return;

		float validPos = 50f;

		float calcDis = CAMathCtl.Instance.GetPointDistance (posX, posY, _searchButtonPosX, _searchButtonPosY);

		if (calcDis > validPos) {
            _curMagnifyStep = StopTimeMagnifyStep.None;

            //if (_startButton != null && _stopButton != null)
            //{
            //    _startButton.SetActive(true);
            //    _stopButton.SetActive(false);
            //}
            //_curTouchID = -1;

        }
	}

	void IMagnifyRenderInfo.OnCheckPos (float posX, float posY)
	{
		if (_curMagnifyStep == StopTimeMagnifyStep.SearchCompleted)
			return;

		float validPos = 50f;

		float calcDis = CAMathCtl.Instance.GetPointDistance (posX, posY, _searchButtonPosX, _searchButtonPosY);

		if (calcDis <= validPos) {
			_curMagnifyStep = StopTimeMagnifyStep.SearchCompleted;

			//_magnifyRenderer.ScopePosSet (_searchButtonPosX + CAResolutionCtl.Instance.GetResolutionHalfWidth()
				//, _searchButtonPosY + CAResolutionCtl.Instance.GetResolutionHalfHeight());
        }
	}

    bool CheckValidButtonTouchArea(float posX, float posY)
    {
        float validPos = 50f;

        float calcDis = CAMathCtl.Instance.GetPointDistance(posX, posY, _searchButtonPosX, _searchButtonPosY);

        if (calcDis <= validPos)
        {
            return true;
        }

        return false;
    }

	#endregion
}
