﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StopTimeReverseTimeStage : StopTimeBaseStage
{
    #region Serialize Variables

    [SerializeField] UGUITweenPosition _danceObjTweenr;
    [SerializeField] Transform _danceMoveObjTrans;

    #endregion

    #region Variables

    float _leftMaxPos = -300f;
    float _rightMaxPos = 9f;
    float _centerPos = -150f;

    bool _isLeftMove = true;

    #endregion

    #region MonoBehaviour Methods

    protected override void Awake()
    {
        base.Awake();
        _danceObjTweenr.gameObject.SetActive(false);
    }

    #endregion

    #region Methods

    public override void SetStartPlayData()
    {
        base.SetStartPlayData();

        _danceObjTweenr.gameObject.SetActive(true);
    }

    #endregion

    #region CallBack Methods

    public void OnFinishedTween()
    {
        //if(_isLeftMove){
        //    _danceMoveObjTrans.localScale = new Vector3(-1f, 1f, 1f);
        //} else {
            //_danceMoveObjTrans.localScale = new Vector3(-1f, 1f, 1f);
        //}

        Vector3 curObjPos = _danceObjTweenr.transform.localPosition;
        float ranMoveValue = Random.Range(100f, 200f);
        float destPosValue = 0f;
        if(curObjPos.x < _centerPos){
            if(curObjPos.x + ranMoveValue > _rightMaxPos){
                destPosValue = _rightMaxPos;
            } else {
                destPosValue = curObjPos.x + ranMoveValue;
            }

            _danceMoveObjTrans.localScale = new Vector3(-1f, 1f, 1f);
        } else {
            if(curObjPos.x - ranMoveValue < _leftMaxPos){
                destPosValue = _leftMaxPos;
            } else {
                destPosValue = curObjPos.x - ranMoveValue;
            }

            _danceMoveObjTrans.localScale = new Vector3(1f, 1f, 1f);
        }

        _danceObjTweenr.duration = 2f;
        _danceObjTweenr.from = curObjPos;
        _danceObjTweenr.to = new Vector3(destPosValue, curObjPos.y, curObjPos.z);

        _danceObjTweenr.ResetToBeginning();
        _danceObjTweenr.PlayForward();
        _danceObjTweenr.enabled = true;
    }

    #endregion
}
