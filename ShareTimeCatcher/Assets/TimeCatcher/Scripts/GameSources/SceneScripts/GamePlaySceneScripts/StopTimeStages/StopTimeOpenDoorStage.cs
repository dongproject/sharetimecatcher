﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum DoorImgState
{
	Close,
	Open,
	Lock,
}

public class OpenDoorObjectSize : StageBaseObjectInfo
{
	#region Variables

	int _touchID;
	bool _moveState = false;

	#endregion

	#region Properties

	public int TouchID
	{
		get{ return _touchID; }
		set{ _touchID = value; }
	}

	public bool MoveState
	{
		get{ return _moveState; }
		set{ _moveState = value; }
	}

	#endregion
}

public class StopTimeOpenDoorStage : StopTimeBaseStage 
{
	#region Sub Class

	[System.Serializable]
	public class DoorObjectInfo
	{
		#region Serialize Variables

		[SerializeField] GameObject _doorPosObj;
		[SerializeField] tk2dSprite _frameSprite;
		[SerializeField] GameObject _closeDoorObj;
		[SerializeField] GameObject _openDoorObj;
		[SerializeField] Transform _lockPosTrans;
        [SerializeField] BoxCollider _openCollider;
        [SerializeField] BoxCollider _closeCollider;

		#endregion

		#region Variables

		DoorImgState _doorState;
		OpenDoorObjectSize _doorObjectSize = new OpenDoorObjectSize();

		#endregion

		#region Properties

		public DoorImgState DoorState
		{
			get{ return _doorState; }
			set{ 
				_doorState = value;
                switch (_doorState)
                {
                    case DoorImgState.Close:
                        _closeDoorObj.SetActive(true);
                        _openDoorObj.SetActive(false);
                        InitCloseDoorPos();
                        break;
                    case DoorImgState.Open:
                        _closeDoorObj.SetActive(false);
                        _openDoorObj.SetActive(true);
                        InitOpenDoorPos();
                        break;
                    case DoorImgState.Lock:
                        break;
                }
            }
		}

		public GameObject DoorPosObj
		{
			get{ return _doorPosObj; }
		}

		public OpenDoorObjectSize DoorObjectSize
		{
			get{ return _doorObjectSize; }
		}

		public tk2dSprite FrameSprite
		{
			get{ return _frameSprite; }
		}

		public GameObject CloseDoorObj
		{
			get{ return _closeDoorObj; }
		}

		public GameObject OpenDoorObj
		{
			get{ return _openDoorObj; }
		}

		public Transform LockPosTrans
		{
			get{ return _lockPosTrans; }
		}

		#endregion

		#region Methods

		//public void InitDoorObjectInfo()
		//{
		//	_doorObjectSize.ObjWidth = _frameSprite.GetBounds ().size.x;
		//	_doorObjectSize.ObjHeight = _frameSprite.GetBounds ().size.y;
		//	_doorObjectSize.ObjTransform = _doorPosObj.transform;
		//}

        public void InitOpenDoorPos()
        {
            _doorObjectSize.ObjWidth = _openCollider.size.x;
            _doorObjectSize.ObjHeight = _openCollider.size.y;
            _doorObjectSize.ObjTransform = _openCollider.transform;
        }

        public void InitCloseDoorPos()
        {
            _doorObjectSize.ObjWidth = _closeCollider.size.x;
            _doorObjectSize.ObjHeight = _closeCollider.size.y;
            _doorObjectSize.ObjTransform = _closeCollider.transform;
        }

        #endregion
    }

	#endregion

	#region Serialize Variables

	[SerializeField] DoorObjectInfo[] _doorObjects;
	[SerializeField] tk2dSprite _lockSprite;

	#endregion

	#region Variables

	OpenDoorObjectSize _curDoorObject = null;
	OpenDoorObjectSize _lockSpriteSize = null;
	int _lockSpriteID = 3;

	DoorObjectInfo _buttonDoorObject;
	float _lockDefaultZ = -5f;
	float _lockMoveZ = - 20f;

	Vector3 _lockStartPos;

	#endregion

	#region Properties

	#endregion

	#region MonoBehaviour Methods

	protected override void Awake()
	{
		base.Awake ();
		if (_doorObjects != null && _doorObjects.Length > 0) {
			for (int i = 0; i < _doorObjects.Length; i++) {
				//_doorObjects [i].InitDoorObjectInfo ();
				_doorObjects [i].DoorObjectSize.ObjectIndex = i;
				if (i < 2) {
					_doorObjects [i].DoorState = DoorImgState.Open;
				} else {
					_doorObjects [i].DoorState = DoorImgState.Close;
				}
			}

			_buttonDoorObject = _doorObjects [1];
		}

		if (_lockSprite != null) {
			_lockSpriteSize = new OpenDoorObjectSize ();
			_lockSpriteSize.ObjectIndex = _lockSpriteID;
			_lockSpriteSize.ObjTransform = _lockSprite.transform;
			_lockSpriteSize.ObjWidth = _lockSprite.GetBounds ().size.x;
			_lockSpriteSize.ObjHeight = _lockSprite.GetBounds ().size.y;
			_lockSpriteSize.MoveState = true;
			_lockStartPos = _lockSprite.transform.localPosition;
		}
	}

	#endregion

	#region Methods

	public override void OnTouchPress(int touchID, float posX, float posY)
	{
		if (_curDoorObject != null)
			return;

		if (_doorObjects == null || _doorObjects.Length == 0)
			return;
		
		if (_doorObjects [2].DoorState == DoorImgState.Open) {
			if (_lockSpriteSize.CheckValidCollideByScreenPos (posX, posY)) {
				_curDoorObject = _lockSpriteSize;
				_curDoorObject.TouchID = touchID;
				_lockSpriteSize.ObjTransform.localPosition = new Vector3 (_lockSpriteSize.ObjTransform.localPosition.x, _lockSpriteSize.ObjTransform.localPosition.y,
					_lockMoveZ);
			}
		}

		if (_curDoorObject == null) {
			for (int i = 0; i < _doorObjects.Length; i++) {
				if(_doorObjects[i].DoorObjectSize.CheckValidCollideByScreenPos(posX, posY)){
					_curDoorObject = _doorObjects [i].DoorObjectSize;
					_curDoorObject.TouchID = touchID;
					break;
				}
			}
		}

	}

	void CheckLockDoorObject(float posX, float posY)
	{
		bool matchState = false;
		for (int i = 0; i < 3; i++) {
			if (_doorObjects [i].DoorState == DoorImgState.Close) {
				if (_doorObjects [i].DoorObjectSize.CheckValidCollideByImgPos (_lockSpriteSize.ObjTransform.localPosition.x, _lockSpriteSize.ObjTransform.localPosition.y)) {
					_lockSpriteSize.ObjTransform.SetParent (_doorObjects [i].LockPosTrans);
					_lockSpriteSize.ObjTransform.localPosition = Vector3.zero;
					_lockSpriteSize.ObjTransform.localScale = Vector3.one;
					_lockSpriteSize.MoveState = false;
					_doorObjects [i].DoorState = DoorImgState.Lock;
					matchState = true;
					break;
				}
			}
		}

		if (!matchState) {
			_lockSpriteSize.ObjTransform.localPosition = _lockStartPos;
		}
	}

	public override void OnTouchMoveValues(int touchID, float moveX, float moveY)
	{
		if (_curDoorObject == null || !_curDoorObject.MoveState)
			return;

		if (_curDoorObject.TouchID == touchID) {
			Vector3 objPos = _curDoorObject.ObjTransform.localPosition;
			float objHalfWidth = _curDoorObject.ObjWidth * 0.5f;
			float objHalfHeight = _curDoorObject.ObjHeight * 0.5f;
			float objPosX = Mathf.Clamp (objPos.x + moveX, 
				-CAResolutionCtl.Instance.GetResolutionHalfWidth() + objHalfWidth, 
				CAResolutionCtl.Instance.GetResolutionHalfWidth() - objHalfWidth);

			float objPosY = Mathf.Clamp (objPos.y + moveY, 
				-CAResolutionCtl.Instance.GetResolutionHalfHeight() + objHalfHeight, 
				CAResolutionCtl.Instance.GetResolutionHalfHeight() - objHalfHeight);

			//			Debug.Log (string.Format ("OnTouchMoveValues objPosX : {0}, objPosY : {1}", objPosX, objPosY));

			_curDoorObject.ObjTransform.localPosition = new Vector3 (objPosX, objPosY,
				_curDoorObject.ObjTransform.localPosition.z);
		}
	}

	public override void OnTouchRelease(int touchID, float posX, float posY)
	{
		if (_curDoorObject == null)
			return;

		if (_curDoorObject.TouchID == touchID) {
			if (_curDoorObject.ObjectIndex >= 0 && _curDoorObject.ObjectIndex < 3) {
				TouchDoor (_curDoorObject.ObjectIndex);
			} else {
				CheckLockDoorObject (posX, posY);
			}
			_curDoorObject = null;
		}
	}

	void TouchDoor(int index)
	{
		if (_doorObjects [index].DoorState == DoorImgState.Lock)
			return;

        SoundManager.Instance.PlayEffectFX(SoundDefinitions.effectSound_15);

        if (_doorObjects [index].DoorState == DoorImgState.Close) {
			_doorObjects [index].DoorState = DoorImgState.Open;
			int nextIndex;
			if (index + 1 >= _doorObjects.Length) {
				nextIndex = 0;
			} else {
				nextIndex = index + 1;
			}

			bool changeState = false;
			for (int i = nextIndex; i < _doorObjects.Length; i++) {
				if (i == index)
					continue;

				if (_doorObjects [i].DoorState == DoorImgState.Open) {
					_doorObjects [i].DoorState = DoorImgState.Close;
                    if (_doorObjects[i] == _buttonDoorObject)
                    {
                        SetSolveButton();
                    }
                    changeState = true;
					break;
				}
			}

			if (!changeState) {
				for (int i = 0; i < nextIndex; i++) {
					if (i == index)
						continue;

					if (_doorObjects [i].DoorState == DoorImgState.Open) {
						_doorObjects [i].DoorState = DoorImgState.Close;
                        if (_doorObjects[i] == _buttonDoorObject)
                        {
                            SetSolveButton();
                        }
                        changeState = true;
						break;
					}
				}
			}
		} else if(_doorObjects [index].DoorState == DoorImgState.Open){
			_doorObjects [index].DoorState = DoorImgState.Close;
            if(_doorObjects[index] == _buttonDoorObject)
            {
                SetSolveButton();
            }

            for (int i = 0; i < _doorObjects.Length; i++) {
				if (i == index)
					continue;

				if (_doorObjects [i].DoorState == DoorImgState.Close) {
					_doorObjects [i].DoorState = DoorImgState.Open;
					break;
				}
			}
		}
//		if(_curDoorObject.
	}

	public override int CheckStopTimeButton(int touchID, float posX, float posY)
	{
		if (_buttonDoorObject.DoorState == DoorImgState.Open)
			return -1;

		return base.CheckStopTimeButton(touchID, posX, posY);
	}

	#endregion
}
