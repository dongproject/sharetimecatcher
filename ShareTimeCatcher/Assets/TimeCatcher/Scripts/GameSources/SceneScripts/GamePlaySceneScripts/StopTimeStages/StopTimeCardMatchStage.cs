﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StopTimeCardMatchStage : StopTimeBaseStage 
{
	#region Serialize Variables

	[SerializeField] int _buttonCardIndex;
	[SerializeField] int _ranPosChangeCount;
	[SerializeField] GameObject _stopButtonObject;
	[SerializeField] CardMatchInfo[] _cardMatchInfos;
	[SerializeField] Transform[] _cardPosTrans;

	#if UNITY_EDITOR
	[SerializeField] RFButtonManager _buttonManager = null;
	#endif

	#endregion

	#region Variables

	protected bool _isCompleteStage = false;
	int _cardSelectCount = 0;
	CardMatchInfo[] _curSelectMatchCardInfos = new CardMatchInfo[2];
	bool _isEnableCardClick = false;

	#endregion

	#region MonoBehaviour Methods

	protected override void Awake()
	{
		base.Awake ();

		SetRandomCardPos ();

		for (int i = 0; i < _cardMatchInfos.Length; i++) {
			_cardMatchInfos [i].SetCloseCard ();
		}

		for (int i = 0; i < _curSelectMatchCardInfos.Length; i++) {
			_curSelectMatchCardInfos [i] = null;
		}
	}

	#endregion

	#region Methods

	public override void SetStartPlayData()
	{
		base.SetStartPlayData ();

		_isEnableCardClick = true;

		StartCoroutine (OnUpdate ());
	}

	void ResetCardSelectInfo()
	{
		_cardSelectCount = 0;
		for (int i = 0; i < _curSelectMatchCardInfos.Length; i++) {
			_curSelectMatchCardInfos [i] = null;
		}
	}

	public override int CheckStopTimeButton(int touchID, float posX, float posY)
	{
		if (!_isCompleteStage)
			return -1;

		return base.CheckStopTimeButton(touchID, posX, posY);
	}

	void SetRandomCardPos()
	{
		for (int i = 0; i < _ranPosChangeCount; i++) {
			int firstRanValue = Random.Range(0, 9);
			int secondRanValue = 0;

			int exceptionCount = 0;
			while (true) {
				secondRanValue = Random.Range (0, 9);

				if (secondRanValue != firstRanValue)
					break;

				exceptionCount++;
				if (exceptionCount > 1000) {
					Debug.Log (string.Format ("SetRandomCardPos exceptionCount !!!!"));
					break;
				}
			}

			Vector3 saveCardPos = _cardMatchInfos [secondRanValue].transform.localPosition;
			_cardMatchInfos [secondRanValue].transform.localPosition = _cardMatchInfos [firstRanValue].transform.localPosition;
			_cardMatchInfos [firstRanValue].transform.localPosition = saveCardPos;
		}
	}

	#endregion

	#region Coroutine Methods

	public IEnumerator OnUpdate()
	{
		while(true)
		{
			_eventTimer.UpdateGameTimer ();
			yield return null;
		}
	}

	#endregion

	#region CallBack Methods

	public override void OnButtonTouched(int buttonID)
	{
		Debug.Log (string.Format ("OnButtonTouched buttonID : {0}", buttonID));

		if (!_isEnableCardClick)
			return;

        //if (_cardSelectCount == 2)
        //{
        //    ResetCardSelectInfo();
        //    return;
        //}

        if (!_cardMatchInfos [buttonID].IsCloseCard)
			return;

        SoundManager.Instance.PlayEffectFX(SoundDefinitions.effectSound_15);

        _curSelectMatchCardInfos [_cardSelectCount] = _cardMatchInfos [buttonID];

		_curSelectMatchCardInfos [_cardSelectCount].SetOpenCard ();

		if (_cardSelectCount == 1) {
			if (_curSelectMatchCardInfos [0].MatchIndex == _curSelectMatchCardInfos [1].MatchIndex) {
				if (_curSelectMatchCardInfos [0].MatchIndex == _buttonCardIndex) {
                    _isEnableCardClick = false;
                    _eventTimer.SetGameTimerData (OnMatchCardButton, 0.5f);
				}

				ResetCardSelectInfo ();
			} else {
                _isEnableCardClick = false;

                _eventTimer.SetGameTimerData (OnFailMatchCard, 0.5f);
			}

            //_cardSelectCount++;

        } else {
			_cardSelectCount++;
		}
	}

	void OnMatchCardButton(object objData)
	{
        SetSolveButton();
        _isEnableCardClick = true;
        _isCompleteStage = true;
		//for (int i = 0; i < _cardMatchInfos.Length; i++) {
		//	_cardMatchInfos [i].gameObject.SetActive (false);
		//}

		//_stopButtonObject.SetActive (true);
		RefreshButtonPos ();
	}

	void OnFailMatchCard(object objData)
	{
        _isEnableCardClick = true;

        for (int i = 0; i < _curSelectMatchCardInfos.Length; i++) {
			_curSelectMatchCardInfos [i].SetCloseCard ();
		}

		ResetCardSelectInfo ();
	}

	#endregion
}
