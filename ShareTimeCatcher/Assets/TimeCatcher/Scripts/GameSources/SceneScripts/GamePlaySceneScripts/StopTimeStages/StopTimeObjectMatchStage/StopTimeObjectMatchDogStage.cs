﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StopTimeObjectMatchDogStage : StopTimeObjectMatchBaseStage 
{
	#region Definitions

	public enum MatchObjectIndex
	{
		WoolObj 		= 0,
		CanObj			= 1,
		DogFeedObj		= 2,
		MeatObj			= 3,
		DollObj			= 4,
		FeedBowlObj		= 5,
	}

    #endregion

    #region Serialize Variables

#pragma warning disable 649

    [SerializeField] Transform _dogPosTrans;

    [Header("Object Match Objects")]

	[SerializeField] tk2dSprite _trapWoolSprite;
    [SerializeField] Transform _trapWoolTrans;

    [SerializeField] tk2dSprite _trapCanSprite;
    [SerializeField] Transform _trapCanTrans;
	
    [SerializeField] tk2dSprite _trapDogFeedSprite;
    [SerializeField] Transform _trapDogFeedTrans;
	
    [SerializeField] tk2dSprite _trapMeatSprite;
    [SerializeField] Transform _trapMeatTrans;
	
    [SerializeField] tk2dSprite _trapDollSprite;
    [SerializeField] Transform _trapDollTrans;

	[SerializeField] tk2dSprite _dogBodySprite;
	[SerializeField] tk2dSprite _dogRefuseSprite;

	[SerializeField] tk2dSprite _feedBowlSprite;
	[SerializeField] Transform _bowlObjPosition;

	[SerializeField] GameObject _successMatchObj;

    [SerializeField] GameObject _dogNormalObj;

#pragma warning restore 649

    #endregion

    #region MonoBehaviour Methods

    protected override void Awake()
	{
		base.Awake ();
        SwapPosition();

        //AddStopTimeObjectInfo (StageObjectType.CatchMove, (int)MatchObjectIndex.WoolObj, _trapWoolSprite, new Vector3(-91f, -320f, -10f));
        //AddStopTimeObjectInfo (StageObjectType.CatchMove, (int)MatchObjectIndex.CanObj, _trapCanSprite, new Vector3(-113f, -313f, -10f));
        //AddStopTimeObjectInfo (StageObjectType.CatchMove, (int)MatchObjectIndex.DogFeedObj, _trapDogFeedSprite, new Vector3(-112f, -298f, -10f));
        //AddStopTimeObjectInfo (StageObjectType.CatchMove, (int)MatchObjectIndex.MeatObj, _trapMeatSprite, new Vector3(-111f, -343f, -10f));
        //AddStopTimeObjectInfo (StageObjectType.CatchMove, (int)MatchObjectIndex.DollObj, _trapDollSprite, new Vector3(-113f, -330f, -10f));


        AddStopTimeObjectInfo(StageObjectType.CatchMove, (int)MatchObjectIndex.WoolObj, _trapWoolTrans, _trapWoolSprite, _trapWoolTrans.localPosition);
        AddStopTimeObjectInfo(StageObjectType.CatchMove, (int)MatchObjectIndex.CanObj, _trapCanTrans, _trapCanSprite, _trapCanTrans.localPosition);
        AddStopTimeObjectInfo(StageObjectType.CatchMove, (int)MatchObjectIndex.DogFeedObj, _trapDogFeedTrans, _trapDogFeedSprite, _trapDogFeedTrans.localPosition);
        AddStopTimeObjectInfo(StageObjectType.CatchMove, (int)MatchObjectIndex.MeatObj, _trapMeatTrans, _trapMeatSprite, _trapMeatTrans.localPosition);
        AddStopTimeObjectInfo(StageObjectType.CatchMove, (int)MatchObjectIndex.DollObj, _trapDollTrans, _trapDollSprite, _trapDollTrans.localPosition);

        AddStopTimeObjectInfo (StageObjectType.CatchMove, (int)MatchObjectIndex.FeedBowlObj, _feedBowlSprite, _feedBowlSprite.transform.localPosition);
	}

    #endregion

    #region Methods

    void SwapPosition()
    {
        int trapCount = 5;
        Vector3[] _trapObjPos = new Vector3[trapCount];

        _trapObjPos[0] = _trapWoolTrans.localPosition;
        _trapObjPos[1] = _trapCanTrans.localPosition;
        _trapObjPos[2] = _trapDogFeedTrans.localPosition;
        _trapObjPos[3] = _trapMeatTrans.localPosition;
        _trapObjPos[4] = _trapDollTrans.localPosition;

        for (int i = 0; i < 20; i++)
        {
            int ranValue = Random.Range(0, trapCount);
            int secRanValue = 0;
            for (int j = 0; j < 100; j++)
            {
                secRanValue = Random.Range(0, trapCount);
                if (secRanValue != ranValue)
                    break;
            }

            if (secRanValue == ranValue)
            {
                if (ranValue == trapCount - 1)
                    secRanValue = 0;
                else
                    secRanValue = ranValue + 1;
            }

            Vector3 tempPos = _trapObjPos[ranValue];
            _trapObjPos[ranValue] = _trapObjPos[secRanValue];
            _trapObjPos[secRanValue] = tempPos;
        }

        _trapWoolTrans.localPosition = _trapObjPos[0];
        _trapCanTrans.localPosition = _trapObjPos[1];
        _trapDogFeedTrans.localPosition = _trapObjPos[2];
        _trapMeatTrans.localPosition = _trapObjPos[3];
        _trapDollTrans.localPosition = _trapObjPos[4];
    }

    protected override bool CheckObjectDisMatching(StopTimeObjectInfo matchObjectInfo)
	{
		float validDis = 70f;
		Vector3 _bowlPosition = _bowlObjPosition.localPosition;

		float calcDis = CAMathCtl.Instance.GetPointDistance (matchObjectInfo.ObjTransform.localPosition.x, 
			matchObjectInfo.ObjTransform.localPosition.y, _bowlPosition.x, _bowlPosition.y);

		Debug.Log (string.Format ("CheckBowlMatching calcDis : {0}", calcDis));

		if (matchObjectInfo.ObjectIndex != _matchIndex) {
			if (calcDis < validDis) {
				Debug.Log (string.Format ("CheckBowlMatching Matching Success!!!!"));
				SetCurMatchingStep (StopTimeMatchStep.Matching);

				return true;
			}
		} else {
			if (calcDis > validDis) {
				Debug.Log (string.Format ("CheckBowlMatching Matching Success!!!!"));
				SetCurMatchingStep (StopTimeMatchStep.Matching);

				return true;
			}
		}

        float dogValidDis = 150f;
        Vector3 dogButtonPos = _dogPosTrans.localPosition;

        float calcDogDis = CAMathCtl.Instance.GetPointDistance(matchObjectInfo.ObjTransform.localPosition.x,
            matchObjectInfo.ObjTransform.localPosition.y, dogButtonPos.x, dogButtonPos.y);

        if (calcDogDis < dogValidDis)
        {
            Debug.Log(string.Format("CheckObjectDisMatching Dog DisMatching!!!!"));
            SetCurMatchingStep(StopTimeMatchStep.MatchFailed);
            return true;
        }

        return false;
	}

	protected override void SetMatchSuccessStep()
	{
        //		_catNormalHeadSprite.gameObject.SetActive (false);
        //		_catRefuseHeadSprite.gameObject.SetActive (false);
        //		_catBodySprite.gameObject.SetActive (false);
        //		_successCatObj.gameObject.SetActive (true);

        //_dogBodySprite.gameObject.SetActive (false);
        _dogNormalObj.SetActive(false);

        _dogRefuseSprite.gameObject.SetActive (false);
		_successMatchObj.SetActive (true);
		_curStepTime = 0f;
	}

	protected override void SetMatchFailedStep()
	{
        //		_catNormalHeadSprite.gameObject.SetActive (false);
        //		_catRefuseHeadSprite.gameObject.SetActive (true);

        _dogNormalObj.SetActive(false);
        _dogRefuseSprite.gameObject.SetActive (true);
		_curStepTime = 0f;
	}

	protected override void SetMatchCompletedStep()
	{
        base.SetMatchCompletedStep();
        //		_trapCanSprite.gameObject.SetActive(false);
        //		_trapFlySprite.gameObject.SetActive (false);
        //		_trapMouseSprite.gameObject.SetActive(false);
        //		_trapSalmonSprite.gameObject.SetActive (false);
    }

	protected override void SetMatchFailedData()
	{
//		_catNormalHeadSprite.gameObject.SetActive (true);
//		_catRefuseHeadSprite.gameObject.SetActive (false);
		_dogRefuseSprite.gameObject.SetActive(false);
        _dogNormalObj.SetActive(true);
    }

	#endregion
}
