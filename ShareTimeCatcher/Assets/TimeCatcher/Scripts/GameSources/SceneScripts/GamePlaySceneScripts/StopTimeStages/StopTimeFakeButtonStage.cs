﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StopTimeFakeButtonStage : StopTimeBaseStage 
{
    #region Serialize Variables

#pragma warning disable 649

    [SerializeField] GameObject[] _fakeButtonObjs;
    [SerializeField] GameObject[] _fakeButtonRings;
    [SerializeField] BoxCollider[] _fakeButtonBoxCollides;

#pragma warning restore 649

    #endregion

    #region Variables

    int _curFakeButtonIndex = 0;
	Vector3 _curFakeButtonStartPos;

	#endregion

	#region MonoBehaviour Methods

	protected override void Awake()
	{
		base.Awake ();

	}

	#endregion

	#region Methods

	public override void InitStopTimeStageData()
	{

        base.InitStopTimeStageData ();

		for (int i = 0; i < _fakeButtonObjs.Length; i++) {
			//float scaleX = _fakeButtonObjs[i].transform.localScale.x;
			//float scaleY = _fakeButtonObjs[i].transform.localScale.x;

            //float width = 506f * scaleX;
            //float height = 506f * scaleY;
            float width = _fakeButtonBoxCollides[i].size.x;
            float height = _fakeButtonBoxCollides[i].size.y;

            //float scalePosY = -288 * scaleY;
            //float scalePosY = _fakeButtonBoxCollides[i].transform.localPosition.y;

            //Transform ringTransform = _fakeButtonObjs [i].transform.Find ("FakeButtonRing");
            Transform ringTransform = _fakeButtonRings[i].transform;

            //Vector3 fakeScreenPos = _stageCamera.WorldToScreenPoint (ringTransform.position);

            //Vector3 fakeImgPos = new Vector3 (CAResolutionCtl.Instance.GetScreenToImagePosX(fakeScreenPos.x),
            //CAResolutionCtl.Instance.GetScreenToImagePosY(fakeScreenPos.y), _fakeButtonObjs[i].transform.localPosition.z);

            Vector3 fakeImgPos = _fakeButtonBoxCollides[i].transform.localPosition;

//			Transform fakeParent = _fakeButtonObjs [i].transform.parent;
            AddStopTimeObjectInfo (StageObjectType.CatchMove, i, _fakeButtonObjs [i].transform, width, height, fakeImgPos, 0f, 0f);
		}
	}

	public override void OnTouchPress(int touchID, float posX, float posY)
	{
		//if (_curFakeButtonIndex >= _fakeButtonObjs.Length)
			//return;

		CheckMatchObject (touchID, posX, posY);
	}

	void CheckMatchObject(int touchID, float posX, float posY)
	{
		if (_curCatchObjectInfo.TouchID != -1) {
			if (_curCatchObjectInfo.TouchID != touchID)
				return;
		}

		float imgPosX = CAResolutionCtl.Instance.GetScreenToImagePosX(posX);
		float imgPosY = CAResolutionCtl.Instance.GetScreenToImagePosY(posY);

        //if ((_catchMoveObjectInfos [_curFakeButtonIndex].XMin <= imgPosX && _catchMoveObjectInfos [_curFakeButtonIndex].XMax >= imgPosX) &&
        //	(_catchMoveObjectInfos [_curFakeButtonIndex].YMin <= imgPosY && _catchMoveObjectInfos [_curFakeButtonIndex].YMax >= imgPosY)) {

        //	_curCatchObjectInfo.TouchID = touchID;
        //	_curCatchObjectInfo.ObjectInfo = _catchMoveObjectInfos [_curFakeButtonIndex];
        //	_curFakeButtonStartPos = _curCatchObjectInfo.ObjectInfo.ObjTransform.localPosition;

        //	Debug.Log (string.Format ("CheckMatchObject Matched index : {0}", _curFakeButtonIndex));
        //}

        for (int i = 0; i < _catchMoveObjectInfos.Count;i++){
            if ((_catchMoveObjectInfos[i].XMin <= imgPosX && _catchMoveObjectInfos[i].XMax >= imgPosX) &&
                (_catchMoveObjectInfos[i].YMin <= imgPosY && _catchMoveObjectInfos[i].YMax >= imgPosY))
            {

                _curCatchObjectInfo.TouchID = touchID;
                _curCatchObjectInfo.ObjectInfo = _catchMoveObjectInfos[i];
                _curFakeButtonStartPos = _curCatchObjectInfo.ObjectInfo.ObjTransform.localPosition;

                Debug.Log(string.Format("CheckMatchObject Matched index : {0}", i));
                break;
            }
        }
	}

	public override void OnTouchRelease(int touchID, float posX, float posY)
	{
		if (_curCatchObjectInfo.TouchID == touchID) {
            //if (CheckValidMoveFakeButton (_curCatchObjectInfo.ObjectInfo.ObjTransform.localPosition)) {
            //	_fakeButtonObjs [_curFakeButtonIndex].SetActive (false);
            //	_curFakeButtonIndex++;
            //	Debug.Log (string.Format ("_curFakeButtonIndex"));
            //} else {
            //	_fakeButtonObjs [_curFakeButtonIndex].transform.localPosition = _curFakeButtonStartPos;
            //}
            if (_catchMoveObjectInfos[_catchMoveObjectInfos.Count - 1] == _curCatchObjectInfo.ObjectInfo)
            {
                SetSolveButton();
            }

            _curCatchObjectInfo.TouchID = -1;
			_curCatchObjectInfo.ObjectInfo = null;
        }
	}

	public override void OnTouchMoveValues(int touchID, float moveX, float moveY)
	{
		if (_curCatchObjectInfo.TouchID == touchID) {

			Vector3 objPos = _curCatchObjectInfo.ObjectInfo.ObjTransform.localPosition;
			float objHalfWidth = _curCatchObjectInfo.ObjectInfo.ObjWidth * 0.5f;
			float objHalfHeight = _curCatchObjectInfo.ObjectInfo.ObjHeight * 0.5f;
            //float objPosX = Mathf.Clamp (objPos.x + moveX, 
            //	-CAResolutionCtl.Instance.GetResolutionHalfWidth() + objHalfWidth, 
            //	CAResolutionCtl.Instance.GetResolutionHalfWidth() - objHalfWidth);

            //float objPosY = Mathf.Clamp (objPos.y + moveY, 
            //-CAResolutionCtl.Instance.GetResolutionHalfHeight() + objHalfHeight, 
            //CAResolutionCtl.Instance.GetResolutionHalfHeight() - objHalfHeight);

            float objPosX = objPos.x + moveX;
            float objPosY = objPos.y + moveY;

			//			Debug.Log (string.Format ("OnTouchMoveValues objPosX : {0}, objPosY : {1}", objPosX, objPosY));

			_curCatchObjectInfo.ObjectInfo.ObjTransform.localPosition = new Vector3 (objPosX, objPosY,
				_curCatchObjectInfo.ObjectInfo.ObjTransform.localPosition.z);
		}
	}

	public override int CheckStopTimeButton(int touchID, float posX, float posY)
	{
        //if (_curFakeButtonIndex < _fakeButtonObjs.Length)
        //return -1;
        if (_curCatchObjectInfo.TouchID != -1)
            return -1;

		return base.CheckStopTimeButton(touchID, posX, posY);
	}

	bool CheckValidMoveFakeButton(Vector3 curObjPos)
	{
		float calcDis = CAMathCtl.Instance.GetPointDistance (_curFakeButtonStartPos.x, _curFakeButtonStartPos.y,
			curObjPos.x, curObjPos.y);

		Debug.Log (string.Format ("CheckValidMoveFakeButton calcDis : {0}", calcDis));

		if (calcDis > 200f)
			return true;
			
		return false;
	}

	#endregion
}
