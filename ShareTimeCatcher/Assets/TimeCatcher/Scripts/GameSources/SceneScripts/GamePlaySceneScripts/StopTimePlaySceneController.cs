﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.U2D;

public enum StopTimePlaySceneStep
{
	None = 0,
    StartDelay,
    ReadyDirectingData,
	ReadyDirectingStep,
	GamePlayingStep,
    ResultDelayStep,
	ResultDirectingStep,
    ResultTimeOverDirectingStep,
    ResultStep,
	InfiniteModeResultStep,
	InfiniteModeRestartStep,
    InfiniteNextStep,
    InfiniteTutorialStep,
}

public enum StopTimeButtonType
{
    Normal,
    PushImmediate,
}

public class StopTimePlaySceneController : CACommonMonoBehaviours, IBackKeyEvent
{
    #region Serialize Variables

#pragma warning disable 649

    [SerializeField] GameObject _pauseImgObject;
	[SerializeField] GameObject _previousObject;
	[SerializeField] GameObject _againButtonObject;
	[SerializeField] GameObject _nextButtonObject;
	[SerializeField] GameObject _resultObject;
	[SerializeField] ResultSuccessPopupController _resultSuccessController;
	[SerializeField] ResultFailPopupController _resultFailController;
	[SerializeField] StageCommonPopupController _commonPopupController;
	[SerializeField] Camera _stageCamera;
    [SerializeField] StageButtonEffect _buttonEffect;
    [SerializeField] GameObject[] _hintTextObjects;
    [SerializeField] GameObject _epilogueAniObj;
    [SerializeField] GameObject _slowTimeObj;

#pragma warning restore 649

    #endregion

    #region Variables

    StopTimeBaseStage _stopTimeBaseStage;
	TimeCatcherSpriteManager _timeCatcherManager = new TimeCatcherSpriteManager ();
	StopTimePlaySceneStep _playSceneStep = StopTimePlaySceneStep.None;
	StageReadyDirectingStep _readyDirectingStep;
	StageReadyDirectingManager _readyDirectingManager;

    AnimationTriggerEvent _readyTutorialTrigger = null;

	StopTimeStageInfo _timeStageInfo;

//	StageResultManager _resultManager;
//	StageResult _resultManager;
	StageResultBasePopupController _resultManager;
    CAGameTimerEventCtl _eventTimer = new CAGameTimerEventCtl();

	List<ITouchMoveable> _touchMoveables = new List<ITouchMoveable>();

	float _perfectJudgeTime = 0.1f;//0.02f;
	float _awesomeJudgeTime = 0.2f;//0.05f;
	float _goodJudgeTime = 0.8f;// 0.1f;
	bool _stageFailed;
    bool _isReverseTime = false;
    StopTimeButtonType _buttonType = StopTimeButtonType.Normal;
    bool _isTimePause = false;
    GameObject _epilogueObj;

    #endregion

    #region MonoBehaviour Methods

    public override void Awake()
	{
		base.Awake ();
//		curTouchState = true;
		//_commonPopupController.OnYesAction = OnYesCommonPopup;
		//_commonPopupController.OnNoAction = OnNoCommonPopup;
		InitGamePlayData ();
		_stageFailed = false;

        if (BackKeyManager.Instance != null)             BackKeyManager.Instance.AddBackKeyEvent((IBackKeyEvent)this);

        if (BackKeyManager.Instance != null)
            BackKeyManager.Instance.IsEnableBackKey = false;
    }

	// Use this for initialization
	public override void Start () 
	{
		base.Start ();
		SetPlaySceneStep (StopTimePlaySceneStep.ReadyDirectingStep);

        //AudioClip musicClip = Resources.Load<AudioClip>(SoundFilePaths.MusicPath_3);
        //SoundManager.Instance.PlayMusic(musicClip);

        _commonPopupController.SetInstance();
    }

	// Update is called once per frame
	void Update () 
	{
        switch (_playSceneStep)
        {
            case StopTimePlaySceneStep.ReadyDirectingStep:

                break;
            case StopTimePlaySceneStep.GamePlayingStep:
                if (!_isTimePause)
                {
                    if (!_isReverseTime)
                        _timeCatcherManager.UpdateStopWatch();
                    else
                        _timeCatcherManager.UpdateReverseStopWatch();
                }
                break;
            case StopTimePlaySceneStep.ResultDelayStep:
                break;
            case StopTimePlaySceneStep.ResultDirectingStep:
                break;
        }

        _eventTimer.UpdateGameTimer();
    }

    public override void OnDisable()
    {
        SoundManager.Instance.StopMusic();
    }

    public override void OnDestroy()
	{
		base.OnDestroy ();
		curTouchState = false;
		ReleaseGamePlayData ();

        if (BackKeyManager.Instance != null)             BackKeyManager.Instance.RemoveBackKeyEvent((IBackKeyEvent)this);
    }

	#endregion

	#region Methods

	void InitGamePlayData()
	{
//		LoadPlanData ();
		_timeStageInfo = GamePlanDataManager.Instance.StopTimeStagePlan.StopTimeStageInfos [CAGameGlobal.Instance.CurrentStageNum];

		string stagePrefabName = string.Format("Prefabs/Stages/Stage_{0}", CAGameGlobal.Instance.CurrentStageNum);
		GameObject stageObject = Instantiate(Resources.Load<GameObject> (stagePrefabName)) as GameObject;
        stageObject.SetActive(true);

        stageObject.transform.SetParent (this.transform);
		stageObject.transform.localScale = Vector3.one;
		stageObject.transform.localPosition = Vector3.zero;

		_stopTimeBaseStage = stageObject.GetComponent<StopTimeBaseStage> ();
		_stopTimeBaseStage.StageCamera = _stageCamera;
		_stopTimeBaseStage.OnAddRealTimeValue = _timeCatcherManager.OnAddRealTimeValue;
		_stopTimeBaseStage.InitStopTimeStageData ();

        _stopTimeBaseStage.OnTimePause = OnTimePause;
        _stopTimeBaseStage.OnTimeResume = OnTimeResume;

        _buttonType = _stopTimeBaseStage.StageButtonType;

        _stopTimeBaseStage.OnFailStage = OnFailedStage;

		RFButtonManager buttonManager = stageObject.GetComponent<RFButtonManager> ();
		if (buttonManager != null) {
			buttonManager.ButtonCamera = _stageCamera;
			buttonManager.InitRFButtonManager ();
			buttonManager.OnButtonTouched = _stopTimeBaseStage.OnButtonTouched;
		}

		SetCurrentStageValue (_timeStageInfo, _stopTimeBaseStage);

		SetImageTimeNumberData (_timeCatcherManager.ImgTargetTimeNum, _stopTimeBaseStage.TargetTimeViewObject, _timeStageInfo.TargetTime);

		SetImageTimeNumberData (_timeCatcherManager.ImgRealTimeNum, _stopTimeBaseStage.RealTimeViewObject, _timeStageInfo.RealTime);

        switch (_stopTimeBaseStage.StageType)
        {
            case StopTimeStageDefinitions.StopTimeStageType.FakeMultipleTime:
                StopTimeFakeMultipleTimeStage fakeTimeStage = _stopTimeBaseStage as StopTimeFakeMultipleTimeStage;
                if (fakeTimeStage.FakeTimeViewObjects.Length > 0)
                {
                    for (int i = 0; i < fakeTimeStage.FakeTimeViewObjects.Length; i++)
                    {
                        SpriteTimeNumberInfo inputFakeTimeNumberInfo = new SpriteTimeNumberInfo();
                        SetImageTimeNumberData(inputFakeTimeNumberInfo, fakeTimeStage.FakeTimeViewObjects[i], _timeStageInfo.fakeTimeValues[i]);
                        _timeCatcherManager.FakeTimeNums.Add(inputFakeTimeNumberInfo);
                    }
                }
                break;
            case StopTimeStageDefinitions.StopTimeStageType.ReflectTime:
                StopTimeReflectStage reflectStage = _stopTimeBaseStage as StopTimeReflectStage;
                SpriteTimeNumberInfo inputReflectTimeNumberInfo = new SpriteTimeNumberInfo();
                SetImageTimeNumberData(inputReflectTimeNumberInfo, reflectStage.ReflectRealTimeViewObject, _timeStageInfo.RealTime);
                _timeCatcherManager.FakeTimeNums.Add(inputReflectTimeNumberInfo);
                break;
            case StopTimeStageDefinitions.StopTimeStageType.SushiObject:
                StopTimeSushiStage sushiStage = _stopTimeBaseStage as StopTimeSushiStage;
                sushiStage.RFButtonManager = buttonManager;
                break;
            case StopTimeStageDefinitions.StopTimeStageType.MosaicStage:
                StopTimeMosaicNumStage mosaicStage = _stopTimeBaseStage as StopTimeMosaicNumStage;
                mosaicStage.TimeManager = _timeCatcherManager;
                _timeCatcherManager.SetChangeTime();
                break;
            case StopTimeStageDefinitions.StopTimeStageType.ReverseTime:
                StopTimeReverseTimeStage reverseTimeStage = _stopTimeBaseStage as StopTimeReverseTimeStage;
                //reverseTimeStage.TimeManager = _timeCatcherManager;
                //_timeCatcherManager.SetChangeTime();
                _isReverseTime = true;
                break;
            case StopTimeStageDefinitions.StopTimeStageType.ButtonHPGauge:
                //StopTimeReverseTimeStage reverseTimeStage = _stopTimeBaseStage as StopTimeReverseTimeStage;
                //_isReverseTime = true;
                break;
        }

        if (!_isReverseTime)
            _timeCatcherManager.SetFailTime();
        else
            _timeCatcherManager.SetReverseFailTime();

        _timeCatcherManager.OnFinishTimeOver = OnFinishTimeOver;

		LoadReadyDirecting ();
//		LoadResultObject ();
	}

	void SetImageTimeNumberData(SpriteTimeNumberInfo timeNumberInfo, StopTimeViewObject timeViewObject, StopTimeValues gameTimeValues)
	{
		timeNumberInfo.SetTimeNumImages (timeViewObject.HourImage_1, timeViewObject.HourImage_2, timeViewObject.MinuteImage_1,
			timeViewObject.MinuteImage_2, timeViewObject.SecondImage_1, timeViewObject.SecondImage_2, 
			timeViewObject.MillisecondImage_1, timeViewObject.MillisecondImage_2);

		timeNumberInfo.SetMosaTimeNumImages(timeViewObject.MosaSecondImage_1, timeViewObject.MosaSecondImage_2, 
			timeViewObject.MosaMillisecondImage_1, timeViewObject.MosaMillisecondImage_2);
		timeNumberInfo.SetTimeValue(gameTimeValues.Hour, gameTimeValues.Minute, gameTimeValues.Second);
	}

	void SetCurrentStageValue(StopTimeStageInfo timeStageInfo, StopTimeBaseStage timeBaseStage)
	{
		switch (timeBaseStage.StageType) {
            case StopTimeStageDefinitions.StopTimeStageType.HitObject:
                StopTimeHitObjectStage hitObjectStage = timeBaseStage as StopTimeHitObjectStage;
                hitObjectStage.SetCurrentHitObjectStep(StopTimeHitObjectStep.HitObjectStand);
                hitObjectStage.HitObjectTotalHP = Mathf.RoundToInt(timeStageInfo.StageValues[(int)StagePlanDefinitions.StagePlanValueType.HitObjectTotalHP].Value);
                hitObjectStage.CurHitObjectHP = hitObjectStage.HitObjectTotalHP;
                hitObjectStage.HitCount = Mathf.RoundToInt(timeStageInfo.StageValues[(int)StagePlanDefinitions.StagePlanValueType.HitObjectPerHPValue].Value);
                break;
            case StopTimeStageDefinitions.StopTimeStageType.ButtonHPGauge:
                StopTimeMoveObjectHitButtonStage hitButtonStage = timeBaseStage as StopTimeMoveObjectHitButtonStage;
                float buttonTotalHP = timeStageInfo.StageValues[(int)StagePlanDefinitions.StagePlanValueType.HitObjectTotalHP].Value;
                float buttonPerHPValue = timeStageInfo.StageValues[(int)StagePlanDefinitions.StagePlanValueType.HitObjectPerHPValue].Value;
                hitButtonStage.SetButtonHPValues(buttonTotalHP, buttonPerHPValue);
                break;
        }
	}

	void LoadReadyDirecting()
	{
        //GameObject readyDirectingObj = Instantiate(Resources.Load<GameObject> ("Prefabs/Directings/StageReadyDirecting")) as GameObject;

        if (CAGameGlobal.Instance.CurrentStageNum == 0)
        {
            GameObject readyTutorialDirectingObj = Instantiate(Resources.Load<GameObject>("Prefabs/Directings/Ready_Tutorial")) as GameObject;
            readyTutorialDirectingObj.transform.SetParent(this.transform);
            readyTutorialDirectingObj.transform.localScale = Vector3.one;
            readyTutorialDirectingObj.transform.localPosition = new Vector3(0f, 0f, -50f);
            readyTutorialDirectingObj.SetActive(false);
            _readyTutorialTrigger = readyTutorialDirectingObj.GetComponent<AnimationTriggerEvent>();
        }
        else
        {
            GameObject readyDirectingObj = Instantiate(Resources.Load<GameObject>("Prefabs/Directings/Ready")) as GameObject;
            readyDirectingObj.transform.SetParent(this.transform);
            readyDirectingObj.transform.localScale = Vector3.one;
            readyDirectingObj.transform.localPosition = new Vector3(0f, 0f, -50f);
            readyDirectingObj.SetActive(false);

            _readyDirectingManager = readyDirectingObj.GetComponent<StageReadyDirectingManager>();
        }

	}

//	void LoadResultObject()
//	{
//		GameObject resultObj = Instantiate(Resources.Load<GameObject> ("Prefabs/Result/StageResultObj")) as GameObject;
//		resultObj.transform.SetParent (this.transform);
//		resultObj.transform.localScale = Vector3.one;
//		resultObj.transform.localPosition = new Vector3 (0f, 0f, -50f);
//		resultObj.SetActive (false);
//
//		_resultManager = resultObj.GetComponent<StageResultManager> ();
//		_resultManager.OnFinishedDirecting = OnFinishedResultDirecting;
//	}

//	void LoadPlanData()
//	{
////		GamePlanDataManager.Instance.LoadStopTimeStagePlanData ();
//	}

//	void ReleasePlanData()
//	{
////		GamePlanDataManager.Instance.StopTimeStagePlan.ReleasePlanData ();
//	}

	void SetPlaySceneStep(StopTimePlaySceneStep _curStep)
	{
		_playSceneStep = _curStep;
        switch (_playSceneStep)
        {
            case StopTimePlaySceneStep.ReadyDirectingStep:
                SetReadyDirectingStep();
                break;
            case StopTimePlaySceneStep.GamePlayingStep:
                SetGamePlayingStep();
                break;
            case StopTimePlaySceneStep.ResultDelayStep:
                SetResultDelayStep();
                break;
            case StopTimePlaySceneStep.ResultDirectingStep:
                SetResultDirectingStep();
                break;
            case StopTimePlaySceneStep.ResultTimeOverDirectingStep:
                SetResultDirectingStep(true);
                break;
            case StopTimePlaySceneStep.ResultStep:
                SetResultStep();
                break;
        }
    }

	void SetReadyDirectingStep()
	{
//		_pauseImgObject.gameObject.SetActive (false);
		_stopTimeBaseStage.HideTimeObject();

        if (CAGameGlobal.Instance.CurrentStageNum == 0) // Tutorial Stage
        {
            _readyTutorialTrigger.OnTriggerEvent = (x) => OnFinishedReadyDirecting();
            _readyTutorialTrigger.gameObject.SetActive(true);

            _eventTimer.SetGameTimerData(OnDelayReadySound, 0.6f);
        }
        else
        {
            _readyDirectingManager.gameObject.SetActive(true);
            _readyDirectingManager.OnFinishedDirecting = OnFinishedReadyDirecting;
            _readyDirectingManager.SetStageNumber(CAGameGlobal.Instance.CurrentStageNum);

            _eventTimer.SetGameTimerData(OnDelayReadySound, 0.6f);
            //      _readyDirectingManager.SetTimeValue (1, 25, 43.37f);
            //_readyDirectingManager.SetTimeValue(_timeStageInfo.TargetTime.Hour, _timeStageInfo.TargetTime.Minute, _timeStageInfo.TargetTime.Second);
        }
	}

    void SetGamePlayingStep()
	{
        if (BackKeyManager.Instance != null)             BackKeyManager.Instance.IsEnableBackKey = true;

        _previousObject.SetActive (true);
		curTouchState = true;
		_stopTimeBaseStage.SetStartPlayData();

        if (_readyTutorialTrigger != null)
            _readyTutorialTrigger.gameObject.SetActive(false);

        if (_readyDirectingManager != null)
            _readyDirectingManager.gameObject.SetActive (false);

//		OnClickStartButton ();
		_timeCatcherManager.StartState = true;

        AudioClip musicClip = Resources.Load<AudioClip>(SoundFilePaths.MusicPath_4);
        SoundManager.Instance.PlayMusic(musicClip);

        if (CAGameGlobal.Instance.IsStageHint)         {             SetHintData();         }
    }

    void SetHintData()     {
        int hintIndex = CAGameGlobal.Instance.CurrentStageNum - 1;
        if(_hintTextObjects[hintIndex] != null)             _hintTextObjects[hintIndex].SetActive(true);

        _timeCatcherManager.SlowRevisionTime = 0.5f;

        _slowTimeObj.SetActive(true);     }

    void SetResultDelayStep()
    {
        //SoundManager.Instance.PlayEffectFX(SoundDefinitions.effectSound_1);

        _eventTimer.SetGameTimerData(OnResultDelayStepTime, 0.5f);
        StageButtonEffect buttonEffectInfo = Instantiate<StageButtonEffect>(_buttonEffect);
        Vector3 buttonPos = _stopTimeBaseStage.StartButton.transform.localPosition;
        buttonEffectInfo.transform.SetParent(_stopTimeBaseStage.StartButton.transform.parent);
        buttonEffectInfo.transform.localPosition = new Vector3(buttonPos.x, buttonPos.y, -5f);
        buttonEffectInfo.transform.localScale = Vector3.one;
        buttonEffectInfo.gameObject.SetActive(true);
        buttonEffectInfo.ResetEffectTweeners();

        //_stopTimeBaseStage.StopButton.gameObject.SetActive

        _stopTimeBaseStage.SetGameEnd();
    }

    void SetResultDirectingStep(bool isTimeOver = false)
	{
        //SoundManager.Instance.PlayEffectFX(SoundDefinitions.effectSound_1);

        _eventTimer.SetGameTimerData(OnResultDelayTime, 0.2f);

        _previousObject.SetActive (false);
		_resultObject.SetActive (true);
		_stopTimeBaseStage.HideTimeObject ();

        bool isStageSuccess = true;

		if (!_stageFailed) {
			float gapTime = _timeCatcherManager.CheckGapStopTime();
			StopTimeJudgeType judgeType = GetResultJudgeType (gapTime);

            if (judgeType != StopTimeJudgeType.Fail) {
                if(judgeType == StopTimeJudgeType.Perfect)
                {   
                    _eventTimer.SetGameTimerData(OnPerfectSoundDelayTime, 2f);
                }
                _resultSuccessController.NextButtonAction = OnClickNext;
				_resultManager = _resultSuccessController;
            } else {
                if (isTimeOver)
                {
                    _resultFailController.FailTextObj.SetActive(false);
                    _resultFailController.TimeOverTextObj.SetActive(true);
                }
                _resultManager = _resultFailController;
                isStageSuccess = false;
            }

            if (isStageSuccess)
            {
                SetAlbumOpenData(CAGameGlobal.Instance.CurrentStageNum);
            }

            _resultManager.AgainButtonAction = OnClickAgain;

            _resultManager.OnFinishAniAction = OnFinishedResultDirecting;
			_resultManager.JudgeType = judgeType;
			_resultManager.gameObject.SetActive (true);

			_resultManager.SetGapTimeValue (gapTime);
			_resultManager.SetTimeValue (_timeCatcherManager.ImgRealTimeNum.HourTime, _timeCatcherManager.ImgRealTimeNum.MinuteTime, 
				_timeCatcherManager.ImgRealTimeNum.TimeSecValue);
			//		_resultManager.SetCurrentResultStep (StopTimeResultStep.ResultDirecting);

		} else {
            if(isTimeOver){
                _resultFailController.FailTextObj.SetActive(false);
                _resultFailController.TimeOverTextObj.SetActive(true);
            }
			_resultManager = _resultFailController;

			_resultManager.gameObject.SetActive (true);
            _resultManager.AgainButtonAction = OnClickAgain;
            _resultManager.OnFinishAniAction = OnFinishedResultDirecting;
			_resultManager.OnFailStage ();
            isStageSuccess = false;
        }

		_stopTimeBaseStage.OnFinishedPlay ();

        if(isStageSuccess){
            AudioClip musicClip = Resources.Load<AudioClip>(SoundFilePaths.MusicPath_5);
            SoundManager.Instance.PlayMusic(musicClip, false);
        } else {
            AudioClip musicClip = Resources.Load<AudioClip>(SoundFilePaths.MusicPath_6);
            SoundManager.Instance.PlayMusic(musicClip, false);
        }

        _stopTimeBaseStage.SetGameEnd();

        _commonPopupController.IsEnablePopup = true;
        _eventTimer.SetGameTimerData(OnDelayResultAni, 3f);
    }

    void SetAlbumOpenData(int stageNum)     {         StopTimeStageInfo timeStageInfo = GamePlanDataManager.Instance.StopTimeStagePlan.StopTimeStageInfos[stageNum];         for (int i = 0; i < timeStageInfo.albumOpens.Count; i++)         {             string[] split = timeStageInfo.albumOpens[i].Split('-');             int page = int.Parse(split[0]);             int albumNum = int.Parse(split[1]);             CAGameGlobal.Instance.DynamicData.AlbumData.AddOpenAlbum(page, albumNum);         }     }

    void SetResultStep()
    {
        if (_stageFailed)
            return;

        //bool isNewRecord = false;
        //if (CAGameSaveDataManager.Instance.StageProgressSave.IsPerfectNotify == 0)
        //{
        //    if (_resultManager.JudgeType == StopTimeJudgeType.Perfect)
        //    {
        //        if (CAGameSaveDataManager.Instance.StageProgressSave.PerfectCount < 3)
        //        {
        //            CAGameSaveDataManager.Instance.StageProgressSave.PerfectCount++;
        //            if(CAGameSaveDataManager.Instance.StageProgressSave.PerfectCount >= 3)
        //            {

        //                CAGameSaveDataManager.Instance.StageProgressSave.IsPerfectNotify = 1;
        //                isNewRecord = true;
        //                _commonPopupController.ShowPopup(CommonPopupType.NewRecord, null, null, OnConfirmNewRecord);
        //            }
        //        }
        //    }
        //    else
        //    {
        //        CAGameSaveDataManager.Instance.StageProgressSave.PerfectCount = 0;
        //    }

        //    CAGameSaveDataManager.Instance.StageProgressSave.SaveData();
        //}

        bool isLevelupPopup = false;
        bool isEpilogue = false;

        if (_resultManager.IsFirstClear && _resultManager.JudgeType != StopTimeJudgeType.Fail)
        {
            if (CAGameGlobal.Instance.CurrentStageNum == 10 || CAGameGlobal.Instance.CurrentStageNum == 20 ||
                CAGameGlobal.Instance.CurrentStageNum == 25)
            {
                isLevelupPopup = true;
                _commonPopupController.ShowPopup(CommonPopupType.LevelUp, null, null, OnConfirmLevelUp);
            } 
            else if(CAGameGlobal.Instance.CurrentStageNum == 30)
            {
                isEpilogue = true;
                _commonPopupController.IsEnablePopup = true;
                _epilogueObj = Instantiate(_epilogueAniObj);
                StoryAniManager aniManager = _epilogueObj.GetComponent<StoryAniManager>();
                aniManager.AniTrigger.OnAniTrigger = OnEndEpilogue;
            }
        }

        if (!isLevelupPopup && !isEpilogue)
        {
            CheckEvaluateAskPopup();
        }
	}

    void CheckEvaluateAskPopup()
    {
        if (_resultManager.JudgeType == StopTimeJudgeType.Fail)
            return;

        if (CAGameSaveDataManager.Instance.StageProgressSave.IsGameEvaluate == 0)
        {
            if (CAGameSaveDataManager.Instance.StageProgressSave.EvaluateAskCount < 3)
            {
                CAGameSaveDataManager.Instance.StageProgressSave.EvaluatePlayCount++;
                if (CAGameGlobal.Instance.CurrentStageNum >= 10 && CAGameSaveDataManager.Instance.StageProgressSave.EvaluatePlayCount >= 5)
                {
                    CAGameSaveDataManager.Instance.StageProgressSave.EvaluateAskCount++;
                    CAGameSaveDataManager.Instance.StageProgressSave.EvaluatePlayCount = 0;

                    _commonPopupController.ShowPopup(CommonPopupType.Appraise, OnYesAppraisePopup, OnNoAppraisePopup, null, OnNextAppraisePopup);
                }

                CAGameSaveDataManager.Instance.StageProgressSave.SaveData();
            }
        }
    }

    void OnConfirmNewRecord()
    {
        CheckEvaluateAskPopup();
    }

    void OnConfirmLevelUp()
    {
        CheckEvaluateAskPopup();
    }

    public void AttachTouchMoveable(ITouchMoveable inputMoveable)
	{
		if (_touchMoveables.Contains (inputMoveable))
			return;
		_touchMoveables.Add (inputMoveable);
	}

	public void DetachTouchMoveable(ITouchMoveable removeMoveable)
	{
		if (!_touchMoveables.Contains (removeMoveable))
			return;
		_touchMoveables.Remove (removeMoveable);
	}

	public void NotifyMoveValue(float moveX, float moveY)
	{
		for (int i = 0; i < _touchMoveables.Count; i++) {
			_touchMoveables [i].OnMoveValue (moveX, moveY);
		}
	}

	StopTimeJudgeType GetResultJudgeType(float gapTime)
	{
		float calcGapTime = Mathf.Abs (gapTime);
		if (calcGapTime <= GetPerfectJudgeTime()) {
			return StopTimeJudgeType.Perfect;
		}  else if (calcGapTime <= GetAwesomeJudgeTime()) {
			return StopTimeJudgeType.Awesome;
		}  else if (calcGapTime <= GetGoodJudgeTime()) {
			return StopTimeJudgeType.Good;
		}  else {
			return StopTimeJudgeType.Fail;
		}
	}

    float GetPerfectJudgeTime()
    {
        if (CAGameGlobal.Instance.CurrentStageNum > 10 && CAGameGlobal.Instance.CurrentStageNum <= 20)
        {
            return _perfectJudgeTime - 0.02f;
        }
        else if (CAGameGlobal.Instance.CurrentStageNum > 20 && CAGameGlobal.Instance.CurrentStageNum <= 25)
        {
            return _perfectJudgeTime - 0.04f;
        }
        else if (CAGameGlobal.Instance.CurrentStageNum > 25 && CAGameGlobal.Instance.CurrentStageNum <= 30)
        {
            return _perfectJudgeTime - 0.09f;
        }

        return _perfectJudgeTime;
    }

    float GetAwesomeJudgeTime()
    {
        if (CAGameGlobal.Instance.CurrentStageNum > 10 && CAGameGlobal.Instance.CurrentStageNum <= 20)
        {
            return _awesomeJudgeTime - 0.05f;
        }
        else if (CAGameGlobal.Instance.CurrentStageNum > 20 && CAGameGlobal.Instance.CurrentStageNum <= 25)
        {
            return _awesomeJudgeTime - 0.1f;
        }
        else if (CAGameGlobal.Instance.CurrentStageNum > 25 && CAGameGlobal.Instance.CurrentStageNum <= 30)
        {
            return _awesomeJudgeTime - 0.15f;
        }

        return _awesomeJudgeTime;
    }

    float GetGoodJudgeTime()
    {
        if (CAGameGlobal.Instance.CurrentStageNum > 10 && CAGameGlobal.Instance.CurrentStageNum <= 20)
        {
            return _goodJudgeTime - 0.2f;
        }
        else if (CAGameGlobal.Instance.CurrentStageNum > 20 && CAGameGlobal.Instance.CurrentStageNum <= 25)
        {
            return _goodJudgeTime - 0.4f;
        }
        else if (CAGameGlobal.Instance.CurrentStageNum > 25 && CAGameGlobal.Instance.CurrentStageNum <= 30)
        {
            return _goodJudgeTime - 0.6f;
        }

        return _goodJudgeTime;
    }

    void ReleaseGamePlayData()
	{
//		ReleasePlanData ();
	}

    void SetTimePause(bool pause)
    {
        _isTimePause = pause;
        if (_isTimePause)
        {
            Time.timeScale = 0f;
        }
        else
        {
            Time.timeScale = 1f;
        }
    }

    public void ExecuteBackKey()     {         OnClickPreviousButton();     }

    #endregion

    #region ClickButton Methods

    public void OnClickPreviousButton()
	{
        SoundManager.Instance.PlayEffectFX(SoundDefinitions.effectSound_2);
        //_commonPopupController.gameObject.SetActive(true);
        _commonPopupController.ShowPopup(CommonPopupType.Exit, OnExitYesCommonPopup, OnExitNoCommonPopup);
        SetTimePause(true);

        if (BackKeyManager.Instance != null)
            BackKeyManager.Instance.IsEnableBackKey = false;
    }

	public void OnClickStartButton()
	{
		Debug.Log (string.Format ("OnClickStartButton"));
		_timeCatcherManager.StartState = true;
//		_stopTimeBaseStage.StartButton.gameObject.SetActive (false);
//		_stopTimeBaseStage.StopButton.gameObject.SetActive (true);
	}

	public void OnClickStopButton()
	{
        SoundManager.Instance.PlayEffectFX(SoundDefinitions.effectSound_4);

		Debug.Log (string.Format ("OnClickStopButton"));
		_timeCatcherManager.StartState = false;
        if (_stopTimeBaseStage.StopButton != null)
        {
            _stopTimeBaseStage.StartButton.gameObject.SetActive(false);
            _stopTimeBaseStage.StopButton.gameObject.SetActive(true);
        }

        //SetPlaySceneStep (StopTimePlaySceneStep.ResultDirectingStep);
        SetPlaySceneStep(StopTimePlaySceneStep.ResultDelayStep);
    }

	public void OnFailedStage()
	{
		_stageFailed = true;
		_timeCatcherManager.StartState = false;
		_stopTimeBaseStage.StartButton.gameObject.SetActive (false);
		if(_stopTimeBaseStage.StopButton != null)
			_stopTimeBaseStage.StopButton.gameObject.SetActive (true);

		SetPlaySceneStep (StopTimePlaySceneStep.ResultDirectingStep);
	}

	public void OnClickPauseButton()
	{
		Debug.Log (string.Format ("OnClickPauseButton"));
        SoundManager.Instance.PlayEffectFX(SoundDefinitions.effectSound_2);
        //		CAGameGlobal.Instance.LoadGameScene(GameSceneEnum.SCENE_STAGE, SceneChangeState.SCENE_CHANGE_DIRECT);
        _commonPopupController.gameObject.SetActive(true);
	}

	public void OnClickAgain()
	{
        SoundManager.Instance.PlayEffectFX(SoundDefinitions.effectSound_2);
        CAGameGlobal.Instance.LoadGameScene(GameSceneEnum.SCENE_GAMEPLAYING, SceneChangeState.SCENE_CHANGE_DIRECT);
	}

	public void OnClickNext()
	{
        SoundManager.Instance.PlayEffectFX(SoundDefinitions.effectSound_2);
        if (CAGameGlobal.Instance.CurrentStageNum >= CAGameGlobal.Instance.ValidStageNum) {
			CAGameGlobal.Instance.LoadGameScene(GameSceneEnum.SCENE_STAGE, SceneChangeState.SCENE_CHANGE_DIRECT);
		} else {
            CAGameGlobal.Instance.DynamicData.StageSceneState = GameDefinitions.StageSceneState.NextStage;
            CAGameGlobal.Instance.CurrentStageNum++;
            //CAGameGlobal.Instance.LoadGameScene(GameSceneEnum.SCENE_GAMEPLAYING, SceneChangeState.SCENE_CHANGE_DIRECT);
            CAGameGlobal.Instance.LoadGameScene(GameSceneEnum.SCENE_STAGE, SceneChangeState.SCENE_CHANGE_DIRECT);
        }
	}

	#endregion

	#region Touch Handler

	public override CACurGamePlayTouchData OnGetTouchData(int touchID, float posX, float posY, bool isTouch)
	{
		if (_playSceneStep == StopTimePlaySceneStep.GamePlayingStep) {
			int touchButtonIndex = _stopTimeBaseStage.CheckStopTimeButton (touchID, posX, posY);
            switch(_buttonType){
                case StopTimeButtonType.Normal:
                    if (touchButtonIndex != -1)
                    {
                        if (touchButtonIndex == _stopTimeBaseStage.StopButtonIndex)
                        {
                            return new CACurGamePlayTouchData(CAGamePlayTouchType.StopTimeButton, null);
                        }
                        else
                        {
                            return new CACurGamePlayTouchData(CAGamePlayTouchType.StopTimeOtherButton, touchButtonIndex);
                        }
                    }
                    break;
                case StopTimeButtonType.PushImmediate:
                    if (touchButtonIndex != -1 && isTouch)
                    {
                        if (touchButtonIndex == _stopTimeBaseStage.StopButtonIndex)
                        {
                            OnTouchEvent(new CACurGamePlayTouchData(CAGamePlayTouchType.StopTimeButton, null), posX, posY);
                        }
                        else
                        {
                            //return new CACurGamePlayTouchData(CAGamePlayTouchType.StopTimeOtherButton, touchButtonIndex);
                            OnTouchEvent(new CACurGamePlayTouchData(CAGamePlayTouchType.StopTimeOtherButton, touchButtonIndex), posX, posY);
                        }
                    }
                    return null;
            }

            CACurGamePlayTouchData gamePlayTouchData = _stopTimeBaseStage.OnGetTouchData (touchID, posX, posY);
			if (gamePlayTouchData != null)
				return gamePlayTouchData;
		}

		return null;
	}

	public override void OnMultiTouchPosSet(float posX, float posY){}

	public override void OnFirstTouchDrag(int touchID, float posX, float posY)
	{
		float moveX = 0f, moveY = 0f;

		if (touchValues [0].m_TouchID != -1) {
			moveX = touchValues [0].m_X - touchValues [0].m_PreX;
			moveY = touchValues [0].m_Y - touchValues [0].m_PreY;
		} else if (touchValues [1].m_TouchID != -1) {
			moveX = touchValues [1].m_X - touchValues [1].m_PreX;
			moveY = touchValues [1].m_Y - touchValues [1].m_PreY;
		}

		if (_stopTimeBaseStage != null && _playSceneStep == StopTimePlaySceneStep.GamePlayingStep)
			_stopTimeBaseStage.OnTouchMoveValues (touchID, moveX, moveY);

//		NotifyMoveValue (moveX, moveY);
	}

	public override void OnTouchDrag(int touchID, float posX, float posY)
	{
		float moveX = 0f, moveY = 0f;

		if (touchValues [0].m_TouchID != -1) {
			moveX = touchValues [0].m_X - touchValues [0].m_PreX;
			moveY = touchValues [0].m_Y - touchValues [0].m_PreY;
		} else if (touchValues [1].m_TouchID != -1) {
			moveX = touchValues [1].m_X - touchValues [1].m_PreX;
			moveY = touchValues [1].m_Y - touchValues [1].m_PreY;
		}

		if (_stopTimeBaseStage != null && _playSceneStep == StopTimePlaySceneStep.GamePlayingStep) {
			_stopTimeBaseStage.OnTouchMoveValues (touchID, moveX, moveY);
			_stopTimeBaseStage.OnTouchDrag (touchID, posX, posY);
		}

//		NotifyMoveValue (moveX, moveY);
	}

	public override void OnTouchScale(){}

	public override void OnTouchEvent(CACurGamePlayTouchData playTouchData, float posX, float posY)
	{
		if (playTouchData != null) {
			switch (playTouchData.playTouchType) {
			case CAGamePlayTouchType.StopTimeButton:
				if (_timeCatcherManager.StartState) {
					OnClickStopButton ();
				} else {
//					OnClickStartButton ();
				}
				break;
			case CAGamePlayTouchType.StopTimeOtherButton:
				int buttonIndex = (int)playTouchData.curTouchObject;
				_stopTimeBaseStage.TouchButtonObj (buttonIndex);
				break;
			}

		}
	}

	public override void OnTouchPress(int touchID, float posX, float posY)
	{
		if (_stopTimeBaseStage != null && _playSceneStep == StopTimePlaySceneStep.GamePlayingStep)
			_stopTimeBaseStage.OnTouchPress (touchID, posX, posY);
	}

	public override void OnTouchRelease(int touchID, float posX, float posY)
	{
		//Temp Code
//		if (_timeCatcherManager.StartState) {
//			OnClickStopButton ();
//		} else {
//			OnClickStartButton ();
//		}

		if (_stopTimeBaseStage != null && _playSceneStep == StopTimePlaySceneStep.GamePlayingStep)
			_stopTimeBaseStage.OnTouchRelease (touchID, posX, posY);
	}

	#endregion

	#region CallBack Methods

	void OnFinishedReadyDirecting()
	{
		SetPlaySceneStep (StopTimePlaySceneStep.GamePlayingStep);
	}

	void OnFinishedResultDirecting()
	{
		SetPlaySceneStep (StopTimePlaySceneStep.ResultStep);

		Debug.Log (string.Format ("!!!!OnFinishedResultDirecting()"));
	}

	void OnFinishTimeOver()
	{
		_stopTimeBaseStage.StartButton.gameObject.SetActive (true);
		if(_stopTimeBaseStage.StopButton != null)
			_stopTimeBaseStage.StopButton.gameObject.SetActive (false);

		SetPlaySceneStep (StopTimePlaySceneStep.ResultTimeOverDirectingStep);
	}

	void OnExitYesCommonPopup()
	{
        Time.timeScale = 1f;
        SoundManager.Instance.PlayEffectFX(SoundDefinitions.effectSound_2);
        CAGameGlobal.Instance.LoadGameScene(GameSceneEnum.SCENE_STAGE, SceneChangeState.SCENE_CHANGE_DIRECT);

        if (BackKeyManager.Instance != null)
            BackKeyManager.Instance.IsEnableBackKey = true;
    }

	void OnExitNoCommonPopup()
	{
        SetTimePause(false);
        SoundManager.Instance.PlayEffectFX(SoundDefinitions.effectSound_2);

        if (BackKeyManager.Instance != null)
            BackKeyManager.Instance.IsEnableBackKey = true;
    }

    void OnResultDelayTime(object objData)
    {
        SoundManager.Instance.PlayEffectFX(SoundDefinitions.effectSound_5);

    }

    void OnPerfectSoundDelayTime(object objData)
    {
        SoundManager.Instance.PlayEffectFX(SoundDefinitions.effectSound_9);
    }

    void OnResultDelayStepTime(object objData)
    {
        SetPlaySceneStep(StopTimePlaySceneStep.ResultDirectingStep);
    }

    void OnTimePause(int hour, int minute, float second)
    {
        _timeCatcherManager.ImgRealTimeNum.SetTimeValue(hour, minute, second);
        _isTimePause = true;
    }

    void OnTimeResume()
    {
        _isTimePause = false;
    }

    void OnDelayReadySound(object objData)
    {
        SoundManager.Instance.PlayEffectFX(SoundDefinitions.effectSound_3);
    }

    void OnYesAppraisePopup()
    {
        Application.OpenURL("http://www.roofzoo.com/playstore_dir.html");
        CAGameSaveDataManager.Instance.StageProgressSave.IsGameEvaluate = 1;
        CAGameSaveDataManager.Instance.StageProgressSave.SaveData();
    }

    void OnNoAppraisePopup()
    {
        CAGameSaveDataManager.Instance.StageProgressSave.IsGameEvaluate = 1;
        CAGameSaveDataManager.Instance.StageProgressSave.SaveData();
    }

    void OnNextAppraisePopup()
    {

    }

    void OnDelayResultAni(object objData)
    {
        _commonPopupController.IsEnablePopup = false;
        SetPlaySceneStep(StopTimePlaySceneStep.ResultStep);
    }

    void OnEndEpilogue(int index)
    {
        _commonPopupController.IsEnablePopup = false;
        _epilogueObj.SetActive(false);

        _commonPopupController.ShowPopup(CommonPopupType.Tobecontinued, null, null, null, OnInfiniteGameMode);
    }

    void OnInfiniteGameMode()
    {
        CAGameGlobal.Instance.LoadGameScene(GameSceneEnum.SCENE_INFINITE_MODE, SceneChangeState.SCENE_CHANGE_DIRECT);
    }

    #endregion
}
