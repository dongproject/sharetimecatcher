﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public enum StageResultAniStep
{
	RealTimeJudge,
}

public class StageResultBasePopupController : CACommonMonoBehaviours 
{
    #region Serialize Variables

#pragma warning disable 649

    [SerializeField] StepAnimationManager _stepAniManager;
	[SerializeField] Animator[] _starAnimators;
	[SerializeField] StopTimeViewObject _realTimeObject;

	[Header("Judge Time")]
	[SerializeField] tk2dSprite _judgePlusObj;
	[SerializeField] tk2dSprite _judgeMinusObj;
	[SerializeField] tk2dSprite _judgeTime_1;
	[SerializeField] tk2dSprite _judgeTime_2;

	[Header("Button Object")]
	[SerializeField] GameObject _buttonObject;

    [Header("Star Objects")]
    [SerializeField] protected GameObject[] _starObjects;

#pragma warning restore 649

    #endregion

    #region Variables

    //	float _perfectJudgeTime = 0.02f;
    //	float _awesomeJudgeTime = 0.05f;
    //	float _goodJudgeTime = 0.1f;

    StopTimeJudgeType _judgeType;

	int _stopTimeStarCount;
	bool[] _saveStarAniState = new bool[3];
	protected Action _onFinishAniAction = null;

    Action _againButtonAction = null;
    bool _isFirstClear = false;

    #endregion

    #region Properties

    public StopTimeJudgeType JudgeType
	{
		get{ return _judgeType; }
		set{ 
			_judgeType = value;
			switch (_judgeType) {
			case StopTimeJudgeType.Perfect:
				SetPerfectResult ();
				break;
			case StopTimeJudgeType.Awesome:
				SetAwesomeResult ();
				break;
			case StopTimeJudgeType.Good:
				SetGoodResult ();
				break;
			case StopTimeJudgeType.Fail:
				SetFailResult ();
				break;
			}
		}
	}

	public Action OnFinishAniAction
	{
		get{ return _onFinishAniAction; }
		set{ _onFinishAniAction = value; }
	}

    public Action AgainButtonAction
    {
        get { return _againButtonAction; }
        set { _againButtonAction = value; }
    }

    public bool IsFirstClear
    {
        get { return _isFirstClear; }
    }

    #endregion

    #region MonoBehaviour Methods

    public override void Awake()
	{
		base.Awake ();
		curTouchState = true;

		if (GameMainManager.Instance == null) {
			JudgeType = StopTimeJudgeType.Perfect;
		}
	}

	// Use this for initialization
	public override void Start () 
	{
		base.Start ();
	}

	public virtual void Update()
	{
		
	}

	#endregion

	#region Methods

	public void SetGapTimeValue(float gapValue)
	{
		float calcGapTime = Mathf.Abs (gapValue);
//		if (calcGapTime <= _perfectJudgeTime) {
//			JudgeType = StopTimeJudgeType.Perfect;
//		} else if (calcGapTime <= _awesomeJudgeTime) {
//			JudgeType = StopTimeJudgeType.Awesome;
//		} else if (calcGapTime <= _goodJudgeTime) {
//			JudgeType = StopTimeJudgeType.Good;
//		} else {
//			JudgeType = StopTimeJudgeType.Fail;
//		}

		if (gapValue < 0f) {
			_judgeMinusObj.gameObject.SetActive (true);
			_judgePlusObj.gameObject.SetActive (false);
		} else {
			_judgeMinusObj.gameObject.SetActive (false);
			_judgePlusObj.gameObject.SetActive (true);
		}

		gapValue = Mathf.Abs (gapValue);

		int viewGapValue = (int)(gapValue * 100f);
		int tenMilsec = viewGapValue / 10;
		int restMilsec = viewGapValue % 10;

		//		Debug.Log (string.Format ("gapValue : {0}, viewGapValue : {1}", gapValue, viewGapValue));

		_judgeTime_1.SetSprite (string.Format ("{0}", tenMilsec));
		_judgeTime_2.SetSprite (string.Format ("{0}", restMilsec));
	}

	public void SetTimeValue(int hourValue, int minValue, float timeSecValue)
	{
		// hour
		SetHourValue(hourValue);

		// Minute
		SetMinuteValue(minValue);

		// Second
		SetSecondValue ((int)timeSecValue);

		// Millisecond
		int millisecond = Mathf.FloorToInt((timeSecValue - (float)Mathf.FloorToInt(timeSecValue)) * 100f);
		SetMillisecondValue (millisecond);

	}

	void SetHourValue(int hourValue)
	{
		int tenHour = hourValue / 10;
		int restHour = hourValue % 10;

		_realTimeObject.HourImage_1.SetSprite (string.Format ("{0}", tenHour));
		_realTimeObject.HourImage_2.SetSprite (string.Format ("{0}", restHour));
	}

	void SetMinuteValue(int minValue)
	{
		int tenMin = minValue / 10;
		int restMin = minValue % 10;

		_realTimeObject.MinuteImage_1.SetSprite (string.Format ("{0}", tenMin));
		_realTimeObject.MinuteImage_2.SetSprite (string.Format ("{0}", restMin));
	}

	void SetSecondValue(int secValue)
	{
		int tenSec = secValue / 10;
		int restSec = secValue % 10;

		_realTimeObject.SecondImage_1.SetSprite (string.Format ("{0}", tenSec));
		_realTimeObject.SecondImage_2.SetSprite (string.Format ("{0}", restSec));
	}

	void SetMillisecondValue(int milsecValue)
	{
		int tenMilsec = milsecValue / 10;
		int restMilsec = milsecValue % 10;

		_realTimeObject.MillisecondImage_1.SetSprite (string.Format ("{0}", tenMilsec));
		_realTimeObject.MillisecondImage_2.SetSprite (string.Format ("{0}", restMilsec));
	}

	void SetResultStarCount(int starCount)
	{
		_stopTimeStarCount = starCount;

//		for (int i = 0; i < _starAnimators.Length; i++) {
//			if (i < _stopTimeStarCount) {
//				_starAnimators [i].enabled = true;
//			} else {
//				_starAnimators [i].enabled = false;
//			}
//		}

		for (int i = 0; i < _saveStarAniState.Length; i++) {
			if (i < _stopTimeStarCount) {
				_saveStarAniState [i] = true;
			} else {
				_saveStarAniState [i] = false;
			}
		}

        StageCurrentInfo stageCurInfo = CAGameSaveDataManager.Instance.StageProgressSave.GetStageCurrentInfo(CAGameGlobal.Instance.CurrentStageNum);
        _isFirstClear = false;
        if (stageCurInfo.starCount == 0)
        {
            _isFirstClear = true;
        }
        CAGameSaveDataManager.Instance.StageProgressSave.SetStageStarCount (CAGameGlobal.Instance.CurrentStageNum, starCount);
	}

	public void StartStarAni()
	{
		for (int i = 0; i < _saveStarAniState.Length; i++) {
			if(_saveStarAniState [i])
				_starAnimators [i].enabled = _saveStarAniState [i];
		}
	}

	protected virtual void SetPerfectResult()
	{
		SetResultStarCount (3);

        for (int i = 0; i < _starObjects.Length; i++){
            _starObjects[i].SetActive(true);
        }
	}

	protected virtual void SetAwesomeResult()
	{
		SetResultStarCount (2);

        for (int i = 0; i < _starObjects.Length; i++)
        {
            if(i == 2){
                _starObjects[i].SetActive(false);
            } else {
                _starObjects[i].SetActive(true);
            }
        }
    }

	protected virtual void SetGoodResult()
	{
		SetResultStarCount (1);

        for (int i = 0; i < _starObjects.Length; i++)
        {
            if (i == 0)
            {
                _starObjects[i].SetActive(true);
            }
            else
            {
                _starObjects[i].SetActive(false);
            }
        }
    }

	protected virtual void SetFailResult()
	{
		SetResultStarCount (0);
    }

	public void SetOtherReasonFail()
	{
		JudgeType = StopTimeJudgeType.Fail;

		_judgeMinusObj.gameObject.SetActive (false);
		_judgePlusObj.gameObject.SetActive (false);

		_judgeTime_1.gameObject.SetActive (false);
		_judgeTime_2.gameObject.SetActive (false);
	}

	#endregion

	#region CallBack Methods

	public void OnFinishResultAni()
	{
		if (_onFinishAniAction != null)
			_onFinishAniAction ();
	}

	public void OnExitButton()
	{
        if (StageCommonPopupController.Instance != null) {
            if (StageCommonPopupController.Instance.IsEnablePopup)
                return;
        }

        SoundManager.Instance.PlayEffectFX(SoundDefinitions.effectSound_2);
        CAGameGlobal.Instance.LoadGameScene(GameSceneEnum.SCENE_STAGE, SceneChangeState.SCENE_CHANGE_DIRECT);
	}

	public void OnFailStage()
	{
		SetOtherReasonFail ();
	}

    public void OnAgainButton()
    {
        if (StageCommonPopupController.Instance != null) {
            if (StageCommonPopupController.Instance.IsEnablePopup)
                return;
        }

        Debug.Log(string.Format("OnAgainButton"));
        if (_againButtonAction != null)
            _againButtonAction();
    }

    #endregion
}
