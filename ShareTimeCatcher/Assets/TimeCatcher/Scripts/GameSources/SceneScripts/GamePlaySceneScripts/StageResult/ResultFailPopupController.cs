﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.Advertisements;

public class ResultFailPopupController : StageResultBasePopupController
{
    #region Serialize Variables

#pragma warning disable 649

    [SerializeField] GameObject _failTextObj;
    [SerializeField] GameObject _timeOverTextObj;

#pragma warning restore 649

    #endregion

    #region Variables

    #endregion

    #region Properties

    public GameObject FailTextObj
    {
        get { return _failTextObj; }
    }

    public GameObject TimeOverTextObj
    {
        get { return _timeOverTextObj; }
    }

	#endregion

	#region MonoBehaviour Methods

	public override void Awake()
	{
		base.Awake ();
	}

	// Use this for initialization
	public override void Start () 
	{
		base.Start ();
	}

	#endregion

	#region Methods

	protected override void SetFailResult()
	{
		base.SetFailResult ();
	}

	#endregion

	#region CallBackMethods

	public void OnAds()
    {
        //UnityAdsManager.Instance.OnResultAdAction = OnResultAD;
        //UnityAdsManager.Instance.ShowRewardedAd();
    }

    //void OnResultAD(ShowResult result)
    //{
    //    if(result == ShowResult.Finished)
    //    {
    //        CAGameGlobal.Instance.IsStageHint = true;
    //        CAGameGlobal.Instance.LoadGameScene(GameSceneEnum.SCENE_GAMEPLAYING, SceneChangeState.SCENE_CHANGE_DIRECT);
    //    }
    //}

    #endregion
}
