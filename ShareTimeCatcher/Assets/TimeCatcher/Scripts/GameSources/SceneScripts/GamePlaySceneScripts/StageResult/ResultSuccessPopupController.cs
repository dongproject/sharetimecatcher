﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class ResultSuccessPopupController : StageResultBasePopupController 
{
    #region Serialize Variables

#pragma warning disable 649

    [Header("Result Text Objects")]
	[SerializeField] GameObject _perfectObject;
	[SerializeField] GameObject _awesomeObject;
	[SerializeField] GameObject _goodObject;

    [Header("Result Character Objects")]
    [SerializeField] GameObject _perfectCharacterObject;
    [SerializeField] GameObject _awesomeCharacterObject;
    [SerializeField] GameObject _goodCharacterObject;

#pragma warning restore 649

    #endregion

    #region Variables

    Action _nextButtonAction = null;

	#endregion

	#region Properties

	public Action NextButtonAction
	{
		get{ return _nextButtonAction; }
		set{ _nextButtonAction = value; }
	}

	#endregion

	#region MonoBehaviour Methods

	public override void Awake()
	{
		base.Awake ();
	}

	// Use this for initialization
	public override void Start () 
	{
		base.Start ();
	}

	#endregion

	#region Methods

	protected override void SetPerfectResult()
	{
		base.SetPerfectResult ();
		_perfectObject.SetActive (true);
        _awesomeObject.SetActive(false);
        _goodObject.SetActive(false);

        _perfectCharacterObject.SetActive(true);
        _awesomeCharacterObject.SetActive(false);
        _goodCharacterObject.SetActive(false);
    }

	protected override void SetAwesomeResult()
	{
		base.SetAwesomeResult ();
        _perfectObject.SetActive(false);
        _awesomeObject.SetActive(true);
        _goodObject.SetActive(false);

        _perfectCharacterObject.SetActive(false);
        _awesomeCharacterObject.SetActive(true);
        _goodCharacterObject.SetActive(false);
    }

	protected override void SetGoodResult()
	{
		base.SetGoodResult ();
        _perfectObject.SetActive(false);
        _awesomeObject.SetActive(false);
        _goodObject.SetActive(true);

        _perfectCharacterObject.SetActive(false);
        _awesomeCharacterObject.SetActive(false);
        _goodCharacterObject.SetActive(true);
    }

	#endregion

	#region CallBack Methods

	public void OnNextButton()
	{
        if(StageCommonPopupController.Instance != null) {
            if(StageCommonPopupController.Instance.IsEnablePopup)
                return;
        }

		if (_nextButtonAction != null)
			_nextButtonAction ();
	}

    #endregion
}
