﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;



[Obsolete]
public class StageResultManager : MonoBehaviour 
{
	#region Serialize Variables

	[SerializeField] GameObject[] _starObjects;

	[Header("Result Time")]
	[SerializeField] tk2dSprite _resultHour_1;
	[SerializeField] tk2dSprite _resultHour_2;

	[SerializeField] tk2dSprite _resultMinute_1;
	[SerializeField] tk2dSprite _resultMinute_2;

	[SerializeField] tk2dSprite _resultSecond_1;
	[SerializeField] tk2dSprite _resultSecond_2;

	[SerializeField] tk2dSprite _resultMilliSecond_1;
	[SerializeField] tk2dSprite _resultMilliSecond_2;

	[Header("ETC Objects")]
	[SerializeField] tk2dSprite _colonSprite_1;
	[SerializeField] tk2dSprite _colonSprite_2;
	[SerializeField] tk2dSprite _colonSprite_3;
	[SerializeField] tk2dSprite _colonSprite_4;
	[SerializeField] tk2dSprite _dotImg;
	[SerializeField] tk2dSlicedSprite _lineSprite_1;
	[SerializeField] tk2dSlicedSprite _lineSprite_2;

	[Header("Judge Time")]
	[SerializeField] tk2dSprite _judgePlusObj;
	[SerializeField] tk2dSprite _judgeMinusObj;
	[SerializeField] tk2dSprite _judgeTime_1;
	[SerializeField] tk2dSprite _judgeTime_2;

	[Header("Success Objects")]
	[SerializeField] GameObject _successRootObject;
	[SerializeField] GameObject _perfectObject;
	[SerializeField] GameObject _awesomeObject;
	[SerializeField] GameObject _goodObject;

	[Header("Fail Objects")]
	[SerializeField] GameObject _failRootObject;
	[SerializeField] GameObject _failObject;

	#endregion

	#region Variables

	Color _successColor = new Color(19f/255f, 118f/255f, 181f/255f, 1f);
	Color _failColor = new Color(176f/255f, 66f/255f, 33f/255f, 1f);

	float _perfectJudgeTime = 0.02f;
	float _awesomeJudgeTime = 0.05f;
	float _goodJudgeTime = 0.1f;

	Action _onFinishedDirecting = null;

	StopTimeResultStep _curResultStep = StopTimeResultStep.None;

	float _tempDirectingWaitTime = 2f;
	float _curdirectingTime = 0f;

	StopTimeJudgeType _judgeType;
	int _stopTimeStarCount;

	#endregion

	#region Properties

	public Action OnFinishedDirecting
	{
		get{ return _onFinishedDirecting; }
		set{ _onFinishedDirecting = value; }
	}

	public StopTimeJudgeType JudgeType
	{
		get{ return _judgeType; }
		set{ 
			_judgeType = value;
			switch (_judgeType) {
			case StopTimeJudgeType.Perfect:
				SetPerfectResult ();
				break;
			case StopTimeJudgeType.Awesome:
				SetAwesomeResult ();
				break;
			case StopTimeJudgeType.Good:
				SetGoodResult ();
				break;
			case StopTimeJudgeType.Fail:
				SetFailResult ();
				break;
			}
		}
	}

	#endregion

	#region MonoBehaviour Methods

	// Use this for initialization
	void Start () 
	{
//		SetCurrentResultStep (StopTimeResultStep.ResultDirecting);
	}
	
	// Update is called once per frame
	void Update () 
	{
		switch (_curResultStep) {
		case StopTimeResultStep.ResultDirecting:
			_curdirectingTime += Time.deltaTime;
			if (_curdirectingTime >= _tempDirectingWaitTime) {
				SetCurrentResultStep (StopTimeResultStep.Result);
			}
			break;
		}
	}

	#endregion

	#region Methods

	public void SetGapTimeValue(float gapValue)
	{
		float calcGapTime = Mathf.Abs (gapValue);
        if (calcGapTime <= GetPerfectJudgeTime()) {
			JudgeType = StopTimeJudgeType.Perfect;
		} else if (calcGapTime <= GetAwesomeJudgeTime()) {
			JudgeType = StopTimeJudgeType.Awesome;
		} else if (calcGapTime <= GetGoodJudgeTime()) {
			JudgeType = StopTimeJudgeType.Good;
		} else {
			JudgeType = StopTimeJudgeType.Fail;
		}

		if (gapValue < 0f) {
			_judgeMinusObj.gameObject.SetActive (true);
			_judgePlusObj.gameObject.SetActive (false);
		} else {
			_judgeMinusObj.gameObject.SetActive (false);
			_judgePlusObj.gameObject.SetActive (true);
		}

		gapValue = Mathf.Abs (gapValue);

		int viewGapValue = (int)(gapValue * 100f);
		int tenMilsec = viewGapValue / 10;
		int restMilsec = viewGapValue % 10;

//		Debug.Log (string.Format ("gapValue : {0}, viewGapValue : {1}", gapValue, viewGapValue));

		_judgeTime_1.SetSprite (string.Format ("{0}", tenMilsec));
		_judgeTime_2.SetSprite (string.Format ("{0}", restMilsec));
	}

    float GetPerfectJudgeTime()
    {
        if(CAGameGlobal.Instance.CurrentStageNum > 10 && CAGameGlobal.Instance.CurrentStageNum <= 20)
        {
            return _perfectJudgeTime - 0.005f;
        }
        else if(CAGameGlobal.Instance.CurrentStageNum > 20 && CAGameGlobal.Instance.CurrentStageNum <= 25)
        {
            return _perfectJudgeTime - 0.01f;
        }
        else if(CAGameGlobal.Instance.CurrentStageNum > 20 && CAGameGlobal.Instance.CurrentStageNum <= 25)
        {
            return 0f;
        }

        return _perfectJudgeTime;
    }

    float GetAwesomeJudgeTime()
    {
        if (CAGameGlobal.Instance.CurrentStageNum > 10 && CAGameGlobal.Instance.CurrentStageNum <= 20)
        {
            return _awesomeJudgeTime - 0.01f;
        }
        else if (CAGameGlobal.Instance.CurrentStageNum > 20 && CAGameGlobal.Instance.CurrentStageNum <= 25)
        {
            return _awesomeJudgeTime - 0.02f;
        }
        else if (CAGameGlobal.Instance.CurrentStageNum > 20 && CAGameGlobal.Instance.CurrentStageNum <= 25)
        {
            return _awesomeJudgeTime - 0.04f;
        }

        return _awesomeJudgeTime;
    }

    float GetGoodJudgeTime()
    {
        if (CAGameGlobal.Instance.CurrentStageNum > 10 && CAGameGlobal.Instance.CurrentStageNum <= 20)
        {
            return _goodJudgeTime - 0.02f;
        }
        else if (CAGameGlobal.Instance.CurrentStageNum > 20 && CAGameGlobal.Instance.CurrentStageNum <= 25)
        {
            return _goodJudgeTime - 0.04f;
        }
        else if (CAGameGlobal.Instance.CurrentStageNum > 20 && CAGameGlobal.Instance.CurrentStageNum <= 25)
        {
            return _goodJudgeTime - 0.07f;
        }

        return _goodJudgeTime;
    }

    public void SetOtherReasonFail()
	{
		JudgeType = StopTimeJudgeType.Fail;

		_judgeMinusObj.gameObject.SetActive (false);
		_judgePlusObj.gameObject.SetActive (false);

		_judgeTime_1.gameObject.SetActive (false);
		_judgeTime_2.gameObject.SetActive (false);
	}

	public void SetCurrentResultStep(StopTimeResultStep resultStep)
	{
		_curResultStep = resultStep;
		switch (_curResultStep) {
		case StopTimeResultStep.ResultDirecting:
			SetResultDirectingStep ();
			break;
		case StopTimeResultStep.Result:
			SetResultStep ();
			break;
		}
	}

	void SetResultDirectingStep()
	{
		_tempDirectingWaitTime = 2f;
		_curdirectingTime = 0f;
    }

	void SetResultStep()
	{
		if (_onFinishedDirecting != null)
			_onFinishedDirecting ();
	}

	void SetResultStarCount(int starCount)
	{
		_stopTimeStarCount = starCount;

		for (int i = 0; i < _starObjects.Length; i++) {
			if (i < _stopTimeStarCount) {
				_starObjects [i].gameObject.SetActive (true);
			} else {
				_starObjects [i].gameObject.SetActive (false);
			}
		}

		CAGameSaveDataManager.Instance.StageProgressSave.SetStageStarCount (CAGameGlobal.Instance.CurrentStageNum, starCount);
	}

	void SetPerfectResult()
	{
		_failRootObject.gameObject.SetActive (false);

		_successRootObject.gameObject.SetActive (true);
		_perfectObject.gameObject.SetActive (true);
		_awesomeObject.gameObject.SetActive (false);
		_goodObject.gameObject.SetActive (false);

		SetResultStarCount (3);

		SetTimeColor (_successColor);
	}

	void SetAwesomeResult()
	{
		_failRootObject.gameObject.SetActive (false);

		_successRootObject.gameObject.SetActive (true);
		_perfectObject.gameObject.SetActive (false);
		_awesomeObject.gameObject.SetActive (true);
		_goodObject.gameObject.SetActive (false);

		SetResultStarCount (2);

		SetTimeColor (_successColor);
	}

	void SetGoodResult()
	{
		_failRootObject.gameObject.SetActive (false);

		_successRootObject.gameObject.SetActive (true);
		_perfectObject.gameObject.SetActive (false);
		_awesomeObject.gameObject.SetActive (false);
		_goodObject.gameObject.SetActive (true);

		SetResultStarCount (1);

		SetTimeColor (_successColor);
	}

	void SetFailResult()
	{
		_successRootObject.gameObject.SetActive (false);

		_failRootObject.gameObject.SetActive (true);

		SetResultStarCount (0);

		SetTimeColor (_failColor);
	}

	void SetTimeColor(Color curTimeTextColor)
	{
		_resultHour_1.color = curTimeTextColor;
		_resultHour_2.color = curTimeTextColor;

		_resultMinute_1.color = curTimeTextColor;
		_resultMinute_2.color = curTimeTextColor;

		_resultSecond_1.color = curTimeTextColor;
		_resultSecond_2.color = curTimeTextColor;

		_resultMilliSecond_1.color = curTimeTextColor;
		_resultMilliSecond_2.color = curTimeTextColor;

		_judgePlusObj.color = curTimeTextColor;
		_judgeMinusObj.color = curTimeTextColor;
		_judgeTime_1.color = curTimeTextColor;
		_judgeTime_2.color = curTimeTextColor;

		_colonSprite_1.color = curTimeTextColor;
		_colonSprite_2.color = curTimeTextColor;
		_colonSprite_3.color = curTimeTextColor;
		_colonSprite_4.color = curTimeTextColor;
		_dotImg.color = curTimeTextColor;
		_lineSprite_1.color = curTimeTextColor;
		_lineSprite_2.color = curTimeTextColor;
	}

	public void SetTimeValue(int hourValue, int minValue, float timeSecValue)
	{
		// hour
		SetHourValue(hourValue);

		// Minute
		SetMinuteValue(minValue);

		// Second
		SetSecondValue ((int)timeSecValue);

		// Millisecond
		int millisecond = Mathf.FloorToInt((timeSecValue - (float)Mathf.FloorToInt(timeSecValue)) * 100f);
		SetMillisecondValue (millisecond);

	}

	void SetHourValue(int hourValue)
	{
		int tenHour = hourValue / 10;
		int restHour = hourValue % 10;

		_resultHour_1.SetSprite (string.Format ("{0}", tenHour));
		_resultHour_2.SetSprite (string.Format ("{0}", restHour));
	}

	void SetMinuteValue(int minValue)
	{
		int tenMin = minValue / 10;
		int restMin = minValue % 10;

		_resultMinute_1.SetSprite (string.Format ("{0}", tenMin));
		_resultMinute_2.SetSprite (string.Format ("{0}", restMin));
	}

	void SetSecondValue(int secValue)
	{
		int tenSec = secValue / 10;
		int restSec = secValue % 10;

		_resultSecond_1.SetSprite (string.Format ("{0}", tenSec));
		_resultSecond_2.SetSprite (string.Format ("{0}", restSec));
	}

	void SetMillisecondValue(int milsecValue)
	{
		int tenMilsec = milsecValue / 10;
		int restMilsec = milsecValue % 10;

		_resultMilliSecond_1.SetSprite (string.Format ("{0}", tenMilsec));
		_resultMilliSecond_2.SetSprite (string.Format ("{0}", restMilsec));
	}

	#endregion

	#region Callback Methods

	public void OnFailStage()
	{
		SetOtherReasonFail ();
	}

	#endregion
}
