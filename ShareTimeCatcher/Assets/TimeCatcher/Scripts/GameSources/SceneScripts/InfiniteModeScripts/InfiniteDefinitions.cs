﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InfiniteDefinitions
{
    public enum ButtonKind
    {
        Ufo             = 0,
        Cactus          = 1,
        Nuclear         = 2,
        Fist            = 3,
        Can             = 4,
        Pipe            = 5,
        Oldgraphic      = 6,
        Mirrorball      = 7,
        Dimension       = 8,
        Cat             = 9,
        Jelly           = 10,
        Slime           = 11,
        Dart            = 12,
        Tornado         = 13,
        Trap            = 14,
        Circus          = 15,
        Plasma          = 16,
        Bubble          = 17,
        Compass         = 18,
        Magic           = 19,
        Max,
    }

    public enum ObstacleKind
    {
        Snail           = 0,
        Dog             = 1,
        Bee             = 2,
        Matryo          = 3,
        Pudding         = 4,
        Blanket         = 5,
        Doli            = 6,
        Cat             = 7,
        Father          = 8,
        Angel1          = 9,
        Angel2          = 10,
        Angel3          = 11,
        Angel4          = 12,
        Moza            = 13,
        Moonwalk        = 14,
        Ninja           = 15,
        Swing           = 16,
    }
}
