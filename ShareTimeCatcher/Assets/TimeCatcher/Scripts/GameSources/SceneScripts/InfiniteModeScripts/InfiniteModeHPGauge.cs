﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InfiniteModeHPGauge : MonoBehaviour
{
    #region Serialize Variables

    [SerializeField] tk2dSlicedSprite _tk2dSpriteBG;
    [SerializeField] tk2dSlicedSprite _tk2dSpriteGauge;

    #endregion

    #region Variables

    float _gaugeBGWidth;
    float _gaugeValue;

    #endregion

    #region Properties

    #endregion

    #region MonoBehaviour Methods

    private void Awake()
    {
        _gaugeBGWidth = _tk2dSpriteBG.dimensions.x;
    }

    #endregion

    #region Methods

    public void SetGaugeValue(float gauge)
    {
        if (gauge > 1f)
            gauge = 1f;
        _gaugeValue = gauge;
        _tk2dSpriteGauge.dimensions = new Vector2(_gaugeBGWidth * gauge, _tk2dSpriteGauge.dimensions.y);
    }

    #endregion
}
