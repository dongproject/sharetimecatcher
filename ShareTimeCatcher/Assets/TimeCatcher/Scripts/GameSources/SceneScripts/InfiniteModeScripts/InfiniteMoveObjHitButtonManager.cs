﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class InfiniteMoveObjHitButtonManager : MonoBehaviour
{
    #region Definitions

    public enum HitButtonObjIndex
    {
        Throwing_1 = 0,
        Throwing_2 = 1,
        Throwing_3 = 2,
        Throwing_4 = 3,
        Throwing_5 = 4,
        Max = 5,
    }

    #endregion

    #region Serialize Variables

#pragma warning disable 649

    [SerializeField] GameObject _buttonObject;

    [Header("Start Positions")]
    [SerializeField] Transform[] _startPositions;

    [Header("Throwing Objects")]
    [SerializeField] tk2dSprite _throwing_1;
    [SerializeField] tk2dSprite _throwing_2;
    [SerializeField] tk2dSprite _throwing_3;
    [SerializeField] tk2dSprite _throwing_4;
    [SerializeField] tk2dSprite _throwing_5;

    [Header("Bomb Objects")]
    [SerializeField] tk2dSpriteAnimator _bombSpriteAniEffect_1;
    [SerializeField] tk2dSpriteAnimator _bombSpriteAniEffect_2;

#pragma warning restore 649

    #endregion

    #region Variables

    float _appearGapTime = 7f;
    float _curAppearTime = 0f;
    Dictionary<int, int> _startPositionDic = new Dictionary<int, int>();
    int _curStartPosIndex = -1;

    Dictionary<int, int> _moveObjectDic = new Dictionary<int, int>();
    int _curMoveIndex = -1;

    int _collideIndex = -1;

    Action _onCollideButton = null;

    CAGameTimerEventCtl _eventTimer = new CAGameTimerEventCtl();

    protected List<StopTimeObjectInfo> _motionAniObjectInfos = new List<StopTimeObjectInfo>();

    protected ObjectMotionManager _motionManager = new ObjectMotionManager();

    protected Dictionary<int /* Object Index */, Transform> _motionAniTransforms = new Dictionary<int, Transform>();

    #endregion

    #region Properties

    public float AppearGapTime
    {
        get { return _appearGapTime; }
        set { _appearGapTime = value; }
    }

    public Action OnCollideButton
    {
        get { return _onCollideButton; }
        set { _onCollideButton = value; }
    }

    #endregion

    #region MonoBehaviour Methods

    private void Awake()
    {
        AddStopTimeObjectInfo(StageObjectType.MotionAni, (int)HitButtonObjIndex.Throwing_1, _throwing_1, new Vector3(479f, 32f, -10f));
        AddStopTimeObjectInfo(StageObjectType.MotionAni, (int)HitButtonObjIndex.Throwing_2, _throwing_2, new Vector3(495f, -170f, -10f));
        AddStopTimeObjectInfo(StageObjectType.MotionAni, (int)HitButtonObjIndex.Throwing_3, _throwing_3, new Vector3(-477f, -331f, -10f));         AddStopTimeObjectInfo(StageObjectType.MotionAni, (int)HitButtonObjIndex.Throwing_4, _throwing_4, new Vector3(-489f, -156f, -10f));         AddStopTimeObjectInfo(StageObjectType.MotionAni, (int)HitButtonObjIndex.Throwing_5, _throwing_5, new Vector3(-485f, -11f, -10f));

        for (int i = 0; i < _startPositions.Length; i++)
        {
            _startPositionDic.Add(i, i);
        }

        for (int i = 0; i < (int)HitButtonObjIndex.Max; i++)
        {
            _moveObjectDic.Add(i, i);
        }
    }

    #endregion

    #region Methods

    public void UpdateHitButton()
    {
        _motionManager.Update();
        CheckAppearObject();
        _eventTimer.UpdateGameTimer();
    }

    //public void SetButtonObject(GameObject btnObj)
    //{
    //    _buttonObject = btnObj;
    //}

    public void AddStopTimeObjectInfo(StageObjectType objectType, int objectIndex, tk2dSprite objectSprite, Vector3 matchingPos)     {         StopTimeObjectInfo inputObjectInfo = new StopTimeObjectInfo();         inputObjectInfo.ObjectIndex = objectIndex;          inputObjectInfo.ObjStartPos = objectSprite.transform.localPosition;          inputObjectInfo.ObjWidth = objectSprite.GetBounds().size.x;         inputObjectInfo.ObjHeight = objectSprite.GetBounds().size.y;          inputObjectInfo.ObjTransform = objectSprite.transform;         inputObjectInfo.MatchingPos = matchingPos;          switch (objectType)         {             case StageObjectType.MotionAni:                 _motionAniObjectInfos.Add(inputObjectInfo);                 break;         }     }      void CheckAppearObject()
    {
        _curAppearTime += Time.deltaTime;
        if (_curAppearTime >= _appearGapTime && _moveObjectDic.Count > 0)
        {
            _curAppearTime = 0f;
            List<int> moveObjKeys = _moveObjectDic.Keys.ToList();
            int ranValue = UnityEngine.Random.Range(0, moveObjKeys.Count);
            if (_curMoveIndex != -1)
            {
                _moveObjectDic.Add(_curMoveIndex, _curMoveIndex);
            }
            _curMoveIndex = _moveObjectDic[moveObjKeys[ranValue]];
            _moveObjectDic.Remove(_curMoveIndex);

            if (_motionAniTransforms.ContainsKey(_curMoveIndex))
                _motionAniTransforms.Remove(_curMoveIndex);

            List<int> postionKeys = _startPositionDic.Keys.ToList();
            int ranPosIndex = UnityEngine.Random.Range(0, postionKeys.Count);
            if (_curStartPosIndex != -1)
                _startPositionDic.Add(_curStartPosIndex, _curStartPosIndex);
            _curStartPosIndex = _startPositionDic[postionKeys[ranPosIndex]];
            _startPositionDic.Remove(_curStartPosIndex);

            //           Debug.Log (string.Format ("CheckAppearObject _curMoveIndex : {0}, _curStartPosIndex : {1}, _moveObjectDic Count {2}", 
            //               _curMoveIndex, _curStartPosIndex, _moveObjectDic.Count));
            AddObjectMoveAni(_curMoveIndex, _startPositions[_curStartPosIndex].localPosition, _buttonObject.transform.localPosition, _curMoveIndex, 1.5f);
        }
    }


    void AddObjectMoveAni(int objIndex, Vector3 startPos, Vector2 destValue, int motionID, float aniTime = 1f)
    {
        Transform moveTransform = null;
        for (int i = 0; i < _motionAniObjectInfos.Count; i++)
        {
            if (_motionAniObjectInfos[i].ObjectIndex == objIndex)
            {
                moveTransform = _motionAniObjectInfos[i].ObjTransform;
                if (moveTransform.localPosition.x == destValue.x && moveTransform.localPosition.y == destValue.y)
                {
                    Debug.Log(string.Format("AddObjectMoveAni Same Position!!!"));
                    return;
                }
                break;
            }
        }

        if (moveTransform == null)
        {
            Debug.Log(string.Format("AddObjectMoveAni Not Exist MoveAni Object!!!"));
            return;
        }

        moveTransform.localPosition = startPos;
        moveTransform.gameObject.SetActive(true);

        Vector3 destPos = new Vector3(destValue.x,
           destValue.y, moveTransform.localPosition.z);
        _motionManager.AddObjTimeMovement(aniTime, startPos, destPos, OnChangeMoveValue, motionID, OnCompleteMotionAni);

        _motionAniTransforms.Add(motionID, moveTransform);
    }

    public void OnTouchPress(int touchID, float posX, float posY)
    {
        //Debug.Log (string.Format ("OnTouchPress touchID : {0}, posX : {1}, posY : {2}", touchID, posX, posY));

        float imgPosX = CAResolutionCtl.Instance.GetScreenToImagePosX(posX);
        float imgPosY = CAResolutionCtl.Instance.GetScreenToImagePosY(posY);

        float revisionValue = 5f;

        for (int i = 0; i < _motionAniObjectInfos.Count; i++)
        {
            int motionID = _motionAniObjectInfos[i].ObjectIndex;

            if (!_motionAniTransforms.ContainsKey(motionID))
                continue;

            if ((_motionAniObjectInfos[i].XMin - revisionValue <= imgPosX && _motionAniObjectInfos[i].XMax + revisionValue >= imgPosX) &&
                (_motionAniObjectInfos[i].YMin - revisionValue <= imgPosY && _motionAniObjectInfos[i].YMax + revisionValue >= imgPosY))
            {

                //Debug.Log(string.Format("CheckMatchObject Matched index : {0}, Object Index : {1}", i, _motionAniObjectInfos[i].ObjectIndex));

                SoundManager.Instance.PlayEffectFX(SoundDefinitions.effectSound_17);

                tk2dSpriteAnimator bombAnimator = _bombSpriteAniEffect_2;
                bombAnimator.transform.localPosition = new Vector3(_motionAniTransforms[motionID].localPosition.x,
                    _motionAniTransforms[motionID].localPosition.y, -10f);
                bombAnimator.gameObject.SetActive(true);
                _eventTimer.SetGameTimerData(OnDisableBombEffect, 0.5f, bombAnimator);

                _motionAniTransforms[motionID].gameObject.SetActive(false);
                _motionAniTransforms.Remove(motionID);

                _motionManager.ReleaseObjMovement(motionID);
                break;
            }
        }
    }

    #endregion

    #region CallBack Methods

    void OnChangeMoveValue(int motionID, ObjectMotionManager.MotionType motionType, Vector2 moveValue)
    {
        if (!_motionAniTransforms.ContainsKey(motionID))
            return;

        Vector3 curPosition = _motionAniTransforms[motionID].localPosition;
        _motionAniTransforms[motionID].localPosition = new Vector3(curPosition.x + moveValue.x, curPosition.y + moveValue.y, curPosition.z);

        float calcDis = CAMathCtl.Instance.CalcDistance(_buttonObject.transform.localPosition.x, _buttonObject.transform.localPosition.y,
            _motionAniTransforms[motionID].localPosition.x, _motionAniTransforms[motionID].localPosition.y);

        if (calcDis < 100f)
        {
            _collideIndex = motionID;
            tk2dSpriteAnimator bombAnimator = _bombSpriteAniEffect_1;

            bombAnimator.transform.localPosition = new Vector3(_motionAniTransforms[motionID].localPosition.x,
                _motionAniTransforms[motionID].localPosition.y, -10f);
            bombAnimator.gameObject.SetActive(true);
            _eventTimer.SetGameTimerData(OnDisableBombEffect, 0.5f, bombAnimator);
            _motionAniTransforms[motionID].gameObject.SetActive(false);
            _motionAniTransforms.Remove(motionID);

            _motionManager.ReleaseObjMovement(motionID);
        }


        //      Debug.Log (string.Format ("OnChangeMoveValue motionID : {0}, calcDis: {1}", motionID, calcDis));
    }

    void OnCompleteMotionAni(int motionID, ObjectMotionAni objMotionAni)
    {
        if (_collideIndex == motionID && objMotionAni != null)
        {
            if(_onCollideButton != null)
            {
                _onCollideButton();
            }
            _collideIndex = -1;
        }

        if (_motionAniTransforms.ContainsKey(motionID))
        {
            _motionAniTransforms.Remove(motionID);
        }
    }

    void OnDisableBombEffect(object objData)
    {
        tk2dSpriteAnimator bombAnimator = objData as tk2dSpriteAnimator;
        bombAnimator.gameObject.SetActive(false);
    }

    #endregion
}
