﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIResultInfiniteBasePopup : MonoBehaviour 
{
    #region Serialize Variables

#pragma warning disable 649

    [SerializeField] StopTimeViewObject _realTimeObject;

#pragma warning restore 649

    #endregion

    #region Variables

    Action _onExitAction = null;
    Action _onAgainAction = null;

    #endregion

    #region Properties

    public Action OnExitAction
    {
        get { return _onExitAction; }
        set { _onExitAction = value; }
    }

    public Action OnAgainAction
    {
        get { return _onAgainAction; }
        set { _onAgainAction = value; }
    }

    public StopTimeViewObject RealTimeObject
    {
        get { return _realTimeObject; }
    }

    #endregion

    #region MonoBehaviour Methods

    protected virtual void Awake()
    {

    }

    #endregion

    #region Methods

    public void SetTotalTimeValue(int hour, int minute, float second)     {         SetHourValue(hour);          SetMinuteValue(minute);          SetSecondValue((int)second);          int millisecond = Mathf.FloorToInt((second - (float)Mathf.FloorToInt(second)) * 100f);         SetMillisecondValue(millisecond);     }

    void SetHourValue(int hourValue)
    {
        int tenHour = hourValue / 10;
        int restHour = hourValue % 10;          _realTimeObject.HourImage_1.SetSprite(string.Format("{0}", tenHour));         _realTimeObject.HourImage_2.SetSprite(string.Format("{0}", restHour));
    }

    void SetMinuteValue(int minValue)
    {
        int tenMin = minValue / 10;         int restMin = minValue % 10;

        _realTimeObject.MinuteImage_1.SetSprite(string.Format("{0}", tenMin));
        _realTimeObject.MinuteImage_2.SetSprite(string.Format("{0}", restMin));
    }

    void SetSecondValue(int secValue)
    {
        int tenSec = secValue / 10;         int restSec = secValue % 10;

        _realTimeObject.SecondImage_1.SetSprite(string.Format("{0}", tenSec));
        _realTimeObject.SecondImage_2.SetSprite(string.Format("{0}", restSec));
    }

    void SetMillisecondValue(int milsecValue)
    {
        int tenMilsec = milsecValue / 10;
        int restMilsec = milsecValue % 10;          _realTimeObject.MillisecondImage_1.SetSprite(string.Format("{0}", tenMilsec));
        _realTimeObject.MillisecondImage_2.SetSprite(string.Format("{0}", restMilsec));
    }

    #endregion

    #region CallBack Methods

    public void OnExitButton()
    {
        if (_onExitAction != null)
            _onExitAction();
    }

    public void OnAgainButton()
    {
        if (_onAgainAction != null)
            _onAgainAction();
    }

    public void OnButtonShop()
    {
        CAGameGlobal.Instance.LoadGameScene(GameSceneEnum.SCENE_INFINITE_THEME, SceneChangeState.SCENE_CHANGE_DIRECT);
    }

    #endregion
}
