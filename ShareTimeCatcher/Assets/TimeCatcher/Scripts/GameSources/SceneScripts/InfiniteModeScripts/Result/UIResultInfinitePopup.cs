﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIResultInfinitePopup : UIResultInfiniteBasePopup
{
    #region Serialize Variables

#pragma warning disable 649

    [SerializeField] StopTimeViewObject _bestRecordTimeObject;
    [SerializeField] StopTimeViewObject _totalPlayTimeViewObj;

#pragma warning restore 649

    #endregion

    #region Variables

    #endregion

    #region Properties

    public StopTimeViewObject BestRecordTimeObject
    {
        get { return _bestRecordTimeObject; }
    }

    #endregion

    #region MonoBehaviour Methods

    protected override void Awake()
    {
        base.Awake();
    }

    #endregion

    #region Methods

    public void SetBestRecordTotalTimeValue(float secTotalValue)
    {
        int quotientHour = (int)secTotalValue / 3600;         int restHourSec = (int)secTotalValue % 3600;          SetTimeViewHourValue(quotientHour, _bestRecordTimeObject);          int quotientMin = (int)restHourSec / 60;         int restMinSec = (int)restHourSec % 60;          SetTimeViewMinuteValue(quotientMin, _bestRecordTimeObject);          SetTimeViewSecondValue(restMinSec, _bestRecordTimeObject);          int millisecond = Mathf.FloorToInt((secTotalValue - (float)Mathf.FloorToInt(secTotalValue)) * 100f);         SetTimeViewMillisecondValue(millisecond, _bestRecordTimeObject);
    }

    public void SetTotalPlayTimeValue(float secTotalValue)
    {
        int quotientHour = (int)secTotalValue / 3600;         int restHourSec = (int)secTotalValue % 3600;          SetTimeViewHourValue(quotientHour, _totalPlayTimeViewObj);          int quotientMin = (int)restHourSec / 60;         int restMinSec = (int)restHourSec % 60;          SetTimeViewMinuteValue(quotientMin, _totalPlayTimeViewObj);          SetTimeViewSecondValue(restMinSec, _totalPlayTimeViewObj);          int millisecond = Mathf.FloorToInt((secTotalValue - (float)Mathf.FloorToInt(secTotalValue)) * 100f);         SetTimeViewMillisecondValue(millisecond, _totalPlayTimeViewObj);
    }

    void SetTimeViewHourValue(int hourValue, StopTimeViewObject timeViewObj)
    {
        int tenHour = hourValue / 10;
        int restHour = hourValue % 10;

        timeViewObj.HourImage_1.SetSprite(string.Format("{0}", tenHour));
        timeViewObj.HourImage_2.SetSprite(string.Format("{0}", restHour));
    }

    void SetTimeViewMinuteValue(int minValue, StopTimeViewObject timeViewObj)
    {
        int tenMin = minValue / 10;
        int restMin = minValue % 10;

        timeViewObj.MinuteImage_1.SetSprite(string.Format("{0}", tenMin));
        timeViewObj.MinuteImage_2.SetSprite(string.Format("{0}", restMin));
    }

    void SetTimeViewSecondValue(int secValue, StopTimeViewObject timeViewObj)
    {
        int tenSec = secValue / 10;
        int restSec = secValue % 10;

        timeViewObj.SecondImage_1.SetSprite(string.Format("{0}", tenSec));
        timeViewObj.SecondImage_2.SetSprite(string.Format("{0}", restSec));
    }

    void SetTimeViewMillisecondValue(int milsecValue, StopTimeViewObject timeViewObj)
    {
        int tenMilsec = milsecValue / 10;
        int restMilsec = milsecValue % 10;

        timeViewObj.MillisecondImage_1.SetSprite(string.Format("{0}", tenMilsec));
        timeViewObj.MillisecondImage_2.SetSprite(string.Format("{0}", restMilsec));
    }

    #endregion
}
