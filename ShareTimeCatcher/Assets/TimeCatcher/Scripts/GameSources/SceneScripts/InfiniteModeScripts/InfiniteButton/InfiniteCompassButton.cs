﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InfiniteCompassButton : InfiniteBaseButton 
{
    #region Serialize Variables

    #endregion

    #region Variables

    #endregion

    #region Properties

    #endregion

    #region Methods

    public override void TouchButton()
    {
        base.TouchButton();
    }

    public override void ReleaseButtonEffect()
    {
        base.ReleaseButtonEffect();
    }

    #endregion
}
