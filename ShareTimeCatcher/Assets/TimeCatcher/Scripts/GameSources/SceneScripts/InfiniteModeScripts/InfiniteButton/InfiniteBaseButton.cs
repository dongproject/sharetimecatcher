﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InfiniteThemeButtonColorInfo
{
    public float start_V;
    public tk2dSprite buttonSprite;
}

public class InfiniteBaseButton : MonoBehaviour
{
    #region Serialize Variables

#pragma warning disable 649

    [SerializeField] protected int _buttonIndex;
    [SerializeField] protected BoxCollider _buttonCollider;
    [SerializeField] protected tk2dSprite[] _buttonSprites;
    [SerializeField] protected float _effectReleaseTime = 1f;

    [Header("Stand Objects")]
    [SerializeField] GameObject[] _standButtonObjs;
    //[SerializeField] UGUITweener[] _standTweeners;
    [SerializeField] GameObject[] _standTweenGroupObjs;
    [SerializeField] Animator[] _standAnimators;

    [Header("Touched Objects")]
    [SerializeField] GameObject[] _touchedObjects;
    //[SerializeField] UGUITweener[] _touchedTweeners;
    [SerializeField] GameObject[] _touchedTweenGroupObjs;
    [SerializeField] Animator[] _touchedAnimators;

#pragma warning restore 649

    #endregion

    #region Variables

    string[] _touchedAniNames;
    string[] _standAniNames;

    //int _isTouchedState = 0;
    //float _curTime = 0f;

    List<UGUITweener> _standTweenerList = new List<UGUITweener>();
    List<UGUITweener> _touchedTweenerList = new List<UGUITweener>();
    List<InfiniteThemeButtonColorInfo> _themeButtonColorInfos = new List<InfiniteThemeButtonColorInfo>();

    #endregion

    #region Properties

    public int ButtonIndex
    {
        get { return _buttonIndex; }
    }

    public BoxCollider ButtonCollider
    {
        get { return _buttonCollider; }
    }

    public tk2dSprite[] ButtonSprites
    {
        get{ return _buttonSprites; }
    }

    public float EffectReleaseTime
    {
        get { return _effectReleaseTime; }
    }

    #endregion

    #region MonoBehaviour Methods

    private void Awake()
    {

        if (_touchedAnimators != null && _touchedAnimators.Length > 0)
        {
            _touchedAniNames = new string[_touchedAnimators.Length];
            for(int i = 0;i< _touchedAnimators.Length; i++)
            {
                _touchedAnimators[i].gameObject.SetActive(true);
                //float playTime = _touchedAnimators[i].GetCurrentAnimatorStateInfo(0).normalizedTime;
                //Debug.Log(string.Format("InfiniteBaseButton playTime : {0}", playTime));
                 AnimatorClipInfo[] clips = _touchedAnimators[i].GetCurrentAnimatorClipInfo(0);
                _effectReleaseTime = clips[0].clip.length + 0.2f;
                Debug.Log(string.Format("InfiniteBaseButton playTime : {0}", clips[0].clip.length));
                _touchedAniNames[i] = clips[0].clip.name;
            }
        }

        if (_standAnimators != null && _standAnimators.Length > 0)
        {
            _standAniNames = new string[_standAnimators.Length];
            for (int i = 0; i < _standAnimators.Length; i++)
            {
                _standAnimators[i].gameObject.SetActive(true);
                AnimatorClipInfo[] clips = _standAnimators[i].GetCurrentAnimatorClipInfo(0);
                
                _standAniNames[i] = clips[0].clip.name;
            }
        }

        InitObjects();

        SetTweeners();
    }

    //private void Update()
    //{
    //    if (_isTouchedState == 0)
    //    {
    //        _curTime += Time.deltaTime;
    //        if(_curTime >= 3f)
    //        {
    //            _isTouchedState = 1;
    //            _curTime = 0f;
    //            TouchButton();
    //        }
    //    }
    //    else if(_isTouchedState == 1)
    //    {
    //        _curTime += Time.deltaTime;
    //        if (_curTime >= 3f)
    //        {
    //            _isTouchedState = 2;
    //            _curTime = 0f;
    //            ReleaseButtonEffect();
    //        }
    //    }
    //    else if(_isTouchedState == 2)
    //    {
    //        _curTime += Time.deltaTime;
    //        if(_curTime >= 3f)
    //        {
    //            _isTouchedState = 0;
    //            _curTime = 0f;
    //        }
    //    }
    //}

    #endregion

    #region Methods

    void InitObjects()
    {
        if(_standButtonObjs != null && _standButtonObjs.Length > 0)
        {
            for(int i = 0;i< _standButtonObjs.Length; i++)
            {
                _standButtonObjs[i].SetActive(true);
            }
        }

        if (_touchedObjects != null && _touchedObjects.Length > 0)
        {
            for (int i = 0; i < _touchedObjects.Length; i++)
            {
                _touchedObjects[i].SetActive(false);
            }
        }

        if(_buttonSprites != null && _buttonSprites.Length > 0)
        {
            for(int i = 0;i< _buttonSprites.Length; i++)
            {
                InfiniteThemeButtonColorInfo inputButtonColorInfo = new InfiniteThemeButtonColorInfo();
                Color buttonColor = _buttonSprites[i].color;

                float button_H;
                float button_S;
                float button_V;
                Color.RGBToHSV(buttonColor, out button_H, out button_S, out button_V);

                inputButtonColorInfo.buttonSprite = _buttonSprites[i];
                inputButtonColorInfo.start_V = button_V;

                _themeButtonColorInfos.Add(inputButtonColorInfo);
            }
        }
    }

    void SetTweeners()
    {
        _standTweenerList.Clear();
        _touchedTweenerList.Clear();

        if (_standTweenGroupObjs != null && _standTweenGroupObjs.Length > 0)
        {
            for(int i = 0;i< _standTweenGroupObjs.Length; i++)
            {
                UGUITweener[] tweeners = _standTweenGroupObjs[i].GetComponents<UGUITweener>();
                if(tweeners != null && tweeners.Length > 0)
                {
                    for(int j = 0;j< tweeners.Length; j++)
                    {
                        _standTweenerList.Add(tweeners[j]);
                    }
                }
            }
        }

        if (_touchedTweenGroupObjs != null && _touchedTweenGroupObjs.Length > 0)
        {
            for (int i = 0; i < _touchedTweenGroupObjs.Length; i++)
            {
                UGUITweener[] tweeners = _touchedTweenGroupObjs[i].GetComponents<UGUITweener>();
                if (tweeners != null && tweeners.Length > 0)
                {
                    for (int j = 0; j < tweeners.Length; j++)
                    {
                        _touchedTweenerList.Add(tweeners[j]);
                    }
                }
            }
        }
    }

    public void SetSpriteAlpha(float alpha)
    {
        //if(_buttonSprites != null && _buttonSprites.Length > 0)
        //{
        //    for(int i = 0;i< _buttonSprites.Length; i++)
        //    {
                //Color buttonColor = _buttonSprites[i].color;

                //float button_H;
                //float button_S;
                //float button_V;
                //Color.RGBToHSV(buttonColor, out button_H, out button_S, out button_V);
        //        button_V *= alpha;
        //        _buttonSprites[i].color = new Color(buttonColor.r, buttonColor.g, buttonColor.b, alpha);
        //    }
        //}

        for(int i = 0;i< _themeButtonColorInfos.Count; i++)
        {
            InfiniteThemeButtonColorInfo buttonColorInfo = _themeButtonColorInfos[i];

            Color buttonColor = buttonColorInfo.buttonSprite.color;

            float button_H;
            float button_S;
            float button_V;
            Color.RGBToHSV(buttonColor, out button_H, out button_S, out button_V);

            button_V = buttonColorInfo.start_V * alpha;

            buttonColorInfo.buttonSprite.color = Color.HSVToRGB(button_H, button_S, button_V);
        }
    }

    public virtual void TouchButton()
    {
        Debug.Log(string.Format("TouchButton !!!!!"));
        if(_touchedAnimators != null && _touchedAnimators.Length > 0)
        {
            for(int i = 0;i< _touchedAnimators.Length; i++)
            {
                _touchedAnimators[i].Play(_touchedAniNames[i], -1, 0f);
                _touchedAnimators[i].enabled = true;
            }
        }

        if(_touchedObjects != null && _touchedObjects.Length > 0)
        {
            for(int i = 0;i< _touchedObjects.Length; i++)
            {
                _touchedObjects[i].SetActive(true);
            }
        }

        if(_touchedTweenerList.Count > 0)
        {
            for(int i = 0;i< _touchedTweenerList.Count; i++)
            {
                _touchedTweenerList[i].ResetToBeginning();
                _touchedTweenerList[i].PlayForward();
                _touchedTweenerList[i].enabled = true;
            }
        }

        if(_standButtonObjs != null && _standButtonObjs.Length > 0)
        {
            for(int i = 0;i< _standButtonObjs.Length; i++)
            {
                _standButtonObjs[i].SetActive(false);
            }
        }

        if(_standTweenerList.Count > 0)
        {
            for(int i = 0;i< _standTweenerList.Count; i++)
            {
                _standTweenerList[i].enabled = false;
            }
        }

        if (_standAnimators != null && _standAnimators.Length > 0)
        {
            for (int i = 0; i < _standAnimators.Length; i++)
            {
                _standAnimators[i].enabled = false;
            }
        }
    }

    public virtual void ReleaseButtonEffect()
    {
        if (_touchedAnimators != null && _touchedAnimators.Length > 0)
        {
            for (int i = 0; i < _touchedAnimators.Length; i++)
            {
                //_touchedAnimators[i].Play(_touchedAniNames[i], -1, 0f);
                _touchedAnimators[i].enabled = false;
            }
        }

        if (_touchedTweenerList.Count > 0)
        {
            for (int i = 0; i < _touchedTweenerList.Count; i++)
            {
                _touchedTweenerList[i].enabled = false;
            }
        }

        if (_touchedObjects != null && _touchedObjects.Length > 0)
        {
            for (int i = 0; i < _touchedObjects.Length; i++)
            {
                _touchedObjects[i].SetActive(false);
            }
        }

        if (_standButtonObjs != null && _standButtonObjs.Length > 0)
        {
            for (int i = 0; i < _standButtonObjs.Length; i++)
            {
                _standButtonObjs[i].SetActive(true);
            }
        }

        if (_standTweenerList.Count > 0)
        {
            for (int i = 0; i < _standTweenerList.Count; i++)
            {
                _standTweenerList[i].ResetToBeginning();
                _standTweenerList[i].PlayForward();
                _standTweenerList[i].enabled = true;
            }
        }

        if (_standAnimators != null && _standAnimators.Length > 0)
        {
            for (int i = 0; i < _standAnimators.Length; i++)
            {
                _standAnimators[i].Play(_standAniNames[i], -1, 0f);
                _standAnimators[i].enabled = true;
            }
        }
    }

    #endregion
}
