﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InfiniteMagicButton : InfiniteBaseButton 
{
    #region Serialize Variables

#pragma warning disable 649

#pragma warning restore 649

    #endregion

    #region Variables

    #endregion

    #region Properties

    #endregion

    #region Methods

    public override void TouchButton()
    {
        base.TouchButton();
    }

    public override void ReleaseButtonEffect()
    {
        base.ReleaseButtonEffect();
    }

    #endregion
}
