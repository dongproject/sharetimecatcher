﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InfiniteButtonScrollInfo : MonoBehaviour
{
    #region Serialize Variables

    #endregion

    #region Variables

    ScrollThemeButtonObj _scrollButonInfo;
    InfiniteBaseButton _infiniteButtonObj;

    #endregion

    #region Properties

    #endregion

    #region Methods

    public void SetInfiniteButtonInfo(InfiniteBaseButton infiniteButton)
    {
        _infiniteButtonObj = infiniteButton;
    }

    #endregion
}
