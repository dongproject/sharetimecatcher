﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class InfiniteThemeController : CACommonMonoBehaviours, IBackKeyEvent
{
    #region Definitions

    public enum ThemeButtonState
    {
        LackButton,
        RequestButton,
        StartButton,
    }

    #endregion

    #region Serialize Variables

#pragma warning disable 649

    [SerializeField] StopTimeViewObject _totalPlayTimeView;

    [SerializeField] InfiniteDefaultButton _defaultButton;

    [Header("Lack Time Objects")]
    [SerializeField] GameObject _lackTimeObj;
    [SerializeField] StopTimeViewObject _lackTimeView;
    [SerializeField] Text _lackTimeText;

    [Header("Request Time Objects")]
    [SerializeField] GameObject _requestedTimeObj;
    [SerializeField] StopTimeViewObject _requestedTimeView;
    [SerializeField] Text _activationText;

    [Header("Enable Button Objects")]
    [SerializeField] Text _pressForGameText;

    [Header("Common Objects")]
    [SerializeField] ScrollCenterFocusManager _scrollCenterManager;
    [SerializeField] InfiniteThemeObjManager _themeObjManager;

    [SerializeField] ScrollThemeButtonObj _scrollThemeButtonObj;
    [SerializeField] Camera _sceneCamera;

    [Header("Cheat Objects")]
    [SerializeField] Button _cheatPlusTimeButton;

#pragma warning restore 649

    #endregion

    #region Variables

    int _curTouchID = -1;
    ThemeButtonState _curButtonState;
    const int _defaultButtonIndex = 10000;

    #endregion

    #region MonoBehaviour Methods

    public override void Awake()
    {
        base.Awake();

        _themeObjManager.InitButtonList();
        _scrollCenterManager.OnFocusScrollIndex = OnScrollFoucusButton;
        //_scrollCenterManager.OnSelectButton = OnSelectButton;

#if _CHEAT
        _cheatPlusTimeButton.onClick.AddListener(() => OnCheatPlusTime());
#else
        _cheatPlusTimeButton.gameObject.SetActive(false);
#endif

        if (BackKeyManager.Instance != null)             BackKeyManager.Instance.AddBackKeyEvent((IBackKeyEvent)this);
    }

    public override void Start()
    {
        base.Start();

        curTouchState = true;

        SetTotalPlayTime();
        MakeScrollList();
    }

    public override void OnDestroy()
    {
        base.OnDestroy();

        if (BackKeyManager.Instance != null)
            BackKeyManager.Instance.RemoveBackKeyEvent((IBackKeyEvent)this);
    }

    #endregion

    #region Methods

    void SetTotalPlayTime()
    {
        float secTotalValue = CAGameSaveDataManager.Instance.InfiniteModeSave.CumulativeSecTime;

        int quotientHour = (int)secTotalValue / 3600;         int restHourSec = (int)secTotalValue % 3600;          SetViewHourValue(_totalPlayTimeView, quotientHour);          int quotientMin = (int)restHourSec / 60;         int restMinSec = (int)restHourSec % 60;          SetViewMinuteValue(_totalPlayTimeView, quotientMin);          SetViewSecondValue(_totalPlayTimeView, restMinSec);          int millisecond = Mathf.FloorToInt((secTotalValue - (float)Mathf.FloorToInt(secTotalValue)) * 100f);         SetViewMillisecondValue(_totalPlayTimeView, millisecond);
    }

    void SetRequestTimeView(int focusButtonIndex)
    {
        if (focusButtonIndex == -1)
            return;

        ScrollThemeButtonObj themeButtonObj = _scrollCenterManager.FocusScrollObject.ScrollObjInfo.ScrollGameObject.GetComponent<ScrollThemeButtonObj>();
        int buttonIndex = themeButtonObj.InfiniteBaseButton.ButtonIndex;

        if (buttonIndex == _defaultButtonIndex)
        {
            SetThemeButtonState(ThemeButtonState.StartButton, new InfiniteButtonPriceValue(0, 0, 0f));
            return;
        }

        bool isHaveButton = CAGameSaveDataManager.Instance.InfiniteModeSave.IsExistButton(buttonIndex);

        InfiniteButtonPriceValue buttonPriceValue = GamePlanDataManager.Instance.InfiniteButtonPlanData.InfiniteButtonInfos[buttonIndex].TimePrice;

        if (isHaveButton)
        {
            SetThemeButtonState(ThemeButtonState.StartButton, buttonPriceValue);
            return;
        }

        float totalPriceSec = buttonPriceValue.GetTotalSecValue();
        float secTotalValue = CAGameSaveDataManager.Instance.InfiniteModeSave.CumulativeSecTime;

        if(secTotalValue >= totalPriceSec)
        {
            SetThemeButtonState(ThemeButtonState.RequestButton, buttonPriceValue);
        }
        else
        {
            SetThemeButtonState(ThemeButtonState.LackButton, buttonPriceValue);
        }
    }

    void SetThemeButtonState(ThemeButtonState buttonState, InfiniteButtonPriceValue buttonPriceValue)
    {
        //InfiniteButtonPriceValue buttonPriceValue = GamePlanDataManager.Instance.InfiniteButtonPlanData.InfiniteButtonInfos[focusButtonIndex].TimePrice;

        int hour = buttonPriceValue.Hour;
        int minute = buttonPriceValue.Minute;
        int second = (int)buttonPriceValue.Second;
        int millisecond = Mathf.FloorToInt((buttonPriceValue.Second - (float)Mathf.FloorToInt(buttonPriceValue.Second)) * 100f);

        _curButtonState = buttonState;
        switch (_curButtonState)
        {
            case ThemeButtonState.LackButton:
                _lackTimeObj.SetActive(true);
                _lackTimeText.gameObject.SetActive(true);
                _requestedTimeObj.SetActive(false);
                _activationText.gameObject.SetActive(false);
                _pressForGameText.gameObject.SetActive(false);

                SetViewHourValue(_lackTimeView, hour);
                SetViewMinuteValue(_lackTimeView, minute);
                SetViewSecondValue(_lackTimeView, second);
                SetViewMillisecondValue(_lackTimeView, millisecond);
                break;
            case ThemeButtonState.RequestButton:
                _lackTimeObj.SetActive(false);
                _lackTimeText.gameObject.SetActive(false);
                _requestedTimeObj.SetActive(true);
                _activationText.gameObject.SetActive(true);
                _pressForGameText.gameObject.SetActive(false);

                SetViewHourValue(_requestedTimeView, hour);
                SetViewMinuteValue(_requestedTimeView, minute);
                SetViewSecondValue(_requestedTimeView, second);
                SetViewMillisecondValue(_requestedTimeView, millisecond);
                break;
            case ThemeButtonState.StartButton:
                _lackTimeObj.SetActive(false);
                _lackTimeText.gameObject.SetActive(false);
                _requestedTimeObj.SetActive(true);
                _activationText.gameObject.SetActive(false);
                _pressForGameText.gameObject.SetActive(true);

                SetViewHourValue(_requestedTimeView, 0);
                SetViewMinuteValue(_requestedTimeView, 0);
                SetViewSecondValue(_requestedTimeView, 0);
                SetViewMillisecondValue(_requestedTimeView, 0);
                break;
        }
    }

    void SetViewHourValue(StopTimeViewObject timeViewObj, int hourValue)     {         int tenHour = hourValue / 10;         int restHour = hourValue % 10;          timeViewObj.HourImage_1.SetSprite(string.Format("{0}", tenHour));         timeViewObj.HourImage_2.SetSprite(string.Format("{0}", restHour));     }

    void SetViewMinuteValue(StopTimeViewObject timeViewObj, int minValue)     {         int tenMin = minValue / 10;         int restMin = minValue % 10;          timeViewObj.MinuteImage_1.SetSprite(string.Format("{0}", tenMin));         timeViewObj.MinuteImage_2.SetSprite(string.Format("{0}", restMin));     }

    void SetViewSecondValue(StopTimeViewObject timeViewObj, int secValue)     {         int tenSec = secValue / 10;         int restSec = secValue % 10;          timeViewObj.SecondImage_1.SetSprite(string.Format("{0}", tenSec));         timeViewObj.SecondImage_2.SetSprite(string.Format("{0}", restSec));     }

    void SetViewMillisecondValue(StopTimeViewObject timeViewObj, int milsecValue)     {         int tenMilsec = milsecValue / 10;         int restMilsec = milsecValue % 10;          timeViewObj.MillisecondImage_1.SetSprite(string.Format("{0}", tenMilsec));         timeViewObj.MillisecondImage_2.SetSprite(string.Format("{0}", restMilsec));     }

    void MakeScrollList()
    {
        _scrollCenterManager.SetCenterX(0f);

        _scrollCenterManager.ResetScrollData();

        SetDefaultButton();

        List<int> buttonKeys = _themeObjManager.InfiniteThemeButtons.Keys.ToList();
        for(int i = 0;i< buttonKeys.Count; i++) {
            int btnKey = buttonKeys[i];
            InfiniteBaseButton infiniteButton = _themeObjManager.InfiniteThemeButtons[btnKey];
            ScrollThemeButtonObj scrollButtonObj = Instantiate<ScrollThemeButtonObj>(_scrollThemeButtonObj);

            int buttonIndex = infiniteButton.ButtonIndex;

            bool isHaveButton = CAGameSaveDataManager.Instance.InfiniteModeSave.IsExistButton(buttonIndex);
            if (isHaveButton)
            {
                scrollButtonObj.LockObj.SetActive(false);
            }
            else
            {
                scrollButtonObj.LockObj.SetActive(true);
            }

            scrollButtonObj.SetInfiniteBaseButton(infiniteButton);

            _scrollCenterManager.AddScrollObject(scrollButtonObj);
        }

        _scrollCenterManager.SetStartFocusObj();
    }

    void SetDefaultButton()
    {
        ScrollThemeButtonObj scrollButtonObj = Instantiate<ScrollThemeButtonObj>(_scrollThemeButtonObj);

        scrollButtonObj.LockObj.SetActive(false);

        _defaultButton.gameObject.SetActive(true);
        scrollButtonObj.SetInfiniteBaseButton(_defaultButton);
        _scrollCenterManager.AddScrollObject(scrollButtonObj);
    }

    void SetUnLockButton(int buttonIndex)
    {
        for(int i = 0;i< _scrollCenterManager.ScrollObjectList.Count; i++)
        {
            ScrollThemeButtonObj scrollButtonObj = _scrollCenterManager.ScrollObjectList[i].ScrollObjInfo as ScrollThemeButtonObj;
            if(scrollButtonObj.InfiniteBaseButton.ButtonIndex == buttonIndex)
            {
                scrollButtonObj.LockObj.SetActive(false);
                break;
            }
        }
    }

    public override CACurGamePlayTouchData OnGetTouchData(int touchID, float posX, float posY, bool isTouch)
    {
        if (_scrollCenterManager.GetFocusObjectState())
        {
            if(_scrollCenterManager.FocusScrollObject != null)
            {
                ScrollThemeButtonObj themeButtonObj = _scrollCenterManager.FocusScrollObject.ScrollObjInfo.ScrollGameObject.GetComponent<ScrollThemeButtonObj>();
                int buttonIndex = CheckThemeButtonIndex(touchID, posX, posY, themeButtonObj);
                if (buttonIndex != -1)
                {
                    return new CACurGamePlayTouchData(CAGamePlayTouchType.InfiniteSelectButton, themeButtonObj);
                }
            }
        }

        return null;
    }

    int CheckThemeButtonIndex(int touchID, float posX, float posY, ScrollThemeButtonObj themeButtonObj)     {
        Vector3 themeButtonScrPos = _sceneCamera.WorldToScreenPoint(themeButtonObj.transform.position);
        float halfWidth = themeButtonObj.InfiniteBaseButton.ButtonCollider.size.x * 0.5f;
        float halfHeight = themeButtonObj.InfiniteBaseButton.ButtonCollider.size.y * 0.5f;

        if ((posX >= (themeButtonScrPos.x - halfWidth) && posX <= (themeButtonScrPos.x + halfWidth)) &&
            (posY >= (themeButtonScrPos.y - halfHeight) && posY <= (themeButtonScrPos.y + halfHeight)))
        {
            if (_curTouchID == -1)
            {
                //if (_startButton != null && _stopButton != null)
                //{
                //    _startButton.SetActive(false);                 //    _stopButton.SetActive(true);
                //}
                _curTouchID = touchID;
            }
            return themeButtonObj.InfiniteBaseButton.ButtonIndex;
        }
        else
        {
            if (_curTouchID == touchID)
            {
                //if (_startButton != null && _stopButton != null)
                //{
                //    _startButton.SetActive(true);
                //    _stopButton.SetActive(false);
                //}
                _curTouchID = -1;
            }
        }

        return -1;
    }

    void SelectThemeButton(int buttonIndex)
    {
        switch (_curButtonState)
        {
            case ThemeButtonState.LackButton:
                break;
            case ThemeButtonState.RequestButton:
                {
                    InfiniteButtonPriceValue buttonPriceValue = GamePlanDataManager.Instance.InfiniteButtonPlanData.InfiniteButtonInfos[buttonIndex].TimePrice;
                    float totalButtonSec = buttonPriceValue.GetTotalSecValue();
                    CAGameSaveDataManager.Instance.InfiniteModeSave.UseCumulativeSecTime(totalButtonSec);
                    CAGameSaveDataManager.Instance.InfiniteModeSave.AddBuyButton(buttonIndex);
                    CAGameSaveDataManager.Instance.InfiniteModeSave.SaveData();
                    SetUnLockButton(buttonIndex);
                    SetThemeButtonState(ThemeButtonState.StartButton, buttonPriceValue);
                    SetTotalPlayTime();
                }
                break;
            case ThemeButtonState.StartButton:
                if(buttonIndex == _defaultButtonIndex)
                {
                    CAGameSaveDataManager.Instance.InfiniteModeSave.SetSelectButtonIndex(-1);
                }
                else
                {
                    CAGameSaveDataManager.Instance.InfiniteModeSave.SetSelectButtonIndex(buttonIndex);
                }

                CAGameSaveDataManager.Instance.InfiniteModeSave.SaveData();
                CAGameGlobal.Instance.LoadGameScene(GameSceneEnum.SCENE_INFINITE_MODE, SceneChangeState.SCENE_CHANGE_DIRECT);
                break;
        }
    }

    public void ExecuteBackKey()     {         OnBackEvent();     }

    #endregion

    #region CallBack Methods

    public override void OnTouchEvent(CACurGamePlayTouchData playTouchData, float posX, float posY)
    {
        if (playTouchData != null)
        {
            switch (playTouchData.playTouchType)
            {
                case CAGamePlayTouchType.InfiniteSelectButton:
                    //int buttonIndex = _scrollCenterManager.FocusScrollIndex;
                    //Debug.Log(string.Format("OnTouchEvent buttonIndex : {0}", buttonIndex));
                    ScrollThemeButtonObj themeButtonObj = _scrollCenterManager.FocusScrollObject.ScrollObjInfo.ScrollGameObject.GetComponent<ScrollThemeButtonObj>();
                    int buttonIndex = themeButtonObj.InfiniteBaseButton.ButtonIndex;

                    SelectThemeButton(buttonIndex);
                    break;
                
            }
        }
    }

    public override void OnBackEvent()
    {
        base.OnBackEvent();

        SoundManager.Instance.PlayEffectFX(SoundDefinitions.effectSound_2);
        CAGameGlobal.Instance.LoadGameScene(GameSceneEnum.SCENE_STAGE, SceneChangeState.SCENE_CHANGE_DIRECT);
    }

    void OnScrollFoucusButton(int buttonIndex)
    {
        SetRequestTimeView(buttonIndex);
    }

    void OnCheatPlusTime()
    {
        CAGameSaveDataManager.Instance.InfiniteModeSave.AddCumulativeSecTime(30f);
        CAGameSaveDataManager.Instance.InfiniteModeSave.SaveData();
        SetTotalPlayTime();
        MakeScrollList();
    }

#endregion
}
