﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InfiniteThemeObjManager : MonoBehaviour 
{
    #region Serizliae Variables

#pragma warning disable 649

    [SerializeField] GameObject[] _infiniteButtonPrefabs;

#pragma warning restore 649

    #endregion

    #region Variables

    Dictionary<int /* Button Index */, InfiniteBaseButton> _infiniteThemeButtons = new Dictionary<int, InfiniteBaseButton>();

    #endregion

    #region Properties

    public Dictionary<int /* Button Index */, InfiniteBaseButton> InfiniteThemeButtons
    {
        get { return _infiniteThemeButtons; }
    }

    #endregion

    #region MonoBehaviour Methods

    private void Awake()
    {
        //InitButtonList();
    }

    #endregion

    #region Methods

    public void InitButtonList()
    {
        _infiniteThemeButtons.Clear();
        for (int i = 0;i< _infiniteButtonPrefabs.Length; i++)
        {
            if (_infiniteButtonPrefabs[i] == null) continue;

            GameObject infiniteButtonObj = Instantiate<GameObject>(_infiniteButtonPrefabs[i]);

            InfiniteBaseButton infiniteButton = infiniteButtonObj.GetComponent<InfiniteBaseButton>();
            _infiniteThemeButtons.Add(infiniteButton.ButtonIndex, infiniteButton);
        }
    }

    public InfiniteBaseButton GetInfiniteButtonObj(int buttonIndex)
    {
        GameObject infiniteButtonObj = Instantiate<GameObject>(_infiniteButtonPrefabs[buttonIndex]);

        return infiniteButtonObj.GetComponent<InfiniteBaseButton>();
    }

    #endregion
}
