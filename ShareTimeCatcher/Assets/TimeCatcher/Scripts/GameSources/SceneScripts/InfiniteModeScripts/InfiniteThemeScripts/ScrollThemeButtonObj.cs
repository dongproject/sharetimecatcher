﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScrollThemeButtonObj : MonoBehaviour, IScrollObjectInfo
{
    #region Serialize Variables

#pragma warning disable 649

    [SerializeField] GameObject _lockObj;

#pragma warning restore 649

    #endregion

    #region Variables

    InfiniteBaseButton _infiniteBaseButton;

    #endregion

    #region Properties

    public InfiniteBaseButton InfiniteBaseButton
    {
        get{ return _infiniteBaseButton; }
    }

    public GameObject ScrollGameObject
    {
        get { return this.gameObject; }
    }

    public GameObject LockObj
    {
        get { return this._lockObj; }
    }

    public float ObjectMaxWidth { get; set; }
    public float ObjectWidth
    {
        get { return 200f; }
    }
    public float ObjectHeight
    {
        get { return 200f; }
    }

    #endregion

    #region Methods

    public void SetInfiniteBaseButton(InfiniteBaseButton infiniteButton)
    {
        _infiniteBaseButton = infiniteButton;
        infiniteButton.transform.SetParent(this.transform);
        infiniteButton.transform.localScale = Vector3.one;
        infiniteButton.transform.localPosition = Vector3.zero;
    }

    public void SetAlphaObject(float alpha)
    {
        _infiniteBaseButton.SetSpriteAlpha(alpha);
    }

    #endregion
}
