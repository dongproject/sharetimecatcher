﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InfiniteObstacleManager : MonoBehaviour 
{
    #region Serialize Variables

#pragma warning disable 649

    [SerializeField] InfiniteObstacleObject[] _obstacleObjects;

#pragma warning restore 649

    #endregion

    #region Variables

    CAGameTimerEventCtl _eventTimer = new CAGameTimerEventCtl();
    List<InfiniteObstacleObject> _curObstacleObjList = new List<InfiniteObstacleObject>();

    #endregion

    #region Properties

    public InfiniteObstacleObject[] ObstacleObjects
    {
        get { return _obstacleObjects;  }
    }

    public List<InfiniteObstacleObject> CurObstacleObjList
    {
        get { return _curObstacleObjList; }
    }

    #endregion

    #region Methods

    public void ResetCurObstacleObjList()
    {
        _curObstacleObjList.Clear();
        for(int i = 0;i< _obstacleObjects.Length; i++)
        {
            _curObstacleObjList.Add(_obstacleObjects[i]);
        }
    }

    public void OnUpdate()
    {
        _eventTimer.UpdateGameTimer();
    }

    #endregion
}
