﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InfiniteObstacleObject : MonoBehaviour 
{
    #region Serialize Variables

#pragma warning disable 649

    [SerializeField] int _obstacleIndex;
    [SerializeField] float _durationTime;
    [SerializeField] float _appearTime;

#pragma warning restore 649

    #endregion

    #region Variables

    UGUITweener[] _tweeners;

    #endregion

    #region Properties

    public int ObstacleIndex
    {
        get { return _obstacleIndex; }
    }

    public float DurationTime
    {
        get { return _durationTime; }
    }

    public float AppearTime
    {
        get { return _appearTime; }
    }

    #endregion

    #region MonoBehaviour Methods

    private void Awake()
    {
        _tweeners = this.transform.GetComponentsInChildren<UGUITweener>(true);
    }

    #endregion

    #region Methods

    public void RefreshObstacle()
    {
        if(_tweeners != null && _tweeners.Length > 0)
        {
            for(int i = 0;i< _tweeners.Length; i++)
            {
                _tweeners[i].ResetToBeginning();
                _tweeners[i].PlayForward();
                _tweeners[i].enabled = true;
            }
        }
    }

    #endregion
}
