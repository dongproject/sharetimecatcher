﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InfiniteBonusManager : MonoBehaviour 
{
    #region Serialize Variables

#pragma warning disable 649

    [SerializeField] GameObject _clockSmokeEffect;
    [SerializeField] GameObject[] _clockObjs;
    [SerializeField] GameObject[] _clockImgObjs;
    [SerializeField] StoryAniTrigger[] _aniTriggers;
    [SerializeField] Transform[] _clockBodyTrans;

#pragma warning restore 649

    #endregion

    #region Variables

    int _curClockIndex = 0;
    bool _isAppearClock = false;

    Action _onTouchClock = null;

    List<int> _clockIndexes = new List<int>();

    #endregion

    #region Properties

    public GameObject ClockSmokeEffect
    {
        get { return _clockSmokeEffect; }
    }

    public int CurClockIndex
    {
        get { return _curClockIndex; }
    }

    public bool IsAppearClock
    {
        get { return _isAppearClock; }
    }

    #endregion

    #region MonoBehaviour Methods

    private void Awake()
    {
        for(int i = 0;i< _aniTriggers.Length; i++)
        {
            _aniTriggers[i].OnAniTrigger = OnFinishClockAni;
        }

        SetClockObjs();
    }

    #endregion

    #region Methods

    void SetClockObjs()
    {
        _clockIndexes.Clear();

        for(int i = 0;i< _clockObjs.Length; i++)
        {
            _clockIndexes.Add(i);
        }
    }

    public void AppearRandomClock()
    {
        if(_clockIndexes.Count == 0)
        {
            SetClockObjs();
        }

        _isAppearClock = true;

        int ranValue = UnityEngine.Random.Range(0, _clockIndexes.Count);
        _curClockIndex = _clockIndexes[ranValue];
        _clockIndexes.RemoveAt(ranValue);

        _clockObjs[_curClockIndex].SetActive(true);
    }

    public void OnTouchPress(int touchID, float posX, float posY)
    {
        //Debug.Log (string.Format ("OnTouchPress touchID : {0}, posX : {1}, posY : {2}", touchID, posX, posY));

        float imgPosX = CAResolutionCtl.Instance.GetScreenToImagePosX(posX);
        float imgPosY = CAResolutionCtl.Instance.GetScreenToImagePosY(posY);

        float revisionValue = 5f;

        //for (int i = 0; i < _motionAniObjectInfos.Count; i++) {
        //    int motionID = _motionAniObjectInfos[i].ObjectIndex;

        //    if (!_motionAniTransforms.ContainsKey(motionID))
        //        continue;

        //    if ((_motionAniObjectInfos[i].XMin - revisionValue <= imgPosX && _motionAniObjectInfos[i].XMax + revisionValue >= imgPosX) &&
        //        (_motionAniObjectInfos[i].YMin - revisionValue <= imgPosY && _motionAniObjectInfos[i].YMax + revisionValue >= imgPosY)) {

        //        Debug.Log(string.Format("CheckMatchObject Matched index : {0}, Object Index : {1}", i, _motionAniObjectInfos[i].ObjectIndex));

        //        tk2dSpriteAnimator bombAnimator = _bombSpriteAniEffect_2;
        //        bombAnimator.transform.localPosition = new Vector3(_motionAniTransforms[motionID].localPosition.x,
        //            _motionAniTransforms[motionID].localPosition.y, -10f);
        //        bombAnimator.gameObject.SetActive(true);
        //        _eventTimer.SetGameTimerData(OnDisableBombEffect, 0.5f, bombAnimator);

        //        _motionAniTransforms[motionID].gameObject.SetActive(false);
        //        _motionAniTransforms.Remove(motionID);

        //        _motionManager.ReleaseObjMovement(motionID);
        //        break;
        //    }
        //}
    }

    public GameObject GetCurClockImgObj()
    {
        return _clockImgObjs[_curClockIndex];
    }

    public Transform GetCurClockBodyTrans()
    {
        return _clockBodyTrans[_curClockIndex];
    }

    public void TouchClockImg()
    {
        if (_curClockIndex == -1)
            return;

        _clockObjs[_curClockIndex].SetActive(false);
        _isAppearClock = false;
        _curClockIndex = -1;
    }

    #endregion

    #region CallBack Methods

    void OnFinishClockAni(int index)
    {
        _clockObjs[index].SetActive(false);
        _isAppearClock = false;
    }

    #endregion
}
