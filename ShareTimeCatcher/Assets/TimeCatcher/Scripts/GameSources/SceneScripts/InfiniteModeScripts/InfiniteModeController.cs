﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Advertisements;
using UnityEngine.UI;

public enum InfiniteModeAniTrigger
{
    InfiniteReadyAni    = 0,
}

public class InfiniteModeController : CACommonMonoBehaviours, IBackKeyEvent
{
    #region Serialize Variables

#pragma warning disable 649

    [SerializeField] Camera _stageCamera;

    [SerializeField] TargetTimeRootInfo _targetTimeInfo;
    [SerializeField] StageButtonEffect _buttonEffect;
    [SerializeField] Color[] _progressColors;

    [SerializeField] StopTimeViewObject _realTimeViewObj;

    [SerializeField] Transform _buttonRootTrans;
    [SerializeField] GameObject _defaultTimeObj;
    [SerializeField] GameObject _startButton;
    [SerializeField] GameObject _stopButton;

    [SerializeField] Button _previousButton;
    [SerializeField] StageCommonPopupController _commonPopupController;

    [SerializeField] InfiniteModeHPGauge _hpGauge;

    [SerializeField] GameObject _updateTimeObject;

    [SerializeField] UIResultInfinitePopup _resultInfinitePopup;
    [SerializeField] UIResultInfiniteNewtopPopup _resultInfiniteNewtopPopup;
    [SerializeField] JudgementInfiniteManager _judgeManager;
    [SerializeField] InfiniteThemeObjManager _themeButtonManager;

    [SerializeField] AnimationTriggerEvent _infiniteReadyTrigger;

    [SerializeField] InfiniteObstacleManager _obstacleManager;

    [SerializeField] InfiniteMoveObjHitButtonManager _moveObjHitButtonManager;
    [SerializeField] InfiniteBonusManager _bonusManager;
    [SerializeField] GameObject _infiniteTutorialObj;

    [SerializeField] Button _questionInfiniteButton;

#pragma warning restore 649

    #endregion

    #region Variables

    float _perfectJudgeTime = 0.05f;//0.02f;
    float _awesomeJudgeTime = 0.1f;//0.05f;
    float _goodJudgeTime = 0.3f;//0.1f;

    float _minusHPTime = 30f;

    int _curColorIndex = 0;

    int _curTouchID = -1;
    int _stopButtonIndex = 1000;

    int _curInfiniteIndex = 0;

    float _buttonStartX;
    float _buttonStartY;
    float _buttonEndX;
    float _buttonEndY;

    StopTimePlaySceneStep _playSceneStep;

    TimeCatcherSpriteManager _timeCatcherManager = new TimeCatcherSpriteManager();

    StopTimeStageInfo _timeStageInfo;

    CAGameTimerEventCtl _eventTimer = new CAGameTimerEventCtl();

    float _buttonHPMaxValue;
    float _curButtonHPValue;
    float _hpMinusValue = 3f;

    float _bestRecordSec;

    UIResultInfiniteBasePopup _resultBasePopup;

    bool _isGamePause = false;
    float _curGapTimeValue = 0f;
    InfiniteBaseButton _curInfiniteButton = null;
    protected StopTimeButtonType _stageButtonType;

    float _nextAppearObstacleTime;
    int _continueCount = 0;
    float[] _hurdleTimeValues = new float[5];
    int _hurdleIndex = 0;

    float _perfectPlusValue = 1.2f;
    float _awesomePlusValue = 0.8f;
    float _goodPlusValue = 0.6f;

    float _startBonusTime = 15f;//15f;
    float _nextBonusTime = 0f;
    float _gapBonusTime = 15f;//15;

    bool _isQuestionInfinite = false;
    bool _isStartTutoTouch = false;

    #endregion

    #region TimerID Values

    const int _resultDelayTimeID = 1;
    const int _themeButtonEffectTimeID = 2;

    #endregion

    #region MonoBehaviour Methods

    public override void Awake()
    {
        base.Awake();
        //Time.timeScale = 0.7f;

        //_commonPopupController.OnYesAction = OnYesCommonPopup;
        //_commonPopupController.OnNoAction = OnNoCommonPopup;

        //      curTouchState = true;

        //_commonPopupController.OnYesAction = OnYesCommonPopup;
        //_commonPopupController.OnNoAction = OnNoCommonPopup;
        //InitGamePlayData();
        //_stageFailed = false;

        _bestRecordSec = CAGameSaveDataManager.Instance.InfiniteModeSave.BestRecordSecTime;
        SetThemeButton();

        _stageButtonType = StopTimeButtonType.PushImmediate;

        _nextAppearObstacleTime = 5f;

        _nextBonusTime = _startBonusTime;

        _questionInfiniteButton.onClick.AddListener(() => OnQuestionInfinite());

        _questionInfiniteButton.gameObject.SetActive(false);
        _previousButton.gameObject.SetActive(false);

        if (BackKeyManager.Instance != null)             BackKeyManager.Instance.AddBackKeyEvent((IBackKeyEvent)this);

        if (BackKeyManager.Instance != null)             BackKeyManager.Instance.IsEnableBackKey = false;
    }

    // Use this for initialization
    public override void Start()
    {
        base.Start();
        SetHurdleTime();

        if(CAGameSaveDataManager.Instance.InfiniteModeSave.ViewTutorialState == 0)
        {
            SetPlaySceneStep(StopTimePlaySceneStep.InfiniteTutorialStep);
        }
        else
        {
            SetPlaySceneStep(StopTimePlaySceneStep.ReadyDirectingData);
        }


        SetButtonHPValue();

        _obstacleManager.ResetCurObstacleObjList();

        _continueCount = 0;

        _commonPopupController.SetInstance();
    }

    void SetButtonHPValue()
    {
        _buttonHPMaxValue = 100f;
        _curButtonHPValue = _buttonHPMaxValue;

        _hpGauge.SetGaugeValue(1f);
    }

    // Update is called once per frame
    void Update()
    {
        switch (_playSceneStep)
        {
            case StopTimePlaySceneStep.InfiniteTutorialStep:
                if (_isStartTutoTouch)
                {
                    if (Input.GetMouseButtonDown(0))
                    {
                        _infiniteTutorialObj.SetActive(false);
                        SetPlaySceneStep(StopTimePlaySceneStep.ReadyDirectingData);
                    }
                }
                break;
            case StopTimePlaySceneStep.ReadyDirectingStep:
                break;
            case StopTimePlaySceneStep.GamePlayingStep:
                if(!_isGamePause) 
                    UpdateGamePlay();
                break;
            case StopTimePlaySceneStep.ResultDirectingStep:
                break;
            case StopTimePlaySceneStep.InfiniteModeResultStep:
                break;
        }

        _eventTimer.UpdateGameTimer();
    }

    public override void OnDisable()
    {
        SoundManager.Instance.StopMusic();
    }

    public override void OnDestroy()
    {
        base.OnDestroy();
        curTouchState = false;

        if (BackKeyManager.Instance != null)             BackKeyManager.Instance.RemoveBackKeyEvent((IBackKeyEvent)this);
    }

    #endregion

    #region Methods

    void SetStartTimeValue()
    {
        _curGapTimeValue = 5f;
        _timeStageInfo = new StopTimeStageInfo(0);
        _timeStageInfo.TargetTime = new StopTimeValues(0, 0, _curGapTimeValue);

        _judgeManager.InitJudgeData();
        _updateTimeObject.SetActive(false);
    }

    void SetGamePause(bool pause)
    {
        _isGamePause = pause;

        if (_isGamePause)
        {
            Time.timeScale = 0f;
        }
        else
        {
            Time.timeScale = 1f;
        }
    }

    void SetHurdleTime()
    {
        _hurdleIndex = 0;

        _hurdleTimeValues[0] = 30f;//35f;
        _hurdleTimeValues[1] = 50f;
        _hurdleTimeValues[2] = 90f;
        _hurdleTimeValues[3] = 120f;
        _hurdleTimeValues[4] = 150f;

        _moveObjHitButtonManager.OnCollideButton = OnHitButton;
    }

    void SetThemeButton()
    {
        int curThemeButtonIndex = CAGameSaveDataManager.Instance.InfiniteModeSave.SelectButtonIndex;
        if(curThemeButtonIndex != -1)
        {
            _defaultTimeObj.SetActive(false);
            _curInfiniteButton = _themeButtonManager.GetInfiniteButtonObj(curThemeButtonIndex);
            //_buttonRootTrans
            _curInfiniteButton.transform.SetParent(_buttonRootTrans);
            _curInfiniteButton.transform.localScale = Vector3.one;
            _curInfiniteButton.transform.localPosition = new Vector3(0f, 0f, -20f);
            RefreshThemeButtonPos();
        }
        else
        {
            RefreshButtonPos();
        }
    }

    void SetTimeCatcherData()
    {
        SetImageTimeNumberData(_timeCatcherManager.ImgTargetTimeNum, _targetTimeInfo.TargetTimeNumObject, _timeStageInfo.TargetTime);

        SetImageTimeNumberData(_timeCatcherManager.ImgRealTimeNum, _realTimeViewObj, _timeStageInfo.RealTime);

        _timeCatcherManager.SetFailTime();
        _timeCatcherManager.OnFinishTimeOver = OnFinishTimeOver;

        _timeCatcherManager.StartState = true;
    }

    void SetImageTimeNumberData(SpriteTimeNumberInfo timeNumberInfo, StopTimeViewObject timeViewObject, StopTimeValues gameTimeValues)
    {
        timeNumberInfo.SetTimeNumImages(timeViewObject.HourImage_1, timeViewObject.HourImage_2, timeViewObject.MinuteImage_1,
            timeViewObject.MinuteImage_2, timeViewObject.SecondImage_1, timeViewObject.SecondImage_2,
            timeViewObject.MillisecondImage_1, timeViewObject.MillisecondImage_2);

        timeNumberInfo.SetMosaTimeNumImages(timeViewObject.MosaSecondImage_1, timeViewObject.MosaSecondImage_2,
            timeViewObject.MosaMillisecondImage_1, timeViewObject.MosaMillisecondImage_2);
        timeNumberInfo.SetTimeValue(gameTimeValues.Hour, gameTimeValues.Minute, gameTimeValues.Second);
    }

    void UpdateGamePlay()
    {
        _timeCatcherManager.UpdateStopWatch();

        if (_timeCatcherManager.StartState)
        {
            UpdateButtonHPGauge();
        }

        _nextAppearObstacleTime -= Time.deltaTime;
        if(_nextAppearObstacleTime <= 0f)
        {
            AppearObstacleObj();
        }

        if(_nextBonusTime <= _timeCatcherManager.ImgRealTimeNum.TimeSecValue)
        {
            AppearBonusClock();
        }

        if (_hurdleIndex < _hurdleTimeValues.Length)
        {
            if (_timeCatcherManager.ImgRealTimeNum.TimeSecValue > _hurdleTimeValues[_hurdleIndex])
            {
                IncreaseDifficulty();
            }
        }

        if(_hurdleIndex > 0)
        {
            _moveObjHitButtonManager.UpdateHitButton();
        }

        //if (_bonusManager.IsAppearClock) {
        //    CheckAppearClock();
        //}
    }

    void CheckAppearClock()
    {

    }

    void IncreaseDifficulty()
    {
        if(_hurdleIndex == 0)
        {
            _moveObjHitButtonManager.AppearGapTime = 5f;
            _perfectPlusValue = 1.1f;
            _awesomePlusValue = 0.77f;
            _goodPlusValue = 0.57f;
        } else if(_hurdleIndex == 1)
        {
            _moveObjHitButtonManager.AppearGapTime = 4f;
            _perfectPlusValue = 1f;
            _awesomePlusValue = 0.75f;
            _goodPlusValue = 0.55f;
        } else if(_hurdleIndex == 2)
        {
            _moveObjHitButtonManager.AppearGapTime = 2f;
            _perfectPlusValue = 1f;
            _awesomePlusValue = 0.73f;
            _goodPlusValue = 0.53f;
        } else if(_hurdleIndex == 3)
        {
            _moveObjHitButtonManager.AppearGapTime = 1f;
            _perfectPlusValue = 1f;
            _awesomePlusValue = 0.7f;
            _goodPlusValue = 0.5f;
        } else if(_hurdleIndex == 4)
        {
            _perfectPlusValue = 0.9f;
            _awesomePlusValue = 0.6f;
            _goodPlusValue = 0.4f;
            _moveObjHitButtonManager.AppearGapTime = 0.5f;
        }

        _hurdleIndex++;
    }

    void AppearObstacleObj()
    {
        int objCount = _obstacleManager.CurObstacleObjList.Count;
        int ranIndex = Random.Range(0, objCount);
        InfiniteObstacleObject obstacleObj = _obstacleManager.CurObstacleObjList[ranIndex];
        _obstacleManager.CurObstacleObjList.RemoveAt(ranIndex);
        if(_obstacleManager.CurObstacleObjList.Count == 0)
        {
            _obstacleManager.ResetCurObstacleObjList();
        }
        obstacleObj.gameObject.SetActive(true);
        obstacleObj.RefreshObstacle();
        _nextAppearObstacleTime = obstacleObj.DurationTime;
        _eventTimer.SetGameTimerData(OnDisappearObstacle, obstacleObj.DurationTime, obstacleObj);
    }

    void AppearBonusClock()
    {
        _bonusManager.AppearRandomClock();
        _nextBonusTime += _gapBonusTime;
    }

    void UpdateButtonHPGauge()
    {
        float minusValue = _hpMinusValue * Time.deltaTime;
        _curButtonHPValue -= minusValue;
        //Debug.Log(string.Format("UpdateButtonHPGauge _curButtonHPValue : {0}, minusValue : {1}", _curButtonHPValue, minusValue));

        if(_curButtonHPValue <= 0f)
        {
            _hpGauge.SetGaugeValue(0f);
            SetGameEnded();
        }
        else
        {
            _hpGauge.SetGaugeValue(_curButtonHPValue / _buttonHPMaxValue);
        }
    }

    void SetGameEnded()
    {
        SetPlaySceneStep(StopTimePlaySceneStep.ResultStep);
    }

    protected void RefreshButtonPos()
    {
        BoxCollider buttonCollider = _startButton.GetComponent<BoxCollider>();
        float objScaleX = _startButton.transform.localScale.x;
        float objScaleY = _startButton.transform.localScale.y;
        float halfWidth = (buttonCollider.bounds.size.x * objScaleX) * 0.5f;
        float halfHeight = (buttonCollider.bounds.size.y * objScaleY) * 0.5f;

        Vector3 buttonScreenPos = _stageCamera.WorldToScreenPoint(_startButton.transform.position);

        _buttonStartX = buttonScreenPos.x - halfWidth;
        _buttonStartY = buttonScreenPos.y - halfHeight;
        _buttonEndX = buttonScreenPos.x + halfWidth;
        _buttonEndY = buttonScreenPos.y + halfHeight;
    }

    protected void RefreshThemeButtonPos()     {         BoxCollider buttonCollider = _curInfiniteButton.ButtonCollider;         float halfWidth = (buttonCollider.bounds.size.x) * 0.5f;         float halfHeight = (buttonCollider.bounds.size.y) * 0.5f;          Vector3 buttonScreenPos = _stageCamera.WorldToScreenPoint(_curInfiniteButton.transform.position);          _buttonStartX = buttonScreenPos.x - halfWidth;         _buttonStartY = buttonScreenPos.y - halfHeight;         _buttonEndX = buttonScreenPos.x + halfWidth;         _buttonEndY = buttonScreenPos.y + halfHeight;     }

    void SetPlaySceneStep(StopTimePlaySceneStep _curStep)
    {
        _playSceneStep = _curStep;
        switch (_playSceneStep)
        {
            case StopTimePlaySceneStep.InfiniteTutorialStep:
                SetInfiniteTutorialStep();
                break;
            case StopTimePlaySceneStep.ReadyDirectingData:
                SetReadyDirectingData();
                break;
            case StopTimePlaySceneStep.ReadyDirectingStep:
                SetReadyDirectingStep();
                break;
            case StopTimePlaySceneStep.GamePlayingStep:
                SetGamePlayingStep();
                break;
            case StopTimePlaySceneStep.ResultDelayStep:
                SetResultDelayStep();
                break;
            case StopTimePlaySceneStep.ResultDirectingStep:
                SetResultDirectingStep();
                break;
            case StopTimePlaySceneStep.ResultStep:
                SetResultStep();
                break;
            case StopTimePlaySceneStep.InfiniteModeResultStep:
                SetInfiniteModeResultStep();
                break;
        }
    }

    void SetStartDelay()
    {

    }

    void SetInfiniteTutorialStep()
    {
        _infiniteTutorialObj.SetActive(true);
        _eventTimer.SetGameTimerData(OnDelayTutorial, 0.5f);
        CAGameSaveDataManager.Instance.InfiniteModeSave.SetViewInfiniteTutorial();
    }

    void SetReadyDirectingData()
    {
        _targetTimeInfo.gameObject.SetActive(false);
        _realTimeViewObj.gameObject.SetActive(false);
        _buttonRootTrans.gameObject.SetActive(false);
        _hpGauge.gameObject.SetActive(false);

        _infiniteReadyTrigger.OnTriggerEvent = OnFinishInfiniteReady;

        _eventTimer.SetGameTimerData(OnDelayReadyDirecting, 0.1f);
    }

    void SetReadyDirectingStep()
    {
        _infiniteReadyTrigger.gameObject.SetActive(true);
        _eventTimer.SetGameTimerData(OnDelayReadySound, 0.6f);
    }

    void SetGamePlayingStep()
    {
        if (BackKeyManager.Instance != null)             BackKeyManager.Instance.IsEnableBackKey = true;

        curTouchState = true;

        SetStartTimeValue();
        SetTimeCatcherData();

        _questionInfiniteButton.gameObject.SetActive(true);
        _previousButton.gameObject.SetActive(true);

        AudioClip musicClip = Resources.Load<AudioClip>(SoundFilePaths.MusicPath_4);         SoundManager.Instance.PlayMusic(musicClip);
    }

    void SetResultDelayStep()
    {
        
    }

    void SetResultDirectingStep()
    {

    }

    void SetResultStep()
    {
        if(_continueCount < 3)
        {
            _commonPopupController.ShowPopup(CommonPopupType.ContinueGame, OnYesContinue, OnNoContinue);
            _continueCount++;
        }
        else
        {
            SetResultPopup();
        }
    }

    void SetResultPopup()
    {
        float curRealTimeSec = _timeCatcherManager.ImgRealTimeNum.GetTotalSecTimeValue();
        CAGameSaveDataManager.Instance.InfiniteModeSave.AddCumulativeSecTime(curRealTimeSec);
        if (_bestRecordSec < curRealTimeSec)
        {
            _resultBasePopup = _resultInfiniteNewtopPopup;
            CAGameSaveDataManager.Instance.InfiniteModeSave.BestRecordSecTime = curRealTimeSec;
        }
        else
        {
            _resultInfinitePopup.SetBestRecordTotalTimeValue(_bestRecordSec);
            _resultInfinitePopup.SetTotalPlayTimeValue(CAGameSaveDataManager.Instance.InfiniteModeSave.CumulativeSecTime);
            _resultBasePopup = _resultInfinitePopup;
        }

        _resultBasePopup.OnExitAction = OnExitGame;
        _resultBasePopup.OnAgainAction = OnAgainGame;

        _resultBasePopup.gameObject.SetActive(true);
        _resultBasePopup.SetTotalTimeValue(_timeCatcherManager.ImgRealTimeNum.HourTime, _timeCatcherManager.ImgRealTimeNum.MinuteTime,
              _timeCatcherManager.ImgRealTimeNum.TimeSecValue);

        CAGameSaveDataManager.Instance.InfiniteModeSave.SaveData();
    }


    void SetInfiniteModeResultStep()
    {
        float gapTime = _timeCatcherManager.CheckGapStopTime();
        StopTimeJudgeType judgeType = GetResultJudgeType(gapTime);

        bool isTimeAdd = true;
        switch (judgeType) {
            case StopTimeJudgeType.Perfect:
                SetPerfectData();
                break;
            case StopTimeJudgeType.Awesome:
                SetAwesomeData();
                break;
            case StopTimeJudgeType.Good:
                SetGoodData();
                break;
            case StopTimeJudgeType.Fail:
                SetFailData();
                if (Mathf.Abs(_timeCatcherManager.GetGapStopTime()) >= 3f) {
                    isTimeAdd = false;
                }
                break;
        }

        _judgeManager.SetJudgeValue(judgeType, gapTime);

        _eventTimer.SetGameTimerData(OnDelayResult, 0.5f, isTimeAdd, _resultDelayTimeID);
    }

    void SetFullGauge()
    {
        _curButtonHPValue = _buttonHPMaxValue;
        _hpGauge.SetGaugeValue(1f);
    }

    void SetPerfectData()
    {
        float plusSecTime = (_curGapTimeValue * _hpMinusValue) * _perfectPlusValue;
        //float plusSecTime = _buttonHPMaxValue * 0.5f;

        _curButtonHPValue += plusSecTime;
        if(_curButtonHPValue > _buttonHPMaxValue)
        {
            _curButtonHPValue = _buttonHPMaxValue;
        }
        _hpGauge.SetGaugeValue(_curButtonHPValue / _buttonHPMaxValue);
    }

    void AddGaugeValue(float addValue)
    {
        _curButtonHPValue += addValue;

        if(_curButtonHPValue > _buttonHPMaxValue)
        {
            _curButtonHPValue = _buttonHPMaxValue;
        } else if(_curButtonHPValue < 0f)
        {
            _curButtonHPValue = 0f;
        }

        _hpGauge.SetGaugeValue(_curButtonHPValue / _buttonHPMaxValue);
    }

    void SetAwesomeData()
    {
        float plusSecTime = (_curGapTimeValue * _hpMinusValue) * _awesomePlusValue;

        _curButtonHPValue += plusSecTime;
        if (_curButtonHPValue > _buttonHPMaxValue)
        {
            _curButtonHPValue = _buttonHPMaxValue;
        }
        _hpGauge.SetGaugeValue(_curButtonHPValue / _buttonHPMaxValue);
    }

    void SetGoodData()
    {
        float plusSecTime = (_curGapTimeValue * _hpMinusValue) * _goodPlusValue;

        _curButtonHPValue += plusSecTime;
        if (_curButtonHPValue > _buttonHPMaxValue)
        {
            _curButtonHPValue = _buttonHPMaxValue;
        }
        _hpGauge.SetGaugeValue(_curButtonHPValue / _buttonHPMaxValue);
    }

    void SetFailData()
    {
        _curButtonHPValue -= _minusHPTime;
        if (_curButtonHPValue < 0f)
            _curButtonHPValue = 0f;
        _hpGauge.SetGaugeValue(_curButtonHPValue / _buttonHPMaxValue);
    }

    StopTimeJudgeType GetResultJudgeType(float gapTime)
    {
        float calcGapTime = Mathf.Abs(gapTime);
        if (calcGapTime <= _perfectJudgeTime)
        {
            return StopTimeJudgeType.Perfect;
        }
        else if (calcGapTime <= _awesomeJudgeTime)
        {
            return StopTimeJudgeType.Awesome;
        }
        else if (calcGapTime <= _goodJudgeTime)
        {
            return StopTimeJudgeType.Good;
        }
        else
        {
            return StopTimeJudgeType.Fail;
        }
    }

    public void OnClickStopButton()
    {
        SoundManager.Instance.PlayEffectFX(SoundDefinitions.effectSound_4);

        Debug.Log(string.Format("OnClickStopButton"));
        _timeCatcherManager.StartState = false;
        if(_curInfiniteButton == null)
        {
            if (_stopButton != null)
            {
                _startButton.gameObject.SetActive(false);
                _stopButton.gameObject.SetActive(true);
            }

            StageButtonEffect buttonEffectInfo = Instantiate<StageButtonEffect>(_buttonEffect);
            Vector3 buttonPos = _startButton.transform.localPosition;
            buttonEffectInfo.transform.SetParent(_startButton.transform.parent);
            buttonEffectInfo.transform.localPosition = new Vector3(buttonPos.x, buttonPos.y, -5f);
            buttonEffectInfo.transform.localScale = Vector3.one;
            buttonEffectInfo.gameObject.SetActive(true);
            buttonEffectInfo.ResetEffectTweeners();

            _eventTimer.SetGameTimerData(OnFinishButtonEffect, 1f, buttonEffectInfo);
        }
        else
        {
            _eventTimer.RemoveTimeEventByID(_themeButtonEffectTimeID);
            _curInfiniteButton.TouchButton();
            float releaseTime = _curInfiniteButton.EffectReleaseTime;
            Debug.Log(string.Format("!!!OnClickStopButton releaseTime : {0}", releaseTime));
            _eventTimer.SetGameTimerData(OnReleaseThemeEffectButton, releaseTime, null, _themeButtonEffectTimeID);
        }

        SetPlaySceneStep(StopTimePlaySceneStep.InfiniteModeResultStep);
    }

    public override void OnTouchPress(int touchID, float posX, float posY)
    {
        if (_moveObjHitButtonManager != null && _playSceneStep == StopTimePlaySceneStep.GamePlayingStep) {
            if (!_isGamePause) {
                _moveObjHitButtonManager.OnTouchPress(touchID, posX, posY);
                if(_bonusManager.IsAppearClock)
                    CheckClockTouch(touchID, posX, posY);
            } else if (_isQuestionInfinite)
            {
                CloseQuestionInfiniteDes();
            }

        }
    }

    void CloseQuestionInfiniteDes()
    {
        _isQuestionInfinite = false;
        _infiniteTutorialObj.SetActive(false);
        SetGamePause(false);
    }

    void CheckClockTouch(int touchID, float posX, float posY)
    {
        //Transform curClockTrans = _bonusManager.GetCurClockBodyTrans();

        GameObject curClockImgObj = _bonusManager.GetCurClockImgObj();
        Vector3 camImgPosition = _stageCamera.WorldToScreenPoint(curClockImgObj.transform.position);

        //float imgPosX = CAResolutionCtl.Instance.GetScreenToImagePosX(posX);
        //float imgPosY = CAResolutionCtl.Instance.GetScreenToImagePosY(posY);

        //float revisionValue = 5f;
        float clockWidth = 70f;
        float clockHeight = 70f;

        Debug.Log(string.Format("CheckClockTouch camImgPosition : {0}, posX : {1}, posY : {2}",
        camImgPosition, posX, posY));

        float clockXMin = camImgPosition.x - clockWidth;
        float clockXMax = camImgPosition.x + clockWidth;
        float clockYMin = camImgPosition.y - clockHeight;
        float clockYMax = camImgPosition.y + clockHeight;

        if ((clockXMin <= posX && clockXMax >= posX) &&
                (clockYMin <= posY && clockYMax >= posY)) {
            Debug.Log(string.Format("!!!!!Clock Touch!!!!!"));

            _bonusManager.TouchClockImg();
            _bonusManager.ClockSmokeEffect.transform.SetParent(curClockImgObj.transform);
            _bonusManager.ClockSmokeEffect.transform.localPosition = Vector3.zero;
            _bonusManager.ClockSmokeEffect.transform.SetParent(_bonusManager.transform);
            _bonusManager.ClockSmokeEffect.SetActive(true);

            _eventTimer.SetGameTimerData(OnTouchClockEffect, 0.5f);
            TouchBonusClock();
        }
    }

    void TouchBonusClock()
    {
        AddGaugeValue(10f);
    }

    public override void OnTouchEvent(CACurGamePlayTouchData playTouchData, float posX, float posY)
    {
        if (playTouchData != null)
        {
            switch (playTouchData.playTouchType)
            {
                case CAGamePlayTouchType.StopTimeButton:
                    if (_timeCatcherManager.StartState)
                    {
                        OnClickStopButton();
                    }
                    else
                    {
                        //                  OnClickStartButton ();
                    }
                    break;
                case CAGamePlayTouchType.StopTimeOtherButton:
                    //int buttonIndex = (int)playTouchData.curTouchObject;
                    //_stopTimeBaseStage.TouchButtonObj(buttonIndex);
                    break;
            }

        }
    }

    public int CheckStopTimeButton(int touchID, float posX, float posY)
    {
        if ((posX >= _buttonStartX && posX <= _buttonEndX) &&
            (posY >= _buttonStartY && posY <= _buttonEndY))
        {
            if (_curTouchID == -1)
            {
                if (_startButton != null && _stopButton != null)
                {
                    _startButton.SetActive(false);
                    _stopButton.SetActive(true);
                }
                _curTouchID = touchID;
            }
            return _stopButtonIndex;
        }
        else
        {
            if (_curTouchID == touchID)
            {
                if (_startButton != null && _stopButton != null)
                {
                    _startButton.SetActive(true);
                    _stopButton.SetActive(false);
                }
                _curTouchID = -1;
            }
        }

        return -1;
    }

    float GetAddSecondValue()
    {
        _curGapTimeValue = Random.Range(2f, 5f);
        return _curGapTimeValue;
    }

    void SetContinueGame()
    {
        SetFullGauge();
        _playSceneStep = StopTimePlaySceneStep.GamePlayingStep;
    }

    public void ExecuteBackKey()
    {
        if (_isQuestionInfinite)
        {
            CloseQuestionInfiniteDes();
        }
        else
        {
            OnPreviousButton();
        }
    }

    #endregion

    #region Touch Handler

    public override CACurGamePlayTouchData OnGetTouchData(int touchID, float posX, float posY, bool isTouch)
    {
        if (_playSceneStep == StopTimePlaySceneStep.GamePlayingStep)
        {
            int touchButtonIndex = CheckStopTimeButton(touchID, posX, posY);
            //if (touchButtonIndex != -1)
            //{
            //    if (touchButtonIndex == _stopButtonIndex)
            //    {
            //        return new CACurGamePlayTouchData(CAGamePlayTouchType.StopTimeButton, null);
            //    }
            //    else
            //    {
            //        return new CACurGamePlayTouchData(CAGamePlayTouchType.StopTimeOtherButton, touchButtonIndex);
            //    }
            //}

            switch (_stageButtonType)
            {                 case StopTimeButtonType.Normal:                     if (touchButtonIndex != -1)                     {                         if (touchButtonIndex == _stopButtonIndex)                         {                             return new CACurGamePlayTouchData(CAGamePlayTouchType.StopTimeButton, null);                         }                         else                         {                             return new CACurGamePlayTouchData(CAGamePlayTouchType.StopTimeOtherButton, touchButtonIndex);                         }                     }                     break;                 case StopTimeButtonType.PushImmediate:                     if (touchButtonIndex != -1 && isTouch)                     {                         if (touchButtonIndex == _stopButtonIndex)                         {                             OnTouchEvent(new CACurGamePlayTouchData(CAGamePlayTouchType.StopTimeButton, null), posX, posY);                         }                         else                         {
                            //return new CACurGamePlayTouchData(CAGamePlayTouchType.StopTimeOtherButton, touchButtonIndex);
                            OnTouchEvent(new CACurGamePlayTouchData(CAGamePlayTouchType.StopTimeOtherButton, touchButtonIndex), posX, posY);                         }                     }                     return null;             }
        }

        return null;
    }

    #endregion

    #region CallBack Methods

    void OnFinishTimeOver()
    {
        //_stopTimeBaseStage.StartButton.gameObject.SetActive(true);
        //if (_stopTimeBaseStage.StopButton != null)
        //    _stopTimeBaseStage.StopButton.gameObject.SetActive(false);

        //SetPlaySceneStep(StopTimePlaySceneStep.ResultTimeOverDirectingStep);

        SetFailData();
        _eventTimer.SetGameTimerData(OnDelayResult, 0.5f, true, _resultDelayTimeID);
    }

    void OnDelayResult(object objData)
    {
        bool isTimeAdd = (bool)objData;
        if (isTimeAdd) {
            _curInfiniteIndex++;
            _timeStageInfo.TargetTime.AddSecondValue(GetAddSecondValue());

            _timeCatcherManager.ImgTargetTimeNum.SetTimeValue(_timeStageInfo.TargetTime.Hour, _timeStageInfo.TargetTime.Minute, _timeStageInfo.TargetTime.Second);

            _timeCatcherManager.SetFailTime();

            _updateTimeObject.SetActive(true);
            _eventTimer.SetGameTimerData(OnTimeUpdateAni, 0.5f);
        }

        _timeCatcherManager.StartState = true;

        _playSceneStep = StopTimePlaySceneStep.GamePlayingStep;

        if (_stopButton != null) {
            _startButton.gameObject.SetActive(true);
            _stopButton.gameObject.SetActive(false);
        }

        _judgeManager.InitJudgeData();
    }

    void OnTimeUpdateAni(object objData)
    {
        _updateTimeObject.SetActive(false);
    }

    void OnFinishButtonEffect(object objData)
    {
        StageButtonEffect buttonEffectObj = objData as StageButtonEffect;
        if(buttonEffectObj != null)
            Destroy(buttonEffectObj.gameObject);
    }

    public void OnPreviousButton()
    {
        SoundManager.Instance.PlayEffectFX(SoundDefinitions.effectSound_2);
        //_commonPopupController.gameObject.SetActive(true);
        _commonPopupController.ShowPopup(CommonPopupType.Exit, OnExitYesCommonPopup, OnExitNoCommonPopup);
        SetGamePause(true);

        if (BackKeyManager.Instance != null)             BackKeyManager.Instance.IsEnableBackKey = false;
    }

    void OnExitYesCommonPopup()
    {
        Time.timeScale = 1f;
        SoundManager.Instance.PlayEffectFX(SoundDefinitions.effectSound_2);
        CAGameGlobal.Instance.LoadGameScene(GameSceneEnum.SCENE_STAGE, SceneChangeState.SCENE_CHANGE_DIRECT);

        if (BackKeyManager.Instance != null)             BackKeyManager.Instance.IsEnableBackKey = true;
    }

    void OnExitNoCommonPopup()
    {
        SoundManager.Instance.PlayEffectFX(SoundDefinitions.effectSound_2);
        SetGamePause(false);

        if (BackKeyManager.Instance != null)             BackKeyManager.Instance.IsEnableBackKey = true;
    }

    void OnExitGame()
    {
        SoundManager.Instance.PlayEffectFX(SoundDefinitions.effectSound_2);
        CAGameGlobal.Instance.LoadGameScene(GameSceneEnum.SCENE_STAGE, SceneChangeState.SCENE_CHANGE_DIRECT);
    }

    void OnAgainGame()
    {
        SoundManager.Instance.PlayEffectFX(SoundDefinitions.effectSound_2);
        CAGameGlobal.Instance.LoadGameScene(GameSceneEnum.SCENE_INFINITE_MODE, SceneChangeState.SCENE_CHANGE_DIRECT);
    }

    void OnDelayReadyDirecting(object objData)
    {
        SetPlaySceneStep(StopTimePlaySceneStep.ReadyDirectingStep);
    }

    void OnFinishInfiniteReady(int aniType)
    {
        _targetTimeInfo.gameObject.SetActive(true);
        _realTimeViewObj.gameObject.SetActive(true);
        _buttonRootTrans.gameObject.SetActive(true);
        _hpGauge.gameObject.SetActive(true);

        _infiniteReadyTrigger.gameObject.SetActive(false);
        SetPlaySceneStep(StopTimePlaySceneStep.GamePlayingStep);
    }

    void OnReleaseThemeEffectButton(object objData)
    {
        _curInfiniteButton.ReleaseButtonEffect();
    }

    void OnDisappearObstacle(object objData)
    {
        InfiniteObstacleObject obstacleObj = objData as InfiniteObstacleObject;
        obstacleObj.gameObject.SetActive(false);
    }

    void OnDelayReadySound(object objData)
    {
        SoundManager.Instance.PlayEffectFX(SoundDefinitions.effectSound_3);
    }

    void OnYesContinue()
    {
        //UnityAdsManager.Instance.OnResultAdAction = OnResultAD;
        //UnityAdsManager.Instance.ShowRewardedAd();
    }

    void OnNoContinue()
    {
        SetResultPopup();
    }

    void OnHitButton()
    {
        AddGaugeValue(-10f);
    }

    //void OnResultAD(ShowResult result)
    //{
    //    if (result == ShowResult.Finished)
    //    {
    //        SetContinueGame();
    //    }
    //    else
    //    {
    //        SetResultPopup();
    //    }
    //}

    void OnTouchClockEffect(object objData)
    {
        _bonusManager.ClockSmokeEffect.SetActive(false);
    }

    void OnQuestionInfinite()
    {
        _isQuestionInfinite = true;
        _infiniteTutorialObj.SetActive(true);
        SetGamePause(true);
    }

    void OnDelayTutorial(object objData)
    {
        _isStartTutoTouch = true;
    }

    #endregion
}
