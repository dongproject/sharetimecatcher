﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JudgementInfiniteManager : MonoBehaviour 
{
    #region Serialize Variables

#pragma warning disable 649

    [Header("Judge Objects")]
    [SerializeField] GameObject _perfectObj;
    [SerializeField] GameObject _awesomeObj;
    [SerializeField] GameObject _goodObj;
    [SerializeField] GameObject _badObj;

    [Header("Judge Time Objects")]
    [SerializeField] GameObject _judgeObject;
    [SerializeField] GameObject _judgePlusObj;
    [SerializeField] GameObject _judgeMinusObj;
    [SerializeField] tk2dSprite _judgeMillisec_1;
    [SerializeField] tk2dSprite _judgeMillisec_2;

#pragma warning restore 649

    #endregion

    #region Variables

    #endregion

    #region Properties

    #endregion

    #region Methods

    public void InitJudgeData()
    {
        _perfectObj.SetActive(false);
        _awesomeObj.SetActive(false);
        _goodObj.SetActive(false);
        _judgeObject.SetActive(false);
        _badObj.SetActive(false);
    }

    public void SetJudgeValue(StopTimeJudgeType judgeType, float judgeTime)
    {
        switch (judgeType)
        {
            case StopTimeJudgeType.Perfect:
                _perfectObj.SetActive(true);
                break;
            case StopTimeJudgeType.Awesome:
                _awesomeObj.SetActive(true);
                break;
            case StopTimeJudgeType.Good:
                _goodObj.SetActive(true);
                break;
            case StopTimeJudgeType.Fail:
                _badObj.SetActive(true);
                break;
        }

        if (judgeTime >= 0f)
        {
            _judgePlusObj.SetActive(true);
            _judgeMinusObj.SetActive(false);
        }
        else
        {
            _judgePlusObj.SetActive(false);
            _judgeMinusObj.SetActive(true);
        }

        judgeTime = Mathf.Abs(judgeTime);

        int viewGapValue = (int)(judgeTime * 100f);
        int tenMilsec = viewGapValue / 10;
        int restMilsec = viewGapValue % 10;

        //      Debug.Log (string.Format ("gapValue : {0}, viewGapValue : {1}", gapValue, viewGapValue));

        _judgeMillisec_1.SetSprite(string.Format("{0}", tenMilsec));
        _judgeMillisec_2.SetSprite(string.Format("{0}", restMilsec));

        _judgeObject.SetActive(true);
    }

    #endregion
}
