﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AlbumSceneController : CACommonMonoBehaviours, IBackKeyEvent
{
    #region Serialize Variables

#pragma warning disable 649

    [SerializeField] UIAlbum _uiAlbum;

#pragma warning restore 649

    #endregion

    #region Variables

    #endregion

    #region MonoBehaviour Methods

    public override void Awake()
    {
        base.Awake();

        _uiAlbum.OnChangePage = OnChangeAlbumPage;
        _uiAlbum.InitAlbum();
        SetAlbumPage(1);
        //SetAllAlbumOpen();

        if (BackKeyManager.Instance != null)             BackKeyManager.Instance.AddBackKeyEvent((IBackKeyEvent)this);
    }

    public override void OnDestroy()
    {
        base.OnDestroy();

        if (BackKeyManager.Instance != null)
            BackKeyManager.Instance.RemoveBackKeyEvent((IBackKeyEvent)this);
    }

    #endregion

    #region Methods

    void SetAllAlbumOpen()
    {
        _uiAlbum.SetAllAlbumOpen();
    }

    void SetAlbumPage(int pageNum)
    {
        _uiAlbum.SetPresentPageNum(pageNum);
        if (CAGameGlobal.Instance.DynamicData.AlbumData.AlbumOpenInfos.ContainsKey(pageNum))
        {
            List<int> openAlbums = CAGameGlobal.Instance.DynamicData.AlbumData.AlbumOpenInfos[pageNum];
            for(int i = 0;i< openAlbums.Count; i++)
            {
                _uiAlbum.SetCurrentPageAlbum(openAlbums[i]);
            }
        }
    }

    public void ExecuteBackKey()     {         OnPreEvent();     }

    #endregion

    #region CallBack Methods

    public void OnPreEvent()
    {
        SoundManager.Instance.PlayEffectFX(SoundDefinitions.effectSound_2);
        CAGameGlobal.Instance.LoadGameScene(GameSceneEnum.SCENE_STAGE, SceneChangeState.SCENE_CHANGE_DIRECT);
    }

    void OnChangeAlbumPage(int pageNum)
    {
        SoundManager.Instance.PlayEffectFX(SoundDefinitions.effectSound_2);
        SetAlbumPage(pageNum);
    }

    #endregion
}
