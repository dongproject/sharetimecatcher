﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIAlbumInnerPage : MonoBehaviour 
{
    #region Serialize Variables

#pragma warning disable 649

    [SerializeField] GameObject[] _innerObjects;

#pragma warning restore 649

    #endregion

    #region Properties

    public GameObject[] InnerObjects
    {
        get { return _innerObjects; }
    }

    #endregion

    #region Methods

    public void InitInnerObject()
    {
        for (int i = 0; i < _innerObjects.Length; i++)
        {
            _innerObjects[i].SetActive(false);
        }
    }

    public void SetAllOpenObject()
    {
        for(int i = 0;i< _innerObjects.Length; i++)
        {
            _innerObjects[i].SetActive(true);
        }
    }

    #endregion
}
