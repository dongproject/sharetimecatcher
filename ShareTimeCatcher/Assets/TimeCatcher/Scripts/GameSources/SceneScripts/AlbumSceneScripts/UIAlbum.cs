﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIAlbum : MonoBehaviour 
{
    #region Serialize Variables

#pragma warning disable 649

    [SerializeField] Texture[] _numTextures;
    [SerializeField] GameObject[] _cardFrontObjs;
    [SerializeField] GameObject[] _cardBackObjs;
    [SerializeField] UIAlbumInnerPage[] _innerPages;
    [SerializeField] tk2dSpriteFromTexture _presentSprite;
    [SerializeField] tk2dSpriteFromTexture _totalSprite;
    [SerializeField] GameObject _preButtonObj;
    [SerializeField] GameObject _nextButtonObj;

#pragma warning restore 649

    #endregion

    #region Variables

    int _presentPageNum;
    int _totalPageNum;
    Action<int> _onChangePage;

    #endregion

    #region Properties

    public int PresentPageNum
    {
        get { return _presentPageNum; }
    }

    public int TotalPageNum
    {
        get { return _totalPageNum; }
    }

    public Action<int> OnChangePage
    {
        get { return _onChangePage; }
        set { _onChangePage = value; }
    }

    #endregion

    #region Methods

    public void InitAlbum()
    {
        for(int i = 0;i< _innerPages.Length; i++)
        {
            _innerPages[i].gameObject.SetActive(false);
        }

        //SetPresentPageNum(curPageNum);
        SetTotalPageNum(_innerPages.Length);
    }

    public void SetPresentPageNum(int numPresent)
    {
        if (numPresent == 1)
        {
            _preButtonObj.SetActive(false);
            _nextButtonObj.SetActive(true);
        }
        else if(numPresent == _totalPageNum)
        {
            _preButtonObj.SetActive(true);
            _nextButtonObj.SetActive(false);
        }
        else
        {
            _preButtonObj.SetActive(true);
            _nextButtonObj.SetActive(true);
        }

        _presentPageNum = numPresent;
        _presentSprite.texture = _numTextures[numPresent];
        _presentSprite.ForceBuild();

        _innerPages[_presentPageNum - 1].gameObject.SetActive(true);

        SetValidCardImg();
    }

    void SetValidCardImg()
    {
        int cardCount = _innerPages[_presentPageNum - 1].InnerObjects.Length;
        for (int i = 0; i < _cardFrontObjs.Length; i++)
        {
            if (i < cardCount)
            {
                _cardFrontObjs[i].SetActive(false);
                _cardBackObjs[i].SetActive(true);
            }
            else
            {
                _cardFrontObjs[i].SetActive(false);
                _cardBackObjs[i].SetActive(false);
            }
        }
    }

    public void SetOpenCard(int[] cardIndexes)
    {
        for(int i = 0;i< cardIndexes.Length; i++)
        {
            int cardIndex = cardIndexes[i];
            _cardFrontObjs[cardIndex].SetActive(true);
            _cardBackObjs[cardIndex].SetActive(false);
        }

    }

    public void SetCurrentPageAlbum(int albumNum)
    {
        int cardIndex = albumNum - 1;
        _cardFrontObjs[cardIndex].SetActive(true);
        _cardBackObjs[cardIndex].SetActive(false);
        if (_innerPages[_presentPageNum - 1].InnerObjects[cardIndex].activeSelf)
            return;

        _innerPages[_presentPageNum - 1].InnerObjects[cardIndex].SetActive(true);
        
    }

    public void SetTotalPageNum(int totalPage)
    {
        _totalPageNum = totalPage;
        _totalSprite.texture = _numTextures[totalPage];
        _totalSprite.ForceBuild();
    }

    public void SetAllAlbumOpen()
    {
        for (int i = 0; i < _innerPages.Length; i++)
        {
            _innerPages[i].SetAllOpenObject();
        }
    }

    #endregion

    #region CallBack Methods

    public void OnPrevious()
    {
        if(_onChangePage != null)
        {
            _innerPages[_presentPageNum - 1].gameObject.SetActive(false);
            _onChangePage(_presentPageNum - 1);
        }

        //Debug.Log(string.Format("OnPrevious")); 
        //SetPresentPageNum(_presentPageNum - 1);
    }

    public void OnNext()
    {
        if (_onChangePage != null)
        {
            _innerPages[_presentPageNum - 1].gameObject.SetActive(false);
            _onChangePage(_presentPageNum + 1);
        }
        //SetPresentPageNum(_presentPageNum + 1);
        //Debug.Log(string.Format("OnNext"));
    }

    #endregion
}
