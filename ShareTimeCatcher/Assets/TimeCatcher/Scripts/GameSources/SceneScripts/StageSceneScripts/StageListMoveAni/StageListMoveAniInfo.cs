﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum StageListMoveDirection
{
	None,
	Left,
	Right,
}

public class StageListMoveAniInfo
{
	#region Variables

	float _moveSpeed;
	float _totalMoveValue;
	float _curMoveValue;
	StageListMoveDirection _moveDirection;
//	float _preTime;
	float _decreaseValue;

	#endregion

	#region Properties

	public float MoveSpeed
	{
		get{ return _moveSpeed; }
		set{ _moveSpeed = value; }
	}

	public float TotalMoveValue
	{
		get{ return _totalMoveValue; }
		set{ _totalMoveValue = value; }
	}

	public float CurMoveValue
	{
		get{ return _curMoveValue; }
		set{ _curMoveValue = value; }
	}

	public StageListMoveDirection MoveDirection
	{
		get{ return _moveDirection; }
		set{ _moveDirection = value; }
	}

//	public float PreTime
//	{
//		get{ return _preTime; }
//		set{ _preTime = value; }
//	}

	public float DecreaseValue
	{
		get{ return _decreaseValue; }
		set{ _decreaseValue = value; }
	}

	#endregion

	#region Methods

	#endregion
}
