﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public enum StageListMoveAniType
{
	None,
	Hang,
	Acceleration,
}

public class StageListMoveAniManager
{
	#region Variables

	float _minSpeed = 10f; // 2f
	float _maxSpeed = 300f;
	float _decreaseSpeed = 2f;

	StageListMoveAniInfo _listMoveAniInfo = null;

	StageListMoveAniType _listMoveAniType = StageListMoveAniType.None;

	Action _onFinishHang;
	Action _onFinishAcceleration;

	#endregion

	#region Properties

	public StageListMoveAniInfo ListMoveAniInfo
	{
		get{ return _listMoveAniInfo; }
		set{ _listMoveAniInfo = value; }
	}

	public StageListMoveAniType ListMoveAniType
	{
		get{ return _listMoveAniType; }
		set{ _listMoveAniType = value; }
	}

	public Action OnFinishHang
	{
		get{ return _onFinishHang; }
		set{ _onFinishHang = value; }
	}

	public Action OnFinishAcceleration
	{
		get{ return _onFinishAcceleration; }
		set{ _onFinishAcceleration = value; }
	}

	#endregion

	#region Methods

	public float UpdateHangListMoveAni()
	{
		if (_listMoveAniInfo == null)
			return 0f;
		
		float retValue = 0f;

		bool completeAniState = false;
		if (_listMoveAniInfo.TotalMoveValue <= _listMoveAniInfo.CurMoveValue + _listMoveAniInfo.MoveSpeed) {
			completeAniState = true;
			retValue = _listMoveAniInfo.TotalMoveValue - _listMoveAniInfo.CurMoveValue;
		} else {
			retValue = _listMoveAniInfo.MoveSpeed;
		}

		if (_listMoveAniInfo.MoveSpeed > _minSpeed) {
			_listMoveAniInfo.MoveSpeed -= _decreaseSpeed;
			if (_listMoveAniInfo.MoveSpeed < _minSpeed) {
				_listMoveAniInfo.MoveSpeed = _minSpeed;
			}
		}

		_listMoveAniInfo.CurMoveValue += retValue;

		if (_listMoveAniInfo.MoveDirection == StageListMoveDirection.Left) {
			retValue = -retValue;
		}

		if (completeAniState) {
			if (_onFinishHang != null)
				_onFinishHang ();
			ReleaseAniData ();
		}

		return retValue;
	}

	public float UpdateAccelerationListMoveAni()
	{
		if (_listMoveAniInfo == null)
			return 0f;

        float retValue = _listMoveAniInfo.MoveSpeed * Time.deltaTime;

		bool completeAniState = false;

		if (_listMoveAniInfo.MoveSpeed - _listMoveAniInfo.DecreaseValue < _minSpeed) {
			completeAniState = true;
        } else {
            _listMoveAniInfo.MoveSpeed -= _listMoveAniInfo.DecreaseValue;
        }

		if (_listMoveAniInfo.MoveDirection == StageListMoveDirection.Left) {
			retValue = -retValue;
		}

		if (completeAniState) {
			ReleaseAniData ();
//			retValue = 0f;
			if (_onFinishAcceleration != null) {
				_onFinishAcceleration ();
			}
		}

		return retValue;
	}

	public void AddHangMoveAniData(float moveValue)
	{
		StageListMoveAniInfo inputMoveAniInfo = new StageListMoveAniInfo();
		inputMoveAniInfo.TotalMoveValue = Mathf.Abs (moveValue);
		if (moveValue < 0f) {
			inputMoveAniInfo.MoveDirection = StageListMoveDirection.Left;
		} else {
			inputMoveAniInfo.MoveDirection = StageListMoveDirection.Right;
		}

		float calcMoveSpeed = inputMoveAniInfo.TotalMoveValue / 5f;
		//Debug.Log (string.Format ("Pre CalcMoveSpeed : {0}, nputMoveAniInfo.TotalMoveValue : {1}", calcMoveSpeed, inputMoveAniInfo.TotalMoveValue));
		if (calcMoveSpeed > _maxSpeed) {
			calcMoveSpeed = _maxSpeed;
		} else if (calcMoveSpeed < _minSpeed) {
			calcMoveSpeed = _minSpeed;
		}

		//Debug.Log (string.Format ("After CalcMoveSpeed : {0}", calcMoveSpeed));

		inputMoveAniInfo.MoveSpeed = calcMoveSpeed;
		inputMoveAniInfo.CurMoveValue = 0f;
		_listMoveAniInfo = inputMoveAniInfo;

		_listMoveAniType = StageListMoveAniType.Hang;
	}

	public bool AddAccelerationListMove(float gapTime, float moveDis)
	{
		if (moveDis == 0f || gapTime == 0f)
			return false;

		float velocity = moveDis / gapTime;

		StageListMoveAniInfo inputMoveAniInfo = new StageListMoveAniInfo();

		inputMoveAniInfo.MoveSpeed = Mathf.Abs (velocity);
		if (velocity < 0f) {
			inputMoveAniInfo.MoveDirection = StageListMoveDirection.Left;
		} else {
			inputMoveAniInfo.MoveDirection = StageListMoveDirection.Right;
		}
        
		inputMoveAniInfo.DecreaseValue = 5f / gapTime;

		if (inputMoveAniInfo.DecreaseValue < 1f)
			inputMoveAniInfo.DecreaseValue = 1f;

		//Debug.Log (string.Format ("AddAccelerationListMove velocity : {0}, gapTime : {1}, moveDis : {2}, inputMoveAniInfo.DecreaseValue : {3}", 
		//	velocity, gapTime, moveDis, inputMoveAniInfo.DecreaseValue));

		_listMoveAniInfo = inputMoveAniInfo;

		_listMoveAniType = StageListMoveAniType.Acceleration;

		return true;
	}

	public void ReleaseAniData()
	{
		_listMoveAniInfo = null;
		_listMoveAniType = StageListMoveAniType.None;
	}

	#endregion
}
