﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StageObjectInfo
{
	#region Variables

	GameObject _stageObject;
	SelectStageInfo _selectStageInfo;
	int _starCount;

	#endregion

	#region Properties

	public GameObject StageObject
	{
		get{ return _stageObject; }
		set{ _stageObject = value; }
	}

	public SelectStageInfo SelectStageInfo
	{
		get{ return _selectStageInfo; }
		set{ _selectStageInfo = value; }
	}

	public int StarCount
	{
		get{ return _starCount; }
		set{ 
			_starCount = value; 
			for (int i = 0; i < _selectStageInfo.StarImgObjs.Length; i++) {
				if(i < _starCount)
					_selectStageInfo.StarImgObjs [i].gameObject.SetActive (true);
				else
					_selectStageInfo.StarImgObjs [i].gameObject.SetActive (false);
			}
		}
	}

	#endregion
}

public class StageCircleObjInfo : MonoBehaviour 
{
	#region Variables

	[SerializeField] GameObject[] _pieceStages;

	List<StageObjectInfo> _stageObjectInfos = new List<StageObjectInfo>();

	#endregion

	#region Properties

	public List<StageObjectInfo> StageObjectInfos
	{
		get{ return _stageObjectInfos; }
	}

	#endregion

	#region MonoBehaviour Methods

	void Awake()
	{
		SetStarObjects ();
	}

	#endregion

	#region Methods

	void SetStarObjects()
	{
		for (int i = 0; i < _pieceStages.Length; i++) {
			StageObjectInfo inputStageObjInfo = new StageObjectInfo ();
			inputStageObjInfo.StageObject = _pieceStages [i];
			inputStageObjInfo.SelectStageInfo = _pieceStages [i].GetComponent<SelectStageInfo> ();
			_stageObjectInfos.Add (inputStageObjInfo);
		}
	}

	#endregion
}
