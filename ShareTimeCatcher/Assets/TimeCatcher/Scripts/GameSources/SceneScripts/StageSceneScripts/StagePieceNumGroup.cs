﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Text;
using UnityEngine.UI;

public class StagePieceNumGroup : MonoBehaviour 
{
	#region Serialize Variables

	[SerializeField] GameObject[] _stageNumObjs;
	[SerializeField] Image[] _stageNumFrontImgs;

	#endregion

	#region Variables

	Color _onColor = new Color (34f / 255f, 1f, 30f / 255f, 1f);
	Color _offColor = new Color (19f / 255f, 92f / 255f, 18f / 255f, 1f);

	bool _isOnStage = true;

	#endregion

	#region Properties

	public GameObject[] StageNumObjs
	{
		get{ return _stageNumObjs; }
	}

	public bool IsOnStage
	{
		get{ return _isOnStage; }
		set{ _isOnStage = value; }
	}

	#endregion

	#region Methods

	public GameObject GetStageNumObject(int number)
	{
		string numberStr = number.ToString ();
		char[] numberChars = numberStr.ToCharArray ();

//		Debug.Log (string.Format ("GetStageNumObject number : {0}, numberChars Length : {1}", number, numberChars.Length));

		GameObject stageNumRoot = new GameObject ("StageNumObjs");

		float gapValueX = 25f;

		int rest = numberChars.Length % 2;
		int quotient = numberChars.Length / 2;

		float startValueX = 0f;
		if (rest == 0) {
			startValueX = -(quotient * gapValueX) - (gapValueX*0.5f);
		} else {
			startValueX = -(quotient * gapValueX);
		}

		for (int i = 0; i < numberChars.Length; i++) {
			int curNumIndex = (int)Char.GetNumericValue (numberChars [i]);
//			Debug.Log (string.Format ("GetStageNumObject numberChars : {0}, curNumIndex : {1}", numberChars [i], curNumIndex));
			GameObject curNumObj = _stageNumObjs [curNumIndex];
			if (_isOnStage) {
				_stageNumFrontImgs [curNumIndex].color = _onColor;
			} else {
				_stageNumFrontImgs [curNumIndex].color = _offColor;
			}
			GameObject insNumObj = Instantiate (curNumObj) as GameObject;
			insNumObj.transform.SetParent (stageNumRoot.transform);
			RectTransform numRectTrans = insNumObj.GetComponent<RectTransform> ();
			numRectTrans.anchoredPosition3D = new Vector3 (startValueX, 0f, 0f);
			startValueX += gapValueX;
		}

		return stageNumRoot;
	}

	#endregion
}
