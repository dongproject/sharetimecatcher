﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum CircleFocusDirection
{
	CircleLeft,
	CircleRight,
}

public class StageCircleGroupInfo : MonoBehaviour 
{
    #region Serialize Variables

#pragma warning disable 649

    [SerializeField] GameObject[] _stageNumObjs;
	[SerializeField] StagePartInfo[] _stagePartInfos;

#pragma warning restore 649

    #endregion

    #region Variables

    int _groupID;
	RectTransform _groupRectTrans;
	CircleFocusDirection _focusDirection;
    Vector2 _saveAnchoredPosition;

    #endregion

    #region Properties

    public int GroupID
	{
		get{ return _groupID; }
		set{ _groupID = value; }
	}

	public StagePartInfo[] StagePartInfos
	{
		get{ return _stagePartInfos; }
	}

	public RectTransform GroupRectTrans
	{
		get{ return _groupRectTrans; }
		set{ _groupRectTrans = value; }
	}

	public CircleFocusDirection FocusDirection
	{
		get{ return _focusDirection; }
		set{ 
			_focusDirection = value;
			SetFocusList ();
		}
	}

    public Vector2 SaveAnchoredPosition
    {
        get { return _saveAnchoredPosition; }
        set { _saveAnchoredPosition = value; }
    }

    #endregion

    #region MonoBehaviour Methods

    void Awake()
	{
		
	}

	#endregion

	#region Methods

	void SetFocusList()
	{
		int middleValue = _stagePartInfos.Length / 2;
		switch (_focusDirection) {
		case CircleFocusDirection.CircleLeft:
			for (int i = 0; i < _stagePartInfos.Length; i++) {
				if (middleValue > i) {
					_stagePartInfos [i].IsFocus = true;
				} else {
					_stagePartInfos [i].IsFocus = false;
				}
			}
			break;
		case CircleFocusDirection.CircleRight:
			for (int i = 0; i < _stagePartInfos.Length; i++) {
				if (middleValue <= i) {
					_stagePartInfos [i].IsFocus = true;
				} else {
					_stagePartInfos [i].IsFocus = false;
				}
			}
			break;
		}
	}

    public int GetCurFirstStageIndex(CircleFocusDirection circleDir)
    {
        if(circleDir == CircleFocusDirection.CircleLeft)
        {
            return (_groupID * _stagePartInfos.Length) + 1;
        }
        else
        {
            return (_groupID * _stagePartInfos.Length) + 6;
        }
    }

    public int GetCurCircleHalfMaxStarCount(CircleFocusDirection circleDir)
    {
        int retValue = 0;
        int pieceStarCount = 3;
        int middleValue = _stagePartInfos.Length / 2;
        switch (circleDir) {
            case CircleFocusDirection.CircleLeft:
                for (int i = 0; i < middleValue; i++) {
                    retValue += pieceStarCount;
                }
                break;
            case CircleFocusDirection.CircleRight:
                for (int i = middleValue; i < _stagePartInfos.Length; i++) {
                    retValue += pieceStarCount;
                }
                break;
        }

        return retValue;
    }

    public int GetCurCircleHalfStarCount(CircleFocusDirection circleDir)
    {
        int retValue = 0;
        int middleValue = _stagePartInfos.Length / 2;
        switch (circleDir) {
            case CircleFocusDirection.CircleLeft:
                for (int i = 0; i < middleValue; i++) {
                    retValue += _stagePartInfos[i].StarCount;
                }
                break;
            case CircleFocusDirection.CircleRight:
                for (int i = middleValue; i < _stagePartInfos.Length; i++) {
                    retValue += _stagePartInfos[i].StarCount;
                }
                break;
        }

        return retValue;
    }

    public bool CheckCurCircleHalfAllClear(CircleFocusDirection circleDir)
    {
        bool retValue = true;
        int middleValue = _stagePartInfos.Length / 2;
        switch (circleDir)
        {
            case CircleFocusDirection.CircleLeft:
                for (int i = 0; i < middleValue; i++)
                {
                    if(_stagePartInfos[i].StarCount == 0)
                    {
                        retValue = false;
                        break;
                    }
                }
                break;
            case CircleFocusDirection.CircleRight:
                for (int i = middleValue; i < _stagePartInfos.Length; i++)
                {
                    if (_stagePartInfos[i].StarCount == 0)
                    {
                        retValue = false;
                        break;
                    }
                }
                break;
        }

        return retValue;
    }

    public void SetAllNotFocus()
	{
		for (int i = 0; i < _stagePartInfos.Length; i++) {
			_stagePartInfos [i].IsFocus = false;
		}
	}

	public void SetLastPart(int lastIndex)
	{
		_stagePartInfos [lastIndex].StageSelectObj.SetActive (true);
	}

	public void SetCheatLastPart(int lastIndex, int starCount = 3)
	{
		if (lastIndex > 0) {
            _stagePartInfos[lastIndex - 1].SetStageStarCount(starCount);
            _stagePartInfos [lastIndex - 1].StageSelectObj.SetActive (false);
		}

		_stagePartInfos [lastIndex].StageSelectObj.SetActive (true);
    }

	#endregion
}
