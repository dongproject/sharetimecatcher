﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StageSceneListController : CACommonMonoBehaviours, IUpdateable, IBackKeyEvent
{
    #region Sub Class

    public class CircleListInfo
    {
        #region Variables

        CircleFocusDirection _circleFocus;
        StageCircleGroupInfo _circleGroupInfo;

        #endregion

        #region Properties

        public CircleFocusDirection CircleFocus
        {
            get{ return _circleFocus; }
            set{ _circleFocus = value; }
        }

        public StageCircleGroupInfo CircleGroupInfo
        {
            get { return _circleGroupInfo; }
            set { _circleGroupInfo = value; }
        }

        #endregion
    }

    #endregion

    #region Serialize Variables

#pragma warning disable 649

    [SerializeField] GameObject _stageSelectCirclePrefab;
	[SerializeField] Transform _circleGroupRoot;
	[SerializeField] StagePieceNumGroup _stagePiceNumGroup;

    [SerializeField] GameObject _stageListRootObj;
    [SerializeField] OptionPopupManager _optionPopupManager;
    [SerializeField] StageCommonPopupController _commonPopupController;

    [SerializeField] Image _button_InfinitemodeImg;
    [SerializeField] Image _button_InfiniteThemeImg;

    [SerializeField] ImgNumFont _curStarImgNumFont;
    [SerializeField] ImgNumFont _goalStarImgNumFont;

    [Header("Episode Objects")]
    [SerializeField] EpisodeImgManager _episodeManager;

    [Header("Cheat Objects")]
    [SerializeField] GameObject _stageClearCheatObj;
    [SerializeField] GameObject _initButtonObj;

#pragma warning restore 649

    #endregion

    #region Variables

    float _radiusValue;
	float _squareRadius;

    float _circleRadius = 415f;

	Vector2 _circleCenterPos = new Vector2(0f, -2000f);
	Vector2 _leftHangPos = new Vector2(-217f, -120f);
	Vector2 _rightHangPos = new Vector2(217f, -120f);
	float _leftHangPosX = -217f;
	float _rightHangPosX = 217f;

	float _leftHangPosY;
	float _rightHangPosY;

	float _startAngle;
	float _gapAngle = 30f;

	int _groupTotalCount = 3;//12;
	int _groupPerStage = 10;
	int _curTouchID = -1;

	float _curStartAngle;
    int _touchPieceIndex;

	Dictionary<int /* GroupID */, StageCircleGroupInfo> _stageCircleGroupInfos = new Dictionary<int, StageCircleGroupInfo>();

	int _validStageNum = 1;

	bool _isTouchState = true;

	StageListMoveAniManager _listMoveAniManager = new StageListMoveAniManager();

	float _startTouchTime = 0f;
	float _startTouchValue = 0f;

	StageListMoveDirection _moveDirectiong = StageListMoveDirection.None;

    StageCircleGroupInfo _curGroupInfo = null;
	StageListMoveDirection _curHangDirection;

    int _lastAccessGroupID = 0;
	bool _isMoveableState = true;
    bool _isEnableUpdate = false;
    bool _isCircleLastBoundary = false;

    StoryAniManager _storyPrologueAniManager = null;

    StageSceneDefinitions.StageSceneStep _curSceneStep = StageSceneDefinitions.StageSceneStep.Normal;

    CAGameTimerEventCtl _evenTimer = new CAGameTimerEventCtl();

    Color _starLackColor = new Color(186f/255f, 61f/255f, 5f/255f, 1f);
    Color _starMaxColor = new Color(8f/255f, 186f/255f, 5f/255f, 1f);

    CircleListInfo _firstCircleGroup = new CircleListInfo();
    CircleListInfo _lastCircleGroup = new CircleListInfo();

    bool _isAllAccess = false;

    bool _isAlbumOpenInit = false;

    bool _isUpdate = true;
    bool _isPopupNeedStar = false;

    #endregion

    #region TimerID Values

    int _episodeTimerID = 1;

    #endregion

    #region Properties

    public bool IsEnableUpdate
    {
        get { return _isEnableUpdate; }
        set { _isEnableUpdate = value; }
    }

    #endregion

    #region MonoBehaviour Methods

    public override void Awake()
	{
		base.Awake ();

        if (CAGameGlobal.Instance.DynamicData.AlbumData.AlbumOpenInfos.Count == 0)
        {
            _isAlbumOpenInit = false;
        }
        else
        {
            _isAlbumOpenInit = true;
        }

        SetStartCurSceneStep();

        if (_curSceneStep == StageSceneDefinitions.StageSceneStep.Normal) {
            curTouchState = true;
        } else {
            curTouchState = false;
        }

        CalcCircleValues ();
		MakeStageCircleGroups ();
		InitCurCircleGroup ();
		CalcMoveHangPos ();
//		SetCurrentFocusCircle ();
		_listMoveAniManager.OnFinishHang = OnFinishHangListAni;
		_listMoveAniManager.OnFinishAcceleration = OnFinishAccelerationListAni;

        CheckFocusStage();

        _optionPopupManager.BackButton.onClick.RemoveAllListeners();
        _optionPopupManager.BackButton.onClick.AddListener(() => OnCloseOptionPopup());

        _optionPopupManager.RateButton.onClick.AddListener(() => OnRateButton());
        _optionPopupManager.TutorialButton.onClick.AddListener(() => OnTutorialButton());
        _optionPopupManager.OpeningButton.onClick.AddListener(() => OnOpeningButton());
        _optionPopupManager.FaceBookShareButton.onClick.AddListener(() => OnFacebookShareButton());
        _optionPopupManager.ContactButton.onClick.AddListener(() => OnContactButton());

        if(BackKeyManager.Instance != null)
            BackKeyManager.Instance.AddBackKeyEvent((IBackKeyEvent)this);

#if !_CHEAT
        _stageClearCheatObj.SetActive(false);
        _initButtonObj.SetActive(false);
#endif
    }

	// Use this for initialization
	public override void Start ()
	{
		base.Start ();

        if(GameMainManager.Instance != null)
            GameMainManager.Instance.AddUpdateInfo(this);
        _isEnableUpdate = true;

        AudioClip musicClip = Resources.Load<AudioClip>(SoundFilePaths.MusicPath_3);
        if(SoundManager.Instance != null)
            SoundManager.Instance.PlayMusic(musicClip);

        SetInfiniteButtonState();

        _commonPopupController.SetInstance();
    }

    public override void OnDisable()
    {
        if(SoundManager.Instance != null)
            SoundManager.Instance.StopMusic();
    }

    public override void OnDestroy()
	{
		base.OnDestroy ();

        if (GameMainManager.Instance != null)
            GameMainManager.Instance.RemoveUpdateInfo(this);

        if (BackKeyManager.Instance != null)
            BackKeyManager.Instance.RemoveBackKeyEvent((IBackKeyEvent)this);
    }

	#endregion

	#region Methods

    public void OnUpdate()
    {
        if (!_isUpdate)
            return;

        UpdateListAni();
        _evenTimer.UpdateGameTimer();
    }

    void SetInfiniteButtonState()
    {
        StageCurrentInfo stageCurInfo = CAGameSaveDataManager.Instance.StageProgressSave.GetStageCurrentInfo(5);         if (stageCurInfo.starCount == 0)
        {
            _button_InfinitemodeImg.color = new Color(93f/255f, 4f/255f, 0f, 1f);
            _button_InfiniteThemeImg.color = new Color(93f / 255f, 4f / 255f, 0f, 1f);
        }
    }

    int GetStarNeedCount(int stageIndex)
    {
        //Debug.Log(string.Format("GetStarNeedCount stageIndex : {0}", stageIndex));
        if(stageIndex >= 1 && stageIndex <= 10)
        {
            return 5;
        } 
        else if(stageIndex > 10 && stageIndex <= 20)
        {
            return 7;
        } 
        else if(stageIndex > 20 && stageIndex <= 30)
        {
            return 10;
        }

        return 15;
    }

    void SetStartCurSceneStep()
    {
        if(!CAGameSaveDataManager.Instance.StoryProgressSave.GetStoryProgressState((int)StoryDefinitions.StoryIndex.Prologue)) {
            SetCurSceneStep(StageSceneDefinitions.StageSceneStep.PrologueStoryStep);
            //SetPrologueStory();
        } else {
            //if(CAGameGlobal.Instance.DynamicData.StageSceneState == GameDefinitions.StageSceneState.NextStage)
            //{
            //    SetCurSceneStep(StageSceneDefinitions.StageSceneStep.FocusStep);
            //    CAGameGlobal.Instance.DynamicData.StageSceneState = GameDefinitions.StageSceneState.None;
            //} 
            //else
            //{
            //    _curSceneStep = StageSceneDefinitions.StageSceneStep.Normal;
            //}
            _curSceneStep = StageSceneDefinitions.StageSceneStep.Normal;
        }
    }

    void SetPrologueStory()
    {
        if(_storyPrologueAniManager == null)
        {
            GameObject prologueStoryObj = Instantiate(Resources.Load<GameObject>("Prefabs/Story/StoryPrologue")) as GameObject;
            prologueStoryObj.transform.SetParent(this.transform.parent);
            prologueStoryObj.transform.localScale = Vector3.one;
            prologueStoryObj.transform.localPosition = new Vector3(0f, 0f, -5f);

            _storyPrologueAniManager = prologueStoryObj.GetComponent<StoryAniManager>();
            _storyPrologueAniManager.AniTrigger.OnAniTrigger = OnStoryAniTrigger;
        }

        _storyPrologueAniManager.AniTrigger.gameObject.SetActive(true);
    }

    void SetFocusStage()
    {
        int stageIndex = CAGameGlobal.Instance.CurrentStageNum - 1;
        int pieceIndex = stageIndex - (_curGroupInfo.GroupID * _groupPerStage);

        if(pieceIndex >= 10)
        {
            pieceIndex = 0;
            if(!_stageCircleGroupInfos.ContainsKey(_curGroupInfo.GroupID + 1))
            {
                _curSceneStep = StageSceneDefinitions.StageSceneStep.Normal;
                return;
            }

            _curGroupInfo = _stageCircleGroupInfos[_curGroupInfo.GroupID + 1];
        }

        _curGroupInfo.StagePartInfos[pieceIndex].SetEnableFocusTween(false);
        GameObject episodeImg = _episodeManager.GetEpisodeImg(CAGameGlobal.Instance.CurrentStageNum - 1);
        if (episodeImg != null)
        {
            _curSceneStep = StageSceneDefinitions.StageSceneStep.EpisodeImg;
            _episodeManager.EpisodeImgBG.SetActive(true);
            episodeImg.SetActive(true);
            _evenTimer.SetGameTimerData(OnDelayEpisodeImg, 5f, pieceIndex, _episodeTimerID);
        }
        else
        {
            SetAutoStart(pieceIndex);
          
        }
    }

    void CalcCircleValues()
	{
		_radiusValue = Mathf.Sqrt (Mathf.Pow (_leftHangPos.x, 2f) + Mathf.Pow (_leftHangPos.y - _circleCenterPos.y, 2f));
		_squareRadius = _radiusValue * _radiusValue;

        float addAngle = 0f;//_gapAngle * 0.5f;

        if(CAGameSaveDataManager.Instance.StageProgressSave.StartStageAngle > -1f)
        {
            _startAngle = CAGameSaveDataManager.Instance.StageProgressSave.StartStageAngle;
        }
        else
        {
            _startAngle = CAMathCtl.Instance.CalcAngle2D(_circleCenterPos.x, _circleCenterPos.y, _rightHangPos.x, _rightHangPos.y) + addAngle;
        }

		_leftHangPosY = Mathf.Sqrt(Mathf.Pow(_radiusValue, 2f) - Mathf.Pow (_leftHangPosX, 2f)) + _circleCenterPos.y;
		_rightHangPosY = Mathf.Sqrt(Mathf.Pow(_radiusValue, 2f) - Mathf.Pow (_rightHangPosX, 2f)) + _circleCenterPos.y;

		Vector2 calcCircleLinePos = CAMathCtl.Instance.GetCircleLinePos (_circleCenterPos, _radiusValue, _startAngle - _gapAngle);

		Debug.Log (string.Format ("CalcCircleHangPos _radiusValue; {0}, _startAngle : {1}, calcCircleLinePos : {2}", _radiusValue, _startAngle, calcCircleLinePos));
	}

	void SetCurCircleInfo(StageCircleGroupInfo circleInfo)
	{
		if (_curGroupInfo == circleInfo)
			return;

		_curGroupInfo = circleInfo;

		int preGroupID = 0;
		if (_curGroupInfo.GroupID == 0) {
			preGroupID = _groupTotalCount - 1;
		} else {
			preGroupID = _curGroupInfo.GroupID - 1;
		}

		int nextGroupID = 0;
		if (_curGroupInfo.GroupID != _groupTotalCount - 1) {
			nextGroupID = _curGroupInfo.GroupID + 1;
		}
	}

	void MakeStageCircleGroups()
	{
		_stageCircleGroupInfos.Clear ();
		_curStartAngle = _startAngle;
		float curAngle = _startAngle;
		for (int i = 0; i < _groupTotalCount; i++) {
			Vector2 calcCircleLinePos = CAMathCtl.Instance.GetCircleLinePos (_circleCenterPos, _radiusValue, curAngle);

			if (curAngle - _gapAngle < 0) {
				curAngle += 360f;
			}
			curAngle -= _gapAngle;

			GameObject stageCircleObj = Instantiate (_stageSelectCirclePrefab) as GameObject;
            stageCircleObj.SetActive(true);
            stageCircleObj.transform.SetParent (_circleGroupRoot);
			stageCircleObj.transform.localScale = Vector3.one;
			RectTransform circleRectTrans = stageCircleObj.GetComponent<RectTransform> ();
			circleRectTrans.anchoredPosition = calcCircleLinePos;

			StageCircleGroupInfo circleGroupInfo = stageCircleObj.GetComponent<StageCircleGroupInfo> ();
			circleGroupInfo.GroupRectTrans = circleRectTrans;
            circleGroupInfo.SaveAnchoredPosition = calcCircleLinePos;

            circleGroupInfo.GroupID = i;
			int lastPartIndex = -1;
			for (int j = 0; j < circleGroupInfo.StagePartInfos.Length; j++) {
				
				int partIndex = j;
				int curStageIndex = (circleGroupInfo.GroupID * _groupPerStage) + (partIndex);

				StageCurrentInfo stageCurInfo = CAGameSaveDataManager.Instance.StageProgressSave.GetStageCurrentInfo (curStageIndex + 1);
                circleGroupInfo.StagePartInfos[j].SetStageStarCount(stageCurInfo.starCount);
                if (stageCurInfo.starCount > 0) {
                    if (!_isAlbumOpenInit)
                    {
                        SetAlbumOpenData(stageCurInfo.stageNum);
                    }

                    Debug.Log(string.Format("partIndex : {0}", partIndex));

                    if(partIndex == 4) {
                        if (CheckValidStarCount(circleGroupInfo, CircleFocusDirection.CircleLeft)) {
                            _validStageNum = stageCurInfo.stageNum + 1;
                        }
                    } else if(partIndex == 9) {
                        if(CheckValidStarCount(circleGroupInfo, CircleFocusDirection.CircleRight)) {
                            _validStageNum = stageCurInfo.stageNum + 1;
                        }
                    } else {
                        _validStageNum = stageCurInfo.stageNum + 1;
                    }
				}

                //Debug.Log(string.Format("curStageIndex : {0}", curStageIndex));
                if(curStageIndex + 1 >= 10 && curStageIndex + 1 < 100) 
                {
                    RectTransform pieceRectTrans = circleGroupInfo.StagePartInfos[j].StageNumRootObj.GetComponent<RectTransform>();
                    if (partIndex == 0)
                    {
                        pieceRectTrans.anchoredPosition = new Vector2(150f, -20f);
                    } 
                    else if(partIndex == 1)
                    {
                        pieceRectTrans.anchoredPosition = new Vector2(140f, -15f);
                    }
                    else if (partIndex == 2)
                    {
                        pieceRectTrans.anchoredPosition = new Vector2(140f, 0f);
                    }
                    else if (partIndex == 3)
                    {
                        pieceRectTrans.anchoredPosition = new Vector2(140f, 15f);
                    }
                    else if (partIndex == 4)
                    {
                        pieceRectTrans.anchoredPosition = new Vector2(158f, 20f);
                    }
                }

                GameObject stageNumObj = _stagePiceNumGroup.GetStageNumObject (curStageIndex + 1);
				stageNumObj.transform.SetParent (circleGroupInfo.StagePartInfos [j].StageNumRootObj.transform);
				stageNumObj.transform.localScale = Vector3.one;
				stageNumObj.transform.localPosition = Vector3.zero;
				circleGroupInfo.StagePartInfos [j].InitStagePartInfo ();

				if (_validStageNum >= curStageIndex + 1) {
					lastPartIndex = j;
					if (_lastAccessGroupID < i)
						_lastAccessGroupID = i;
                    Debug.Log(string.Format("_validStageNum : {0}, curStageIndex + 1: {1}", _validStageNum, curStageIndex + 1));
					circleGroupInfo.StagePartInfos [j].CurState = StageSceneDefinitions.StagePieceState.OpenStage;
				} else {
					circleGroupInfo.StagePartInfos [j].CurState = StageSceneDefinitions.StagePieceState.LockStage;
				}

                if(_curSceneStep != StageSceneDefinitions.StageSceneStep.Normal){
                    circleGroupInfo.StagePartInfos[j].DisableButton();
                }

                circleGroupInfo.StagePartInfos[j].SelectButton.enabled = false;
            }

			if(lastPartIndex != -1)
				circleGroupInfo.SetLastPart (lastPartIndex);
			_stageCircleGroupInfos.Add (i, circleGroupInfo);
		}

        SetBoundaryGroupInfo();
    }

    void SetAlbumOpenData(int stageNum)
    {
        StopTimeStageInfo timeStageInfo = GamePlanDataManager.Instance.StopTimeStagePlan.StopTimeStageInfos[stageNum];
        for(int i = 0;i< timeStageInfo.albumOpens.Count; i++)
        {
            string[] split = timeStageInfo.albumOpens[i].Split('-');
            int page = int.Parse(split[0]);
            int albumNum = int.Parse(split[1]);
            CAGameGlobal.Instance.DynamicData.AlbumData.AddOpenAlbum(page, albumNum);
        }
    }

    void SetBoundaryGroupInfo()
    {
        _isAllAccess = false;
        CircleFocusDirection lastFocusDir = CircleFocusDirection.CircleLeft;
        int lastClearIndex = 0;
        for (int i = 0; i < _stageCircleGroupInfos.Count; i++) {
            if (i == 0) {
                //if (_stageCircleGroupInfos[i].CheckCurCircleHalfAllClear(CircleFocusDirection.CircleLeft)) {
                //    lastFocusDir = CircleFocusDirection.CircleRight;
                //}

                if(CheckValidStarCount(_stageCircleGroupInfos[i], CircleFocusDirection.CircleLeft)) {
                    lastFocusDir = CircleFocusDirection.CircleRight;
                }
            } else {
                //if (_stageCircleGroupInfos[i - 1].CheckCurCircleHalfAllClear(CircleFocusDirection.CircleRight)) {
                if (CheckValidStarCount(_stageCircleGroupInfos[i - 1], CircleFocusDirection.CircleRight)) {
                    lastClearIndex = i;
                    //if (_stageCircleGroupInfos[i].CheckCurCircleHalfAllClear(CircleFocusDirection.CircleLeft)) {
                    if (CheckValidStarCount(_stageCircleGroupInfos[i], CircleFocusDirection.CircleLeft)) {
                        if (i == _stageCircleGroupInfos.Count - 1) {
                            //if (_stageCircleGroupInfos[i].CheckCurCircleHalfAllClear(CircleFocusDirection.CircleRight)) {
                            if (CheckValidStarCount(_stageCircleGroupInfos[i], CircleFocusDirection.CircleRight)) {
                                _isAllAccess = true;
                            }
                        } else {
                            lastFocusDir = CircleFocusDirection.CircleRight;
                        }
                    } else {
                        lastFocusDir = CircleFocusDirection.CircleLeft;
                        break;
                    }
                }
            }
        }

        _firstCircleGroup.CircleFocus = CircleFocusDirection.CircleLeft;
        _firstCircleGroup.CircleGroupInfo = _stageCircleGroupInfos[0];

        _lastCircleGroup.CircleFocus = lastFocusDir;
        _lastCircleGroup.CircleGroupInfo = _stageCircleGroupInfos[lastClearIndex];
        //Debug.Log(string.Format("SetBoundaryGroupInfo _lastCircleGroup GroupID : {0}, CircleFocus : {1}", _lastCircleGroup.CircleGroupInfo.GroupID, _lastCircleGroup.CircleFocus));
    }

	void RefreshCircleGroupPos()
	{
		float curAngle = _curStartAngle;
		for (int i = 0; i < _groupTotalCount; i++) {
            _stageCircleGroupInfos[i].SaveAnchoredPosition = CAMathCtl.Instance.GetCircleDetailLinePos(_circleCenterPos, _radiusValue, curAngle);

            if (_stageCircleGroupInfos [i].SaveAnchoredPosition.x >= -320f && 
				_stageCircleGroupInfos [i].SaveAnchoredPosition.x <= 320f &&
				_stageCircleGroupInfos [i].SaveAnchoredPosition.y >= -150f) {
				if (_curGroupInfo != _stageCircleGroupInfos [i] 
					&& _stageCircleGroupInfos [i].GroupID <= _lastAccessGroupID) {
					SetCurCircleInfo(_stageCircleGroupInfos [i]);
				}

                _stageCircleGroupInfos[i].GroupRectTrans.anchoredPosition = _stageCircleGroupInfos[i].SaveAnchoredPosition;
            } else if (_stageCircleGroupInfos[i].SaveAnchoredPosition.x >= -1100f &&
                _stageCircleGroupInfos[i].SaveAnchoredPosition.x <= 1100f &&
                _stageCircleGroupInfos[i].SaveAnchoredPosition.y >= -500f) {

                _stageCircleGroupInfos[i].GroupRectTrans.anchoredPosition = _stageCircleGroupInfos[i].SaveAnchoredPosition;
            }

			if (curAngle - _gapAngle < 0) {
				curAngle += 360f;
			}
			curAngle -= _gapAngle;
		}
	}

	public override void OnTouchPress(int touchID, float posX, float posY)
	{

        if(_curSceneStep == StageSceneDefinitions.StageSceneStep.EpisodeImg)
        {
            _evenTimer.CompleteTimerID(_episodeTimerID);
            return;
        }

        if(_commonPopupController.IsEnablePopup)
            return;

        if (!IsEnableTouchArea(posX, posY)) {
			return;
		}

        if (_curSceneStep != StageSceneDefinitions.StageSceneStep.Normal)
            return;

		if (_curTouchID == -1) {
			_curTouchID = touchID;
			_listMoveAniManager.ReleaseAniData ();

            _touchPieceIndex = GetTouchedPieceIndex(touchID, posX, posY);
        }
    }

    int GetTouchedPieceIndex(int touchID, float posX, float posY)
    {
        if(_curGroupInfo == null)
            return -1;

        int retValue = -1;

        float circleGroupScrPosX = CAResolutionCtl.Instance.GetImageToScreenPosX(_curGroupInfo.GroupRectTrans.transform.localPosition.x);
        float circleGroupScrPosY = CAResolutionCtl.Instance.GetImageToScreenPosY(_curGroupInfo.GroupRectTrans.transform.localPosition.y);

        float touchDis = CAMathCtl.Instance.GetPointDistance(circleGroupScrPosX, circleGroupScrPosY, posX, posY);

        if(touchDis <= _circleRadius) {

            float calcAngle =  (float)CAMathCtl.Instance.GetAnglePoint(circleGroupScrPosX, circleGroupScrPosY, posX, posY);

            if (calcAngle < 0f) {
                calcAngle = 360f + calcAngle;
            }

            //Debug.Log(string.Format("GetTouchedPieceIndex calcAngle : {0}", calcAngle));

            float gapAngle = 36f;
            float startAngle = 90f;
            float endAngle = startAngle + gapAngle;
            int pieceCount = _curGroupInfo.StagePartInfos.Length;
            for(int i = 0; i < pieceCount; i++) {
                bool isCheckZeroAngle = false;
                if(Mathf.Abs(360f - startAngle) <= ((gapAngle * 0.5f) + 1f))
                    isCheckZeroAngle = true;
                if((calcAngle >= startAngle && calcAngle < endAngle) ||
                    (isCheckZeroAngle && calcAngle >= startAngle && calcAngle <= 360f) ||
                     (isCheckZeroAngle && calcAngle >= 0f && calcAngle <= gapAngle * 0.5f)) {
                    //retValue = i;
                    if(_curGroupInfo.StagePartInfos[i].CurState == StageSceneDefinitions.StagePieceState.OpenStage &&
                        _curGroupInfo.StagePartInfos[i].IsFocus) {
                        retValue = i;
                    }
                    break;
                }

                startAngle = endAngle;
                if(endAngle + gapAngle > 360f) {
                    endAngle = (endAngle + gapAngle) - 360f;
                } else {
                    endAngle = endAngle + gapAngle;
                }
            }
        }

        return retValue;
    }

	public override void OnFirstTouchDrag(int touchID, float posX, float posY)
	{
		if (_curTouchID == -1)
			return;
		
		float moveX = 0f, moveY = 0f;

		if (touchValues [0].m_TouchID != -1) {
			moveX = touchValues [0].m_X - touchValues [0].m_PreX;
			moveY = touchValues [0].m_Y - touchValues [0].m_PreY;
		} else if (touchValues [1].m_TouchID != -1) {
			moveX = touchValues [1].m_X - touchValues [1].m_PreX;
			moveY = touchValues [1].m_Y - touchValues [1].m_PreY;
		}

		if (moveX > 0f) {
			_moveDirectiong = StageListMoveDirection.Right;
		} else {
			_moveDirectiong = StageListMoveDirection.Left;
		}

		OnTouchMoveValues (touchID, moveX, moveY);

		_startTouchTime = Time.time;
		_startTouchValue = posX;
		//Debug.Log (string.Format ("OnFirstTouchDrag _startTouchTime : {0}", _startTouchTime));

		if (_curGroupInfo != null)
			_curGroupInfo.SetAllNotFocus ();

		if (_isTouchState)
			_isTouchState = false;
	}

	public override void OnTouchDrag(int touchID, float posX, float posY)
	{
		if (_curTouchID == -1)
			return;

		float moveX = 0f, moveY = 0f;

		if (touchValues [0].m_TouchID != -1) {
			moveX = touchValues [0].m_X - touchValues [0].m_PreX;
			moveY = touchValues [0].m_Y - touchValues [0].m_PreY;
		} else if (touchValues [1].m_TouchID != -1) {
			moveX = touchValues [1].m_X - touchValues [1].m_PreX;
			moveY = touchValues [1].m_Y - touchValues [1].m_PreY;
		}

		if (moveX == 0f) {
			_startTouchTime = Time.time;
			_startTouchValue = posX;
			_moveDirectiong = StageListMoveDirection.None;
		}

		if (_moveDirectiong == StageListMoveDirection.None) {
			if (moveX > 0f) {
				_moveDirectiong = StageListMoveDirection.Right;
			} else if (moveX < 0f) {
				_moveDirectiong = StageListMoveDirection.Left;
			}
			_startTouchTime = Time.time;
			_startTouchValue = posX;
		} else if (_moveDirectiong == StageListMoveDirection.Right) {
			if (moveX < 0f) {
				_startTouchTime = Time.time;
				_startTouchValue = posX;
				_moveDirectiong = StageListMoveDirection.Left;
			}
		} else if (_moveDirectiong == StageListMoveDirection.Left) {
			if (moveX > 0f) {
				_startTouchTime = Time.time;
				_startTouchValue = posX;
				_moveDirectiong = StageListMoveDirection.Right;
			}
		}

		OnTouchMoveValues (touchID, moveX, moveY);

		if (_isTouchState)
			_isTouchState = false;
	}

	void OnTouchMoveValues(int touchID, float moveX, float moveY)
	{
		if (_curTouchID != -1) {
			if (_curTouchID == touchID) {
				CalcCircleMoveValue(moveX);
                if (!_isMoveableState && !_isPopupNeedStar && !_isCircleLastBoundary) {
                    _isPopupNeedStar = true;
                    _commonPopupController.ShowPopup(CommonPopupType.NeedStar, null, null, OnNeedStarConfirm);
                }
                _touchPieceIndex = -1;
            }
		}
	}

    bool CheckValidBoundaryCircle(out StageCircleGroupInfo boundaryGroupInfo)
    {
        boundaryGroupInfo = null;
        bool retValue = true;
        if (!_isAllAccess) {
            if (_firstCircleGroup.CircleGroupInfo.transform.localPosition.x >= _rightHangPosX) {
                boundaryGroupInfo = _firstCircleGroup.CircleGroupInfo;
                _curHangDirection = StageListMoveDirection.Right;
                retValue = false;
            }

            if (retValue) {
                if (_lastCircleGroup.CircleFocus == CircleFocusDirection.CircleLeft) {
                    if (_lastCircleGroup.CircleGroupInfo.transform.localPosition.x <= _rightHangPosX) {
                        boundaryGroupInfo = _lastCircleGroup.CircleGroupInfo;
                        _curHangDirection = StageListMoveDirection.Right;
                        retValue = false;
                    }
                } else {
                    if (_lastCircleGroup.CircleGroupInfo.transform.localPosition.x <= _leftHangPosX) {
                        boundaryGroupInfo = _lastCircleGroup.CircleGroupInfo;
                        _curHangDirection = StageListMoveDirection.Left;
                        retValue = false;
                    }
                }
            }
        }

        return retValue;
    }

	void CalcCircleMoveValue(float moveX)
	{
		_isMoveableState = true;
        _isCircleLastBoundary = false;
        if (!_isAllAccess) {
            if (moveX > 0f) {
                if (_firstCircleGroup.CircleGroupInfo.transform.localPosition.x >= _rightHangPosX) {
                    if(!(_lastCircleGroup.CircleGroupInfo.GroupID == 11 && _lastCircleGroup.CircleFocus == CircleFocusDirection.CircleRight)) {
                        moveX *= 0.25f;
                        _isMoveableState = false;
                        _isCircleLastBoundary = true;
                    }
                }
            } else if (moveX < 0f) {
                if(_lastCircleGroup.CircleFocus == CircleFocusDirection.CircleLeft) {
                    if(_lastCircleGroup.CircleGroupInfo.transform.localPosition.x <= _rightHangPosX) {
                        moveX *= 0.25f;
                        _isMoveableState = false;
                    }
                } else {
                    if (_lastCircleGroup.CircleGroupInfo.transform.localPosition.x <= _leftHangPosX) {
                        moveX *= 0.25f;
                        _isMoveableState = false;
                        if (!_stageCircleGroupInfos.ContainsKey(_lastCircleGroup.CircleGroupInfo.GroupID + 1)) {
                            _isCircleLastBoundary = true;
                        }
                    }
                }
            }
        }

		if (!_isMoveableState) {
			if (_listMoveAniManager.ListMoveAniType == StageListMoveAniType.Acceleration) {
				_listMoveAniManager.ReleaseAniData ();
                OnFinishAccelerationListAni();
				return;
			}
		}

		float calcY = Mathf.Sqrt (_squareRadius - (moveX * moveX));
		float calcAngle = CAMathCtl.Instance.CalcAngle2D (0f, 0f, moveX, calcY);
		float moveAngle = calcAngle - 90f;
		_curStartAngle += moveAngle;
		//				Debug.Log (string.Format ("_curStartAngle : {0}", _curStartAngle));
		if (_curStartAngle < 0f) {
			_curStartAngle = 360f - _curStartAngle;
		} else if (_curStartAngle > 360f) {
			_curStartAngle = _curStartAngle - 360f;
		}
		RefreshCircleGroupPos ();
	}

	public override void OnTouchRelease(int touchID, float posX, float posY)
	{
		if (_curTouchID == touchID) {
			_curTouchID = -1;
			CalcMoveAccelerationList (posX);
			_startTouchTime = 0f;
			_startTouchValue = 0f;
            if(_curGroupInfo != null && _touchPieceIndex != -1) {
                if (_touchPieceIndex == GetTouchedPieceIndex(touchID, posX, posY)) {
                    OnClickPieceButton(_curGroupInfo.GroupID, _touchPieceIndex);
                }
            }
		}

		if (!_isTouchState)
			_isTouchState = true;
	}

	void CalcMoveAccelerationList(float posX)
	{
		if (_isMoveableState) {
			float gapTime = Time.time - _startTouchTime;
			float gapMove = posX - _startTouchValue;
			if (!_listMoveAniManager.AddAccelerationListMove (gapTime, gapMove)) {
				CalcMoveHangPos ();
			}
		} else {
			CalcMoveHangPos ();
		}
	}

	void InitCurCircleGroup()
	{
		int nearIndex = 0;
		float curMinDis = 10000f;
		for (int i = 0; i < _stageCircleGroupInfos.Count; i++) {
			StageCircleGroupInfo circleGroupInfo = _stageCircleGroupInfos [i];
			float calcDis = CAMathCtl.Instance.GetPointDistance (0f, 0f, circleGroupInfo.transform.localPosition.x,
				circleGroupInfo.transform.localPosition.y);

			if (calcDis < curMinDis) {
				nearIndex = i;
				curMinDis = calcDis;
			}
		}

		SetCurCircleInfo(_stageCircleGroupInfos [nearIndex]);
    }

    void CheckFocusStage()
    {
        if (CAGameGlobal.Instance.DynamicData.StageSceneState == GameDefinitions.StageSceneState.NextStage)
        {
            if (_validStageNum >= CAGameGlobal.Instance.CurrentStageNum)
            {
                SetCurSceneStep(StageSceneDefinitions.StageSceneStep.FocusStep);
                CAGameGlobal.Instance.DynamicData.StageSceneState = GameDefinitions.StageSceneState.None;
            }
            else{
                _commonPopupController.ShowPopup(CommonPopupType.NeedStar, null, null);
            }

        }
    }

    void CalcMoveHangPos(StageListMoveDirection focusMoveDirection = StageListMoveDirection.None)
	{

        StageCircleGroupInfo checkGroupInfo = null;
        if(CheckValidBoundaryCircle(out checkGroupInfo)) {
            _curHangDirection = StageListMoveDirection.Right;

            if (_curGroupInfo.transform.localPosition.x < 0f) {
                _curHangDirection = StageListMoveDirection.Left;
            }

            checkGroupInfo = _curGroupInfo;
        }

        //Debug.Log(string.Format("CalcMoveHangPos _isMoveableState : {0}, _curHangDirection : {1}", _isMoveableState, _curHangDirection));

        float moveX = 0f;
		switch (_curHangDirection) {
		case StageListMoveDirection.Left:
			{
				moveX = _leftHangPosX - checkGroupInfo.transform.localPosition.x;
			}
			break;
		case StageListMoveDirection.Right:
			{
				moveX = _rightHangPosX - checkGroupInfo.transform.localPosition.x;
			}
			break;
		}

		if (moveX != 0f) {
			_listMoveAniManager.AddHangMoveAniData (moveX);
		} else {
			SetCurrentFocusCircle ();
		}
	}

    bool CheckValidStarCount(StageCircleGroupInfo circleInfo, CircleFocusDirection circleFocusDir)
    {
        int curStarCount = 0;
        int needStarCount = 0;

        curStarCount = circleInfo.GetCurCircleHalfStarCount(circleFocusDir);
        needStarCount = GetStarNeedCount(circleInfo.GetCurFirstStageIndex(circleFocusDir));

        if (curStarCount >= needStarCount && circleInfo.CheckCurCircleHalfAllClear(circleFocusDir))
            return true;

        return false;
    }

    bool CheckValidOnlyStarCount(StageCircleGroupInfo circleInfo, CircleFocusDirection circleFocusDir)
    {
        int curStarCount = 0;
        int needStarCount = 0;

        curStarCount = circleInfo.GetCurCircleHalfStarCount(circleFocusDir);
        needStarCount = GetStarNeedCount(circleInfo.GetCurFirstStageIndex(circleFocusDir));

        if (curStarCount >= needStarCount)
            return true;

        return false;
    }

    void UpdateListAni()
	{
		float moveX = 0f;

		switch (_listMoveAniManager.ListMoveAniType) {
		case StageListMoveAniType.Hang:
			moveX = _listMoveAniManager.UpdateHangListMoveAni ();
			break;
		case StageListMoveAniType.Acceleration:
			moveX = _listMoveAniManager.UpdateAccelerationListMoveAni ();
			break;
		}

		if (moveX != 0f) {
			CalcCircleMoveValue(moveX);
		}
	}

	void SetCurrentFocusCircle()
	{
		if (_curGroupInfo != null) {

            switch (_curHangDirection) {

			case StageListMoveDirection.Left:
				{
					_curGroupInfo.FocusDirection = CircleFocusDirection.CircleRight;
				}
				break;
			case StageListMoveDirection.Right:
				{
					_curGroupInfo.FocusDirection = CircleFocusDirection.CircleLeft;
				}
				break;
			}

            if (_curSceneStep != StageSceneDefinitions.StageSceneStep.Normal)
            {
                for (int i = 0; i < _curGroupInfo.StagePartInfos.Length; i++){
                    _curGroupInfo.StagePartInfos[i].DisableButton();
                }
            }

            //int maxStarCount = _curGroupInfo.GetCurCircleHalfMaxStarCount(_curGroupInfo.FocusDirection);
            int curStarCount = _curGroupInfo.GetCurCircleHalfStarCount(_curGroupInfo.FocusDirection);
            int needStarCount = GetStarNeedCount(_curGroupInfo.GetCurFirstStageIndex(_curGroupInfo.FocusDirection));

            _curStarImgNumFont.SetNumberStr(curStarCount.ToString());
            _goalStarImgNumFont.SetNumberStr(needStarCount.ToString());

            if (curStarCount >= needStarCount) {
                _curStarImgNumFont.SetNumImageColor(_starMaxColor);
            } else {
                _curStarImgNumFont.SetNumImageColor(_starLackColor);
            }
        }
	}

	bool IsEnableTouchArea(float posX, float posY)
	{
		//Debug.Log (string.Format ("IsEnableTouchArea posX : {0}, posY : {1}", posX, posY));
		if (posY <= 917f)
			return true;
		
		return false;
	}

//	bool IsAccessDirectionCircle(int groupID)
//	{
////		StageCircleGroupInfo circleGroup = _stageCircleGroupInfos [groupID];
//		int nextGroupID = 0;
//		switch (_moveDirectiong) {
//		case StageListMoveDirection.Left:
//			if (groupID == 0)
//				nextGroupID = _groupTotalCount - 1;
//			break;
//		case StageListMoveDirection.Right:
//			if (groupID >= _groupTotalCount)
//				nextGroupID = 0;
//			break;
//		}
//
//		_isNextAccessState = false;
//		if (nextGroupID >= _lastAccessGroupID) {
//			_isNextAccessState = true;
//		}
//
////		for (int i = 0; i < circleGroup.StagePartInfos.Length; i++) {
////			if (circleGroup.StagePartInfos [i].CurState == SelectStageDefinitions.StagePieceState.OpenStage)
////				return true;
////		}
//
//		return _isNextAccessState;
//	}

    void SetCurSceneStep(StageSceneDefinitions.StageSceneStep sceneStep)
    {
        _curSceneStep = sceneStep;
        switch(sceneStep){
            case StageSceneDefinitions.StageSceneStep.PrologueStoryStep:
                SetPrologueStory();
                break;
            case StageSceneDefinitions.StageSceneStep.FocusStep:
                _evenTimer.SetGameTimerData(OnDelayFocus, 0.2f);
                //SetFocusStage();
                break;
            case StageSceneDefinitions.StageSceneStep.AutoStaryDelay:
                _evenTimer.SetGameTimerData((x) => OnAutoStart(0, 0), 0.7f);
                break;
            case StageSceneDefinitions.StageSceneStep.AutoStart:
                break;
        }
    }

    void SetAutoStart(int pieceIndex)
    {
        _curSceneStep = StageSceneDefinitions.StageSceneStep.AutoStart;
        _curGroupInfo.StagePartInfos[pieceIndex].SetEnableFocusTween(true);
        _evenTimer.SetGameTimerData(OnDelayAutoStart, 1f, pieceIndex);
    }

    public void ExecuteBackKey()
    {
        _commonPopupController.ShowPopup(CommonPopupType.ApplicationQuit, OnYesApplicationQuit, OnNoApplicationQuit);
        if(BackKeyManager.Instance != null)
            BackKeyManager.Instance.IsEnableBackKey = false;
    }

    #endregion

    #region Call Back Methods

    void OnClickPieceButton(int groupID, int pieceIndex)
	{
		if (!_isTouchState)
			return;
		
		int stageIndex = (groupID * _groupPerStage) + (pieceIndex);

        CAGameSaveDataManager.Instance.StageProgressSave.SetStartStartAngle(_curStartAngle);

        CAGameGlobal.Instance.IsStageHint = false;
        CAGameGlobal.Instance.CurrentStageNum = stageIndex + 1;
		CAGameGlobal.Instance.LoadGameScene(GameSceneEnum.SCENE_GAMEPLAYING, SceneChangeState.SCENE_CHANGE_DIRECT);
	}

	void OnFinishHangListAni()
	{
        if (_curSceneStep == StageSceneDefinitions.StageSceneStep.AutoNextFocus)
        {
            if(_curHangDirection == StageListMoveDirection.Left)
            {
                _curHangDirection = StageListMoveDirection.Right;
            }
            else
            {
                _curHangDirection = StageListMoveDirection.Left;
            }
            SetCurrentFocusCircle();
            int stageIndex = CAGameGlobal.Instance.CurrentStageNum - 1;
            int pieceIndex = stageIndex - (_curGroupInfo.GroupID * _groupPerStage);

            if (pieceIndex >= 10)
            {
                pieceIndex = 0;
                if (_stageCircleGroupInfos.ContainsKey(_curGroupInfo.GroupID + 1))
                {
                    _curGroupInfo = _stageCircleGroupInfos[_curGroupInfo.GroupID + 1];
                }
            }
            SetAutoStart(pieceIndex);
        }
        else
        {
            SetCurrentFocusCircle();
        }

    }

	void OnFinishAccelerationListAni()
	{
		CalcMoveHangPos ();
	}

	public void OnPreviousButton()
	{
		CalcMoveHangPos (StageListMoveDirection.Left);
	}

	public void OnNextButton()
	{
		CalcMoveHangPos (StageListMoveDirection.Right);
	}

	public void OnClearLastStage()
	{
		int quotient = _validStageNum / _groupPerStage;
		int partIndex = _validStageNum % _groupPerStage;

        int starCount = 3;

		CAGameSaveDataManager.Instance.StageProgressSave.SetStageStarCount (_validStageNum, starCount);

        if(partIndex == 0 && quotient > 0) {
            StageCircleGroupInfo preGroupInfo = _stageCircleGroupInfos[quotient - 1];
            preGroupInfo.StagePartInfos[preGroupInfo.StagePartInfos.Length - 1].SetStageStarCount(starCount);
        }

		StageCircleGroupInfo circleGroupInfo = _stageCircleGroupInfos [quotient];
		circleGroupInfo.StagePartInfos [partIndex].CurState = StageSceneDefinitions.StagePieceState.OpenStage;
        //circleGroupInfo.StagePartInfos[partIndex].IsFocus = true;

        circleGroupInfo.SetCheatLastPart (partIndex, starCount);

        _lastAccessGroupID = quotient;

        _validStageNum++;

        SetBoundaryGroupInfo();
	}

	public void OnInitGame()
	{
		CAGameGlobal.Instance.CurrentStageNum = 0;
		CAGameGlobal.Instance.LoadGameScene(GameSceneEnum.SCENE_LOGO, SceneChangeState.SCENE_CHANGE_DIRECT);

        CAGameSaveDataManager.Instance.ResetAllSaveData();
    }

	public void OnInfiniteMode()
	{
        StageCurrentInfo stageCurInfo = CAGameSaveDataManager.Instance.StageProgressSave.GetStageCurrentInfo(5);
        if(stageCurInfo.starCount == 0)
        {
            _commonPopupController.ShowPopup(CommonPopupType.InfiniteMode_1, null, null);
            return;
        }

        SoundManager.Instance.PlayEffectFX(SoundDefinitions.effectSound_2);
        CAGameGlobal.Instance.LoadGameScene(GameSceneEnum.SCENE_INFINITE_MODE, SceneChangeState.SCENE_CHANGE_DIRECT);
	}

    public void OnInfiniteTheme()
    {
        StageCurrentInfo stageCurInfo = CAGameSaveDataManager.Instance.StageProgressSave.GetStageCurrentInfo(5);
        if (stageCurInfo.starCount == 0)
        {
            _commonPopupController.ShowPopup(CommonPopupType.InfiniteMode_1, null, null);
            return;
        }

        SoundManager.Instance.PlayEffectFX(SoundDefinitions.effectSound_2);
        CAGameGlobal.Instance.LoadGameScene(GameSceneEnum.SCENE_INFINITE_THEME, SceneChangeState.SCENE_CHANGE_DIRECT);
    }

    public void OnSetting()
    {
        SoundManager.Instance.PlayEffectFX(SoundDefinitions.effectSound_2);
        _stageListRootObj.SetActive(false);
        _optionPopupManager.ShowOption();

        _isUpdate = false;
        curTouchState = false;
    }

    public void OnCloseOptionPopup()
    {
        SoundManager.Instance.PlayEffectFX(SoundDefinitions.effectSound_2);
        _stageListRootObj.SetActive(true);
        _optionPopupManager.CloseOption();

        _isUpdate = true;
        curTouchState = true;
    }

    public void OnAlbum()
    {
        SoundManager.Instance.PlayEffectFX(SoundDefinitions.effectSound_2);
        CAGameGlobal.Instance.LoadGameScene(GameSceneEnum.SCENE_ALBUM, SceneChangeState.SCENE_CHANGE_DIRECT);
    }

    void OnStoryAniTrigger(int storyIndex)
    {
        if(storyIndex == (int)StoryDefinitions.StoryIndex.Prologue){
            _storyPrologueAniManager.gameObject.SetActive(false);
            if (!CAGameSaveDataManager.Instance.StoryProgressSave.GetStoryProgressState((int)StoryDefinitions.StoryIndex.Prologue))
            {
                CAGameSaveDataManager.Instance.StoryProgressSave.SetStoryProgressState((int)StoryDefinitions.StoryIndex.Prologue, true);
                SetCurSceneStep(StageSceneDefinitions.StageSceneStep.AutoStaryDelay);
            }
            else
            {
                _stageListRootObj.SetActive(true);
                _isUpdate = true;
                curTouchState = true;
                SetCurSceneStep(StageSceneDefinitions.StageSceneStep.Normal);
            }

        }

    }

    void OnAutoStart(int groupID, int pieceIndex)
    {
        //OnClickPieceButton(groupID, pieceIndex);
        OnStartTutorialStage();
    }

    void OnDelayAutoStart(object objData)
    {
        int pieceIndex = (int)objData;
        _curGroupInfo.StagePartInfos[pieceIndex].SetEnableFocusTween(false);

        CAGameGlobal.Instance.IsStageHint = false;
        CAGameSaveDataManager.Instance.StageProgressSave.SetStartStartAngle(_curStartAngle);
        CAGameGlobal.Instance.LoadGameScene(GameSceneEnum.SCENE_GAMEPLAYING, SceneChangeState.SCENE_CHANGE_DIRECT);
    }

    void OnDelayFocus(object objData)
    {
        SetFocusStage();
    }

    void OnDelayEpisodeImg(object objData)
    {
        int pieceIndex = (int)objData;

        GameObject episodeImg = _episodeManager.GetEpisodeImg(CAGameGlobal.Instance.CurrentStageNum - 1);
        _episodeManager.EpisodeImgBG.SetActive(false);
        episodeImg.SetActive(false);

        if (_curGroupInfo.GroupID == 0 && pieceIndex == 1)
        {
           
            SetAutoStart(pieceIndex);
        }
        else if(CAGameGlobal.Instance.CurrentStageNum < 30)
        {
            _curSceneStep = StageSceneDefinitions.StageSceneStep.AutoNextFocus;
            float moveX = Mathf.Abs(_leftHangPosX - _rightHangPosX);
            _listMoveAniManager.AddHangMoveAniData(-moveX);
        }
        else
        {
            _curSceneStep = StageSceneDefinitions.StageSceneStep.Normal;
        }

        //CAGameSaveDataManager.Instance.StageProgressSave.SetStartStartAngle(_curStartAngle);
        //CAGameGlobal.Instance.LoadGameScene(GameSceneEnum.SCENE_GAMEPLAYING, SceneChangeState.SCENE_CHANGE_DIRECT);
    }

    void OnStartTutorialStage()
    {
        if (!_isTouchState)
            return;

        int stageIndex = 0;

        CAGameSaveDataManager.Instance.StageProgressSave.SetStartStartAngle(_curStartAngle);

        CAGameGlobal.Instance.IsStageHint = false;
        CAGameGlobal.Instance.CurrentStageNum = stageIndex;
        CAGameGlobal.Instance.LoadGameScene(GameSceneEnum.SCENE_GAMEPLAYING, SceneChangeState.SCENE_CHANGE_DIRECT);
    }

    void OnRateButton()
    {
        Application.OpenURL("http://www.roofzoo.com/playstore_dir.html");
    }

    void OnTutorialButton()
    {
        OnStartTutorialStage();
    }

    void OnOpeningButton()
    {
        _optionPopupManager.CloseOption();
        SetCurSceneStep(StageSceneDefinitions.StageSceneStep.PrologueStoryStep);
    }

    void OnFacebookShareButton()
    {
        Application.OpenURL("https://www.facebook.com/roofzoogame");
    }

    void OnContactButton()
    {
        Application.OpenURL("http://www.roofzoo.com/");
    }

    void OnNeedStarConfirm()
    {
        _isPopupNeedStar = false;
    }

    void OnYesApplicationQuit()
    {
        if (BackKeyManager.Instance != null)
            BackKeyManager.Instance.IsEnableBackKey = true;
        Application.Quit();
    }

    void OnNoApplicationQuit()
    {
        if (BackKeyManager.Instance != null)
            BackKeyManager.Instance.IsEnableBackKey = true;
    }

    #endregion
}
