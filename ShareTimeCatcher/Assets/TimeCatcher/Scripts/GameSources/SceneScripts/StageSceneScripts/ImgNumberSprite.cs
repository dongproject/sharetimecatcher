﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ImgNumberSprite : MonoBehaviour
{
    #region Serialize Variables

#pragma warning disable 649

    [SerializeField] Sprite[] _imgNumSprites;

#pragma warning restore 649

    #endregion

    #region Properties

    public Sprite[] ImgNumSprites
    {
        get{ return _imgNumSprites; }
    }

    #endregion
}
