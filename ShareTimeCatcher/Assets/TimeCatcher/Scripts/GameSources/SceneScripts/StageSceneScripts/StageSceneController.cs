﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StageSelectPieceInfo
{
	#region Variables

	int _stageNum;
	float _startAngle;
	float _endAngle;
	StageObjectInfo _stageInfo;

	#endregion

	#region Properties

	public int StageNum
	{
		get{ return _stageNum; }
		set{ _stageNum = value; }
	}

	public float StartAngle
	{
		get{ return _startAngle; }
		set{ _startAngle = value; }
	}

	public float EndAngle
	{
		get{ return _endAngle; }
		set{ _endAngle = value; }
	}

	public StageObjectInfo StageInfo
	{
		get{ return _stageInfo; }
		set{ _stageInfo = value; }
	}

	#endregion
}

public class StageSceneController : CACommonMonoBehaviours 
{
	#region Variables

	[SerializeField] StageCircleObjInfo _circleObjInfo;

	List<StageSelectPieceInfo> _stagePieceInfos = new List<StageSelectPieceInfo>();

	float _radius = 296f;
	float _circlePosX = 0f;
	float _circlePosY = 0f;
	int _stagePieceCount = 10;
	int _validStageNum = 1;

	#endregion

	#region Properties

	List<StageSelectPieceInfo> StagePieceInfos
	{
		get{ return _stagePieceInfos; }
	}

	#endregion

	#region MoneBehaviour Methods

	public override void Awake()
	{
		base.Awake ();
		curTouchState = true;
	}

	// Use this for initialization
	public override void Start ()
	{
		base.Start ();

		InitGroupStagePieces (1);
	//	double degree = CAMathCtl.Instance.GetAnglePoint (0f, 0f, -1f, 1f);
	//	Debug.Log (string.Format ("degree : {0}", degree));
	}

	public override void OnDestroy()
	{
		base.OnDestroy ();
		curTouchState = false;
	}

	#endregion

	#region Touch Handler

	public override CACurGamePlayTouchData OnGetTouchData(int touchID, float posX, float posY, bool isTouch)
	{
		Debug.Log (string.Format ("OnGetTouchData x : {0}, y : {1}", posX, posY));

		StageSelectPieceInfo selectPieceInfo = CheckTouchSelectPiece (posX, posY);
		if (selectPieceInfo != null) {
			Debug.Log (string.Format ("OnGetTouchData selectPieceInfo StageNum : {0}", selectPieceInfo.StageNum));
		
			return new CACurGamePlayTouchData (CAGamePlayTouchType.StageSelect, selectPieceInfo);
		}

		return null;
	}

	StageSelectPieceInfo CheckTouchSelectPiece(float posX, float posY)
	{
		float imgPosX = posX - CAResolutionCtl.Instance.GetResolutionHalfWidth ();
		float imgPosY = posY - CAResolutionCtl.Instance.GetResolutionHalfHeight ();

		float touchDistance = CAMathCtl.Instance.GetPointDistance (_circlePosX, _circlePosY, imgPosX, imgPosY);

		if (touchDistance < _radius) {
			float degree = (float)CAMathCtl.Instance.GetAnglePoint (_circlePosX, _circlePosY, imgPosX, imgPosY);
//			Debug.Log (string.Format ("imgPosX : {0}, imgPosY: {1}, touchDistance: {2}, degree : {3}", imgPosX, imgPosY, touchDistance, degree));
			if (degree < 0f) {
				degree = 360f + degree;
			}

			for (int i = 0; i < _stagePieceCount; i++) {
				if (degree > _stagePieceInfos [i].StartAngle && degree < _stagePieceInfos [i].EndAngle) {
					return _stagePieceInfos [i];
				}
			}
		}

		return null;
	}

	public override void OnMultiTouchPosSet(float posX, float posY){}
	public override void OnTouchDrag(int touchID, float posX, float posY){}
	public override void OnTouchScale(){}

	public override void OnTouchEvent(CACurGamePlayTouchData playTouchData, float posX, float posY)
	{
		if (playTouchData != null) {
			if (playTouchData.playTouchType == CAGamePlayTouchType.StageSelect) {
				StageSelectPieceInfo selectPieceInfo = playTouchData.curTouchObject as StageSelectPieceInfo;
				if (selectPieceInfo != null) {
					if (_validStageNum >= selectPieceInfo.StageNum) {
						CAGameGlobal.Instance.CurrentStageNum = selectPieceInfo.StageNum;
						CAGameGlobal.Instance.LoadGameScene(GameSceneEnum.SCENE_GAMEPLAYING, SceneChangeState.SCENE_CHANGE_DIRECT);
					}
				}
			}
		}
	}

	public override void OnTouchRelease(int touchID, float posX, float posY)
	{
		
	}

	#endregion

	#region Methods

	void InitGroupStagePieces(int groupNum)
	{
		float startAngle = 72f;
		_validStageNum = 1;
		for (int i = 0; i < _stagePieceCount; i++) {
			StageCurrentInfo stageCurInfo = CAGameSaveDataManager.Instance.StageProgressSave.GetStageCurrentInfo (i + 1);
			StageObjectInfo stageObjInfo = _circleObjInfo.StageObjectInfos [i];
			stageObjInfo.StarCount = stageCurInfo.starCount;
			if (stageCurInfo.starCount > 0) {
				_validStageNum = stageCurInfo.stageNum + 1;
			}
			StageSelectPieceInfo inputPieceInfo = new StageSelectPieceInfo ();
			inputPieceInfo.StageNum = stageCurInfo.stageNum;
			inputPieceInfo.StageInfo = stageObjInfo;
			float calcAngle = startAngle;
			if (startAngle < 0f) {
				calcAngle = 360f + startAngle;
			}
			inputPieceInfo.StartAngle = calcAngle;
			inputPieceInfo.EndAngle = calcAngle + 36f;

			if (_validStageNum >= inputPieceInfo.StageNum) {
				stageObjInfo.SelectStageInfo.gameObject.SetActive (true);
			} else {
				stageObjInfo.SelectStageInfo.gameObject.SetActive (false);
			}

			//Debug.Log (string.Format ("inputPieceInfo.StartAngle : {0}, inputPieceInfo.EndAngle : {1}", inputPieceInfo.StartAngle, inputPieceInfo.EndAngle));
			_stagePieceInfos.Add (inputPieceInfo);
			startAngle -= 36f;
		}
	}

	#endregion

	#region Callback methods

	public void OnClickReset()
	{
        //CAGameSaveDataManager.Instance.ResetAllSaveData();

        InitGroupStagePieces (1);
	}

	#endregion
}
