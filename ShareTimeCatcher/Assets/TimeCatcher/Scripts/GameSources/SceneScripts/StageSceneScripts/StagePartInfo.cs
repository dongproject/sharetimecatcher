﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StagePartInfo : MonoBehaviour 
{
    #region Serialize Variables

#pragma warning disable 649

    [SerializeField] Image _backImage;
	[SerializeField] GameObject _stageLineObj;
	[SerializeField] GameObject _stageSelectObj;
	[SerializeField] Image[] _starImages;
	[SerializeField] Image[] _stageNumImages;
	[SerializeField] Image[] _stageNumBackImages;

	[SerializeField] GameObject _stageNumRootObj;

	[SerializeField] Button _selectButton;
    [SerializeField] UGUITweener _focusTweener;

#pragma warning restore 649

    #endregion

    #region Variables

    StageSceneDefinitions.StagePieceState _curState;
	Color _openStageColor = new Color (8f/255f, 186f/255f, 5f/255f, 1f);
	Color _lockStageColor = new Color (18f/255f, 66f/255f, 17f/255f, 1f);
	int _starCount;
	bool _isFocus;

	#endregion

	#region Properties

	public StageSceneDefinitions.StagePieceState CurState
	{
		get{ return _curState; }
		set{ 
			_curState = value;
			switch (_curState) {
			case StageSceneDefinitions.StagePieceState.OpenStage:
				if (_isFocus) {
					_backImage.color = _openStageColor;
					EnableButton ();
				} else {
					_backImage.color = _lockStageColor;
					DisableButton ();
				}
				_stageLineObj.SetActive (true);
				break;
			case StageSceneDefinitions.StagePieceState.LockStage:
				DisableButton ();
				_backImage.color = _lockStageColor;
				_stageLineObj.SetActive (true);
				for (int i = 0; i < _starImages.Length; i++) {
					_starImages [i].gameObject.SetActive (false);
				}
				break;
			}
		}
	}

	public Button SelectButton
	{
		get{ return _selectButton; }
	}

	public GameObject StageNumRootObj
	{
		get{ return _stageNumRootObj; }
	}

	public int StarCount
	{
		get{ return _starCount; }
	}

	public bool IsFocus
	{
		get{ return _isFocus; }
		set{ 
			_isFocus = value;
			if (_isFocus) {
				if (_curState == StageSceneDefinitions.StagePieceState.OpenStage) {
					_backImage.color = _openStageColor;
					EnableButton ();
				}
			} else {
				_backImage.color = _lockStageColor;
				DisableButton ();
			}
		}
	}

	public GameObject StageSelectObj
	{
		get{ return _stageSelectObj; }
	}

	#endregion

	#region Methods

	public void InitStagePartInfo()
	{
		for (int i = 0; i < _stageNumBackImages.Length; i++) {
			_stageNumBackImages [i].gameObject.SetActive (false);
		}
	}

	public void SetStageStarCount(int starCountValue)
	{
		_starCount = starCountValue;
		for (int i = 0; i < _starImages.Length; i++) {
			if (_starCount > i) {
				_starImages [i].gameObject.SetActive (true);
			} else {
				_starImages [i].gameObject.SetActive (false);
			}
		}
	}

	public void EnableButton()
	{
		//_selectButton.enabled = true;
	}

	public void DisableButton()
	{
		//_selectButton.enabled = false;
	}

    public void SetEnableFocusTween(bool isEnable)
    {
        _focusTweener.enabled = isEnable;
    }

	#endregion
}
