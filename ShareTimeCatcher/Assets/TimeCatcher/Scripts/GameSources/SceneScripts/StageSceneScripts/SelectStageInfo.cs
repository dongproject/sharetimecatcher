﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelectStageInfo : MonoBehaviour
{
	#region Serialize Variables

	[SerializeField] tk2dSprite[] _stageNumSprites;

	[SerializeField] GameObject[] _starBGObjs;
	[SerializeField] GameObject[] _starImgObjs;

	#endregion

	#region Properties

	public tk2dSprite[] StageNumSprites
	{
		get{ return _stageNumSprites; }
	}

	public GameObject[] StarBGObjs
	{
		get{ return _starBGObjs; }
	}

	public GameObject[] StarImgObjs
	{
		get{ return _starImgObjs; }
	}

	#endregion
}
