﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EpisodeImgManager : MonoBehaviour 
{
    #region Serialize Variables

#pragma warning disable 649

    [SerializeField] GameObject _episodeImgBG;
    [SerializeField] GameObject[] _storyImgs;

#pragma warning restore 649

    #endregion

    #region Variables

    Dictionary<int /* stage Num */, GameObject> _storyTriggerImgs = new Dictionary<int, GameObject>();

    #endregion

    #region Properties

    public GameObject EpisodeImgBG
    {
        get { return _episodeImgBG; }
    }

    #endregion

    #region MonoBehaviour Methods

    private void Awake()
    {
        for(int i = 0;i< _storyImgs.Length;i++)
        {
            string objName = _storyImgs[i].name;
            int stageNum = int.Parse(objName.Split('_')[1]);
            _storyTriggerImgs.Add(stageNum, _storyImgs[i]);
        }
    }

    #endregion

    #region Methods

    public GameObject GetEpisodeImg(int stageNum)
    {
        if (!_storyTriggerImgs.ContainsKey(stageNum))
            return null;

        return _storyTriggerImgs[stageNum];
    }

    #endregion
}
