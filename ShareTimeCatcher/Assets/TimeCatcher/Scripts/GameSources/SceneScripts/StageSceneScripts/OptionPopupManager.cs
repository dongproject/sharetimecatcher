﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OptionPopupManager : MonoBehaviour, IBackKeyEvent
{
    #region Serialize Variables

#pragma warning disable 649

    [SerializeField] Button _backButton;

    [SerializeField] Button _musicOnButton;
    [SerializeField] Button _musicOffButton;

    [SerializeField] Button _soundOnButton;
    [SerializeField] Button _soundOffButton;

    [SerializeField] Button _rateButton;
    [SerializeField] Button _tutorialButton;
    [SerializeField] Button _openingButton;
    [SerializeField] Button _faceBookShareButton;
    [SerializeField] Button _contactButton;

#pragma warning restore 649

    #endregion

    #region Variables

    //bool _isSoundEnable = true;
    //bool _isMusicEnable = true;

    #endregion

    #region Properties

    public Button BackButton
    {
        get { return _backButton; }
    }

    public Button RateButton
    {
        get { return _rateButton; }
    }

    public Button TutorialButton
    {
        get { return _tutorialButton; }
    }

    public Button OpeningButton
    {
        get { return _openingButton; }
    }

    public Button FaceBookShareButton
    {
        get { return _faceBookShareButton; }
    }

    public Button ContactButton
    {
        get { return _contactButton; }
    }

    #endregion

    #region MonoBehaviour Methods

    private void Awake()
    {
        _musicOnButton.onClick.RemoveAllListeners();
        _musicOnButton.onClick.AddListener(() => OnMusicSwitch(false));

        _musicOffButton.onClick.RemoveAllListeners();
        _musicOffButton.onClick.AddListener(() => OnMusicSwitch(true));

        _soundOnButton.onClick.RemoveAllListeners();
        _soundOnButton.onClick.AddListener(() => OnSoundSwitch(false));

        _soundOffButton.onClick.RemoveAllListeners();
        _soundOffButton.onClick.AddListener(() => OnSoundSwitch(true));

        InitOptionData();
    }

    void OnEnable()
    {
        if (BackKeyManager.Instance != null)
            BackKeyManager.Instance.AddBackKeyEvent((IBackKeyEvent)this);
    }

    void OnDisable()
    {
        if (BackKeyManager.Instance != null)
            BackKeyManager.Instance.RemoveBackKeyEvent((IBackKeyEvent)this);
    }

    #endregion

    #region Methods

    void InitOptionData()
    {
        if (CAGameSaveDataManager.Instance.OptionSave.SoundFXState == 1) {
            OnSoundSwitch(true);
        } else {
            OnSoundSwitch(false);
        }

        if (CAGameSaveDataManager.Instance.OptionSave.MusicState == 1) {
            OnMusicSwitch(true);
        } else {
            OnMusicSwitch(false);
        }

        GameDefinitions.Language language = (GameDefinitions.Language)CAGameSaveDataManager.Instance.OptionSave.Language;
        switch (language) {
            case GameDefinitions.Language.Kor:
                break;
            case GameDefinitions.Language.Eng:
                break;
        }
    }

    public void ShowOption()
    {
        this.gameObject.SetActive(true);
    }

    public void CloseOption()
    {
        this.gameObject.SetActive(false);
    }

    public void ExecuteBackKey()     {
        _backButton.onClick.Invoke();     }

    #endregion

    #region CallBack Methods

    void OnMusicSwitch(bool isOn)
    {
        if (isOn)
        {
            _musicOnButton.gameObject.SetActive(true);
            _musicOffButton.gameObject.SetActive(false);
        }
        else
        {
            _musicOnButton.gameObject.SetActive(false);
            _musicOffButton.gameObject.SetActive(true);
        }

        if (SoundManager.Instance != null)
            SoundManager.Instance.SetMusicEnable(isOn);

        if (isOn) {
            if(CAGameSaveDataManager.Instance.OptionSave.MusicState != 1){
                CAGameSaveDataManager.Instance.OptionSave.MusicState = 1;
                CAGameSaveDataManager.Instance.OptionSave.SaveData();
            }
        } else {
            if (CAGameSaveDataManager.Instance.OptionSave.MusicState != 0) {
                CAGameSaveDataManager.Instance.OptionSave.MusicState = 0;
                CAGameSaveDataManager.Instance.OptionSave.SaveData();
            }
        }
    }

    void OnSoundSwitch(bool isOn)
    {
        if (isOn)
        {
            _soundOnButton.gameObject.SetActive(true);
            _soundOffButton.gameObject.SetActive(false);
        }
        else
        {
            _soundOnButton.gameObject.SetActive(false);
            _soundOffButton.gameObject.SetActive(true);
        }

        if (SoundManager.Instance != null)
            SoundManager.Instance.SetSoundEnable(isOn);

        if (isOn) {
            if (CAGameSaveDataManager.Instance.OptionSave.SoundFXState != 1) {
                CAGameSaveDataManager.Instance.OptionSave.SoundFXState = 1;
                CAGameSaveDataManager.Instance.OptionSave.SaveData();
            }
        } else {
            if (CAGameSaveDataManager.Instance.OptionSave.SoundFXState != 0) {
                CAGameSaveDataManager.Instance.OptionSave.SoundFXState = 0;
                CAGameSaveDataManager.Instance.OptionSave.SaveData();
            }
        }
    }

    #endregion
}
