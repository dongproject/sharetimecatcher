﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StageSceneDefinitions
{
	public enum StagePieceState
	{
		OpenStage,
		LockStage,
	}

    public enum StageSceneStep
    {
        Normal = 0,
        PrologueStoryStep,
        FocusStep,
        AutoStaryDelay,
        AutoStart,
        EpisodeImg,
        AutoNextFocus,
    }
}
