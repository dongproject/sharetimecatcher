﻿using UnityEngine;
using System.Collections;

public class LogoSceneController : MonoBehaviour 
{
	#region Definitions

	public enum FirstStartStep{
		FirstStartStepSetResolution,
		FirstStartStepLogo,
		FirstStartStepLoading,
		LoadCommonPlanData,
		LoadSaveData,
		FirstStartStepCompleted
	}

	#endregion

	#region Variables

	[SerializeField] float _logoWaitTime;
    [SerializeField] GameObject _logoObject;
	float m_CurrentT = 0.0f;
	bool m_LogoImgUpdateState;
	//public Camera m_LogoCamera;
	
	FirstStartStep m_FirstStartStep;
	bool m_LogoImgState;

	AsyncOperation async;

	#endregion

	#region MonoBehaviour Methods
	
	void Awake()
	{
        _logoObject.SetActive(false);
        CAGameGlobal.InitGameGlobal ();
		m_LogoImgUpdateState = false;
		
		m_LogoImgState = true;
		SetCurrentStartStep (FirstStartStep.FirstStartStepSetResolution);
	}
	
	void Start()
	{
#if UNITY_ANDROID 
#endif
	}
	
	void Update()
	{
		switch(m_FirstStartStep)
		{
		case FirstStartStep.FirstStartStepSetResolution:
			InitResolution();
			if(m_LogoImgState)
			{
				SetCurrentStartStep (FirstStartStep.FirstStartStepLogo);
			}
			else
			{
				SetCurrentStartStep (FirstStartStep.FirstStartStepLoading);
			}
			break;
		case FirstStartStep.FirstStartStepLogo:
			if(LogoImgUpdate() == 1)
			{
				SetCurrentStartStep (FirstStartStep.FirstStartStepLoading);
			}
			break;
		case FirstStartStep.FirstStartStepLoading:
			SetLoadingResources ();
//			SetCurrentStartStep (FirstStartStep.LoadSaveData); // TODO : Temp code
			SetCurrentStartStep(FirstStartStep.LoadCommonPlanData);
			break;
		case FirstStartStep.FirstStartStepCompleted:
			break;
		}
	}

	#endregion

	#region Methods

	void SetCurrentStartStep(FirstStartStep startStep)
	{
		m_FirstStartStep = startStep;
        switch (m_FirstStartStep)
        {
            case FirstStartStep.FirstStartStepSetResolution:
                break;
            case FirstStartStep.FirstStartStepLogo:
                _logoObject.SetActive(true);
                break;
            case FirstStartStep.FirstStartStepLoading:
                break;
            case FirstStartStep.LoadCommonPlanData:
                LoadGameCommonPlanData();
                break;
            case FirstStartStep.LoadSaveData:
                LoadGameSaveData();
                break;
            case FirstStartStep.FirstStartStepCompleted:
                ChangeTitleScene();
                break;
        }
    }
	
	void InitResolution()
	{
		CAResolutionCtl.InitResolutionCtl ();
		CAResolutionCtl.Instance.InitResolution(Screen.width, Screen.height, false);
		#if _DEBUG
		Debug.Log ("width, height : " + CAResolutionCtl.Instance.GetResolutionWidth() + CAResolutionCtl.Instance.GetResolutionHeight());
		#endif
		Screen.SetResolution((int)CAResolutionCtl.Instance.GetResolutionWidth(), (int)CAResolutionCtl.Instance.GetResolutionHeight(), true);
	}

	public int LogoImgUpdate()
	{
		int retValue = -1;
		m_CurrentT += Time.deltaTime;
		if(m_CurrentT > _logoWaitTime){
			retValue = 1;
		}
		
		return retValue;
	}
	
	public void SetLoadingResources()
	{
//		if(CResolution.Instance.GetRatioWidth() > 1f){
//			m_GameLoadingBG.transform.localScale = CResolution.Instance.GetDefaultImgScale();
//		}
		
//		m_LoadingObject.SetActive(true);
//		m_LoadingBarSlider.value = 0f;
		
//		StartCoroutine("StartLoad");
	}

	void LoadGameCommonPlanData()
	{
		GamePlanDataManager.Instance.LoadStartGamePlanData();

        SetCurrentStartStep (FirstStartStep.LoadSaveData);
	}

	void LoadGameSaveData()
	{
		CAGameSaveDataManager.Instance.LoadCommonSaveData ();
		SetCurrentStartStep (FirstStartStep.FirstStartStepCompleted);
	}

	void ChangeTitleScene()
	{
		CAGameGlobal.Instance.LoadGameScene (GameSceneEnum.SCENE_TITLE, SceneChangeState.SCENE_CHANGE_ASYNC);
	}
	
	public IEnumerator StartLoad()
	{
		yield return null;
		
		async = Application.LoadLevelAsync("TitleScene");
		
		while(!async.isDone)
		{
			#if _DEBUG
			Debug.Log ("async.progress : " + async.progress);
			#endif
//			if(async.progress > 0.8f)
//			{
//				m_LoadingBarSlider.value = 1f;
//			}
//			else
//			{
//				m_LoadingBarSlider.value = async.progress;	
//			}
			yield return null;
		}
	}

	#endregion
}











