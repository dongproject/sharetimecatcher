﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.U2D;

public class TitleSceneController : CACommonMonoBehaviours
{
    #region Serialize Variables

    [SerializeField] Image TestImage;
    [SerializeField] SpriteAtlas testAtlas;
    [SerializeField] UGUITweenAlpha _touchTextAppearTween;

    #endregion

    #region Variables

    bool _isFinishAppearAni = false;

    #endregion

    #region MonoBehaviour Methods

    public override void Awake()
    {
        base.Awake();
        curTouchState = true;

        _touchTextAppearTween.AddOnFinished(() => OnFinishTitleAppearAni());
    }

    // Use this for initialization
    public override void Start()
    {
        base.Start();
        //		TestImage.sprite = testAtlas.GetSprite ("7");
        //		TestImage.SetNativeSize ();

        AudioClip musicClip = Resources.Load<AudioClip>(SoundFilePaths.MusicPath_1);
        SoundManager.Instance.PlayMusic(musicClip);
    }

    // Update is called once per frame
    void Update()
    {

    }

    public override void OnDisable()
    {
        base.OnDisable();
        SoundManager.Instance.StopMusic();
    }

    public override void OnDestroy()
    {
        base.OnDestroy();
        curTouchState = false;
    }

    #endregion

    #region Methods

    void DoPlaySceneLoad()
    {
        CAGameGlobal.Instance.LoadGameScene(GameSceneEnum.SCENE_STAGE, SceneChangeState.SCENE_CHANGE_DIRECT);
    }

    #endregion

    #region Touch Handler

    public override CACurGamePlayTouchData OnGetTouchData(int touchID, float posX, float posY, bool isTouch)
    {
        return null;
    }

    public override void OnMultiTouchPosSet(float posX, float posY) { }
    public override void OnTouchDrag(int touchID, float posX, float posY) { }
    public override void OnTouchScale() { }

    public override void OnTouchEvent(CACurGamePlayTouchData playTouchData, float posX, float posY) { }
    public override void OnTouchRelease(int touchID, float posX, float posY)
    {
        if(_isFinishAppearAni)
            DoPlaySceneLoad();
    }

    #endregion

    #region CallBack Methods

    public void OnFinishTitleAppearAni()
    {
        _isFinishAppearAni = true;
    }

    #endregion
}
