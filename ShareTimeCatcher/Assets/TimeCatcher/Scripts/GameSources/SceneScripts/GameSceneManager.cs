﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameSceneManager 
{
    private static GameSceneManager _instacne = null;
    public static GameSceneManager Instacne
    {
        get { return _instacne; }
    }

    #region Variables

    #endregion

    #region Properties

    #endregion

    #region Methods

    public static void InitGameSceneManager()
    {
        _instacne = new GameSceneManager();
    }

    public static void Release()
    {
        _instacne = null;
    }

    #endregion
}
