﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.U2D;
using System;

public class SpriteTimeNumberInfo
{
	#region Variables

	tk2dSprite _hourImage_1;
	tk2dSprite _hourImage_2;

	tk2dSprite _minuteImage_1;
	tk2dSprite _minuteImage_2;

	tk2dSprite _secondImage_1;
	tk2dSprite _secondImage_2;

	tk2dSprite _millisecondImage_1;
	tk2dSprite _millisecondImage_2;

	tk2dSprite _mosaSecondImage_1;
	tk2dSprite _mosaSecondImage_2;

	tk2dSprite _mosaMillisecondImage_1;
	tk2dSprite _mosaMillisecondImage_2;

	int _hourTime;
	int _minuteTime;
	float _timeSecValue;

	int _saveSecond;

	bool _isMosaSecondNum = false;
	bool _isMosaMillisecondNum = false;

	#endregion

	#region Properties

	public tk2dSprite HourImage_1
	{
		get{ return _hourImage_1; }
		set{ _hourImage_1 = value; }
	}

	public tk2dSprite HourImage_2
	{
		get{ return _hourImage_2; }
		set{ _hourImage_2 = value; }
	}

	public tk2dSprite MinuteImage_1
	{
		get{ return _minuteImage_1; }
		set{ _minuteImage_1 = value; }
	}

	public tk2dSprite MinuteImage_2
	{
		get{ return _minuteImage_2; }
		set{ _minuteImage_2 = value; }
	}

	public tk2dSprite SecondImage_1
	{
		get{ return _secondImage_1; }
		set{ _secondImage_1 = value; }
	}

	public tk2dSprite SecondImage_2
	{
		get{ return _secondImage_2; }
		set{ _secondImage_2 = value; }
	}

	public tk2dSprite MillisecondImage_1
	{
		get{ return _millisecondImage_1; }
		set{ _millisecondImage_1 = value; }
	}

	public tk2dSprite MillisecondImage_2
	{
		get{ return _millisecondImage_2; }
		set{ _millisecondImage_2 = value; }
	}

	public tk2dSprite MosaSecondImage_1
	{
		get{ return _mosaSecondImage_1; }
		set{ _mosaSecondImage_1 = value; }
	}

	public tk2dSprite MosaSecondImage_2
	{
		get{ return _mosaSecondImage_2; }
		set{ _mosaSecondImage_2 = value; }
	}

	public tk2dSprite MosaMillisecondImage_1
	{
		get{ return _mosaMillisecondImage_1; }
		set{ _mosaMillisecondImage_1 = value; }
	}

	public tk2dSprite MosaMillisecondImage_2
	{
		get{ return _mosaMillisecondImage_2; }
		set{ _mosaMillisecondImage_2 = value; }
	}

	public int HourTime
	{
		get{ return _hourTime; }
		set{ _hourTime = value; }
	}

	public int MinuteTime
	{
		get{ return _minuteTime; }
		set{ _minuteTime = value; }
	}

	public float TimeSecValue
	{
		get{ return _timeSecValue; }
		set{ _timeSecValue = value; }
	}

	public bool IsMosaSecondNum
	{
		get{ return _isMosaSecondNum; }
		set{ 
			_isMosaSecondNum = value;
			if (_isMosaSecondNum) {
				_secondImage_1.gameObject.SetActive (false);
				_secondImage_2.gameObject.SetActive (false);
				_mosaSecondImage_1.gameObject.SetActive (true);
				_mosaSecondImage_2.gameObject.SetActive (true);
			} else {
				_secondImage_1.gameObject.SetActive (true);
				_secondImage_2.gameObject.SetActive (true);
				_mosaSecondImage_1.gameObject.SetActive (false);
				_mosaSecondImage_2.gameObject.SetActive (false);
			}
		}
	}

	public bool IsMosaMillisecondNum
	{
		get{ return _isMosaMillisecondNum; }
		set{ 
			_isMosaMillisecondNum = value;
			if (_isMosaMillisecondNum) {
				_millisecondImage_1.gameObject.SetActive (false);
				_millisecondImage_2.gameObject.SetActive (false);
				_mosaMillisecondImage_1.gameObject.SetActive (true);
				_mosaMillisecondImage_2.gameObject.SetActive (true);
			} else {
				_millisecondImage_1.gameObject.SetActive (true);
				_millisecondImage_2.gameObject.SetActive (true);
				_mosaMillisecondImage_1.gameObject.SetActive (false);
				_mosaMillisecondImage_2.gameObject.SetActive (false);
			}
		}
	}

	#endregion

	#region Methods

	public void SetTimeNumImages(tk2dSprite hourImg_1, tk2dSprite hourImg_2, tk2dSprite minImg_1, tk2dSprite minImg_2, 
		tk2dSprite secImg_1, tk2dSprite secImg_2, tk2dSprite millisecImg_1, tk2dSprite millisecImg_2)
	{
		_hourImage_1 = hourImg_1;
		_hourImage_2 = hourImg_2;
		_minuteImage_1 = minImg_1;
		_minuteImage_2 = minImg_2;
		_secondImage_1 = secImg_1;
		_secondImage_2 = secImg_2;
		_millisecondImage_1 = millisecImg_1;
		_millisecondImage_2 = millisecImg_2;
	}

	public void SetMosaTimeNumImages(tk2dSprite mosaSecImg_1, tk2dSprite mosaSecImg_2, tk2dSprite mosaMillisecImg_1, tk2dSprite mosaMillisecImg_2)
	{
		_mosaSecondImage_1 = mosaSecImg_1;
		_mosaSecondImage_2 = mosaSecImg_2;
		_mosaMillisecondImage_1 = mosaMillisecImg_1;
		_mosaMillisecondImage_2 = mosaMillisecImg_2;
	}

	public void SetTimeValue(int hourValue, int minValue,  float timeValue)
	{
		// hour
		SetHourValue(hourValue);

		// Minute
		SetMinuteValue(minValue);

		// Second
		_timeSecValue = timeValue;
		_saveSecond = (int)_timeSecValue;
		SetSecondValue (_saveSecond);

		// Millisecond
		int millisecond = Mathf.FloorToInt((_timeSecValue - (float)Mathf.FloorToInt(_timeSecValue)) * 100f);
		SetMillisecondValue (millisecond);

//		int hourValue = (int)(_timeSecValue / 216000f);
//		float restHourValue = _timeSecValue % 216000f;
//
//		int minuteValue = (int)(restHourValue / 3600f);
//		float restMinuteValue = restHourValue % 3600f;
//
//		int secValue = (int)(_timeSecValue / 60f);
//		int restSecValue = _timeSecValue % 60f;

	}


	void SetHourValue(int hourValue)
	{
		_hourTime = hourValue;
		int tenHour = _hourTime / 10;
		int restHour = _hourTime % 10;

		_hourImage_1.SetSprite (string.Format ("{0}", tenHour));
		_hourImage_2.SetSprite (string.Format ("{0}", restHour));
	}

	void SetMinuteValue(int minValue)
	{
		_minuteTime = minValue;
		if (_minuteTime >= 60) {
			_minuteTime = 0;
			SetHourValue (_hourTime + 1);
		}
		int tenMin = _minuteTime / 10;
		int restMin = _minuteTime % 10;

		_minuteImage_1.SetSprite (string.Format ("{0}", tenMin));
		_minuteImage_2.SetSprite (string.Format ("{0}", restMin));
	}

	void SetSecondValue(int secValue)
	{
		int tenSec = secValue / 10;
		int restSec = secValue % 10;

		if (!_isMosaSecondNum) {
			_secondImage_1.SetSprite (string.Format ("{0}", tenSec));
			_secondImage_2.SetSprite (string.Format ("{0}", restSec));
		} else {
			_mosaSecondImage_1.SetSprite (string.Format ("m{0}", tenSec));
			_mosaSecondImage_2.SetSprite (string.Format ("m{0}", restSec));
		}
	}

	void SetMillisecondValue(int milsecValue)
	{
		int tenMilsec = milsecValue / 10;
		int restMilsec = milsecValue % 10;

		if (!_isMosaMillisecondNum) {
			_millisecondImage_1.SetSprite (string.Format ("{0}", tenMilsec));
			_millisecondImage_2.SetSprite (string.Format ("{0}", restMilsec));
		} else {
			_mosaMillisecondImage_1.SetSprite (string.Format ("m{0}", tenMilsec));
			_mosaMillisecondImage_2.SetSprite (string.Format ("m{0}", restMilsec));
		}
	}

	public void SetCurrentTimeSecValue(float secValue)
	{
		// Second
		_timeSecValue = secValue;

		if (_timeSecValue >= 60f) {
			_timeSecValue = _timeSecValue - 60f;
			SetMinuteValue (_minuteTime + 1);
		}

		if (_saveSecond != (int)_timeSecValue) {
			_saveSecond = (int)_timeSecValue;
			SetSecondValue (_saveSecond);
		}

		// Millisecond
		int millisecond = Mathf.FloorToInt((_timeSecValue - (float)Mathf.FloorToInt(_timeSecValue)) * 100f);
		SetMillisecondValue (millisecond);
	}

    public void SetTotalTimeValue(float secTotalValue)
    {
        int quotientHour = (int)secTotalValue / 3600;
        int restHourSec = (int)secTotalValue % 3600;

        SetHourValue(quotientHour);

        int quotientMin = (int)restHourSec / 60;
        int restMinSec = (int)restHourSec % 60;

        SetMinuteValue(quotientMin);

        _timeSecValue = restMinSec + (secTotalValue - (float)Mathf.FloorToInt(secTotalValue));
        _saveSecond = restMinSec;
        SetSecondValue(_saveSecond);

        int millisecond = Mathf.FloorToInt((secTotalValue - (float)Mathf.FloorToInt(secTotalValue)) * 100f);
        SetMillisecondValue(millisecond);
    }

    public float GetTotalSecTimeValue()
    {
        return (_hourTime * 3600) + (_minuteTime * 60) + _timeSecValue;
    }

    #endregion
}

public class TimeCatcherSpriteManager
{
	#region Variables

	float _permitTime = 3f;
	bool _startState = false;
	SpriteTimeNumberInfo _imgTargetTimeNum = new SpriteTimeNumberInfo ();
	SpriteTimeNumberInfo _imgRealTimeNum = new SpriteTimeNumberInfo();
	List<SpriteTimeNumberInfo> _fakeTimeNums = new List<SpriteTimeNumberInfo>();

	float _finishFailTime;

	Action _onFinishTimeOver = null;

	Action _onChangeStageState = null;
	float _leftStageChangeTime = 0f;
	float _changeTime = 0f;
    float _slowRevisionTime = 1f;

	#endregion

	#region Properties

	public bool StartState
	{
		get{ return _startState; }
		set{ _startState = value; }
	}

	public SpriteTimeNumberInfo ImgTargetTimeNum
	{
		get{ return _imgTargetTimeNum; }
	}

	public SpriteTimeNumberInfo ImgRealTimeNum
	{
		get{ return _imgRealTimeNum; }
	}

	public List<SpriteTimeNumberInfo> FakeTimeNums
	{
		get{ return _fakeTimeNums; }
	}

	public Action OnFinishTimeOver
	{
		get{ return _onFinishTimeOver; }
		set{ _onFinishTimeOver = value; }
	}

	public Action OnChangeStageState
	{
		get{ return _onChangeStageState; }
		set{ _onChangeStageState = value; }
	}

	public float LeftStageChangeTime
	{
		get{ return _leftStageChangeTime; }
		set{ _leftStageChangeTime = value; }
	}

    public float SlowRevisionTime
    {
        get { return _slowRevisionTime; }
        set { _slowRevisionTime = value; }
    }

    #endregion

    #region Methods

    public void SetFailTime()
	{
		float realTotalSecValue = (float)_imgRealTimeNum.HourTime * 3600f + (float)_imgRealTimeNum.MinuteTime * 60f + _imgRealTimeNum.TimeSecValue;
		float targetTotalSecValue = (float)_imgTargetTimeNum.HourTime * 3600f + (float)_imgTargetTimeNum.MinuteTime * 60f + _imgTargetTimeNum.TimeSecValue;

		_finishFailTime = targetTotalSecValue - realTotalSecValue + _permitTime;
	}

    public void SetReverseFailTime()
    {
        float realTotalSecValue = (float)_imgRealTimeNum.HourTime * 3600f + (float)_imgRealTimeNum.MinuteTime * 60f + _imgRealTimeNum.TimeSecValue;
        float targetTotalSecValue = (float)_imgTargetTimeNum.HourTime * 3600f + (float)_imgTargetTimeNum.MinuteTime * 60f + _imgTargetTimeNum.TimeSecValue;

        _finishFailTime = realTotalSecValue - targetTotalSecValue + _permitTime;
    }

    public void SetChangeTime()
	{
		float realTotalSecValue = (float)_imgRealTimeNum.HourTime * 3600f + (float)_imgRealTimeNum.MinuteTime * 60f + _imgRealTimeNum.TimeSecValue;
		float targetTotalSecValue = (float)_imgTargetTimeNum.HourTime * 3600f + (float)_imgTargetTimeNum.MinuteTime * 60f + _imgTargetTimeNum.TimeSecValue;

		_changeTime = targetTotalSecValue - realTotalSecValue - _leftStageChangeTime;
	}

	public void UpdateStopWatch()
	{
		if (!_startState)
			return;

        float calcDeltaTime = Time.deltaTime * _slowRevisionTime;
        _imgRealTimeNum.SetCurrentTimeSecValue (_imgRealTimeNum.TimeSecValue += calcDeltaTime);
		for (int i = 0; i < FakeTimeNums.Count; i++) {
			FakeTimeNums [i].SetCurrentTimeSecValue (FakeTimeNums [i].TimeSecValue += Time.deltaTime);
		}

		_finishFailTime -= calcDeltaTime;

		if (_onChangeStageState != null) {
			_changeTime -= calcDeltaTime;
			if (_changeTime <= 0f) {
				_onChangeStageState ();
				_onChangeStageState = null;
			}
		}

		if (_finishFailTime <= 0f) {
			if (_onFinishTimeOver != null)
				_onFinishTimeOver ();

            _startState = false;
        }
	}

    public void UpdateReverseStopWatch()
    {
        if (!_startState)
            return;

        float calcDeltaTime = Time.deltaTime * _slowRevisionTime;
        _imgRealTimeNum.SetCurrentTimeSecValue(_imgRealTimeNum.TimeSecValue -= calcDeltaTime);
        for (int i = 0; i < FakeTimeNums.Count; i++)
        {
            FakeTimeNums[i].SetCurrentTimeSecValue(FakeTimeNums[i].TimeSecValue -= Time.deltaTime);
        }

        _finishFailTime -= calcDeltaTime;

        if (_onChangeStageState != null)
        {
            _changeTime -= calcDeltaTime;
            if (_changeTime <= 0f)
            {
                _onChangeStageState();
                _onChangeStageState = null;
            }
        }

        if (_finishFailTime <= 0f)
        {
            if (_onFinishTimeOver != null)
                _startState = false;
            _onFinishTimeOver();
        }
    }

    public float CheckGapStopTime()
	{

		float realTotalSecValue = (float)_imgRealTimeNum.HourTime * 3600f + (float)_imgRealTimeNum.MinuteTime * 60f + _imgRealTimeNum.TimeSecValue;
		float targetTotalSecValue = (float)_imgTargetTimeNum.HourTime * 3600f + (float)_imgTargetTimeNum.MinuteTime * 60f + _imgTargetTimeNum.TimeSecValue;

		float gapValue = realTotalSecValue - targetTotalSecValue;

		if (gapValue >= 1f) {
			return 0.99f;
		} else if (gapValue <= -1f) {
			return -0.99f;
		}

		return gapValue;
	}

    public float GetGapStopTime()
    {
        float realTotalSecValue = (float)_imgRealTimeNum.HourTime * 3600f + (float)_imgRealTimeNum.MinuteTime * 60f + _imgRealTimeNum.TimeSecValue;
        float targetTotalSecValue = (float)_imgTargetTimeNum.HourTime * 3600f + (float)_imgTargetTimeNum.MinuteTime * 60f + _imgTargetTimeNum.TimeSecValue;

        return realTotalSecValue - targetTotalSecValue;
    }

	#endregion

	#region Methods

	public void OnAddRealTimeValue(float addTimeValue)
	{
		if (!_startState)
			return;

		_imgRealTimeNum.SetCurrentTimeSecValue (_imgRealTimeNum.TimeSecValue += addTimeValue);
		for (int i = 0; i < FakeTimeNums.Count; i++) {
			FakeTimeNums [i].SetCurrentTimeSecValue (FakeTimeNums [i].TimeSecValue += addTimeValue);
		}

		_finishFailTime -= addTimeValue;
		if (_finishFailTime <= 0f) {
			if (_onFinishTimeOver != null)
				_startState = false;
			_onFinishTimeOver ();
		}
	}

	#endregion
}
