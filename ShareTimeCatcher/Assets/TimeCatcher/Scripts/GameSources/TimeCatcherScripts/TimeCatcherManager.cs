﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.U2D;

public class ImageTimeNumStruct
{
	#region Variables

	Image _hourImage_1;
	Image _hourImage_2;

	Image _minuteImage_1;
	Image _minuteImage_2;

	Image _secondImage_1;
	Image _secondImage_2;

	Image _millisecondImage_1;
	Image _millisecondImage_2;

	SpriteAtlas _numSpriteAtlas;

	int _hourTime;
	int _minuteTime;
	float _timeSecValue;
	int _saveSecond;

	#endregion

	#region Properties

	public Image HourImage_1
	{
		get{ return _hourImage_1; }
		set{ _hourImage_1 = value; }
	}

	public Image HourImage_2
	{
		get{ return _hourImage_2; }
		set{ _hourImage_2 = value; }
	}

	public Image MinuteImage_1
	{
		get{ return _minuteImage_1; }
		set{ _minuteImage_1 = value; }
	}

	public Image MinuteImage_2
	{
		get{ return _minuteImage_2; }
		set{ _minuteImage_2 = value; }
	}

	public Image SecondImage_1
	{
		get{ return _secondImage_1; }
		set{ _secondImage_1 = value; }
	}

	public Image SecondImage_2
	{
		get{ return _secondImage_2; }
		set{ _secondImage_2 = value; }
	}

	public Image MillisecondImage_1
	{
		get{ return _millisecondImage_1; }
		set{ _millisecondImage_1 = value; }
	}

	public Image MillisecondImage_2
	{
		get{ return _millisecondImage_2; }
		set{ _millisecondImage_2 = value; }
	}

	public SpriteAtlas NumSpriteAtlas
	{
		get{ return _numSpriteAtlas; }
		set{ _numSpriteAtlas = value; }
	}

	public int HourTime
	{
		get{ return _hourTime; }
		set{ _hourTime = value; }
	}

	public int MinuteTime
	{
		get{ return _minuteTime; }
		set{ _minuteTime = value; }
	}

	public float TimeSecValue
	{
		get{ return _timeSecValue; }
		set{ _timeSecValue = value; }
	}

	#endregion

	#region Methods

	public void SetTimeNumImages(Image hourImg_1, Image hourImg_2, Image minImg_1, Image minImg_2, 
		Image secImg_1, Image secImg_2, Image millisecImg_1, Image millisecImg_2)
	{
		_hourImage_1 = hourImg_1;
		_hourImage_2 = hourImg_2;
		_minuteImage_1 = minImg_1;
		_minuteImage_2 = minImg_2;
		_secondImage_1 = secImg_1;
		_secondImage_2 = secImg_2;
		_millisecondImage_1 = millisecImg_1;
		_millisecondImage_2 = millisecImg_2;
	}

	public void SetTimeValue(int hourValue, int minValue,  float timeValue)
	{
		// hour
		SetHourValue(hourValue);

		// Minute
		SetMinuteValue(minValue);

		// Second
		_timeSecValue = timeValue;
		_saveSecond = (int)_timeSecValue;
		SetSecondValue (_saveSecond);

		// Millisecond
		int millisecond = Mathf.RoundToInt((_timeSecValue - (float)Mathf.FloorToInt(_timeSecValue)) * 100f);
		SetMillisecondValue (millisecond);

//		int hourValue = (int)(_timeSecValue / 216000f);
//		float restHourValue = _timeSecValue % 216000f;
//
//		int minuteValue = (int)(restHourValue / 3600f);
//		float restMinuteValue = restHourValue % 3600f;
//
//		int secValue = (int)(_timeSecValue / 60f);
//		int restSecValue = _timeSecValue % 60f;

	}

	void SetHourValue(int hourValue)
	{
		_hourTime = hourValue;
		int tenHour = _hourTime / 10;
		int restHour = _hourTime % 10;
		_hourImage_1.sprite = GetTimeNumSprite (tenHour);
		_hourImage_1.SetNativeSize ();
		_hourImage_2.sprite = GetTimeNumSprite (restHour);
		_hourImage_2.SetNativeSize ();
	}

	void SetMinuteValue(int minValue)
	{
		_minuteTime = minValue;
		if (_minuteTime >= 60) {
			_minuteTime = 0;
			SetHourValue (_hourTime + 1);
		}
		int tenMin = _minuteTime / 10;
		int restMin = _minuteTime % 10;
		_minuteImage_1.sprite = GetTimeNumSprite (tenMin);
		_minuteImage_1.SetNativeSize ();
		_minuteImage_2.sprite = GetTimeNumSprite (restMin);
		_minuteImage_2.SetNativeSize ();
	}

	void SetSecondValue(int secValue)
	{
		int tenSec = secValue / 10;
		int restSec = secValue % 10;
		_secondImage_1.sprite = GetTimeNumSprite (tenSec);
		_secondImage_1.SetNativeSize ();
		_secondImage_2.sprite = GetTimeNumSprite (restSec);
		_secondImage_2.SetNativeSize ();
	}

	void SetMillisecondValue(int milsecValue)
	{
		int tenMilsec = milsecValue / 10;
		int restMilsec = milsecValue % 10;

		//		Debug.Log (string.Format ("_timeSecValue : {0}, tenMilsec : {1}, restMilsec : {2}", _timeSecValue, tenMilsec, restMilsec));

		_millisecondImage_1.sprite = GetTimeNumSprite (tenMilsec);
		_millisecondImage_1.SetNativeSize ();
		_millisecondImage_2.sprite = GetTimeNumSprite (restMilsec);
		_millisecondImage_2.SetNativeSize ();
	}

	public void SetCurrentTimeSecValue(float secValue)
	{
		// Second
		_timeSecValue = secValue;
		if (_timeSecValue >= 60f) {
			_timeSecValue = _timeSecValue - 60f;
			SetMinuteValue (_minuteTime + 1);
		}
		if (_saveSecond != (int)_timeSecValue) {
			_saveSecond = (int)_timeSecValue;
			SetSecondValue (_saveSecond);
		}

		// Millisecond
		int millisecond = Mathf.RoundToInt((_timeSecValue - (float)Mathf.FloorToInt(_timeSecValue)) * 100f);
		SetMillisecondValue (millisecond);
	}

	Sprite GetTimeNumSprite(int numValue)
	{
		return _numSpriteAtlas.GetSprite (string.Format("{0}", numValue));
	}

	#endregion
}

public class TimeCatcherManager
{
	#region Variables

	float _permitTime = 3f;
	float _goalTime;
	float _currentTime;
	Text _goalTimeText;
	Text _currentTimeText;
	bool _startState = false;
	ImageTimeNumStruct _imgTargetTimeNum = new ImageTimeNumStruct ();
	ImageTimeNumStruct _imgRealTimeNum = new ImageTimeNumStruct();

	#endregion

	#region Properties

	public float GoalTime
	{
		get{ return _goalTime; }
		set{ _goalTime = value; }
	}

	public float CurrentTime
	{
		get{ return _currentTime; }
		set{ _currentTime = value; }
	}

	public bool StartState
	{
		get{ return _startState; }
		set{ _startState = value; }
	}

	public ImageTimeNumStruct ImgTargetTimeNum
	{
		get{ return _imgTargetTimeNum; }
		set{ _imgTargetTimeNum = value; }
	}

	public ImageTimeNumStruct ImgRealTimeNum
	{
		get{ return _imgRealTimeNum; }
		set{ _imgRealTimeNum = value; }
	}

	#endregion

	#region Methods

	public void InitStopWatchData(float destTime, float startTime, Text goalText, Text currentText)
	{
		_goalTime = destTime;
		_currentTime = 0f;
		_goalTimeText = goalText;
		_currentTimeText = currentText;

		_goalTimeText.text = string.Format("Goal Time : {0}", _goalTime);
		_currentTimeText.text = string.Format ("{0:D2} : {1:D2} : {2:D2}", 0, 0, 0);
	}

	public void UpdateStopWatch()
	{
		if (!_startState)
			return;

		_imgRealTimeNum.SetCurrentTimeSecValue (_imgRealTimeNum.TimeSecValue += Time.deltaTime);

//		_currentTime += Time.deltaTime;
//
//		Debug.Log (string.Format ("_currentTime : {0}", _currentTime));
//		int miniute = (int)_currentTime/60;
//		int second = (int)_currentTime%60;
//		int milliSecond = Mathf.RoundToInt((_currentTime - (float)Mathf.FloorToInt(_currentTime)) * 100f);
//		_currentTimeText.text = string.Format ("{0:D2} : {1:D2} : {2:D2}", miniute, second, milliSecond);
	}

	#endregion
}
