﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IUpdateable
{
	void OnUpdate();
	bool IsEnableUpdate {
		get;
		set;
	}
}
