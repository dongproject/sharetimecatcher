﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StoryAniManager : MonoBehaviour
{
    #region Serialize Variables

    [SerializeField] StoryAniTrigger _aniTrigger;

    #endregion

    #region Properties

    public StoryAniTrigger AniTrigger
    {
        get { return _aniTrigger; }
    }

    #endregion

    //#region MonoBehaviour Methods

    //public void Awake()
    //{
    //    //_aniTrigger.OnAniTrigger = OnStoryAniTrigger;
    //}

    //#endregion

    //#region CallBack Methods

    //void OnStoryAniTrigger(int storyIndex)
    //{

    //}

    //#endregion
}
