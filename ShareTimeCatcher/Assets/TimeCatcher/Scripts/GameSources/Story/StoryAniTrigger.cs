﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StoryAniTrigger : MonoBehaviour
{
    #region Variables

    Action<int> _onAniTrigger = null;

    #endregion

    #region Properties

    public Action<int> OnAniTrigger
    {
        get { return _onAniTrigger; }
        set { _onAniTrigger = value; }
    }

    #endregion

    #region CallBack Methods

    public void OnStoryAniTrigger(int storyIndex)
    {
        if (_onAniTrigger != null)
            _onAniTrigger(storyIndex);
    }

    #endregion
}
