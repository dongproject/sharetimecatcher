﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AlbumDynamicData
{
    #region Variables

    Dictionary<int /* Page */, List<int> /* Album Num */> _albumOpenInfos = new Dictionary<int, List<int>>();

    #endregion

    #region Properties

    public Dictionary<int /* Page */, List<int> /* Album Num */> AlbumOpenInfos
    {
        get { return _albumOpenInfos; }
    }

    #endregion

    #region Methods

    public bool IsOpenAlbum(int page, int albumNum)
    {
        if (_albumOpenInfos.ContainsKey(page))
        {
            List<int> albums = _albumOpenInfos[page];
            if (albums.Contains(albumNum))
                return true;
        }

        return false;
    }

    public void AddOpenAlbum(int page, int addAlbum)
    {
        List<int> albums = null;
        if (_albumOpenInfos.ContainsKey(page))
        {
            albums = _albumOpenInfos[page];
        }
        else
        {
            albums = new List<int>();
            _albumOpenInfos.Add(page, albums);
        }

        if (!albums.Contains(addAlbum))
        {
            albums.Add(addAlbum);
        }
    }

    #endregion
}
