﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameDynamicData
{
    #region Variables

    AlbumDynamicData _albumData = new AlbumDynamicData();
    GameDefinitions.StageSceneState _stageSceneState = GameDefinitions.StageSceneState.None;

    #endregion

    #region Properties

    public AlbumDynamicData AlbumData
    {
        get { return _albumData; }
    }

    public GameDefinitions.StageSceneState StageSceneState
    {
        get { return _stageSceneState; }
        set { _stageSceneState = value; }
    }

    #endregion
}
