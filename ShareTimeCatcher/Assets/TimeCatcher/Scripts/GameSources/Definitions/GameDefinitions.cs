﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameDefinitions
{
    public enum StageSceneState
    {
        None,
        NextStage,
    }

    public enum Language
    {
        Kor = 0,
        Eng = 1,
    }
}
