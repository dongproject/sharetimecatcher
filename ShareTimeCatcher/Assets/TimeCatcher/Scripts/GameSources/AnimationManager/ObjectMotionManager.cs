﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class ObjectMotionAni
{
	#region Variables

	ObjectMotionManager.MotionType _motionType;

	int _objectIndex;
	int _motionID;
	float _startScale;
	float _destScale;
	float _gapScale;
	float _scaleSpeedValue;
	float _speedValue;
    float _speedChangeValue;
    float _speedDestValue;
	float _curTime = 0f;
	float _motionTime = 0f;
	bool _isPlusState = false;
    float _velocity;

	float _sinA;
	float _cosA;
	float _distance;
	float _curMoveValue = 0f;

	float _minSpeedValue = 8f;
	float _maxSpeedValue = 16f;

	float _startDistance;

	Action<int /* Motion ID */,ObjectMotionManager.MotionType, float> _onChangeObjScaleValue;
	Action<int /* Motion ID */, ObjectMotionManager.MotionType, Vector2> _onChangeMoveMotionValue;
	Action<int /* Motion ID */, ObjectMotionAni> _onCompletedMotion;

	#endregion

	#region Properties

	public ObjectMotionManager.MotionType MotionType
	{
		get{ return _motionType; }
		set{ _motionType = value; }
	}

	public int ObjectIndex
	{
		get{ return _objectIndex; }
		set{ _objectIndex = value; }
	}

	public int MotionID
	{
		get{ return _motionID; }
		set{ _motionID = value; }
	}

	public float StartScale
	{
		get{ return _startScale; }
		set{ _startScale = value; }
	}

	public float DestScale
	{
		get{ return _destScale; }
		set{ _destScale = value; }
	}

	public float GapScale
	{
		get{ return _gapScale; }
		set{ _gapScale = value; }
	}

	public float SpeedValue
	{
		get{ return _speedValue; }
		set{ 
			_speedValue = value; 

			if (_speedValue < _minSpeedValue)
				_speedValue = _minSpeedValue;
			else if (_speedValue > _maxSpeedValue)
				_speedValue = _maxSpeedValue;
		}
	}

    public float SpeedChangeValue
    {
        get { return _speedChangeValue; }
        set { _speedChangeValue = value; }
    }

    public float SpeedDestValue
    {
        get { return _speedDestValue; }
        set { _speedDestValue = value; }
    }

    public float MinSpeedValue
    {
        get { return _minSpeedValue; }
    }

    public float ScaleSpeedValue
	{
		get{ return _scaleSpeedValue; }
		set{ _scaleSpeedValue = value; }
	}

	public bool IsPlusState
	{
		get{ return _isPlusState; }
		set{ _isPlusState = value; }
	}

	public float CurTime
	{
		get{ return _curTime; }
		set{ _curTime = value; }
	}

	public float MotionTime
	{
		get{ return _motionTime; }
		set{ _motionTime = value; }
	}

	public float SinA
	{
		get{ return _sinA; }
		set{ _sinA = value; }
	}

	public float CosA
	{
		get{ return _cosA; }
		set{ _cosA = value; }
	}

	public float Distance
	{
		get{ return _distance; }
		set{ _distance = value; }
	}

	public float StartDistance
	{
		get{ return _startDistance; }
		set{ _startDistance = value; }
	}

	public float CurMoveValue
	{
		get{ return _curMoveValue; }
		set{ _curMoveValue = value; }
	}

    public float Velocity
    {
        get { return _velocity; }
        set { _velocity = value; }
    }

	public Action<int /* Motion ID */, ObjectMotionManager.MotionType, float> OnChangeObjScaleValue
	{
		get{ return _onChangeObjScaleValue; }
		set{ _onChangeObjScaleValue = value; }
	}

	public Action<int /* Motion ID */, ObjectMotionManager.MotionType, Vector2> OnChangeMoveMotionValue
	{
		get{ return _onChangeMoveMotionValue; }
		set{ _onChangeMoveMotionValue = value; }
	}

	public Action<int /* Motion ID */, ObjectMotionAni> OnCompletedMotion
	{
		get{ return _onCompletedMotion; }
		set{ _onCompletedMotion = value; }
	}

	#endregion

	#region Methods



	#endregion
}

public class ObjectMotionManager
{
	#region Definitions

	public enum MotionType
	{
		ScaleMotion,
		TimeMoveMotion,
		SpeedMoveMotion,
        SpeedUpMotion,
        SpeedDownMotion,
        UniformVelocityMotion,
    }

	#endregion

	#region Variables

	List<ObjectMotionAni> _objMotionAniList = new List<ObjectMotionAni>();
	Action _onCompleteAllMotionAni = null;

    float playTime = 0f;
    float moveDistance = 0f;

	#endregion

	#region Properties

	public List<ObjectMotionAni> ObjMotionAniList
	{
		get{ return _objMotionAniList; }
	}

	public Action OnCompleteAllMotionAni
	{
		get{ return _onCompleteAllMotionAni; }
		set{ _onCompleteAllMotionAni = value; }
	}

	#endregion

	#region Methods

	public void Update()
	{
		if (_objMotionAniList.Count == 0)
			return;

		List<ObjectMotionAni> delObjMotionAnis = new List<ObjectMotionAni> ();
		for (int i = 0; i < _objMotionAniList.Count; i++) {
            switch (_objMotionAniList[i].MotionType)
            {
                case MotionType.ScaleMotion:
                    if (UpdateScaleMotion(_objMotionAniList[i]) == 1)
                    {
                        delObjMotionAnis.Add(_objMotionAniList[i]);
                    }
                    break;
                case MotionType.TimeMoveMotion:
                    if (UpdateTimeMoveMotion(_objMotionAniList[i]) == 1)
                    {
                        delObjMotionAnis.Add(_objMotionAniList[i]);
                    }
                    break;
                case MotionType.SpeedMoveMotion:
                    if (UpdateSpeedMoveMotion(_objMotionAniList[i]) == 1)
                    {
                        delObjMotionAnis.Add(_objMotionAniList[i]);
                    }
                    break;
                case MotionType.SpeedUpMotion:
                    if (UpdateSpeedUpMotion(_objMotionAniList[i]) == 1)
                    {
                        delObjMotionAnis.Add(_objMotionAniList[i]);
                    }
                    break;
                case MotionType.SpeedDownMotion:
                    if (UpdateSpeedDownMotion(_objMotionAniList[i]) == 1)
                    {
                        delObjMotionAnis.Add(_objMotionAniList[i]);
                    }
                    break;
                case MotionType.UniformVelocityMotion:
                    if (UpdateUniformVelocityMotion(_objMotionAniList[i]) == 1)
                    {
                        delObjMotionAnis.Add(_objMotionAniList[i]);
                    }
                    break;
            }
        }

		for (int i = 0; i < delObjMotionAnis.Count; i++) {
			_objMotionAniList.Remove (delObjMotionAnis [i]);
			if (delObjMotionAnis [i].OnCompletedMotion != null) {
				delObjMotionAnis [i].OnCompletedMotion (delObjMotionAnis [i].MotionID, delObjMotionAnis [i]);
			}

			if (_objMotionAniList.Count == 0) {
				if (_onCompleteAllMotionAni != null)
					_onCompleteAllMotionAni ();
			}
		}
	}

	int UpdateScaleMotion(ObjectMotionAni objMotionAni)
	{
		int retValue = 0;
		objMotionAni.CurTime += Time.deltaTime;
		float moveScale = objMotionAni.ScaleSpeedValue * Time.deltaTime;
		if (objMotionAni.GapScale - moveScale <= 0f) {
			moveScale = objMotionAni.GapScale;
			retValue = 1;
		}

		objMotionAni.GapScale -= moveScale;

		if (objMotionAni.IsPlusState) {
			if(objMotionAni.OnChangeObjScaleValue != null)
				objMotionAni.OnChangeObjScaleValue (objMotionAni.MotionID, objMotionAni.MotionType, moveScale);
		} else {
			if(objMotionAni.OnChangeObjScaleValue != null)
				objMotionAni.OnChangeObjScaleValue (objMotionAni.MotionID, objMotionAni.MotionType, -moveScale);
		}

		return retValue;
	}

	int UpdateTimeMoveMotion(ObjectMotionAni objMotionAni)
	{
		int retValue = 0;

		float leftTime = objMotionAni.MotionTime - (objMotionAni.CurTime + Time.deltaTime);
		float moveValue = 0f;
		if (leftTime <= 0f) {
			leftTime = objMotionAni.MotionTime - objMotionAni.CurTime;
			objMotionAni.CurTime = objMotionAni.MotionTime;
			retValue = 1;
			moveValue = objMotionAni.Distance;
			objMotionAni.Distance = 0f;
		} else {
			objMotionAni.CurTime += Time.deltaTime;
			leftTime = objMotionAni.MotionTime - objMotionAni.CurTime;
			moveValue = (objMotionAni.Distance * Time.deltaTime) / leftTime;

            if (objMotionAni.Distance - moveValue <= 0f) {
				moveValue = objMotionAni.Distance;
				retValue = 1;
				objMotionAni.Distance = 0f;
			} else {
				objMotionAni.Distance -= moveValue;
			}
		}

        objMotionAni.SpeedValue = moveValue;

        //		Debug.Log (string.Format ("UpdateTimeMoveMotion objMotionAni.Distance : {0}", objMotionAni.Distance));

        float calcX = objMotionAni.CosA * moveValue;
		float calcY = objMotionAni.SinA * moveValue;

		if (objMotionAni.OnChangeMoveMotionValue != null) {
			objMotionAni.OnChangeMoveMotionValue (objMotionAni.MotionID, objMotionAni.MotionType, new Vector2 (calcX, calcY));
		}

		return retValue;
	}

	int UpdateSpeedMoveMotion(ObjectMotionAni objMotionAni)
	{
		int retValue = 0;
		objMotionAni.CurTime += Time.deltaTime;
		float moveValue = objMotionAni.SpeedValue * Time.deltaTime;
		if (objMotionAni.CurMoveValue + moveValue >= objMotionAni.Distance) {
			moveValue = objMotionAni.Distance - objMotionAni.CurMoveValue;
			retValue = 1;
		}

		objMotionAni.SpeedValue -= 0.2f;

		objMotionAni.CurMoveValue += moveValue;

		float widthRevisionValue = 0.83f;
		float revisionValue = 0.87f;
		float calcX = objMotionAni.CosA * moveValue * widthRevisionValue;
		float calcY = objMotionAni.SinA * moveValue * revisionValue;

		if (objMotionAni.OnChangeMoveMotionValue != null) {
			objMotionAni.OnChangeMoveMotionValue (objMotionAni.MotionID, objMotionAni.MotionType, new Vector2 (calcX, calcY));
		}

		return retValue;
	}

    int UpdateSpeedUpMotion(ObjectMotionAni objMotionAni)
    {
        int retValue = 0;
        float moveValue = objMotionAni.Velocity * Time.deltaTime;
        if (objMotionAni.CurMoveValue + moveValue >= objMotionAni.Distance)
        {
            moveValue = objMotionAni.Distance - objMotionAni.CurMoveValue;
            retValue = 1;
        }

        if (objMotionAni.Velocity + objMotionAni.SpeedChangeValue > objMotionAni.SpeedDestValue)
        {
            objMotionAni.Velocity = objMotionAni.SpeedDestValue;
        }
        else
        {
            objMotionAni.Velocity += objMotionAni.SpeedChangeValue;
        }


        objMotionAni.CurMoveValue += moveValue;

        float calcX = objMotionAni.CosA * moveValue;
        float calcY = objMotionAni.SinA * moveValue;

        if (objMotionAni.OnChangeMoveMotionValue != null)
        {
            objMotionAni.OnChangeMoveMotionValue(objMotionAni.MotionID, objMotionAni.MotionType, new Vector2(calcX, calcY));
        }

        return retValue;
    }

    int UpdateSpeedDownMotion(ObjectMotionAni objMotionAni)
    {
        int retValue = 0;
        float moveValue = objMotionAni.Velocity * Time.deltaTime;
        if (objMotionAni.CurMoveValue + moveValue >= objMotionAni.Distance)
        {
            moveValue = objMotionAni.Distance - objMotionAni.CurMoveValue;
            retValue = 1;
        }

        if (objMotionAni.Velocity + objMotionAni.SpeedChangeValue < objMotionAni.SpeedDestValue)
        {
            objMotionAni.Velocity = objMotionAni.SpeedDestValue;
            retValue = 1;
        }
        else
        {
            objMotionAni.Velocity += objMotionAni.SpeedChangeValue;
        }

        objMotionAni.CurMoveValue += moveValue;

        float calcX = objMotionAni.CosA * moveValue;
        float calcY = objMotionAni.SinA * moveValue;

        if (objMotionAni.OnChangeMoveMotionValue != null)
        {
            objMotionAni.OnChangeMoveMotionValue(objMotionAni.MotionID, objMotionAni.MotionType, new Vector2(calcX, calcY));
        }

        return retValue;
    }

    int UpdateUniformVelocityMotion(ObjectMotionAni objMotionAni)
    {
        int retValue = 0;

        float moveValue = objMotionAni.Velocity * Time.deltaTime;

        //playTime += Time.deltaTime;
        //moveDistance += moveValue;
        //if(playTime >= 1f){
        //    Debug.Log(string.Format("UpdateUniformVelocityMotion playTime : {0}, moveDistance : {1}, objMotionAni.Velocity : {2}", 
        //                            playTime, moveDistance, objMotionAni.Velocity));
        //    playTime = 0f;
        //    moveDistance = 0f;
        //}

        if (objMotionAni.CurMoveValue + moveValue >= objMotionAni.Distance)
        {
            moveValue = objMotionAni.Distance - objMotionAni.CurMoveValue;
            retValue = 1;
        }

        objMotionAni.CurMoveValue += moveValue;

        float calcX = objMotionAni.CosA * moveValue;
        float calcY = objMotionAni.SinA * moveValue;

        if (objMotionAni.OnChangeMoveMotionValue != null)
        {
            objMotionAni.OnChangeMoveMotionValue(objMotionAni.MotionID, objMotionAni.MotionType, new Vector2(calcX, calcY));
        }

        return retValue;
    }

    public void ChangeDistanceValue(int motionID, float changeScale)
	{
		for (int i = 0; i < _objMotionAniList.Count; i++) {
			if(_objMotionAniList[i].MotionID == motionID){
				_objMotionAniList [i].Distance = _objMotionAniList [i].StartDistance + ((_objMotionAniList [i].StartDistance * changeScale - _objMotionAniList [i].StartDistance) * 0.1f);
			}
		}
	}

	public void AddObjScaleMotion(float motionTime, float startScale, float destScale, Action<int, ObjectMotionManager.MotionType, float> onChangeScaleValue, int motionID = -1, Action<int, ObjectMotionAni> onCompletedMotionAni = null)
	{
		if (startScale == destScale) {
			if (_objMotionAniList.Count == 0 && onCompletedMotionAni != null)
				onCompletedMotionAni (motionID, null);
			return;
		}

		ObjectMotionAni inputMotionAni = new ObjectMotionAni ();
		inputMotionAni.MotionID = motionID;
		inputMotionAni.MotionType = MotionType.ScaleMotion;
		inputMotionAni.StartScale = startScale;
		inputMotionAni.DestScale = destScale;
		inputMotionAni.GapScale = destScale - startScale;
		if (inputMotionAni.GapScale > 0f) {
			inputMotionAni.IsPlusState = true;
		} else {
			inputMotionAni.IsPlusState = false;
			inputMotionAni.GapScale = Mathf.Abs (inputMotionAni.GapScale);
		}

		inputMotionAni.ScaleSpeedValue = inputMotionAni.GapScale / motionTime;
		inputMotionAni.OnChangeObjScaleValue = onChangeScaleValue;
		inputMotionAni.OnCompletedMotion = onCompletedMotionAni;

		_objMotionAniList.Add (inputMotionAni);
	}

	public ObjectMotionAni AddObjTimeMovement(float motionTime, Vector3 startPos, Vector3 destPos, Action<int, ObjectMotionManager.MotionType, Vector2> OnChangeMoveMotionValue, int motionID = -1, Action<int, ObjectMotionAni> onCompletedMotionAni = null)
	{
		if (startPos == destPos) {
			if (_objMotionAniList.Count == 0 && onCompletedMotionAni != null)
				onCompletedMotionAni (motionID, null);
			return null;
		}

		float curTime = 0f;
		for (int i = 0; i < _objMotionAniList.Count; i++) {
			if (_objMotionAniList [i].MotionID == motionID) {
				curTime = _objMotionAniList [i].CurTime;
				_objMotionAniList.RemoveAt (i);
				break;
			}
		}

		motionTime -= curTime;

		if (motionTime <= 0f) {
			if (_objMotionAniList.Count == 0 && onCompletedMotionAni != null)
				onCompletedMotionAni (motionID, null);
			return null;
		}

		float gapX = destPos.x - startPos.x;
		float gapY = destPos.y - startPos.y;
		float distance = Mathf.Sqrt((float)(Mathf.Pow(gapX, 2f) + Mathf.Pow(gapY, 2f)));

		ObjectMotionAni inputMotionAni = new ObjectMotionAni ();
		inputMotionAni.MotionID = motionID;
		inputMotionAni.SinA = gapY/distance;
		inputMotionAni.CosA = gapX/distance;
		inputMotionAni.Distance = distance;
        //inputMotionAni.SpeedValue = distance / motionTime;
        float leftTime = inputMotionAni.MotionTime - inputMotionAni.CurTime;
        inputMotionAni.SpeedValue = (inputMotionAni.Distance * Time.deltaTime) / leftTime;
        inputMotionAni.MotionType = MotionType.TimeMoveMotion;
		inputMotionAni.MotionTime = motionTime;
		inputMotionAni.OnChangeMoveMotionValue = OnChangeMoveMotionValue;
		inputMotionAni.OnCompletedMotion = onCompletedMotionAni;

		_objMotionAniList.Add (inputMotionAni);

		return inputMotionAni;
	}

    public ObjectMotionAni AddObjSpeedMovement(float speedValue, Vector3 startPos, Vector3 destPos, Action<int, ObjectMotionManager.MotionType, Vector2> OnChangeMoveMotionValue, int motionID = -1, Action<int, ObjectMotionAni> onCompletedMotionAni = null)
    {
        if (startPos == destPos)
        {
            if (_objMotionAniList.Count == 0 && onCompletedMotionAni != null)
                onCompletedMotionAni(motionID, null);
            return null;
        }

        float curTime = 0f;
        for (int i = 0; i < _objMotionAniList.Count; i++)
        {
            if (_objMotionAniList[i].MotionID == motionID)
            {
                curTime = _objMotionAniList[i].CurTime;
                _objMotionAniList.RemoveAt(i);
                break;
            }
        }

        //motionTime -= curTime;

        //if (motionTime <= 0f)
        //{
        //    if (_objMotionAniList.Count == 0 && onCompletedMotionAni != null)
        //        onCompletedMotionAni(motionID, null);
        //    return null;
        //}

        float gapX = destPos.x - startPos.x;
        float gapY = destPos.y - startPos.y;
        float distance = Mathf.Sqrt((float)(Mathf.Pow(gapX, 2f) + Mathf.Pow(gapY, 2f)));

        ObjectMotionAni inputMotionAni = new ObjectMotionAni();
        inputMotionAni.MotionID = motionID;
        inputMotionAni.SinA = gapY / distance;
        inputMotionAni.CosA = gapX / distance;
        inputMotionAni.Distance = distance;
        //inputMotionAni.SpeedValue = distance / motionTime;
        float leftTime = inputMotionAni.MotionTime - inputMotionAni.CurTime;
        //inputMotionAni.SpeedValue = (inputMotionAni.Distance * Time.deltaTime) / leftTime;
        inputMotionAni.SpeedValue = speedValue;
        inputMotionAni.MotionType = MotionType.TimeMoveMotion;
        //inputMotionAni.MotionTime = motionTime;
        inputMotionAni.OnChangeMoveMotionValue = OnChangeMoveMotionValue;
        inputMotionAni.OnCompletedMotion = onCompletedMotionAni;

        _objMotionAniList.Add(inputMotionAni);

        return inputMotionAni;
    }

    public void AddObjMotionAniTimeMovement(float motionTime, ObjectMotionAni objMotionAni, Action<int, ObjectMotionManager.MotionType, Vector2> OnChangeMoveMotionValue, int motionID = -1, Action<int, ObjectMotionAni> onCompletedMotionAni = null)
	{

		float curTime = 0f;
		for (int i = 0; i < _objMotionAniList.Count; i++) {
			if (_objMotionAniList [i].MotionID == motionID) {
				curTime = _objMotionAniList [i].CurTime;
				_objMotionAniList.RemoveAt (i);
				break;
			}
		}

		motionTime -= curTime;

		if (motionTime <= 0f) {
			if (_objMotionAniList.Count == 0 && onCompletedMotionAni != null)
				onCompletedMotionAni (motionID, null);
			return;
		}

		ObjectMotionAni inputMotionAni = new ObjectMotionAni ();
		inputMotionAni.MotionID = motionID;
		inputMotionAni.SinA = objMotionAni.SinA * 0.5f;
		inputMotionAni.CosA = objMotionAni.CosA * 0.5f;
		inputMotionAni.Distance = 200f;
		inputMotionAni.SpeedValue = objMotionAni.SpeedValue * 0.5f;
		inputMotionAni.MotionType = MotionType.TimeMoveMotion;
		inputMotionAni.MotionTime = motionTime;
		inputMotionAni.OnChangeMoveMotionValue = OnChangeMoveMotionValue;
		inputMotionAni.OnCompletedMotion = onCompletedMotionAni;

		_objMotionAniList.Add (inputMotionAni);
	}

	public void AddObjSpeedMovement(Vector3 startPos, Vector3 destPos, Action<int, ObjectMotionManager.MotionType, Vector2> OnChangeMoveMotionValue, int motionID = -1, Action<int, ObjectMotionAni> onCompletedMotionAni = null)
	{
		if (startPos == destPos) {
			if (_objMotionAniList.Count == 0 && onCompletedMotionAni != null)
				onCompletedMotionAni (motionID, null);
			return;
		}

		float gapX = destPos.x - startPos.x;
		float gapY = destPos.y - startPos.y;
		float distance = Mathf.Sqrt((float)(Mathf.Pow(gapX, 2f) + Mathf.Pow(gapY, 2f)));

		for (int i = 0; i < _objMotionAniList.Count; i++) {
			if (_objMotionAniList [i].MotionID == motionID) {
				_objMotionAniList [i].SinA = gapY/distance;
				_objMotionAniList [i].CosA = gapX/distance;
				_objMotionAniList [i].Distance = distance;
				_objMotionAniList [i].StartDistance = distance;
				_objMotionAniList [i].CurMoveValue = 0f;
				return;
			}
		}

		ObjectMotionAni inputMotionAni = new ObjectMotionAni ();
		inputMotionAni.MotionID = motionID;
		inputMotionAni.SinA = gapY/distance;
		inputMotionAni.CosA = gapX/distance;
		inputMotionAni.Distance = distance;
		inputMotionAni.StartDistance = distance;
		inputMotionAni.SpeedValue = (distance / 0.3f);
		inputMotionAni.MotionType = MotionType.SpeedMoveMotion;
		inputMotionAni.OnChangeMoveMotionValue = OnChangeMoveMotionValue;
		inputMotionAni.OnCompletedMotion = onCompletedMotionAni;

		_objMotionAniList.Add (inputMotionAni);
	}

    public ObjectMotionAni AddObjSpeedUpMovement(Vector3 startPos, Vector3 destPos, float curSpeedValue, float changeSpeedValue, float destSpeedValue, Action<int, ObjectMotionManager.MotionType, Vector2> OnChangeMoveMotionValue, int motionID = -1, Action<int, ObjectMotionAni> onCompletedMotionAni = null)
    {
        if (startPos == destPos)
        {
            if (_objMotionAniList.Count == 0 && onCompletedMotionAni != null)
                onCompletedMotionAni(motionID, null);
            return null;
        }

        float gapX = destPos.x - startPos.x;
        float gapY = destPos.y - startPos.y;
        float distance = Mathf.Sqrt((float)(Mathf.Pow(gapX, 2f) + Mathf.Pow(gapY, 2f)));

        for (int i = 0; i < _objMotionAniList.Count; i++)
        {
            if (_objMotionAniList[i].MotionID == motionID)
            {
                _objMotionAniList[i].SinA = gapY / distance;
                _objMotionAniList[i].CosA = gapX / distance;
                _objMotionAniList[i].Distance = distance;
                _objMotionAniList[i].StartDistance = distance;
                _objMotionAniList[i].CurMoveValue = 0f;
                return _objMotionAniList[i];
            }
        }

        ObjectMotionAni inputMotionAni = new ObjectMotionAni();
        inputMotionAni.MotionID = motionID;
        inputMotionAni.SinA = gapY / distance;
        inputMotionAni.CosA = gapX / distance;
        inputMotionAni.Distance = distance;
        inputMotionAni.StartDistance = distance;
        //inputMotionAni.SpeedValue = curSpeedValue;
        inputMotionAni.Velocity = curSpeedValue;
        inputMotionAni.SpeedChangeValue = changeSpeedValue;
        inputMotionAni.SpeedDestValue = destSpeedValue;
        inputMotionAni.MotionType = MotionType.SpeedUpMotion;
        inputMotionAni.OnChangeMoveMotionValue = OnChangeMoveMotionValue;
        inputMotionAni.OnCompletedMotion = onCompletedMotionAni;

        _objMotionAniList.Add(inputMotionAni);

        return inputMotionAni;
    }

    public ObjectMotionAni AddObjSpeedDownMovement(Vector3 startPos, Vector3 destPos, float curSpeedValue, float changeSpeedValue, float destSpeedValue, Action<int, ObjectMotionManager.MotionType, Vector2> OnChangeMoveMotionValue, int motionID = -1, Action<int, ObjectMotionAni> onCompletedMotionAni = null)
    {
        if (startPos == destPos)
        {
            if (_objMotionAniList.Count == 0 && onCompletedMotionAni != null)
                onCompletedMotionAni(motionID, null);
            return null;
        }

        float gapX = destPos.x - startPos.x;
        float gapY = destPos.y - startPos.y;
        float distance = Mathf.Sqrt((float)(Mathf.Pow(gapX, 2f) + Mathf.Pow(gapY, 2f)));

        for (int i = 0; i < _objMotionAniList.Count; i++)
        {
            if (_objMotionAniList[i].MotionID == motionID)
            {
                _objMotionAniList[i].SinA = gapY / distance;
                _objMotionAniList[i].CosA = gapX / distance;
                _objMotionAniList[i].Distance = distance;
                _objMotionAniList[i].StartDistance = distance;
                _objMotionAniList[i].CurMoveValue = 0f;
                return _objMotionAniList[i];
            }
        }

        ObjectMotionAni inputMotionAni = new ObjectMotionAni();
        inputMotionAni.MotionID = motionID;
        inputMotionAni.SinA = gapY / distance;
        inputMotionAni.CosA = gapX / distance;
        inputMotionAni.Distance = distance;
        inputMotionAni.StartDistance = distance;
        //inputMotionAni.SpeedValue = curSpeedValue;
        inputMotionAni.Velocity = curSpeedValue;
        inputMotionAni.SpeedChangeValue = changeSpeedValue;
        inputMotionAni.SpeedDestValue = destSpeedValue;
        inputMotionAni.MotionType = MotionType.SpeedDownMotion;
        inputMotionAni.OnChangeMoveMotionValue = OnChangeMoveMotionValue;
        inputMotionAni.OnCompletedMotion = onCompletedMotionAni;

        _objMotionAniList.Add(inputMotionAni);

        return inputMotionAni;
    }

    public ObjectMotionAni AddObjUniformVelocityMovement(Vector3 startPos, Vector3 destPos, float speedValue, Action<int, ObjectMotionManager.MotionType, Vector2> OnChangeMoveMotionValue, int motionID = -1, Action<int, ObjectMotionAni> onCompletedMotionAni = null)
    {
        if (startPos == destPos)
        {
            if (_objMotionAniList.Count == 0 && onCompletedMotionAni != null)
                onCompletedMotionAni(motionID, null);
            return null;
        }

        float gapX = destPos.x - startPos.x;
        float gapY = destPos.y - startPos.y;
        float distance = Mathf.Sqrt((float)(Mathf.Pow(gapX, 2f) + Mathf.Pow(gapY, 2f)));

        for (int i = 0; i < _objMotionAniList.Count; i++)
        {
            if (_objMotionAniList[i].MotionID == motionID)
            {
                _objMotionAniList[i].SinA = gapY / distance;
                _objMotionAniList[i].CosA = gapX / distance;
                _objMotionAniList[i].Distance = distance;
                _objMotionAniList[i].StartDistance = distance;
                _objMotionAniList[i].CurMoveValue = 0f;
                return _objMotionAniList[i];
            }
        }

        ObjectMotionAni inputMotionAni = new ObjectMotionAni();
        inputMotionAni.MotionID = motionID;
        inputMotionAni.SinA = gapY / distance;
        inputMotionAni.CosA = gapX / distance;
        inputMotionAni.Distance = distance;
        inputMotionAni.StartDistance = distance;
        //inputMotionAni.SpeedValue = speedValue;
        inputMotionAni.Velocity = speedValue;
        inputMotionAni.MotionType = MotionType.UniformVelocityMotion;
        inputMotionAni.OnChangeMoveMotionValue = OnChangeMoveMotionValue;
        inputMotionAni.OnCompletedMotion = onCompletedMotionAni;

        _objMotionAniList.Add(inputMotionAni);

        return inputMotionAni;
    }

    public ObjectMotionAni GetObjMovement(int motionID)
    {
        if (_objMotionAniList.Count == 0)
            return null;

        for (int i = 0; i < _objMotionAniList.Count; i++)
        {
            if (_objMotionAniList[i].MotionID == motionID)
            {
                return _objMotionAniList[i];
            }
        }

        return null;
    }

    public void ReleaseObjMovement(int motionID)
	{
		if (_objMotionAniList.Count == 0)
			return;

		for (int i = 0; i < _objMotionAniList.Count; i++) {
			if (_objMotionAniList [i].MotionID == motionID) {
				if (_objMotionAniList [i].OnCompletedMotion != null) {
					_objMotionAniList [i].OnCompletedMotion (_objMotionAniList [i].MotionID, _objMotionAniList [i]);
				}
				_objMotionAniList.RemoveAt (i);
				break;
			}
		}
	}

    public void ReleaseAllObjMovement()
    {
        _objMotionAniList.Clear();
    }

    public bool CheckMotionState()
	{
		if (_objMotionAniList.Count > 0)
			return true;

		return false;
	}

	#endregion
}
