﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IBackKeyEvent
{
    void ExecuteBackKey();
}

public class BackKeyManager : MonoBehaviour, IUpdateable
{
    static BackKeyManager _instance;
    public static BackKeyManager Instance
    {
        get { return _instance; }
    }

    #region Variables

    bool _isEnableUpdate = false;
    bool _isEnableBackKey = true;

    List<IBackKeyEvent> _backKeyEventList = new List<IBackKeyEvent>();

    #endregion

    #region Properties

    public bool IsEnableUpdate
    {
        get { return _isEnableUpdate; }
        set { _isEnableUpdate = value; }
    }

    public bool IsEnableBackKey
    {
        get { return _isEnableBackKey; }
        set { _isEnableBackKey = value; }
    }

    #endregion

    #region MonoBehavior Methods

    void Start()
    {
        _isEnableUpdate = true;

        if (GameMainManager.Instance != null)
            GameMainManager.Instance.AddUpdateInfo(this);

        _instance = this;
    }

    private void OnDestroy()
    {
        if (GameMainManager.Instance != null)
            GameMainManager.Instance.RemoveUpdateInfo(this);
    }

    #endregion

    #region Methods

    public void OnUpdate()
    {
#if UNITY_EDITOR || UNITY_ANDROID
        if (_isEnableBackKey) {
            if (Input.GetKeyUp(KeyCode.Escape)) {
                if (_backKeyEventList.Count > 0) {
                    _backKeyEventList[_backKeyEventList.Count - 1].ExecuteBackKey();
                }
            }
        }
#endif
    }

    public void AddBackKeyEvent(IBackKeyEvent inputBackKeyEvent)
    {
        if(_backKeyEventList.Contains(inputBackKeyEvent))
            return;

        _backKeyEventList.Add(inputBackKeyEvent);
    }

    public void RemoveBackKeyEvent(IBackKeyEvent removeBackKeyEvent)
    {
        if(!_backKeyEventList.Contains(removeBackKeyEvent))
            return;

        if(!_isEnableBackKey)
            _isEnableBackKey = true;

        _backKeyEventList.Remove(removeBackKeyEvent);
    }

    #endregion
}
