﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameMainManager : MonoBehaviour 
{
	static GameMainManager _instance = null;
	public static GameMainManager Instance
	{
		get{ return _instance; }
	}

	public static void Exit()
	{
		_instance = null;
	}

	#region Serialize Variables

	#endregion

	#region Variables

	List<IUpdateable> _updateInfos = new List<IUpdateable>();

	#endregion

	#region Properties

	#endregion

	#region MonoBehaviour Methods

	void Awake()
	{
		_instance = this;
		DontDestroyOnLoad (this.gameObject);

        QualitySettings.vSyncCount = 0;  // VSync must be disabled
        Application.targetFrameRate = 60;
    }

    // Use this for initialization
    void Start () {
        InitSoundData();
        InitGameData();
    }
	
	// Update is called once per frame
	void Update () 
	{
		for (int i = 0; i < _updateInfos.Count; i++) {
			if (_updateInfos [i].IsEnableUpdate) {
				_updateInfos [i].OnUpdate ();
			}
		}
	}

	#endregion

	#region Methods

    public void InitSoundData()
    {
        SetEffectGroup_1();
    }

    public void InitGameData()
    {
        GameSceneManager.InitGameSceneManager();
    }

    void SetEffectGroup_1()
    {
        //AudioClip effectClip_1 = Resources.Load<AudioClip>(SoundFilePaths.SoundFXPath_1);
        //SoundManager.Instance.SetEffectAudioClip(SoundDefinitions.effectSound_1, effectClip_1);

        AudioClip effectClip_2 = Resources.Load<AudioClip>(SoundFilePaths.SoundFXPath_2);
        SoundManager.Instance.SetEffectAudioClip(SoundDefinitions.effectSound_2, effectClip_2);

        AudioClip effectClip_3 = Resources.Load<AudioClip>(SoundFilePaths.SoundFXPath_3);
        SoundManager.Instance.SetEffectAudioClip(SoundDefinitions.effectSound_3, effectClip_3);

        AudioClip effectClip_4 = Resources.Load<AudioClip>(SoundFilePaths.SoundFXPath_4);
        SoundManager.Instance.SetEffectAudioClip(SoundDefinitions.effectSound_4, effectClip_4);

        AudioClip effectClip_5 = Resources.Load<AudioClip>(SoundFilePaths.SoundFXPath_5);
        SoundManager.Instance.SetEffectAudioClip(SoundDefinitions.effectSound_5, effectClip_5);

        AudioClip effectClip_6 = Resources.Load<AudioClip>(SoundFilePaths.SoundFXPath_6);
        SoundManager.Instance.SetEffectAudioClip(SoundDefinitions.effectSound_6, effectClip_6);

        AudioClip effectClip_9 = Resources.Load<AudioClip>(SoundFilePaths.SoundFXPath_9);
        SoundManager.Instance.SetEffectAudioClip(SoundDefinitions.effectSound_9, effectClip_9);

        AudioClip effectClip_10 = Resources.Load<AudioClip>(SoundFilePaths.SoundFXPath_10);
        SoundManager.Instance.SetEffectAudioClip(SoundDefinitions.effectSound_10, effectClip_10);

        AudioClip effectClip_11 = Resources.Load<AudioClip>(SoundFilePaths.SoundFXPath_11);
        SoundManager.Instance.SetEffectAudioClip(SoundDefinitions.effectSound_11, effectClip_11);

        AudioClip effectClip_12 = Resources.Load<AudioClip>(SoundFilePaths.SoundFXPath_12);
        SoundManager.Instance.SetEffectAudioClip(SoundDefinitions.effectSound_12, effectClip_12);

        AudioClip effectClip_13 = Resources.Load<AudioClip>(SoundFilePaths.SoundFXPath_13);
        SoundManager.Instance.SetEffectAudioClip(SoundDefinitions.effectSound_13, effectClip_13);

        AudioClip effectClip_14 = Resources.Load<AudioClip>(SoundFilePaths.SoundFXPath_14);
        SoundManager.Instance.SetEffectAudioClip(SoundDefinitions.effectSound_14, effectClip_14);

        AudioClip effectClip_15 = Resources.Load<AudioClip>(SoundFilePaths.SoundFXPath_15);
        SoundManager.Instance.SetEffectAudioClip(SoundDefinitions.effectSound_15, effectClip_15);

        AudioClip effectClip_16 = Resources.Load<AudioClip>(SoundFilePaths.SoundFXPath_16);
        SoundManager.Instance.SetEffectAudioClip(SoundDefinitions.effectSound_16, effectClip_16);

        AudioClip effectClip_17 = Resources.Load<AudioClip>(SoundFilePaths.SoundFXPath_17);
        SoundManager.Instance.SetEffectAudioClip(SoundDefinitions.effectSound_17, effectClip_17);
    }

	public void AddUpdateInfo(IUpdateable addUpdate)
	{
		if (_updateInfos.Contains (addUpdate))
			return;

		_updateInfos.Add (addUpdate);
	}

	public void RemoveUpdateInfo(IUpdateable removeUpdate)
	{
		if (!_updateInfos.Contains (removeUpdate))
			return;

		_updateInfos.Remove (removeUpdate);
	}

	#endregion
}
