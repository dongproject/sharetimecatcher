﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;

public enum GameSceneEnum
{
	SCENE_LOGO,
	SCENE_TITLE,
	SCENE_STAGE,
	SCENE_GAMEPLAYING,
	SCENE_LOADING,
	SCENE_INFINITE_MODE,
    SCENE_ALBUM,
    SCENE_INFINITE_THEME,
}

public enum SceneChangeState
{
	SCENE_CHANGE_LOADINGSCENE,
	SCENE_CHANGE_DIRECT,
	SCENE_CHANGE_ASYNC,
}

public interface CAGameGlobalObserver
{
	void OnScenePopupState(bool curScenePopupState);
}

public class CAGameGlobal : ScriptableObject
{
	private static CAGameGlobal _instance = null;
	public static CAGameGlobal Instance
	{
		get
		{
//			if(_instance == null)
//			{
//				_instance = ScriptableObject.CreateInstance<CAGameGlobal>();
//			}
			return _instance;
		}
	}
	
	private CAGameGlobal()
	{
		
	}

	#region Variables

	public const string _BuildVersion = "0.0.1";
	GameSceneEnum m_CurrentSceneIndex;
	object m_CurSceneUIObject;
	string m_CurrentScene;
	bool m_GameScenePopupState = false;
	List<CAGameGlobalObserver> m_lCAGameGlobalOb = new List<CAGameGlobalObserver>();
	public const long m_SysTimeToSecond = 10000000;
    int _validStageNum = 30;
    int _validStageGroup = 3;

	int _currentStageNum;

    GameDynamicData _dynamicData = new GameDynamicData();
    bool _isStageHint = false;

    #endregion

    #region Properties

    public object curSceneUIObject
	{
		get{ return m_CurSceneUIObject; }
		set{ m_CurSceneUIObject = value; }
	}

	public GameSceneEnum currentSceneIndex
	{
		get{ return m_CurrentSceneIndex; }
		set{ m_CurrentSceneIndex = value; }
	}

	public bool gameScenePopupState
	{
		get{ return m_GameScenePopupState; }
		set
		{
			m_GameScenePopupState = value;
			NotifyGameScenePopupState(m_GameScenePopupState);
		}
	}

	public int CurrentStageNum
	{
		get{ return _currentStageNum; }
		set{ _currentStageNum = value; }
	}

    public GameDynamicData DynamicData
    {
        get { return _dynamicData; }
    }

    public bool IsStageHint
    {
        get { return _isStageHint; }
        set { _isStageHint = value; }
    }

    public int ValidStageNum
    {
        get{ return _validStageNum; }
    }

    public int ValidStageGroup
    {
        get { return _validStageGroup; }
    }

    #endregion

    #region Methods

    public static void InitGameGlobal()
	{
		if(_instance == null)
		{
			_instance = ScriptableObject.CreateInstance<CAGameGlobal>();
		}
	}

	public static void Exit()
	{
		_instance = null;
	}

	public void SetCurSceneUIObject(object tempCurSceneUIObject)
	{
		curSceneUIObject = tempCurSceneUIObject;
	}

	public void LoadGameScene(string sceneName)
	{
		m_CurrentScene = sceneName;
		SceneManager.LoadScene(m_CurrentScene);
	}

	public void LoadGameScene(GameSceneEnum tempSceneIndex)
	{
		currentSceneIndex = tempSceneIndex;
		SetCurrentScene(tempSceneIndex);
		SceneManager.LoadScene(m_CurrentScene);
	}

	public void LoadGameScene(GameSceneEnum tempSceneIndex, SceneChangeState tempChangeState)
	{
		currentSceneIndex = tempSceneIndex;
		SetCurrentScene(tempSceneIndex); 

		switch(tempChangeState)
		{
		case SceneChangeState.SCENE_CHANGE_LOADINGSCENE:

			break;
		case SceneChangeState.SCENE_CHANGE_DIRECT:
			SceneManager.LoadScene(m_CurrentScene);
			break;
		case SceneChangeState.SCENE_CHANGE_ASYNC:
			SceneManager.LoadSceneAsync(m_CurrentScene);
			break;
		}
	}

	void SetCurrentScene(GameSceneEnum tempSceneIndex)
	{
        switch (currentSceneIndex)
        {
            case GameSceneEnum.SCENE_LOGO:
                m_CurrentScene = "LogoScene";
                break;
            case GameSceneEnum.SCENE_TITLE:
                m_CurrentScene = "TitleScene";
                break;
            case GameSceneEnum.SCENE_STAGE:
                m_CurrentScene = "StageScene";
                break;
            case GameSceneEnum.SCENE_GAMEPLAYING:
                m_CurrentScene = "GamePlayScene";
                break;
            case GameSceneEnum.SCENE_INFINITE_MODE:
                m_CurrentScene = "InfiniteModeScene";
                break;
            case GameSceneEnum.SCENE_ALBUM:
                m_CurrentScene = "AlbumScene";
                break;
            case GameSceneEnum.SCENE_INFINITE_THEME:
                m_CurrentScene = "InfiniteThemeScene";
                break;
        }
    }

	public string GetCurrentScene()
	{
		return m_CurrentScene;
	}

	public void AttachGameGlobalOb(CAGameGlobalObserver inputGameGlobalOb)
	{
		if(m_lCAGameGlobalOb.Contains(inputGameGlobalOb))
			return;
		
		m_lCAGameGlobalOb.Add(inputGameGlobalOb);
	}

	public void DetachGameGlobalOb(CAGameGlobalObserver removeGameGlobalOb)
	{
		if(!m_lCAGameGlobalOb.Contains(removeGameGlobalOb))
			return;

		m_lCAGameGlobalOb.Remove(removeGameGlobalOb);
	}

	void NotifyGameScenePopupState(bool curPopupState)
	{
		for(int i = 0;i<m_lCAGameGlobalOb.Count;i++)
		{
			m_lCAGameGlobalOb[i].OnScenePopupState(curPopupState);
		}
	}

	#endregion
}






