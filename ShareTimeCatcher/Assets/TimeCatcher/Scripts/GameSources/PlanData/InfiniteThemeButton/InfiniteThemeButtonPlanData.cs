﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LitJson;


public struct InfiniteButtonPriceValue
{
	public InfiniteButtonPriceValue(int hourTime, int minTime, float secTime)
	{
		Hour = hourTime;
		Minute = minTime;
		Second = secTime;
	}

    public int Hour;
    public int Minute;
    public float Second;

    public void AddSecondValue(float addSec)
    {
        Second += addSec;
        int quotient = (int)Second / 60;
        if(quotient > 0) {
            Second = Second - (float)(quotient * 60);
            AddMinuteValue(quotient);
        }
    }

    public void AddMinuteValue(int addMin)
    {
        Minute += addMin;
        int quotient = (int)Minute / 60;
        if (quotient > 0) {
            Second = Second - (float)(quotient * 60);
            AddHourValue(quotient);
        }
    }

    public void AddHourValue(int addHour)
    {
        Hour += addHour;
    }

    public float GetTotalSecValue()
    {
        return (float)Hour * 3600f + (float)Minute * 60f + Second;
    }
}

public struct InfiniteThemeButtonInfo
{
	public InfiniteThemeButtonInfo(int num)
	{
        ButtonIndex = num;
        TimePrice = new InfiniteButtonPriceValue(0, 0, 0f);
    }

	public int ButtonIndex;
	public InfiniteButtonPriceValue TimePrice;
}

public class InfiniteThemeButtonPlanData : GameBasePlanData
{
	#region Variables

	Dictionary<int /* Button Index */, InfiniteThemeButtonInfo> _infiniteButtonInfos = new Dictionary<int, InfiniteThemeButtonInfo>();

	#endregion

	#region Properties

	public override string GetPlanDataName 
	{
		get{ return "InfiniteThemeButtonPlan"; }
	}

	public Dictionary<int /* Button Index */, InfiniteThemeButtonInfo> InfiniteButtonInfos
    {
		get{ return _infiniteButtonInfos; }
	}

	#endregion

	#region Methods

	public override void LoadPlanData()
	{
		string textResPath = string.Format("PlanData/{0}", GetPlanDataName);
		TextAsset infiniteButtonTextAsset = Resources.Load<TextAsset> (textResPath);

		if (infiniteButtonTextAsset == null) {
			Debug.Log (string.Format ("LoadPlanData -> Failed Load TextAsset PlanData Path : {0}", textResPath));
			return;
		}

		string readString = infiniteButtonTextAsset.ToString ();
		Debug.Log (string.Format ("InfiniteThemeButtonPlanData LoadPlanData readString : {0}", readString));

		JsonReader reader = new JsonReader (readString);
		int objectCount = 0;
		int arrayCount = 0;
		while (reader.Read ()) {
			switch (reader.Token) {
			case JsonToken.ObjectStart:
				objectCount++;
				break;
			case JsonToken.ObjectEnd:
				objectCount--;
				break;
			case JsonToken.ArrayStart:
				arrayCount++;
				break;
			case JsonToken.ArrayEnd:
				arrayCount--;
				break;
			}

			if (reader.Token == JsonToken.PropertyName && (string)reader.Value == "InfiniteButtons") {
                InfiniteThemeButtonInfo infiniteButtonInfo = new InfiniteThemeButtonInfo(0);
				while (reader.Read ()) {
					switch (reader.Token) {
					case JsonToken.ObjectStart:
						objectCount++;
						break;
					case JsonToken.ObjectEnd:
						objectCount--;
						break;
					case JsonToken.ArrayStart:
						arrayCount++;
						break;
					case JsonToken.ArrayEnd:
						arrayCount--;
						break;
					}

					if (reader.Token == JsonToken.ArrayEnd && arrayCount == 0) {
						break;
					} else if (reader.Token == JsonToken.ObjectStart && objectCount == 2) {
                        infiniteButtonInfo = new InfiniteThemeButtonInfo(0);
					} else if (reader.Token == JsonToken.ObjectEnd && objectCount == 1) {
                        _infiniteButtonInfos.Add (infiniteButtonInfo.ButtonIndex, infiniteButtonInfo);
					} else if (reader.Token == JsonToken.PropertyName && (string)reader.Value == "ButtonIndex") {
						reader.Read ();
                        infiniteButtonInfo.ButtonIndex = (int)reader.Value;
					} else if (reader.Token == JsonToken.PropertyName && (string)reader.Value == "TimePrice") {
						while (reader.Read ()) {
							switch (reader.Token) {
							case JsonToken.ObjectStart:
								objectCount++;
								break;
							case JsonToken.ObjectEnd:
								objectCount--;
								break;
							case JsonToken.ArrayStart:
								arrayCount++;
								break;
							case JsonToken.ArrayEnd:
								arrayCount--;
								break;
							}

							if (reader.Token == JsonToken.ObjectEnd && objectCount == 2) {
								break;
							} else if (reader.Token == JsonToken.PropertyName && (string)reader.Value == "Hour") {
								reader.Read ();
                                infiniteButtonInfo.TimePrice.Hour = (int)reader.Value;
							} else if (reader.Token == JsonToken.PropertyName && (string)reader.Value == "Minute") {
								reader.Read ();
                                infiniteButtonInfo.TimePrice.Minute = (int)reader.Value;
							} else if (reader.Token == JsonToken.PropertyName && (string)reader.Value == "Second") {
								reader.Read ();
								if (reader.Token == JsonToken.Int) {
                                    infiniteButtonInfo.TimePrice.Second = (float)(int)reader.Value;
								} else if (reader.Token == JsonToken.Double) {
                                    infiniteButtonInfo.TimePrice.Second = (float)(double)reader.Value;
								}
							}
						} // while (reader.Read ()) {
                    } // else if (reader.Token == JsonToken.PropertyName && (string)reader.Value == "TimePrice") {
                } // while (reader.Read ()) {
            } // if (reader.Token == JsonToken.PropertyName && (string)reader.Value == "InfiniteButtons") {
            else if (reader.Token == JsonToken.ObjectEnd && objectCount == 0) {
				break;
			}
		}
	}

	public override void ReleasePlanData()
	{
        _infiniteButtonInfos.Clear ();
	}

	#endregion
}
