﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GamePlanDataManager
{
	private static GamePlanDataManager _instance = null;
	public static GamePlanDataManager Instance
	{
		get{
			if (_instance == null) {
				_instance = new GamePlanDataManager ();
			}
			return _instance;
		}
	}

	#region Variables

	StopTimeStagePlanData _stopTimeStagePlan;
    InfiniteThemeButtonPlanData _infiniteButtonPlanData;

	#endregion

	#region Properties

	public StopTimeStagePlanData StopTimeStagePlan
	{
		get{ return _stopTimeStagePlan; }
	}

    public InfiniteThemeButtonPlanData InfiniteButtonPlanData
    {
        get { return _infiniteButtonPlanData; }
    }

    #endregion

    #region Methods

    public void LoadStartGamePlanData()
    {
        LoadStopTimeStagePlanData();
        LoadInfiniteButtonPlanData();
    }

    public void LoadStopTimeStagePlanData()
	{
		_stopTimeStagePlan = new StopTimeStagePlanData ();
		_stopTimeStagePlan.LoadPlanData ();
	}

    public void LoadInfiniteButtonPlanData()
    {
        _infiniteButtonPlanData = new InfiniteThemeButtonPlanData();
        _infiniteButtonPlanData.LoadPlanData();
    }

    public void ReleaseStopTimeStagePlanData()
	{
        if (_stopTimeStagePlan != null)
            _stopTimeStagePlan.ReleasePlanData();
	}

    public void ReleaseInfiniteButtonPlanData()
    {
        if (_infiniteButtonPlanData != null)
            _infiniteButtonPlanData.ReleasePlanData();
    }

    #endregion
}
