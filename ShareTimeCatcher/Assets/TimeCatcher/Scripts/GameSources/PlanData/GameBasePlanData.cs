﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameBasePlanData
{
	#region Variables

	#endregion

	#region Properties

	public virtual string GetPlanDataName {
		get{ return ""; }
	}

	#endregion

	#region Methods

	public virtual void LoadPlanData(){}
	public virtual void ReleasePlanData(){}

	#endregion
}
