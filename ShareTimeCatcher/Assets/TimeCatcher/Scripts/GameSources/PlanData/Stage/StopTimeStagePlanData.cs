﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LitJson;

public struct StopTimeStageValues
{
	public StopTimeStageValues(int stageValueType, float stageValue)
	{
		ValueType = stageValueType;
		Value = stageValue;
	}

	public int ValueType;
	public float Value;
}

public struct StopTimeValues
{
	public StopTimeValues(int hourTime, int minTime, float secTime)
	{
		Hour = hourTime;
		Minute = minTime;
		Second = secTime;
	}

    public int Hour;
    public int Minute;
    public float Second;

    public void AddSecondValue(float addSec)
    {
        Second += addSec;
        int quotient = (int)Second / 60;
        if(quotient > 0) {
            Second = Second - (float)(quotient * 60);
            AddMinuteValue(quotient);
        }
    }

    public void AddMinuteValue(int addMin)
    {
        Minute += addMin;
        int quotient = (int)Minute / 60;
        if (quotient > 0) {
            Second = Second - (float)(quotient * 60);
            AddHourValue(quotient);
        }
    }

    public void AddHourValue(int addHour)
    {
        Hour += addHour;
    }
}

public struct StopTimeStageInfo
{
	public StopTimeStageInfo(int num)
	{
		StageNum = num;
		StageType = 0;
		TargetTime = new StopTimeValues (0, 0, 0f);
		RealTime = new StopTimeValues (0, 0, 0f);
		StageValues = new Dictionary<int, StopTimeStageValues> ();
		fakeTimeValues = new List<StopTimeValues> ();
        albumOpens = new List<string>();
    }

	public int StageNum;
	public int StageType;
	public StopTimeValues TargetTime;
	public StopTimeValues RealTime;
	public Dictionary<int /* Value Type */, StopTimeStageValues> StageValues;
	public List<StopTimeValues> fakeTimeValues;
    public List<string> albumOpens;
}

public class StopTimeStagePlanData : GameBasePlanData
{
	#region Variables

	Dictionary<int /* Stage Number */, StopTimeStageInfo> _stopTimeStageInfos = new Dictionary<int, StopTimeStageInfo>();

	#endregion

	#region Properties

	public override string GetPlanDataName 
	{
		get{ return "StageInfoPlan"; }
	}

	public Dictionary<int /* Stage Number */, StopTimeStageInfo> StopTimeStageInfos
	{
		get{ return _stopTimeStageInfos; }
	}

	#endregion

	#region Methods

	public override void LoadPlanData()
	{
		string textResPath = string.Format("PlanData/{0}", GetPlanDataName);
		TextAsset stageInfoTextAsset = Resources.Load<TextAsset> (textResPath);

		if (stageInfoTextAsset == null) {
			Debug.Log (string.Format ("LoadPlanData -> Failed Load TextAsset PlanData Path : {0}", textResPath));
			return;
		}

		string readString = stageInfoTextAsset.ToString ();
		Debug.Log (string.Format ("StopTimeStagePlanData LoadPlanData readString : {0}", readString));

		JsonReader reader = new JsonReader (readString);
		int objectCount = 0;
		int arrayCount = 0;
		while (reader.Read ()) {
			switch (reader.Token) {
			case JsonToken.ObjectStart:
				objectCount++;
				break;
			case JsonToken.ObjectEnd:
				objectCount--;
				break;
			case JsonToken.ArrayStart:
				arrayCount++;
				break;
			case JsonToken.ArrayEnd:
				arrayCount--;
				break;
			}

			if (reader.Token == JsonToken.PropertyName && (string)reader.Value == "StageInfos") {
				StopTimeStageInfo inputTimeStageInfo = new StopTimeStageInfo (0);
				while (reader.Read ()) {
					switch (reader.Token) {
					case JsonToken.ObjectStart:
						objectCount++;
						break;
					case JsonToken.ObjectEnd:
						objectCount--;
						break;
					case JsonToken.ArrayStart:
						arrayCount++;
						break;
					case JsonToken.ArrayEnd:
						arrayCount--;
						break;
					}

					if (reader.Token == JsonToken.ArrayEnd && arrayCount == 0) {
						break;
					} else if (reader.Token == JsonToken.ObjectStart && objectCount == 2) {
						inputTimeStageInfo = new StopTimeStageInfo (0);
					} else if (reader.Token == JsonToken.ObjectEnd && objectCount == 1) {
						_stopTimeStageInfos.Add (inputTimeStageInfo.StageNum, inputTimeStageInfo);
					} else if (reader.Token == JsonToken.PropertyName && (string)reader.Value == "StageNum") {
						reader.Read ();
						inputTimeStageInfo.StageNum = (int)reader.Value;
					} else if (reader.Token == JsonToken.PropertyName && (string)reader.Value == "StageType") {
						reader.Read ();
						inputTimeStageInfo.StageType = (int)reader.Value;
					} else if (reader.Token == JsonToken.PropertyName && (string)reader.Value == "TargetTime") {
						while (reader.Read ()) {
							switch (reader.Token) {
							case JsonToken.ObjectStart:
								objectCount++;
								break;
							case JsonToken.ObjectEnd:
								objectCount--;
								break;
							case JsonToken.ArrayStart:
								arrayCount++;
								break;
							case JsonToken.ArrayEnd:
								arrayCount--;
								break;
							}

							if (reader.Token == JsonToken.ObjectEnd && objectCount == 2) {
								break;
							} else if (reader.Token == JsonToken.PropertyName && (string)reader.Value == "Hour") {
								reader.Read ();
								inputTimeStageInfo.TargetTime.Hour = (int)reader.Value;
							} else if (reader.Token == JsonToken.PropertyName && (string)reader.Value == "Minute") {
								reader.Read ();
								inputTimeStageInfo.TargetTime.Minute = (int)reader.Value;
							} else if (reader.Token == JsonToken.PropertyName && (string)reader.Value == "Second") {
								reader.Read ();
								if (reader.Token == JsonToken.Int) {
									inputTimeStageInfo.TargetTime.Second = (float)(int)reader.Value;
								} else if (reader.Token == JsonToken.Double) {
									inputTimeStageInfo.TargetTime.Second = (float)(double)reader.Value;
								}
							}
						} // while (reader.Read ()) {
					} // else if (reader.Token == JsonToken.PropertyName && (string)reader.Value == "TargetTime") {
					else if (reader.Token == JsonToken.PropertyName && (string)reader.Value == "RealTime") {
						while (reader.Read ()) {
							switch (reader.Token) {
							case JsonToken.ObjectStart:
								objectCount++;
								break;
							case JsonToken.ObjectEnd:
								objectCount--;
								break;
							case JsonToken.ArrayStart:
								arrayCount++;
								break;
							case JsonToken.ArrayEnd:
								arrayCount--;
								break;
							}

							if (reader.Token == JsonToken.ObjectEnd && objectCount == 2) {
								break;
							} else if (reader.Token == JsonToken.PropertyName && (string)reader.Value == "Hour") {
								reader.Read ();
								inputTimeStageInfo.RealTime.Hour = (int)reader.Value;
							} else if (reader.Token == JsonToken.PropertyName && (string)reader.Value == "Minute") {
								reader.Read ();
								inputTimeStageInfo.RealTime.Minute = (int)reader.Value;
							} else if (reader.Token == JsonToken.PropertyName && (string)reader.Value == "Second") {
								reader.Read ();
								if (reader.Token == JsonToken.Int) {
									inputTimeStageInfo.RealTime.Second = (float)(int)reader.Value;
								} else if (reader.Token == JsonToken.Double) {
									inputTimeStageInfo.RealTime.Second = (float)(double)reader.Value;
								}
							}
						} // while (reader.Read ()) {
					} // else if (reader.Token == JsonToken.PropertyName && (string)reader.Value == "RealTime") {
					else if (reader.Token == JsonToken.PropertyName && (string)reader.Value == "StageValues") {
						StopTimeStageValues inputStageValues = new StopTimeStageValues (0, 0f);
						while (reader.Read ()) {
							switch (reader.Token) {
							case JsonToken.ObjectStart:
								objectCount++;
								break;
							case JsonToken.ObjectEnd:
								objectCount--;
								break;
							case JsonToken.ArrayStart:
								arrayCount++;
								break;
							case JsonToken.ArrayEnd:
								arrayCount--;
								break;
							}

							if (reader.Token == JsonToken.ArrayEnd && arrayCount == 1) {
								break;
							} else if (reader.Token == JsonToken.ObjectStart && objectCount == 3) {
								inputStageValues = new StopTimeStageValues (0, 0f);
							} else if (reader.Token == JsonToken.ObjectEnd && objectCount == 2) {
								inputTimeStageInfo.StageValues.Add (inputStageValues.ValueType, inputStageValues);
							} else if (reader.Token == JsonToken.PropertyName && (string)reader.Value == "Type") {
								reader.Read ();
								inputStageValues.ValueType = (int)reader.Value;
							} else if (reader.Token == JsonToken.PropertyName && (string)reader.Value == "Value") {
								reader.Read ();
								if (reader.Token == JsonToken.Int) {
									inputStageValues.Value = (float)(int)reader.Value;
								} else if (reader.Token == JsonToken.Double) {
									inputStageValues.Value = (float)(double)reader.Value;
								}
							}
						}
					} // else if (reader.Token == JsonToken.PropertyName && (string)reader.Value == "StageValues") {
					else if (reader.Token == JsonToken.PropertyName && (string)reader.Value == "FakeTimes") {
						StopTimeValues inputTimeValues = new StopTimeValues (0, 0, 0f);
						while (reader.Read ()) {
							switch (reader.Token) {
							case JsonToken.ObjectStart:
								objectCount++;
								break;
							case JsonToken.ObjectEnd:
								objectCount--;
								break;
							case JsonToken.ArrayStart:
								arrayCount++;
								break;
							case JsonToken.ArrayEnd:
								arrayCount--;
								break;
							}

							if (reader.Token == JsonToken.ArrayEnd && arrayCount == 1) {
								break;
							} else if (reader.Token == JsonToken.ObjectStart && objectCount == 3) {
								inputTimeValues = new StopTimeValues (0, 0, 0f);
							} else if (reader.Token == JsonToken.ObjectEnd && objectCount == 2) {
								inputTimeStageInfo.fakeTimeValues.Add (inputTimeValues);
							} else if (reader.Token == JsonToken.PropertyName && (string)reader.Value == "Hour") {
								reader.Read ();
								inputTimeValues.Hour = (int)reader.Value;
							} else if (reader.Token == JsonToken.PropertyName && (string)reader.Value == "Minute") {
								reader.Read ();
								inputTimeValues.Minute = (int)reader.Value;
							} else if (reader.Token == JsonToken.PropertyName && (string)reader.Value == "Second") {
								reader.Read ();
								if (reader.Token == JsonToken.Int) {
									inputTimeValues.Second = (float)(int)reader.Value;
								} else if (reader.Token == JsonToken.Double) {
									inputTimeValues.Second = (float)(double)reader.Value;
								}
							}
						}
					} // else if (reader.Token == JsonToken.PropertyName && (string)reader.Value == "FakeTimes") {
                    else if (reader.Token == JsonToken.PropertyName && (string)reader.Value == "AlbumOpen")
                    {
                        while (reader.Read())
                        {
                            switch (reader.Token)
                            {
                                case JsonToken.ObjectStart:
                                    objectCount++;
                                    break;
                                case JsonToken.ObjectEnd:
                                    objectCount--;
                                    break;
                                case JsonToken.ArrayStart:
                                    arrayCount++;
                                    break;
                                case JsonToken.ArrayEnd:
                                    arrayCount--;
                                    break;
                            }

                            if (reader.Token == JsonToken.ArrayEnd && arrayCount == 1)
                            {
                                break;
                            } 
                            else if(reader.Token == JsonToken.String)
                            {
                                inputTimeStageInfo.albumOpens.Add((string)reader.Value);
                            }
                        }
                    } // else if (reader.Token == JsonToken.PropertyName && (string)reader.Value == "StageValues")

                } // while (reader.Read ()) {
			} // if (reader.Token == JsonToken.PropertyName && (string)reader.Value == "StageInfos") {
			else if (reader.Token == JsonToken.ObjectEnd && objectCount == 0) {
				break;
			}
		}
	}

	public override void ReleasePlanData()
	{
		_stopTimeStageInfos.Clear ();
	}

	#endregion
}
