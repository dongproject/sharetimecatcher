﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StagePlanDefinitions
{
    public enum StagePlanValueType
    {
        HitObjectTotalHP        = 1,
        HitObjectPerHPValue     = 2,
        ButtonHPMax             = 3,
        ButtonHPPerValue        = 4,
    }
}
