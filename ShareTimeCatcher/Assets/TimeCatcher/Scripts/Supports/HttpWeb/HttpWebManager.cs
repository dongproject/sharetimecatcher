﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class HttpWebManager : MonoBehaviour 
{
    static HttpWebManager _instance = null;
    public static HttpWebManager Instance
    {
        get { return _instance; }
    }

    private void Awake()
    {
        _instance = this;
        DontDestroyOnLoad(this.gameObject);
    }

    #region Variables

    Action<string, bool> _onReceiveUnityWeb;

    #endregion

    #region UnityWebRequest Methods

    public void RequestChatUnityWeb(string url, string requestJson, Action<string, bool> onReceiveWeb)     {         _onReceiveUnityWeb = onReceiveWeb;         StartCoroutine(UploadUnityWeb(url, requestJson));     }      IEnumerator UploadUnityWeb(string url, string requestJson)     {
        UnityWebRequest www = new UnityWebRequest(url, "POST");
        byte[] bodyRaw = new System.Text.UTF8Encoding().GetBytes(requestJson);
        www.uploadHandler = (UploadHandler)new UploadHandlerRaw(bodyRaw);
        www.downloadHandler = (DownloadHandler)new DownloadHandlerBuffer();
        www.SetRequestHeader("Content-Type", "application/json");
        www.SetRequestHeader("Cache-Control", "max-age=0, no-cache, no-store");
        www.SetRequestHeader("Pragma", "no-cache");

        www.timeout = 10;
         yield return www.SendWebRequest();          if (www.isNetworkError || www.isHttpError) { #if _CHATTING_LOG             Debug.Log(www.error); #endif             if (_onReceiveUnityWeb != null)                 _onReceiveUnityWeb(www.error, false);

            www.Dispose();         } else {             string receiveText = www.downloadHandler.text; #if _CHATTING_LOG             Debug.Log(string.Format("Form upload complete! receiveText : {0}", receiveText)); #endif             if (_onReceiveUnityWeb != null)                 _onReceiveUnityWeb(receiveText, true);

            www.Dispose();         }     }      #endregion
}
