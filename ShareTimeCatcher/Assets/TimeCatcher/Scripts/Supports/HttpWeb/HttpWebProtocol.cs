﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Net;
using System.IO;
using System;
using UnityEngine.Networking;

public class HttpWebProtocol<TReq, TRes> where TReq : RequestParam where TRes : ResponseParam
{
	public HttpWebProtocol(Action<TRes> onSuccessAction, Action<TRes> onFailAction)
	{
		_onSuccessChatHttp = onSuccessAction;
		_onFailChatHttp = onFailAction;
	}

	#region Variables

	protected Action<TRes> _onSuccessChatHttp;
	protected Action<TRes> _onFailChatHttp;

    string _requestData;
	protected TRes _resParam = null;

	#endregion

	#region Properties

	protected virtual string Url { get{ return ""; } }

	#endregion

	#region Methods

	protected virtual string GetRequestJsonString()
	{
		return "";
	}

	protected virtual void SetResponseJsonData(string result)
	{
        try {
            _resParam = LitJson.JsonMapper.ToObject<TRes>(result);
        } catch (System.Exception exception) {
            Debug.Log("Deserialize exception : " + exception.ToString());
            Debug.Log(string.Format("#Error Deserialize. Request={0}, Response={1}/n Stack={2}", _requestData, result, exception.StackTrace));
        }
    }

    public virtual void RequestHttpWeb()
    {
        SetRequestData();

        try {
            _requestData = GetRequestJsonString();

            HttpWebManager.Instance.RequestChatUnityWeb(Url, _requestData, OnReceiveUnityWeb);
        } catch (Exception e) {
            Debug.Log(string.Format("RequestHttpWeb {0}", e.ToString()));
            if (_onFailChatHttp != null)
                _onFailChatHttp(_resParam);
        }
    }

    void OnReceiveUnityWeb(string receiveText, bool isSuccess)
    {
        if(!isSuccess) {
            Debug.Log(string.Format("OnReceiveUnityWeb Error : {0}", receiveText));
            if (_onFailChatHttp != null)
                _onFailChatHttp(_resParam);
        }

        try {
            SetResponseJsonData(receiveText);

            if (_resParam.result_code == (int)HttpResultCode.Success) {

                if (_onSuccessChatHttp != null) {
                    _onSuccessChatHttp(_resParam);
                }

                OnSuccess(_resParam);
            } else {
                if (_onFailChatHttp != null) {
                    _onFailChatHttp(_resParam);
                }

                OnFail(_resParam);
            }
        } catch (Exception e) {
            Debug.Log(string.Format("RequestHttpWeb {0}", e.ToString()));
            if (_onFailChatHttp != null)
                _onFailChatHttp(_resParam);
        }
    }

    protected virtual void SetRequestData()
	{
	}

	protected virtual void OnSuccess(TRes resParam)
	{

	}

	protected virtual void OnFail(TRes resParam)
	{

	}

	#endregion
}
