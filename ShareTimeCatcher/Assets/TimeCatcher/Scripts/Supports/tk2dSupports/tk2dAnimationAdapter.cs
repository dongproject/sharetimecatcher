﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class tk2dAnimationAdapter : MonoBehaviour 
{
	Color color = Color.white;
	Vector3 scale = Vector3.one;
	tk2dBaseSprite sprite = null;
	tk2dTextMesh textMesh = null;

	public Color textColor = Color.white;
	public Vector3 textScale = Vector3.one;

	tk2dTiledSprite tiledSprite = null;
	Vector2 dimensions;

	void Awake()
	{
		sprite = GetComponent<tk2dBaseSprite>();
		textMesh = GetComponent<tk2dTextMesh>();
		if (sprite != null) {
			color = sprite.color;
			scale = sprite.scale;
		}
		if (textMesh != null) {
			textColor = textMesh.color;
			textScale = textMesh.scale;
		}

		tiledSprite = GetComponent<tk2dTiledSprite>();
		if (tiledSprite != null) {
			dimensions = tiledSprite.dimensions;
		}
	}
	
	// Update is called once per frame
	void LateUpdate () {
		DoUpdate();
	}

	void DoUpdate() {
		if (sprite != null && (sprite.color != color || sprite.scale != scale)) {
			color = sprite.color;
			scale = sprite.scale;
			sprite.Build();
		}
		if (textMesh != null && (textMesh.color != textColor || textMesh.scale != textScale)) {
			textMesh.color = textColor;
			textMesh.scale = textScale;
			textMesh.Commit();
		}

		if (tiledSprite != null && tiledSprite.dimensions != dimensions) {
			dimensions = tiledSprite.dimensions;
			tiledSprite.Build();
		}
	}
}
