﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public delegate void GameTimerCBFunc(object objData = null);

public class GameTimerEventData
{
	#region Definitions

	public enum TimerType
	{
		eNormal,
		eInterval,
	}

	#endregion

	#region Properties

	public TimerType gameTimerType
	{
		get{ return m_GameTimerType; }
		set{ m_GameTimerType = value; }
	}

	public GameTimerCBFunc gameTimerCBFunc
	{
		get{ return m_GameTimerCBFunc; }
		set{ m_GameTimerCBFunc = value; }
	}

	public object objData
	{
		get{ return m_ObjData; }
		set{ m_ObjData = value; }
	}

	public float eventCalcSecTime
	{
		get{ return m_EventCalcSecTime; }
		set{ m_EventCalcSecTime = value; }
	}

	public int timerID
	{
		get{ return m_TimerID; }
		set{ m_TimerID = value; }
	}

	public float intervalTime
	{
		get{ return m_IntervalTime; }
		set{ m_IntervalTime = value; }
	}

	public float curInterTime
	{
		get{ return m_CurInterTime; }
		set{ m_CurInterTime = value; }
	}

	public GameTimerCBFunc intervalCBFunc
	{
		get{ return m_IntervalCBFunc; }
		set{ m_IntervalCBFunc = value; }
	}

	public object interObjData
	{
		get{ return m_InterObjData; }
		set{ m_InterObjData = value; }
	}

	public int skillType
	{
		get{ return m_SkillType; }
		set{ m_SkillType = value; }
	}

	#endregion

	#region Variables

	TimerType m_GameTimerType = TimerType.eNormal;
	GameTimerCBFunc m_GameTimerCBFunc = null;
	object m_ObjData;
	float m_EventCalcSecTime = 0f;
	int m_TimerID = -1;
	float m_IntervalTime = 0f;
	float m_CurInterTime = 0f;
	GameTimerCBFunc m_IntervalCBFunc = null;
	object m_InterObjData = null;
	int m_SkillType;

	#endregion
}

public class CAGameTimerEventCtl
{

	#region Methods

	public void UpdateGameTimer()
	{
		if(m_GameTimerCBFuncList.Count == 0)
			return;

		GameTimerEventData completeEventData = null;
		for(int i = 0;i<m_GameTimerCBFuncList.Count;i++)
		{
			GameTimerEventData tempTimerEventData = m_GameTimerCBFuncList [i];
			tempTimerEventData.eventCalcSecTime -= Time.deltaTime;
			if(tempTimerEventData.eventCalcSecTime < 0f)
			{
				completeEventData = tempTimerEventData;
			}

			if (tempTimerEventData.gameTimerType == GameTimerEventData.TimerType.eInterval) {
				tempTimerEventData.curInterTime += Time.deltaTime;
				if (tempTimerEventData.intervalTime <= tempTimerEventData.curInterTime) {
					tempTimerEventData.curInterTime = 0f;
					if(tempTimerEventData.intervalCBFunc != null)
						tempTimerEventData.intervalCBFunc (tempTimerEventData.interObjData);
				}
			}
		}

		CompleteGameTimerEvent(completeEventData);
	}

	public void UpdateOnlyInterval()
	{
		if(m_GameTimerCBFuncList.Count == 0)
			return;

		for(int i = 0;i<m_GameTimerCBFuncList.Count;i++)
		{
			GameTimerEventData tempTimerEventData = m_GameTimerCBFuncList [i];

			tempTimerEventData.curInterTime += Time.deltaTime;
			if (tempTimerEventData.intervalTime <= tempTimerEventData.curInterTime) {
				tempTimerEventData.curInterTime = 0f;
				if(tempTimerEventData.intervalCBFunc != null)
					tempTimerEventData.intervalCBFunc (tempTimerEventData.interObjData);
			}
		}
	}

	public void SetGameTimerData(GameTimerCBFunc tempGameTimerCBFunc, float tempEventCalcSecTime, object tempObjData = null, int timerID = -1, int skillType = -1)
	{
		bool existState = false;
		if(timerID != -1)
		{
			for(int i = 0;i<m_GameTimerCBFuncList.Count;i++)
			{
				if(m_GameTimerCBFuncList[i].timerID == timerID)
				{
					existState = true;
					m_GameTimerCBFuncList[i].eventCalcSecTime = tempEventCalcSecTime;
					break;
				}
			}
		}

		if(!existState)
		{
			GameTimerEventData inputTimerEventData = new GameTimerEventData();
			inputTimerEventData.gameTimerType = GameTimerEventData.TimerType.eNormal;
			inputTimerEventData.gameTimerCBFunc = tempGameTimerCBFunc;
			inputTimerEventData.eventCalcSecTime = tempEventCalcSecTime;
			inputTimerEventData.objData = tempObjData;
			inputTimerEventData.timerID = timerID;
			inputTimerEventData.skillType = skillType;
			m_GameTimerCBFuncList.Add(inputTimerEventData);
		}

	}

	public void SetIntervalTimerData(GameTimerCBFunc tempGameTimerCBFunc, float tempEventCalcSecTime, float intervalTime, GameTimerCBFunc intervalCBFunc, object tempObjData = null, object interObjData = null, int timerID = -1, int skillType = -1)
	{
		bool existState = false;
		if(timerID != -1)
		{
			for(int i = 0;i<m_GameTimerCBFuncList.Count;i++)
			{
				if(m_GameTimerCBFuncList[i].timerID == timerID)
				{
					existState = true;
					if(tempEventCalcSecTime > m_GameTimerCBFuncList[i].eventCalcSecTime)
						m_GameTimerCBFuncList[i].eventCalcSecTime = tempEventCalcSecTime;
					break;
				}
			}
		}

		if(!existState)
		{
			GameTimerEventData inputTimerEventData = new GameTimerEventData();
			inputTimerEventData.gameTimerType = GameTimerEventData.TimerType.eInterval;
			inputTimerEventData.gameTimerCBFunc = tempGameTimerCBFunc;
			inputTimerEventData.eventCalcSecTime = tempEventCalcSecTime;
			inputTimerEventData.intervalCBFunc = intervalCBFunc;
			inputTimerEventData.intervalTime = intervalTime;
			inputTimerEventData.objData = tempObjData;
			inputTimerEventData.interObjData = interObjData;
			inputTimerEventData.timerID = timerID;
			inputTimerEventData.skillType = skillType;
			m_GameTimerCBFuncList.Add(inputTimerEventData);
		}
	}

	public void CompleteGameTimerEvent(GameTimerEventData completeTimerEventData)
	{
		if(completeTimerEventData == null)
			return;

		if(completeTimerEventData.gameTimerCBFunc != null)
			completeTimerEventData.gameTimerCBFunc(completeTimerEventData.objData);
		m_GameTimerCBFuncList.Remove(completeTimerEventData);
	}

	public void ReleaseGameTimerList()
	{
		m_GameTimerCBFuncList.Clear();
	}

	public void CompleteGameTimerList()
	{
		for (int i = 0; i < m_GameTimerCBFuncList.Count; i++) {
			if (m_GameTimerCBFuncList [i].gameTimerCBFunc != null) {
				m_GameTimerCBFuncList [i].gameTimerCBFunc(m_GameTimerCBFuncList [i].objData);
			}
		}

		m_GameTimerCBFuncList.Clear();
	}

    public void CompleteTimerID(int timerID)
    {
        for (int i = 0; i < m_GameTimerCBFuncList.Count; i++)
        {
            GameTimerEventData tempTimerEventData = m_GameTimerCBFuncList[i];
            if (tempTimerEventData.timerID == timerID)
            {
                if (m_GameTimerCBFuncList[i].gameTimerCBFunc != null)
                {
                    m_GameTimerCBFuncList[i].gameTimerCBFunc(m_GameTimerCBFuncList[i].objData);
                }
                m_GameTimerCBFuncList.Remove(tempTimerEventData);
                break;
            }
        }
    }

    public void RemoveTimeEventByID(int timerID)
	{
		for (int i = 0; i < m_GameTimerCBFuncList.Count; i++) {
			GameTimerEventData tempTimerEventData = m_GameTimerCBFuncList [i];
			if (tempTimerEventData.timerID == timerID) {
				m_GameTimerCBFuncList.Remove (tempTimerEventData);
				break;
			}
		}
	}

	public void ReleaseIntervalTimer(int timerID)
	{
		for (int i = 0; i < m_GameTimerCBFuncList.Count; i++) {
			GameTimerEventData tempTimerEventData = m_GameTimerCBFuncList [i];
			if (tempTimerEventData.gameTimerType == GameTimerEventData.TimerType.eInterval) {
				if (tempTimerEventData.timerID == timerID) {
					m_GameTimerCBFuncList.Remove (tempTimerEventData);
					break;
				}
			}

		}
	}

	public void ReleaseTimersBySkillType(int skillType)
	{
		List<GameTimerEventData> lReleaseTimer = new List<GameTimerEventData> ();
		for (int i = 0; i < m_GameTimerCBFuncList.Count; i++) {
			GameTimerEventData tempTimerEventData = m_GameTimerCBFuncList [i];
			if (tempTimerEventData.skillType == skillType) {
				lReleaseTimer.Add (tempTimerEventData);
			}
		}

		for (int i = 0; i < lReleaseTimer.Count; i++) {
			m_GameTimerCBFuncList.Remove (lReleaseTimer [i]);
		}
	}

	public void RemoveCompareObjData(object objData)
	{
		for (int i = 0; i < m_GameTimerCBFuncList.Count; i++) {
			GameTimerEventData tempTimerEventData = m_GameTimerCBFuncList [i];
			if (tempTimerEventData.objData == objData) {
				m_GameTimerCBFuncList.Remove (tempTimerEventData);
				break;
			}
		}
	}

	public bool ExistTimerDataByID(int timerID)
	{
		for (int i = 0; i < m_GameTimerCBFuncList.Count; i++) {
			if (m_GameTimerCBFuncList [i].timerID == timerID) {
				return true;
			}
		}

		return false;
	}

	public bool ExistTimerList()
	{
		if (m_GameTimerCBFuncList.Count > 0)
			return true;

		return false;
	}

	#endregion

	#region Variables

	List<GameTimerEventData> m_GameTimerCBFuncList = new List<GameTimerEventData>();

	#endregion
}
