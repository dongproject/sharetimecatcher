﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum TestDir
{
	right = 0,
	down,
	left,
	up,
}

public class TestScript : MonoBehaviour 
{
	int[,] abc;
	int arraySize = 6;

	// Use this for initialization
	void Start () {
		abc = new int[arraySize, arraySize];
		for (int i = 0; i < arraySize; i++) {
			for (int j = 0; j < arraySize; j++) {
				abc [i, j] = -1;
			}
		}

		int exceptionCount = 0;
		int curX = 0;
		int curY = 0;
		int curCount = 0;
		TestDir curDir = TestDir.right;
		while (true) {
			curCount++;
			switch (curDir) {
			case TestDir.right:
				if (abc [curX, curY] == -1) {
					abc [curX, curY] = curCount;
				}

				if (curX + 1 <= (arraySize - 1)) {
					if (abc [curX + 1, curY] != -1) {
						curDir = TestDir.down;
						curY++;
					} else {
						curX++;
					}
				} else {
					curDir = TestDir.down;
					curY++;
				}
				break;
			case TestDir.down:
				if (abc [curX, curY] == -1) {
					abc [curX, curY] = curCount;
				}

				if (curY + 1 <= (arraySize - 1)) {
					if (abc [curX, curY + 1] != -1) {
						curDir = TestDir.left;
						curX--;
					} else {
						curY++;
					}
				} else {
					curDir = TestDir.left;
					curX--;
				}
				break;
			case TestDir.left:
				if (abc [curX, curY] == -1) {
					abc [curX, curY] = curCount;
				}

				if (curX - 1 >= 0) {
					if (abc [curX - 1, curY] != -1) {
						curDir = TestDir.up;
						curY--;
					} else {
						curX--;
					}
				} else {
					curDir = TestDir.up;
					curY--;
				}
				break;
			case TestDir.up:
				if (abc [curX, curY] == -1) {
					abc [curX, curY] = curCount;
				}

				if (curY - 1 >= 0) {
					if (abc [curX, curY - 1] != -1) {
						curDir = TestDir.right;
						curX++;
					} else {
						curY--;
					}
				} else {
					curDir = TestDir.right;
					curX++;
				}
				break;
			}

			if (abc [curX, curY] != -1) {
				break;
			}

			exceptionCount++;
			if (exceptionCount > 10000)
				break;
		}

		string debugStr = "";
		for (int j = 0; j < arraySize; j++) {
			debugStr = "";
			for (int i = 0; i < arraySize; i++) {
				debugStr += string.Format ("{0} ", abc [i, j]);
			}
			Debug.Log (debugStr);
		}
	}
	
	// Update is called once per frame
	void Update () {
		

	}
}
