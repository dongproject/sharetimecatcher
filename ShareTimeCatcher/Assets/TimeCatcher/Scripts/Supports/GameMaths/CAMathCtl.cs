﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public struct CA2DAngleChangeValue
{
	public CA2DAngleChangeValue(float cosA, float sinA)
	{
		m_CosA = cosA;
		m_SinA = sinA;
	}

	public float m_CosA; // X Change Value
	public float m_SinA; // Y Change Value
}

public struct CA2DIndexValue
{
	public CA2DIndexValue(int indexX, int indexY)
	{
		m_IndexX = indexX;
		m_IndexY = indexY;
	}

	public int m_IndexX;
	public int m_IndexY;
}

public struct ObjRotateSize
{
	public float m_ImgWidth;
	public float m_ImgHeight;
}

public struct ObjPosValue
{
	public float m_PosX;
	public float m_PosY;
	public bool m_PosState;
}

public struct CAPolygonLine
{
	public CAPolygonLine(Vector2 startVertex, Vector2 endVertex)
	{
		m_StartVertex = startVertex;
		m_EndVertex = endVertex;
	}

	public Vector2 m_StartVertex;
	public Vector2 m_EndVertex;
}

public struct CARectVectex
{
	public CARectVectex(float startX, float endX, float startY, float endY)
	{
		m_StartX = startX;
		m_EndX = endX;
		m_StartY = startY;
		m_EndY = endY;
	}

	public float m_StartX;
	public float m_EndX;
	public float m_StartY;
	public float m_EndY;
}

public class ObjRectPosData
{
	public Vector2[] m_Vertexs;

	public ObjRectPosData(int tempVertex)
	{
		this.m_Vertexs = new Vector2[4];
		for(int i = 0;i<4;i++)
		{
			this.m_Vertexs[i] = new Vector2(0f,0f);
		}
	}
}

public class CAMathCtl
{
	private static CAMathCtl _instance = null;
	public static CAMathCtl Instance
	{
		get
		{
			if(_instance == null)
			{
				_instance = new CAMathCtl();
			}
			return _instance;
		}
	}
	
	public CAMathCtl()
	{
		SetTrigonTable();
	}
	
	float m_RadToDeg = 57.29577951f;
	float m_DegToRad = 0.017453293f;
	
	public float[] m_Sin_Table = new float[360];
	public float[] m_Cos_Table = new float[360];
	public float[] m_Tan_Table = new float[360];
	
	void SetTrigonTable()
	{
		for(int i = 0;i<360; i++){
			m_Sin_Table[i] = Mathf.Sin(i * m_DegToRad);
			m_Cos_Table[i] = Mathf.Cos(i * m_DegToRad);
			m_Tan_Table[i] = Mathf.Tan(i * m_DegToRad);
		}
	}
	
	public float CalcAngle2D(float pos_X1, float pos_Y1, float pos_X2, float pos_Y2)
	{
		float upValue = pos_Y2 - pos_Y1;
		float bottomValue = pos_X2 - pos_X1;

		if (upValue == 0f && bottomValue == 0f)
			return 0f;

		if (bottomValue == 0f) {
			if (pos_Y2 < pos_Y1)
				return 270f;
			else
				return 90f;
		}

		if (upValue == 0f) {
			if (pos_X2 < pos_X1)
				return 180f;
			else
				return 0f;
		}
		
		float ang = (float)Mathf.Atan(upValue/bottomValue) * m_RadToDeg;
//		Debug.Log(string.Format("pos_X1 : {0}, pos_Y1 : {1}, pos_X2 : {2}, pos_Y2 : {3}", pos_X1, pos_Y1, pos_X2, pos_Y2));
//		Debug.Log ("calc Angle 2d 1 ang : " + ang);
		if(pos_Y2 < pos_Y1 && pos_X2 > pos_X1) // 1
		{
			return ang + 360f;
		}
		else if((pos_Y2 < pos_Y1 && pos_X2 < pos_X1) || (pos_Y2 > pos_Y1 && pos_X2 < pos_X1)) // 2, 3
		{
			return ang + 180f;
		}
		else // 1
		{
			return ang;
		}
	}

	public float CalcDistance(float pos_X1, float pos_Y1, float pos_X2, float pos_Y2)
	{
		return Mathf.Sqrt((pos_X2 - pos_X1)*(pos_X2 - pos_X1) + (pos_Y2 - pos_Y1)*(pos_Y2 - pos_Y1));
	}

	public float CalcDistanceOnlyCompare(float pos_X1, float pos_Y1, float pos_X2, float pos_Y2)
	{
		return ((pos_X2 - pos_X1)*(pos_X2 - pos_X1) + (pos_Y2 - pos_Y1)*(pos_Y2 - pos_Y1));
	}

	public CA2DAngleChangeValue GetAngleChangeValue(float startX, float startY, float destX, float destY)
	{
		float gapX = destX - startX;
		float gapY = destY - startY;
		float distance = Mathf.Sqrt((float)(Mathf.Pow(gapX, 2f) + Mathf.Pow(gapY, 2f)));

		return new CA2DAngleChangeValue (gapX / distance, gapY / distance);
	}
	
	//------------------------ Rotate Image Value ----------------------------
	public ObjRotateSize GetObjRotateSize(float imgPosX, float imgPosY, float imgWidth, float imgHeight, float tempRotate)
	{
		float imgRotate = tempRotate - 360f;

		ObjRotateSize retValue;

		float tempPosX1 = imgPosX + imgWidth/2f;
		float tempPosY1 = imgPosY + imgHeight/2f;

		float tempDis1 = Mathf.Sqrt((tempPosX1 - imgPosX)*(tempPosX1 - imgPosX) + (tempPosY1 - imgPosY)*(tempPosY1 - imgPosY));

		int tempAng = (int)CalcAngle2D(imgPosX, imgPosY, imgPosX + imgWidth/2f, imgPosY + imgHeight/2f);
		int tempAng_2 = (int)CalcAngle2D(imgPosX, imgPosY, imgPosX - imgWidth/2f, imgPosY + imgHeight/2f);

		//		Debug.Log(string.Format("1 tempAng : {0}, tempAng_2 : {1}, tempRotate : {2}", tempAng, tempAng_2, tempRotate));

		tempAng = tempAng + (int)imgRotate;
		if(tempAng < 0)
		{
			tempAng = 360 + tempAng;
		}
		else if(tempAng >= 360)
		{
			tempAng = tempAng - 360;
		}

		tempAng_2 = tempAng_2 + (int)imgRotate;
		if(tempAng_2 < 0)
		{
			tempAng_2 = 360 + tempAng_2;
		}
		else if(tempAng_2 >= 360)
		{
			tempAng_2 = tempAng_2 - 360;
		}

		//		Debug.Log(string.Format("2 tempAng : {0}, tempAng_2 : {1}", tempAng, tempAng_2));
		//		Debug.Log (string.Format("imgRotate : {0}, tempAng {1}", imgRotate, tempAng));

		float calcPosX1 = tempDis1*m_Cos_Table[tempAng];
		float calcPosY1 = tempDis1*m_Sin_Table[tempAng];
		float calcPosX2 = tempDis1*m_Cos_Table[tempAng_2];
		float calcPosY2 = tempDis1*m_Sin_Table[tempAng_2];

		//		Debug.Log (string.Format("calcPosX1 : {0}, calcPosY1 : {1}, calcPosX2 : {2}, calcPosY2 : {3}",
		//		                         calcPosX1, calcPosY1, calcPosX2, calcPosY2));

		float calcWidth;
		if(Mathf.Abs(calcPosX1) > Mathf.Abs(calcPosX2))
		{
			calcWidth = Mathf.Abs(calcPosX1)*2f;
		}
		else
		{
			calcWidth = Mathf.Abs(calcPosX2)*2f;
		}

		float calcHeight;
		if(Mathf.Abs(calcPosY1) > Mathf.Abs(calcPosY2))
		{
			calcHeight = Mathf.Abs(calcPosY1)*2f;
		}
		else
		{
			calcHeight = Mathf.Abs(calcPosY2)*2f;
		}

		//		Debug.Log (string.Format("imgWidth : {0}, imgHeight : {1}", imgWidth, imgHeight));
		//		Debug.Log (string.Format("calcWidth : {0}, calcHeight : {1}", calcWidth, calcHeight));

		retValue.m_ImgWidth = calcWidth;
		retValue.m_ImgHeight = calcHeight;

		return retValue;
	}
	//------------------------ Rotate Image Value ---------------------------//

	public ObjPosValue GetLineNodeValue(Vector2 startVertex_1, Vector2 endVertex_1, Vector2 startVertex_2, Vector2 endVertex_2)
	{
		ObjPosValue retValue;

		float under = (endVertex_2.y - startVertex_2.y)*(endVertex_1.x - startVertex_1.x)-(endVertex_2.x - startVertex_2.x)*(endVertex_1.y - startVertex_1.y);

		if(under == 0f)
		{
			retValue.m_PosX = 0f;
			retValue.m_PosY = 0f;
			retValue.m_PosState = false;
			return retValue;
		}

		float _t = (endVertex_2.x - startVertex_2.x)*(startVertex_1.y - startVertex_2.y) - (endVertex_2.y - startVertex_2.y)*(startVertex_1.x - startVertex_2.x);
		float _s = (endVertex_1.x - startVertex_1.x)*(startVertex_1.y - startVertex_2.y) - (endVertex_1.y - startVertex_1.y)*(startVertex_1.x - startVertex_2.x);

		float t = _t/under;
		float s = _s/under;

		if(t<0f || t>1f || s<0f || s>1f)
		{
			retValue.m_PosX = 0f;
			retValue.m_PosY = 0f;
			retValue.m_PosState = false;
			return retValue;
		}

		if(_t == 0f && _s == 0f)
		{
			retValue.m_PosX = 0f;
			retValue.m_PosY = 0f;
			retValue.m_PosState = false;
			return retValue;
		}

		retValue.m_PosX = startVertex_1.x + t * (float)(endVertex_1.x - startVertex_1.x);
		retValue.m_PosY = startVertex_1.y + t * (float)(endVertex_1.y - startVertex_1.y);
		retValue.m_PosState = true;

		return retValue;
	}

	public bool CheckLineIntersection(Vector2 startVertex_1, Vector2 endVertex_1, Vector2 startVertex_2, Vector2 endVertex_2)
	{

		float under = (endVertex_2.y - startVertex_2.y)*(endVertex_1.x - startVertex_1.x)-(endVertex_2.x - startVertex_2.x)*(endVertex_1.y - startVertex_1.y);

		if(under == 0f)
		{
			return false;
		}

		float _t = (endVertex_2.x - startVertex_2.x)*(startVertex_1.y - startVertex_2.y) - (endVertex_2.y - startVertex_2.y)*(startVertex_1.x - startVertex_2.x);
		float _s = (endVertex_1.x - startVertex_1.x)*(startVertex_1.y - startVertex_2.y) - (endVertex_1.y - startVertex_1.y)*(startVertex_1.x - startVertex_2.x);

		float t = _t/under;
		float s = _s/under;

		if(t<0f || t>1f || s<0f || s>1f)
		{
			return false;
		}

		if(_t == 0f && _s == 0f)
		{
			return false;
		}

		return true;
	}

	public ObjRectPosData GetObjRectPosData(float imgPosX, float imgPosY, float imgWidth, float imgHeight, float imgRotate, float imgScaleX, float imgScaleY)
	{
		ObjRectPosData retValue = new ObjRectPosData(0);
		//		retValue.m_Vertexs = new Vector2[4];

		float halfWidth = imgWidth*imgScaleX*0.5f;
		float halfHeight = imgHeight*imgScaleY*0.5f;

		//		Debug.Log(string.Format("GetObjRectPosData imgRotate : {0}, halfWidth : {1}, halfHeight : {2}, imgWidth : {3}, imgScaleX : {4}", imgRotate, halfWidth, halfHeight,
		//		                        imgWidth, imgScaleX));

		//		if(imgRotate != 0f)
		//		{
		//			Debug.Log (string.Format("Pre m_PosX1 : {0}, m_PosY1 : {1}, m_PosX2 : {2}, m_PosY2 : {3}, m_PosX3 : {4}, m_PosY3 : {5}, m_PosX4 : {6}, m_PosY4 : {7}", 
		//			                         imgPosX - halfWidth, imgPosY - halfHeight, imgPosX + halfWidth, imgPosY - halfHeight, 
		//			                         imgPosX + halfWidth, imgPosY + halfHeight, imgPosX - halfWidth, imgPosY + halfHeight));
		//		}

		if(imgRotate == 0f)
		{
			retValue.m_Vertexs[0] = new Vector2(imgPosX - halfWidth, imgPosY - halfHeight);
			retValue.m_Vertexs[1] = new Vector2(imgPosX + halfWidth, imgPosY - halfHeight);
			retValue.m_Vertexs[2] = new Vector2(imgPosX + halfWidth, imgPosY + halfHeight);
			retValue.m_Vertexs[3] = new Vector2(imgPosX - halfWidth, imgPosY + halfHeight);
		}
		else
		{
			imgRotate = imgRotate  - 360f;

			float tempPosX1 = imgPosX + halfWidth;
			float tempPosY1 = imgPosY + halfHeight;

			float tempDis1 = Mathf.Sqrt((tempPosX1 - imgPosX)*(tempPosX1 - imgPosX) + (tempPosY1 - imgPosY)*(tempPosY1 - imgPosY));

			int tempAng = (int)CalcAngle2D(imgPosX, imgPosY, imgPosX + halfWidth, imgPosY + halfHeight);
			int tempAng_2 = (int)CalcAngle2D(imgPosX, imgPosY, imgPosX - halfWidth, imgPosY + halfHeight);

			tempAng = tempAng + (int)imgRotate;
			if(tempAng < 0)
			{
				tempAng = 360 + tempAng;
			}
			else if(tempAng >= 360)
			{
				tempAng = tempAng - 360;
			}

			tempAng_2 = tempAng_2 + (int)imgRotate;
			if(tempAng_2 < 0)
			{
				tempAng_2 = 360 + tempAng_2;
			}
			else if(tempAng_2 >= 360)
			{
				tempAng_2 = tempAng_2 - 360;
			}

			float calcPosX1 = tempDis1*m_Cos_Table[tempAng];
			float calcPosY1 = tempDis1*m_Sin_Table[tempAng];
			float calcPosX2 = tempDis1*m_Cos_Table[tempAng_2];
			float calcPosY2 = tempDis1*m_Sin_Table[tempAng_2];

			retValue.m_Vertexs[0] = new Vector2(imgPosX - calcPosX1, imgPosY - calcPosY1);
			retValue.m_Vertexs[1] = new Vector2(imgPosX - calcPosX2, imgPosY - calcPosY2);
			retValue.m_Vertexs[2] = new Vector2(imgPosX + calcPosX1, imgPosY + calcPosY1);
			retValue.m_Vertexs[3] = new Vector2(imgPosX + calcPosX2, imgPosY + calcPosY2);
		}

		//		if(imgRotate != 0f)
		//		{
		//			Debug.Log (string.Format("imgPosX : {0}, imgPosY : {1}, imgRotate : {2}", imgPosX, imgPosY, imgRotate));
		//			Debug.Log (string.Format("GetObjRectPosData m_Vertexs_X1 : {0}, m_Vertexs_Y1 : {1}, m_Vertexs_X2 : {2}, m_Vertexs_Y2 : {3}, m_Vertexs_X3 : {4}, m_Vertexs_Y3 : {5}, m_Vertexs_X4 : {6}, m_Vertexs_Y4 : {7}", 
		//			                         retValue.m_Vertexs[0].x, retValue.m_Vertexs[0].y, retValue.m_Vertexs[1].x, retValue.m_Vertexs[1].y, 
		//			                         retValue.m_Vertexs[2].x, retValue.m_Vertexs[2].y, retValue.m_Vertexs[3].x, retValue.m_Vertexs[3].y));
		//		}


		return retValue;
	}

//	public static GPMatrix2d GetTransformedMatrix (Vector2 objPos, Vector2 objScale, float fRotate) {
//		GPMatrix2d m = new GPMatrix2d ();
//		m.Translate (-objPos);
//		m.Scale (objScale);
//		m.Rotate (fRotate);
//		m.Translate (objPos);
//		return m;
//	}
//
//	public static GPMatrix2d GetTransformedMatrix (float fObjX, float fObjY, float fObjSW, float fObjSH, float fRotate) {
//		GPMatrix2d m = new GPMatrix2d ();
//		m.Translate (-fObjX, -fObjY);
//		m.Scale (fObjSW, fObjSH);
//		m.Rotate (fRotate);
//		m.Translate (fObjX, fObjY);
//		return m;
//	}

	//----------------------- Polygon Area Manager ------------------------
	public double GetArea(List<Vector2> objVertexs)
	{
		double area = 0f;

		Vector2 firstPoint;
		Vector2 secondPoint;

		double factor = 0f;
		int sizeOfVertexs = objVertexs.Count;

		for(int firstIndex = 0;firstIndex < sizeOfVertexs; firstIndex++)
		{
			int secondIndex = (firstIndex + 1) % sizeOfVertexs;

			firstPoint = objVertexs[firstIndex];
			secondPoint = objVertexs[secondIndex];

			factor = ((firstPoint.x * secondPoint.y) - (secondPoint.x * firstPoint.y));

			area += factor;
		}

		area /= 2f;
		return area;
	}
	//----------------------- Polygon Area Manager -----------------------//

	//----------------------- Random Manager --------------------------
	public int[] GetRandArray(int nStart, int nEnd, int nSize)
	{
		int[] retValue;
		retValue = new int[nSize];

		for(int i=0;i<nSize;i++)
		{        
			retValue[i] = Random.Range(nStart, nEnd + 1);
			for(int j=0; j<i;j++)
			{
				if(retValue[j] == retValue[i])
				{
					i--;
					continue;
				}
			}
		}

		return retValue;
	}

	public int GetRandomPosIndex(List<float> proWeightingList)
	{
		int retValue = 0;

		int tempListCount;
		float totalProValue = 0f;

		tempListCount = proWeightingList.Count;
		float[] probability = new float[tempListCount];
		float saveProbability = 0f;
		float[] c_Probability = new float[tempListCount];
		int randValue;
		int tempCount;
		int normalCount;
		int keyCount;

		tempCount = 0;
		foreach(float tempProWeighting in proWeightingList)
		{
			probability[tempCount] = tempProWeighting;
			totalProValue += tempProWeighting;
			tempCount++;
		}

		for(int i=0;i<tempListCount;i++)
		{
			c_Probability[i] = 0f;
		}

		for(int i=0;i<tempListCount;i++)
		{
			saveProbability += probability[i];
			c_Probability[i] = saveProbability;
		}

		randValue = Random.Range(1, (int)totalProValue + 1);

		for(int i=0;i<tempListCount;i++)
		{
			if((float)randValue<=c_Probability[i])
			{
				retValue = i;
				break;
			}
		}

		return retValue;
	}

	public bool GetProbabilityState(float iProbability)
	{
		bool retValue = false;

		int ranValue = Random.Range(1, 101);
		if(ranValue <= (int)iProbability)
		{
			retValue = true;
		}

		return retValue;
	}
	//----------------------- Random Manager -------------------------//

	//------------------------- Matrix Touch Event Manager ------------------------------
//	GPMatrix2d m_GPMatrix2d = new GPMatrix2d();
//
//	public bool MatrixTouchEvent(float imgX, float imgY, float imgScaleX, float imgScaleY, float imgHalfWidth, float imgHalfHeight, float imgRotate, float touchX, float touchY, float touchGap = 0f)
//	{
//		GPMatrix2d m = MathCtl.GetTransformedMatrix (imgX, imgY, imgScaleX, imgScaleY, imgRotate);
//		m.Inverse ();  
//		Vector2 localPos = m.Transformed (touchX, touchY);
//
//		GPRectF r = new GPRectF(imgX - imgHalfWidth*imgScaleX - touchGap, 
//			imgY - imgHalfHeight*imgScaleY - touchGap, 
//			(imgHalfWidth*imgScaleX+ touchGap)*2, 
//			(imgHalfHeight*imgScaleY + touchGap)*2);
//		return r.IsContains (localPos);
//		/*bool retValue = false; 
//		if((retPoint.x >= (imgX - imgHalfWidth*imgScaleX - touchGap) && retPoint.x <= (imgX + imgHalfWidth*imgScaleX + touchGap)) &&
//		   (retPoint.y >= (imgY - imgHalfHeight*imgScaleY - touchGap) && retPoint.y <= (imgY + imgHalfHeight*imgScaleY + touchGap)))
//		{
//			retValue = true;
//		}
//		
//		return retValue;*/
//	}
	//------------------------- Matrix Touch Event Manager -----------------------------//

	#region Methods

	public float GetPointDistance(float x1, float y1, float x2, float y2)
	{
		float x_Value = x2-x1;
		float y_Value = y2-y1;
		float x_Square = x_Value * x_Value;
		float y_Square = y_Value * y_Value;

		return Mathf.Sqrt(x_Square + y_Square);
	}

	public bool CheckCollideFigures(float circlePosX, float circlePosY, float circleRadius, float rectPosX, float rectPosY, Vector2 recVector_1, Vector2 recVector_2, Vector3 recVector_3, Vector3 recVector_4)
	{

		float figureDistance = GetPointDistance (circlePosX, circlePosY, rectPosX, rectPosY);
		if (figureDistance <= circleRadius) {
			Debug.Log (string.Format ("CheckCollideFigures true 1"));
			return true;
		}

		figureDistance = GetPointDistance (circlePosX, circlePosY, recVector_1.x, recVector_1.y);
		if (figureDistance <= circleRadius) {
			Debug.Log (string.Format ("CheckCollideFigures true 2"));
			return true;
		}

		figureDistance = GetPointDistance (circlePosX, circlePosY, recVector_2.x, recVector_2.y);
		if (figureDistance <= circleRadius) {
			Debug.Log (string.Format ("CheckCollideFigures true 3"));
			return true;
		}

		figureDistance = GetPointDistance (circlePosX, circlePosY, recVector_3.x, recVector_3.y);
		if (figureDistance <= circleRadius) {
			Debug.Log (string.Format ("CheckCollideFigures true 4"));
			return true;
		}

		figureDistance = GetPointDistance (circlePosX, circlePosY, recVector_4.x, recVector_4.y);
		if (figureDistance <= circleRadius) {
			Debug.Log (string.Format ("CheckCollideFigures true 5"));
			return true;
		}

		//		ObjPosValue checkPosValue = GetLineNodeValue (circlePosX, circlePosY, rectPosX, rectPosY, recVector_1.x, recVector_1.y, recVector_2.x, recVector_2.y);
		//		if (checkPosValue.m_PosState) {
		//			Debug.Log (string.Format ("CheckCollideFigures true 2"));
		//			return true;
		//		}
		//
		//		checkPosValue = GetLineNodeValue (circlePosX, circlePosY, rectPosX, rectPosY, recVector_2.x, recVector_2.y, recVector_3.x, recVector_3.y);
		//		if (checkPosValue.m_PosState) {
		//			Debug.Log (string.Format ("CheckCollideFigures true 3"));
		//			return true;
		//		}
		//
		//		checkPosValue = GetLineNodeValue (circlePosX, circlePosY, rectPosX, rectPosY, recVector_3.x, recVector_3.y, recVector_4.x, recVector_4.y);
		//		if (checkPosValue.m_PosState) {
		//			Debug.Log (string.Format ("CheckCollideFigures true 4"));
		//			return true;
		//		}
		//
		//		checkPosValue = GetLineNodeValue (circlePosX, circlePosY, rectPosX, rectPosY, recVector_4.x, recVector_4.y, recVector_1.x, recVector_1.y);
		//		if (checkPosValue.m_PosState) {
		//			Debug.Log (string.Format ("CheckCollideFigures true 5"));
		//			return true;
		//		}

		return false;
	}

//	public bool CheckCollidePolygon(List<Vector2> lPolygonVec_1, List<Vector2> lPolygonVec_2)
//	{
//
//		List<CAPolygonLine> lPolygonLine_1 = GetPolygonLine(lPolygonVec_1);
//		List<CAPolygonLine> lPolygonLine_2 = GetPolygonLine(lPolygonVec_2);
//
//		return false;
//	}

	public bool CheckCollidePolygon(List<Vector2> lPolygonVec_1, List<Vector2> lPolygonVec_2, List<CAPolygonLine> lPolygonLine_1,
		List<CAPolygonLine> lPolygonLine_2, Vector2 polygonPosVec_1, Vector2 polygonPosVec_2, float minDistance_1, float minDistance_2, float maxDistance_1, float maxDistance_2)
	{
		if (polygonPosVec_1.x == polygonPosVec_2.x && polygonPosVec_1.y == polygonPosVec_2.y)
			return true;

		float polygonDis = CalcDistance (polygonPosVec_1.x, polygonPosVec_1.y, polygonPosVec_2.x, polygonPosVec_2.y);

		if (polygonDis <= minDistance_1 || polygonDis <= minDistance_2) {
			return true;
		}

		if (polygonDis > (maxDistance_1 + maxDistance_2))
			return false;

//		List<CAPolygonLine> lPolygonLine_1 = GetPolygonLine(lPolygonVec_1, polygonPosVec_1);
//		List<CAPolygonLine> lPolygonLine_2 = GetPolygonLine(lPolygonVec_2, polygonPosVec_2);

		for (int i = 0; i < lPolygonLine_1.Count; i++) {
			for (int j = 0; j < lPolygonLine_2.Count; j++) {
				if (CheckLineIntersection (new Vector2 (lPolygonLine_1 [i].m_StartVertex.x + polygonPosVec_1.x, lPolygonLine_1 [i].m_StartVertex.y + polygonPosVec_1.y), 
					    new Vector2 (lPolygonLine_1 [i].m_EndVertex.x + polygonPosVec_1.x, lPolygonLine_1 [i].m_EndVertex.y + polygonPosVec_1.y),
						new Vector2 (lPolygonLine_2 [j].m_StartVertex.x + polygonPosVec_2.x, lPolygonLine_2 [j].m_StartVertex.y + polygonPosVec_2.y),
						new Vector2 (lPolygonLine_2 [j].m_EndVertex.x + polygonPosVec_2.x, lPolygonLine_2 [j].m_EndVertex.y + polygonPosVec_2.y))) {
					return true;
				}
			}
		}

		return false;
	}

	public List<CAPolygonLine> GetPolygonLine(List<Vector2> lPolygonVec)
	{
		List<CAPolygonLine> lretPolygonLine = new List<CAPolygonLine> ();

		CAPolygonLine inputPolygonLine = new CAPolygonLine (Vector2.zero, Vector2.zero);
		for (int i = 0; i < lPolygonVec.Count; i++) {
			if (lPolygonVec.Count - 1 > i) {
				inputPolygonLine.m_StartVertex = new Vector2(lPolygonVec [i].x, lPolygonVec [i].y);
				inputPolygonLine.m_EndVertex = new Vector2(lPolygonVec [i + 1].x, lPolygonVec [i + 1].y);
			} else {
				inputPolygonLine.m_StartVertex = new Vector2(lPolygonVec [i].x, lPolygonVec [i].y);
				inputPolygonLine.m_EndVertex = new Vector2(lPolygonVec [0].x, lPolygonVec [0].y);
			}

			lretPolygonLine.Add (inputPolygonLine);
		}

		return lretPolygonLine;
	}

	public List<CAPolygonLine> GetPolygonLine(List<Vector2> lPolygonVec, Vector2 polygonPosVec)
	{
		List<CAPolygonLine> lretPolygonLine = new List<CAPolygonLine> ();

		CAPolygonLine inputPolygonLine = new CAPolygonLine (Vector2.zero, Vector2.zero);
		for (int i = 0; i < lPolygonVec.Count; i++) {
			if (lPolygonVec.Count - 1 > i) {
				inputPolygonLine.m_StartVertex = new Vector2(lPolygonVec [i].x + polygonPosVec.x, lPolygonVec [i].y + polygonPosVec.y);
				inputPolygonLine.m_EndVertex = new Vector2(lPolygonVec [i + 1].x + polygonPosVec.x, lPolygonVec [i + 1].y + polygonPosVec.y);
			} else {
				inputPolygonLine.m_StartVertex = new Vector2(lPolygonVec [i].x + polygonPosVec.x, lPolygonVec [i].y + polygonPosVec.y);
				inputPolygonLine.m_EndVertex = new Vector2(lPolygonVec [0].x + polygonPosVec.x, lPolygonVec [0].y + polygonPosVec.y);
			}

			lretPolygonLine.Add (inputPolygonLine);
		}

		return lretPolygonLine;
	}

	public double GetAnglePoint(float x1, float y1, float x2, float y2)
	{
		float dx = x2 - x1;
		float dy = y2 - y1;

		double rad = Mathf.Atan2 (dy, dx);
		double degree = (rad * 180f) / Mathf.PI;

		return degree;
	}

	public Vector2 GetCircleLinePos(Vector2 centerPos, float radius, float angle)
	{
		float posX = (m_Cos_Table [(int)angle] * radius) + centerPos.x;
		float posY = (m_Sin_Table [(int)angle] * radius) + centerPos.y;

		return new Vector2(posX, posY);
	}

	public Vector2 GetCircleDetailLinePos(Vector2 centerPos, float radius, float angle)
	{
		float posX = (Mathf.Cos(angle * m_DegToRad) * radius) + centerPos.x;
		float posY = (Mathf.Sin(angle * m_DegToRad) * radius) + centerPos.y;

		return new Vector2(posX, posY);
	}

    public Vector2 GetPosByAngle(Vector2 curPos, float disValue, int angle)
    {
        if(angle == 360){
            angle = 0;
        }
        float posX = disValue * m_Cos_Table[angle] + curPos.x;
        float posY = disValue * m_Sin_Table[angle] + curPos.y;

        return new Vector2(posX, posY);
    }

    #endregion
}











