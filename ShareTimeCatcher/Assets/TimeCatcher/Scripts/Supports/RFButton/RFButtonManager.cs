﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public struct RFButtonTouchValue
{
	public RFButtonTouchValue(int id, int btnID)
	{
		touchID = id;
		buttonID = btnID;
	}

	public int touchID;
	public int buttonID;
}

public struct RFButtonRect
{
	public RFButtonRect(float startX, float endX, float startY, float endY)
	{
		startPosX = startX;
		endPosX = endX;
		startPosY = startY;
		endPosY = endY;
	}

	public float startPosX;
	public float endPosX;
	public float startPosY;
	public float endPosY;
}

public struct RFButtonRectInfo
{
	public RFButtonRectInfo(int btnID, RFButtonRect btnRect)
	{
		buttonID = btnID;
		buttonRect = btnRect;
	}

	public int buttonID;
	public RFButtonRect buttonRect;
}

public class RFButtonManager : MonoBehaviour 
{
	#region Serialize Variables

	[SerializeField] Camera _buttonCamera;
	[SerializeField] RFButton[] _rfButtons;

	#endregion

	#region Variables

	int _touchCount;
	RFButtonTouchValue _touchValue = new RFButtonTouchValue(-1, -1);

	bool _isEnableTouch = true;
	RFButtonRectInfo[] _cacheButtonRectInfos;
	Action <int> _onButtonTouched = null;

	#endregion

	#region Properties

	public bool IsEnableTouch
	{
		get{ return _isEnableTouch; }
		set{ _isEnableTouch = value; }
	}

	public Camera ButtonCamera
	{
		get{ return _buttonCamera; }
		set{ _buttonCamera = value; }
	}

	public Action <int> OnButtonTouched
	{
		get{ return _onButtonTouched; }
		set{ _onButtonTouched = value; }
	}

	#endregion

	#region MonoBehaviour Methods

	void Awake()
	{
		InitTouchValue ();
//		SetCacheButtonRect ();
//
//		if(Application.platform == RuntimePlatform.OSXEditor || Application.platform == RuntimePlatform.WindowsEditor)
//		{
//			StartCoroutine(UpdateEditorTouch());
//		}
//		else
//		{
//			StartCoroutine(UpdateTouchHandle());
//		}
	}

	public void OnApplicationPause(bool isPause)
	{
		if(isPause)
		{
			InitTouchValue();
		}
	}

	#endregion

	#region Methods

	void InitTouchValue()
	{

		_touchValue.touchID = -1;
		_touchValue.buttonID = -1;
	}

	int GetTouchValidButtonID(float posX, float posY)
	{
		if (_cacheButtonRectInfos != null) {
			for (int i = 0; i < _cacheButtonRectInfos.Length; i++) {
				if ((_cacheButtonRectInfos [i].buttonRect.startPosX <= posX && _cacheButtonRectInfos [i].buttonRect.endPosX >= posX) &&
				   (_cacheButtonRectInfos [i].buttonRect.startPosY <= posY && _cacheButtonRectInfos [i].buttonRect.endPosY >= posY)) {
					return _cacheButtonRectInfos [i].buttonID;
				}
			}
		}

		return -1;
	}

	public void SetCacheButtonRect()
	{
		if (_buttonCamera == null) {
			Debug.Log (string.Format ("SetCacheButtonRect _buttonCamera == null"));
			return;
		}

		if (CAResolutionCtl.Instance == null) {
			CAResolutionCtl.InitResolutionCtl ();
			CAResolutionCtl.Instance.InitResolution(Screen.width, Screen.height, false);
			Screen.SetResolution((int)CAResolutionCtl.Instance.GetResolutionWidth(), (int)CAResolutionCtl.Instance.GetResolutionHeight(), true);
		}

		if (_rfButtons != null) {
			_cacheButtonRectInfos = new RFButtonRectInfo[_rfButtons.Length];
			for (int i = 0; i < _rfButtons.Length; i++) {
				Vector3 btnScreenPos = _buttonCamera.WorldToScreenPoint (_rfButtons [i].transform.position);

				float halfWidth = _rfButtons [i].ButtonBoxCollider.size.x * 0.5f;
				float halfHeight = _rfButtons [i].ButtonBoxCollider.size.y * 0.5f;

                float centerX = _rfButtons[i].ButtonBoxCollider.center.x;
                float centerY = _rfButtons[i].ButtonBoxCollider.center.y;

				_cacheButtonRectInfos [i] = new RFButtonRectInfo ();
				_cacheButtonRectInfos [i].buttonID = _rfButtons [i].ButtonID;
				_cacheButtonRectInfos [i].buttonRect = new RFButtonRect (btnScreenPos.x - halfWidth + centerX, btnScreenPos.x + halfWidth + centerX, 
					btnScreenPos.y - halfHeight + centerY, btnScreenPos.y + halfHeight + centerY);
			}
		}
	}

	public void InitRFButtonManager()
	{
		SetCacheButtonRect ();

		if(Application.platform == RuntimePlatform.OSXEditor || Application.platform == RuntimePlatform.WindowsEditor)
		{
			StartCoroutine(UpdateEditorTouch());
		}
		else
		{
			StartCoroutine(UpdateTouchHandle());
		}
	}

	public void ResetRFButton()
	{
		_isEnableTouch = true;

		if(Application.platform == RuntimePlatform.OSXEditor || Application.platform == RuntimePlatform.WindowsEditor)
		{
			StartCoroutine(UpdateEditorTouch());
		}
		else
		{
			StartCoroutine(UpdateTouchHandle());
		}
	}

	#endregion

	#region Coroutine Methods
	
	public IEnumerator UpdateTouchHandle()
	{
		while(_isEnableTouch)
		{
			if(Input.touchCount > 0)
			{
				for(int i=0;i<Input.touchCount;i++)
				{
					Touch inputTouch = Input.GetTouch(i);
					float inputTouchX = inputTouch.position.x;
					float inputTouchY = inputTouch.position.y;

					if(_touchValue.touchID == -1 && inputTouch.phase == TouchPhase.Began)
					{
						int touchButtonID = GetTouchValidButtonID (inputTouchX, inputTouchY);
						if (touchButtonID != -1) {
							_touchValue.touchID = inputTouch.fingerId;
							_touchValue.buttonID = touchButtonID;
						}
					}
					else if(_touchValue.touchID != -1 && inputTouch.phase == TouchPhase.Moved)
					{
						
					}
					else if(_touchValue.touchID != -1 && inputTouch.phase == TouchPhase.Ended)
					{
						if(_touchValue.touchID == inputTouch.fingerId)
						{
							int touchButtonID = GetTouchValidButtonID (inputTouchX, inputTouchY);
							if (_touchValue.buttonID == touchButtonID) {
								if (_onButtonTouched != null) {
									_onButtonTouched (touchButtonID);
								}
								_touchValue.touchID = -1;
								_touchValue.buttonID = -1;
							}
						}
					}
					else if(_touchValue.touchID != -1 && inputTouch.phase == TouchPhase.Canceled)
					{
						if(_touchValue.touchID == inputTouch.fingerId){
							_touchValue.touchID = -1;
							_touchValue.buttonID = -1;
						}

					}
				} // for(i=0;i<Input.touchCount;i++)
			} // if(Input.touchCount > 0)
			yield return null;
		}
	}

	IEnumerator UpdateEditorTouch()
	{
		while(_isEnableTouch)
		{
			float inputTouchX = Input.mousePosition.x;
			float inputTouchY = Input.mousePosition.y;

			if(_touchValue.touchID == -1 && Input.GetMouseButtonDown(0))
			{
				int touchButtonID = GetTouchValidButtonID (inputTouchX, inputTouchY);
				if (touchButtonID != -1) {
					_touchValue.touchID = 0;
					_touchValue.buttonID = touchButtonID;
				}
			}
			else if(_touchValue.touchID != -1 && Input.GetMouseButton(0))
			{

			}
			else if(_touchValue.touchID != -1 && Input.GetMouseButtonUp(0))
			{
				int touchButtonID = GetTouchValidButtonID (inputTouchX, inputTouchY);
				if (_touchValue.buttonID == touchButtonID) {
					if (_onButtonTouched != null) {
						_onButtonTouched (touchButtonID);
					}
					_touchValue.touchID = -1;
					_touchValue.buttonID = -1;
				}
			}

			yield return null;
		}

	}

	#endregion
}
