﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RFButton : MonoBehaviour 
{
	#region Serialize Variables

	[SerializeField] int _buttonID;
	[SerializeField] BoxCollider _buttonBoxCollider;

	#endregion

	#region Properties

	public int ButtonID
	{
		get{ return _buttonID; }
	}

	public BoxCollider ButtonBoxCollider
	{
		get{ return _buttonBoxCollider; }
	}

	#endregion
}
