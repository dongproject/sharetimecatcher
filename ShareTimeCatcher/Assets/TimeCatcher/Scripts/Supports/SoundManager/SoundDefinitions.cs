﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundDefinitions
{
    #region Definitions

    #endregion

    #region Variables

    public const short effectSound_1 = 0;
    public const short effectSound_2 = 1;
    public const short effectSound_3 = 2;
    public const short effectSound_4 = 3;
    public const short effectSound_5 = 4;
    public const short effectSound_6 = 5;
    public const short effectSound_9 = 6;
    public const short effectSound_10 = 7;
    public const short effectSound_11 = 8;
    public const short effectSound_12 = 9;
    public const short effectSound_13 = 10;
    public const short effectSound_14 = 11;
    public const short effectSound_15 = 12;
    public const short effectSound_16 = 13;
    public const short effectSound_17 = 14;

    #endregion
}
