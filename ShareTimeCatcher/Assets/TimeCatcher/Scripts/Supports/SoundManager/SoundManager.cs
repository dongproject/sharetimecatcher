﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour 
{
	static SoundManager _instance = null;
	public static SoundManager Instance
	{
		get{ return _instance; }
	}

	public static void Exit()
	{
		_instance = null;
	}

    #region Serialize Variables

#pragma warning disable 649

    [SerializeField] AudioSource _musicSource;
    [SerializeField] AudioSource[] _subMusicSource;
    [SerializeField] AudioSource[] _efxSources;

#pragma warning restore 649

    #endregion

    #region Variables

    float lowPitchRange = .95f;             //The lowest a sound effect will be randomly pitched.
	float highPitchRange = 1.05f;			//The highest a sound effect will be randomly pitched.

    AudioClip[] _effectAudioClipPools = new AudioClip[256];
    int _curSoundFXIndex = 0;

    bool _isMusicEnable = true;
    bool _isSoundEnbale = true;

    int _curSubMusicIndex = 0;
    List<int> _curSubMusicIndexes = new List<int>();
    AudioClip _curMusicClip = null;
    AudioClip[] _curSubMusicClip = new AudioClip[4];

    #endregion

    #region Properties

    #endregion

    #region MonoBehaviour Methods

    void Awake()
	{
        _instance = this;
		DontDestroyOnLoad (this.gameObject);
	}

	#endregion

	#region Methods

    public void InitSoundManager()
    {
        _curSoundFXIndex = 0;
    }

    public void SetEffectAudioClip(short effectIndex, AudioClip effectAudio)
    {
        _effectAudioClipPools[effectIndex] = effectAudio;
    }

    public void ReleaseSounds()
    {
        for (int i = 0; i < _effectAudioClipPools.Length;i++) {
            _effectAudioClipPools[i] = null;
        }
    }

	//Used to play single sound clips.
	public void PlaySingle(AudioClip clip)
	{
        if (!_isSoundEnbale)
            return;

		//Set the clip of our efxSource audio source to the clip passed in as a parameter.
		_efxSources[_curSoundFXIndex].clip = clip;

		//Play the clip.
		_efxSources[_curSoundFXIndex].Play ();

        _curSoundFXIndex++;
        if (_curSoundFXIndex >= _efxSources.Length){
            _curSoundFXIndex = 0;
        }
	}

	//RandomizeSfx chooses randomly between various audio clips and slightly changes their pitch.
	public void RandomizeSfx (params AudioClip[] clips)
	{
        if (!_isSoundEnbale)
            return;

		//Generate a random number between 0 and the length of our array of clips passed in.
		int randomIndex = Random.Range(0, clips.Length);

		//Choose a random pitch to play back our clip at between our high and low pitch ranges.
		float randomPitch = Random.Range(lowPitchRange, highPitchRange);

		//Set the pitch of the audio source to the randomly chosen pitch.
		_efxSources[_curSoundFXIndex].pitch = randomPitch;

		//Set the clip to the clip at our randomly chosen index.
		_efxSources[_curSoundFXIndex].clip = clips[randomIndex];

		//Play the clip.
		_efxSources[_curSoundFXIndex].Play();

        _curSoundFXIndex++;
        if (_curSoundFXIndex >= _efxSources.Length)
        {
            _curSoundFXIndex = 0;
        }
    }

    public void PlayEffectFX(short effectIndex)
    {
        if (!_isSoundEnbale)
            return;

        if(_efxSources[_curSoundFXIndex].clip != null){
            if(_efxSources[_curSoundFXIndex].isPlaying){
                _efxSources[_curSoundFXIndex].Stop();
            }
        }

        _efxSources[_curSoundFXIndex].clip = _effectAudioClipPools[effectIndex];
       
        //Play the clip.
        _efxSources[_curSoundFXIndex].Play();

        _curSoundFXIndex++;
        if (_curSoundFXIndex >= _efxSources.Length)
        {
            _curSoundFXIndex = 0;
        }
    }

    public void PlayMusic(AudioClip musicClip, bool isloop = true)
    {
        if (!_isMusicEnable)
            return;

        StopMusic();

        _curMusicClip = musicClip;
        _musicSource.clip = musicClip;

        _musicSource.loop = isloop;
        _musicSource.Play();
    }

    public void PlaySubMusic(AudioClip musicClip, bool isloop = true, bool disable = false)
    {
        if (!_isMusicEnable)
            return;

        if (disable)
        {
            StopSubMusic(disable);
        }


        if(_curSubMusicIndexes.Count > 3)
        {
            Debug.Log(string.Format("SubMucis Index Out"));
            return;
        }

        int curIndex = -1;
        for(int i = 0;i< 4; i++)
        {
            if (!_curSubMusicIndexes.Contains(i))
            {
                curIndex = i;
                break;
            }
        }

        _curSubMusicIndexes.Add(curIndex);

        if (curIndex == -1)
        {
            Debug.Log(string.Format("SubMucis Index Out"));
            return;
        }

        _curSubMusicClip[curIndex] = musicClip;

        _subMusicSource[curIndex].clip = musicClip;

        _subMusicSource[curIndex].loop = isloop;
        _subMusicSource[curIndex].Play();
    }

    public void PlayCurMusic()
    {
        if (!_isMusicEnable)
            return;

        if (_curMusicClip == null)
            return;

        _musicSource.clip = _curMusicClip;
        _musicSource.Play();
    }

    public void PlaySubCurMusic()
    {
        if (!_isMusicEnable)
            return;

        if (_curSubMusicIndexes.Count == 0)
            return;

        for(int i = 0;i< _curSubMusicIndexes.Count; i++)
        {
            int index = _curSubMusicIndexes[i];
            if (_curSubMusicClip[index] == null) continue;
            _subMusicSource[index].clip = _curSubMusicClip[index];
            _subMusicSource[index].Play();
        }
    }

    public void StopMusic()
    {
        if (_musicSource != null && _musicSource.clip != null) {
            if (_musicSource.isPlaying)
                _musicSource.Stop();

            _musicSource.clip = null;
        }
    }

    public void StopSubMusic(bool disable = false)
    {
        if (_curSubMusicIndexes.Count == 0)
            return;

        for(int i = 0;i< _curSubMusicIndexes.Count; i++)
        {
            if (_subMusicSource[_curSubMusicIndexes[i]].isPlaying)
                _subMusicSource[_curSubMusicIndexes[i]].Stop();

            _subMusicSource[_curSubMusicIndexes[i]].clip = null;
        }

        if (disable)
        {
            _curSubMusicIndexes.Clear();
            for(int i = 0;i< _curSubMusicClip.Length; i++)
            {
                _curSubMusicClip[i] = null;
            }
        }
    }

    public void StopTargetSubMusic(int index)
    {
        if (!_curSubMusicIndexes.Contains(index))
            return;

        if (_subMusicSource[index].isPlaying)
            _subMusicSource[index].Stop();

        _subMusicSource[index].clip = null;

        _curSubMusicClip[index] = null;

        _curSubMusicIndexes.RemoveAt(index);
    }

    public void SetSoundEnable(bool isEnable)
    {
        _isSoundEnbale = isEnable;
    }

    public void SetMusicEnable(bool isEnable)
    {
        _isMusicEnable = isEnable;
        if (!isEnable)
        {
            StopMusic();
            StopSubMusic();
        }
        else
        {
            PlayCurMusic();
            PlaySubCurMusic();
        }
            
    }

    #endregion
}
