﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundFilePaths 
{
    public static string musicCommonPath = "Sound/Music/";
    public static string soundFXCommonPath = "Sound/SoundFX/";

    public static string MusicPath_1
    {
        get { return musicCommonPath + "music1_title"; }
    }

    public static string MusicPath_3
    {
        get { return musicCommonPath + "music3_menu"; }
    }

    public static string MusicPath_4
    {
        get { return musicCommonPath + "music4_ingame"; }
    }

    public static string MusicPath_5
    {
        get { return musicCommonPath + "music5_success"; }
    }

    public static string MusicPath_6
    {
        get { return musicCommonPath + "music6_fail"; }
    }

    public static string MusicPath_7
    {
        get { return musicCommonPath + "St12_beeRepeat"; }
    }

    //public static string SoundFXPath_1
    //{
    //    get { return soundFXCommonPath + "sound1"; }
    //}

    public static string SoundFXPath_2
    {
        get { return soundFXCommonPath + "sound2_common"; }
    }

    public static string SoundFXPath_3
    {
        get { return soundFXCommonPath + "sound3_ready"; }
    }

    public static string SoundFXPath_4
    {
        get { return soundFXCommonPath + "sound4_button"; }
    }

    public static string SoundFXPath_5
    {
        get { return soundFXCommonPath + "sound5_count"; }
    }

    public static string SoundFXPath_6
    {
        get { return soundFXCommonPath + "sound6"; }
    }

    public static string SoundFXPath_9
    {
        get { return soundFXCommonPath + "sound9_cheer"; }
    }

    public static string SoundFXPath_10
    {
        get { return soundFXCommonPath + "sound10_solution"; }
    }

    public static string SoundFXPath_11
    {
        get { return soundFXCommonPath + "St8_hit1"; }
    }

    public static string SoundFXPath_12
    {
        get { return soundFXCommonPath + "St8_hit2"; }
    }

    public static string SoundFXPath_13
    {
        get { return soundFXCommonPath + "St8_hit3"; }
    }

    public static string SoundFXPath_14
    {
        get { return soundFXCommonPath + "St8_hit4"; }
    }

    public static string SoundFXPath_15
    {
        get { return soundFXCommonPath + "St10_14_16_open"; }
    }

    public static string SoundFXPath_16
    {
        get { return soundFXCommonPath + "St18_26_Switch"; }
    }

    public static string SoundFXPath_17
    {
        get { return soundFXCommonPath + "St30_boom"; }
    }
}
