﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Advertisements;

public enum ADType
{
    StageHintSlow,
}

public class UnityAdsManager : MonoBehaviour
{
    static UnityAdsManager _instance = null;
    public static UnityAdsManager Instance
    {
        get { return _instance; }
    }

    #region Variables

#if UNITY_ADNROID
    const string _androidGameID = "3146239";
#endif

#if UNITY_IOS
    const string _iosGameID = "3146238";
#endif

    const string _rewardedVideoID = "rewardedVideo";
    ADType _curADType;
    //Action<ShowResult> _onResultAdAction = null;

    #endregion

    #region Properties

    public ADType CurADType
    {
        get { return _curADType; }
        set { _curADType = value; }
    }

    //public Action<ShowResult> OnResultAdAction
    //{
    //    get { return _onResultAdAction; }
    //    set { _onResultAdAction = value; }
    //}

    #endregion

    #region MonoBehaviour Methods

    void Awake()
    {
        _instance = this;
        DontDestroyOnLoad(this.gameObject);
    }

    // Use this for initialization
    void Start ()
    {
        Initialize();
    }

    #endregion

    #region Methods

    void Initialize()
    {
#if UNITY_ADNROID
        Advertisement.Initialize(_androidGameID);
#elif UNITY_IOS
        Advertisement.Initialize(_iosGameID);
#endif
    }

    public void ShowRewardedAd()
    {
        //if (Advertisement.IsReady(_rewardedVideoID)) {
        //    var options = new ShowOptions { resultCallback = HandleShowResult };

        //    Advertisement.Show(_rewardedVideoID, options);
        //}
    }

    //private void HandleShowResult(ShowResult result)
    //{
    //    switch (result) {
    //        case ShowResult.Finished: {
    //                Debug.Log("The ad was successfully shown.");

    //                // to do ...
    //                // 광고 시청이 완료되었을 때 처리

    //                break;
    //            }
    //        case ShowResult.Skipped: {
    //                Debug.Log("The ad was skipped before reaching the end.");

    //                // to do ...
    //                // 광고가 스킵되었을 때 처리

    //                break;
    //            }
    //        case ShowResult.Failed: {
    //                Debug.LogError("The ad failed to be shown.");

    //                // to do ...
    //                // 광고 시청에 실패했을 때 처리

    //                break;
    //            }
    //    }

    //    if(_onResultAdAction != null)
    //    {
    //        _onResultAdAction(result);
    //        _onResultAdAction = null;
    //    }
    //}

    #endregion
}
