﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScrollDefinitions
{
    public enum MoveType
    {
        HorizontalScroll = 1,
        VerticalScroll = 2,
    }

    public enum MoveDirection
    {
        None = 0,
        PlusMove = 1,
        MinusMove = 2,
    }
}
