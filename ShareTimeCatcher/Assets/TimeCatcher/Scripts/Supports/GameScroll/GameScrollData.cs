﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct ScreenTouchValue
{
    public int touchID;
    public Vector2 curPosition;
    public Vector2 prePosition;
    public bool firstDragState;
}
