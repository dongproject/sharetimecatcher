﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CAScrollMoveAni
{
	//public enum ListDirection
	//{
	//	eVertical,
	//	eHorizontal,
	//}

	public enum ListMoveAniType
	{
		eRectOutAniType,
		eScrollMoveAniType,
	}

	#region Methods

	public void CalcMoveAniData()
	{
		m_GapPosValue =	m_EndPosValue - m_StartPosValue;
		//		Debug.Log(string.Format("m_GapPosValue : {0}, m_EndPosValue : {1}, m_StartPosValue : {2}", m_GapPosValue, m_EndPosValue, m_StartPosValue));
		m_MoveSpeed = m_GapPosValue*5f;
		if(m_MoveSpeed > 0f)
		{
			plusNumState = true;	
		}
		else
		{
			plusNumState = false;
		}

		if(plusNumState)
		{
			if(moveSpeed > m_MaxSpeedValue)
				moveSpeed = m_MaxSpeedValue;
		}
		else
		{
			if(moveSpeed < -m_MaxSpeedValue)
				moveSpeed = -m_MaxSpeedValue;
		}
	}

	public void CalcScrollMoveAniData()
	{
		if(m_MoveSpeed > 0f)
		{
			plusNumState = true;	
		}
		else
		{
			plusNumState = false;
		}

		if(plusNumState)
		{
			if(moveSpeed > m_MaxSpeedValue)
				moveSpeed = m_MaxSpeedValue;
		}
		else
		{
			if(moveSpeed < -m_MaxSpeedValue)
				moveSpeed = -m_MaxSpeedValue;
		}

		minusSpeed = m_MoveSpeed/10f;

	}

	public void CalcCurMinusSpeed()
	{
		if (Mathf.Abs(minusSpeed) == m_MinMinusValue)
			return;

		minusSpeed = m_MoveSpeed/10f;
		if (Mathf.Abs(minusSpeed) < m_MinMinusValue) {
			if (minusSpeed < 0f) {
				minusSpeed = -m_MinMinusValue;
			} else {
				minusSpeed = m_MinMinusValue;
			}
		}
	}

	public void CalcArriveEndValue()
	{
		m_MoveSpeed = m_MoveSpeed / 10f;
		CalcCurMinusSpeed ();
	}

	#endregion

	#region Properties

	public float startPosValue
	{
		get{ return m_StartPosValue; }
		set{ m_StartPosValue = value; }
	}

	public float endPosValue
	{
		get{ return m_EndPosValue; }
		set{ m_EndPosValue = value; }
	}

	public float gapPosValue
	{
		get{ return m_GapPosValue; }
		set{ m_GapPosValue = value; }
	}

	public float moveSpeed
	{
		get{ return m_MoveSpeed; }
		set
		{ 
			m_MoveSpeed = value; 
		}
	}

	public float minusSpeed
	{
		get{ return m_MinusSpeed; }
		set
		{
			m_MinusSpeed = value; 
		}
	}

	public List<CAScrItemData> lMoveScrItemData
	{
		get{ return m_lMoveScrItemData; }
		set{ m_lMoveScrItemData = value; }
	}

	public CAScroll.ScrType eListDirection
	{
		get{ return m_eListDirection; }
		set{ m_eListDirection = value; }
	}

	public ListMoveAniType eListMoveAniType
	{
		get{ return m_eListMoveAniType; }
		set{ m_eListMoveAniType = value; }
	}

	public bool plusNumState
	{
		get{ return m_PlusNumState; }
		set{ m_PlusNumState = value; }
	}

	public bool areaMeetState
	{
		get{ return m_AreaMeetState; }
		set{ m_AreaMeetState = value; }
	}

	#endregion

	#region Variables

	float m_StartPosValue;
	float m_EndPosValue;
	float m_GapPosValue;
	float m_MoveSpeed;
	float m_MinusSpeed;
	//Transform m_MoveTransform;
	List<CAScrItemData> m_lMoveScrItemData = null;
	CAScroll.ScrType m_eListDirection = CAScroll.ScrType.eScrHorizontal;
	ListMoveAniType m_eListMoveAniType;
	bool m_PlusNumState;
	float m_MaxSpeedValue = 4500f;
	float m_MinMinusValue = 2f;
	bool m_AreaMeetState = false;

	#endregion
}
