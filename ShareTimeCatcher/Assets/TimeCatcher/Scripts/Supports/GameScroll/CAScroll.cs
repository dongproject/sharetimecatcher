﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CAScrItemData
{
	#region Methods

	public void SetScrItemPos(float itemPosX, float itemPosY)
	{
		posX = itemPosX;
		posY = itemPosY;
		if(itemGameObject != null)
		{
			itemGameObject.transform.localPosition = new Vector3(posX, posY, 0f);
		}
	}

	#endregion

	#region Properties

	public GameObject itemGameObject
	{
		get{ return m_ItemGameObject; }
		set{ m_ItemGameObject = value; }
	}

	public float posX
	{
		get{ return m_PosX; }
		set
		{ 
			m_PosX = value;
		}
	}

	public float posY
	{
		get{ return m_PosY; }
		set
		{ 
			m_PosY = value;
		}
	}

	public object itemInfoObj
	{
		get{ return m_ItemInfoObj; }
		set{ m_ItemInfoObj = value; }
	}

	#endregion

	#region Variables

	GameObject m_ItemGameObject = null;
	float m_PosX;
	float m_PosY;
	object m_ItemInfoObj = null;

	#endregion
}

public interface CASCrollObserver
{
	void OnInitScrListData(CAScroll scroll);
}

public class CAScroll : CACommonMonoBehaviours, CAScrollMoveAniObserver
{
	#region Definitions

	public enum ScrType
	{
		eScrVerticality,
		eScrHorizontal,
	}

	#endregion

	#region MonoBehaviour

	public override void Awake()
	{
		base.Awake();
		m_ScrMoveAniManager.AttachMoveAniObserver ((CAScrollMoveAniObserver)this);
	}

    public override void OnDestroy()
	{
		base.OnDestroy();
		curTouchState = false;
	}

    public override void Start()
	{
		InitTouchArea();
		NotifyInitScrListData();

		curTouchState = true;
	}

	void Update()
	{
		m_ScrMoveAniManager.UpdateListMoveAni ();
	}

	#endregion

	#region Methods

	void InitTouchArea()
	{
		Vector3 scrAreaVec = Vector3.zero;
		if(m_UICamera != null)
		{
			scrAreaVec = m_UICamera.WorldToScreenPoint(m_ScrBoxCollider.transform.position);
		}

		m_ScrAreaStartX = (scrAreaVec.x - m_ScrBoxCollider.size.x*0.5f) + m_ScrBoxCollider.center.x;
		m_ScrAreaEndX = (scrAreaVec.x + m_ScrBoxCollider.size.x*0.5f) + m_ScrBoxCollider.center.x;
		m_ScrAreaStartY = (scrAreaVec.y - m_ScrBoxCollider.size.y*0.5f) + m_ScrBoxCollider.center.y;
		m_ScrAreaEndY = (scrAreaVec.y + m_ScrBoxCollider.size.y*0.5f) + m_ScrBoxCollider.center.y;
	}

	public void AttachScrollObserver(CASCrollObserver inputScrOb)
	{
		if(m_lScrObserver.Contains(inputScrOb))
			return;
	
		m_lScrObserver.Add(inputScrOb);
	}

	void NotifyInitScrListData()
	{
		for(int i = 0;i<m_lScrObserver.Count;i++)
		{
			m_lScrObserver[i].OnInitScrListData(this);
		}
	}

	public void AlignScrItemData()
	{
		float curItemPosValue = 0f;
		switch(m_ScrollType)
		{
		case ScrType.eScrVerticality:
			curItemPosValue = m_ScrStartY;
			break;
		case ScrType.eScrHorizontal:
			curItemPosValue = m_ScrStartX;
			break;
		}

		for(int i = 0;i<m_lScrItemData.Count;i++)
		{
			switch(m_ScrollType)
			{
			case ScrType.eScrVerticality:
				m_lScrItemData [i].SetScrItemPos (m_ScrStartX, curItemPosValue);
				if(m_lScrItemData.Count - 1 != i)
					curItemPosValue -= m_ItemGapY;
				break;
			case ScrType.eScrHorizontal:
				m_lScrItemData[i].SetScrItemPos(curItemPosValue, m_ScrStartY);
				if(m_lScrItemData.Count - 1 != i)
					curItemPosValue += m_ItemGapX;
				break;
			}
		}
			
		switch(m_ScrollType)
		{
		case ScrType.eScrVerticality:
			if (curItemPosValue < m_ScrEndY)
				m_ScrItemEndValue = m_ScrEndY;
			else
				m_ScrItemEndValue = curItemPosValue;
			break;
		case ScrType.eScrHorizontal:
			if (curItemPosValue > m_ScrEndX)
				m_ScrItemEndValue = m_ScrEndX;
			else
				m_ScrItemEndValue = curItemPosValue;
			break;
		}
	}

	void MoveScrItemData(float moveValue)
	{
		switch(m_ScrollType)
		{
		case ScrType.eScrVerticality:
			if(moveValue < 0f)
			{
				if(m_lScrItemData[0].posY + moveValue < m_ScrStartY)
				{
					moveValue *= 0.5f;
					//return;
				}
			}
			else 
			{
				if(m_lScrItemData[m_lScrItemData.Count - 1].posY + moveValue > m_ScrItemEndValue)
				{
					moveValue *= 0.5f;
					//return;
				}
			}
			break;
		case ScrType.eScrHorizontal:
			if(moveValue > 0f)
			{
				if(m_lScrItemData[0].posX + moveValue > m_ScrStartX)
				{
					moveValue *= 0.5f;
					//return;
				}
			}
			else 
			{
				if(m_lScrItemData[m_lScrItemData.Count - 1].posX + moveValue < m_ScrItemEndValue)
				{
					moveValue *= 0.5f;
					//return;
				}
			}
			break;
		}

		for(int i = 0;i<m_lScrItemData.Count;i++)
		{
			switch(m_ScrollType)
			{
			case ScrType.eScrVerticality:
				m_lScrItemData[i].SetScrItemPos(m_ScrStartX, m_lScrItemData[i].posY + moveValue);
				break;
			case ScrType.eScrHorizontal:
				m_lScrItemData[i].SetScrItemPos(m_lScrItemData[i].posX + moveValue, m_ScrStartY);
				break;
			}
		}
	}

	bool AddRecOutMoveAni()
	{
		if(m_ScrMoveAniManager.CheckAniList())
			return true;

		switch (m_ScrollType) {
		case ScrType.eScrVerticality:
			if (m_ScrStartY > m_lScrItemData [0].posY) {
				m_ScrMoveAniManager.AddRectOutListMoveAni (m_lScrItemData, m_lScrItemData [0].posY, m_ScrStartY, m_ScrollType, CAScrollMoveAni.ListMoveAniType.eRectOutAniType);
				return true;
			} else if (m_ScrItemEndValue < m_lScrItemData [m_lScrItemData.Count - 1].posY) {
				m_ScrMoveAniManager.AddRectOutListMoveAni (m_lScrItemData, m_lScrItemData [m_lScrItemData.Count - 1].posY, m_ScrItemEndValue, m_ScrollType, CAScrollMoveAni.ListMoveAniType.eRectOutAniType);
				return true;
			}
			break;
		case ScrType.eScrHorizontal:
			if (m_ScrStartX < m_lScrItemData [0].posX) {
				m_ScrMoveAniManager.AddRectOutListMoveAni (m_lScrItemData, m_lScrItemData [0].posX, m_ScrStartX, m_ScrollType, CAScrollMoveAni.ListMoveAniType.eRectOutAniType);
				return true;
			} else if (m_ScrItemEndValue > m_lScrItemData [m_lScrItemData.Count - 1].posX) {
				m_ScrMoveAniManager.AddRectOutListMoveAni (m_lScrItemData, m_lScrItemData [m_lScrItemData.Count - 1].posX, m_ScrItemEndValue, m_ScrollType, CAScrollMoveAni.ListMoveAniType.eRectOutAniType);
				return true;
			}
			break;
		}

		return false;
	}

	void AddScrollMoveAni()
	{
		if(m_ScrMoveAniManager.CheckAniList())
			return;
		if(m_StartListTouchTime == 0)
			return;
		float gapMoveValue = 0f;
		switch (m_ScrollType) {
		case ScrType.eScrVerticality:
			gapMoveValue = touchValues[0].m_Y - m_StartValue;
			break;
		case ScrType.eScrHorizontal:
			gapMoveValue = touchValues[0].m_X - m_StartValue;
			break;
		}

		if(Mathf.Abs(gapMoveValue) < 20f)
			return;

		long endTime = System.DateTime.Now.ToBinary();
		long gapTime = endTime - m_StartListTouchTime;
		float calcGapTime = (float)((double)gapTime/(double)m_SysTimeToSecond);

		float calcMoveSpeed = gapMoveValue/calcGapTime;

		m_StartListTouchTime = 0;

		if(Mathf.Abs(calcMoveSpeed) > 10f)
		{
			m_ScrMoveAniManager.AddScrollListMoveDragAni(lScrItemData, calcMoveSpeed, m_ScrollType);
		}
	}

	#endregion

	#region Touch Handler

	public override CACurGamePlayTouchData OnGetTouchData(int touchID, float posX, float posY, bool isTouch)
	{

		return null;
	}

	public override bool OnGetTouchState(int touchID, float posX, float posY)
	{
		if(m_ScrAreaStartX <= posX && m_ScrAreaEndX >= posX && m_ScrAreaStartY <= posY && m_ScrAreaEndY >= posY)
		{
//			Debug.Log(string.Format("OnGetTouchState true"));
			m_ScrMoveAniManager.StopListMoveAni();
			m_ListRectOutState = false;
			return true;
		}
		else
		{
//			Debug.Log(string.Format("OnGetTouchState false"));
			return false;
		}
	}

	public override void OnMultiTouchPosSet(float posX, float posY){}

	public override void OnFirstTouchDrag (int touchID, float posX, float posY)
	{
		m_StartListTouchTime = System.DateTime.Now.ToBinary();
		switch(m_ScrollType)
		{
		case ScrType.eScrVerticality:
			m_StartValue = posY;
			break;
		case ScrType.eScrHorizontal:
			m_StartValue = posX;
			break;
		}
	}

	public override void OnTouchDrag(int touchID, float posX, float posY)
	{
		float moveValue = 0f;
		switch(m_ScrollType)
		{
		case ScrType.eScrVerticality:
			if (touchValues[0].m_TouchID != -1) {
				moveValue = touchValues[0].m_Y - touchValues[0].m_PreY;
			} else if (touchValues[1].m_TouchID != -1) {
				moveValue = touchValues[1].m_Y - touchValues[1].m_PreY;
			}
			break;
		case ScrType.eScrHorizontal:
			if (touchValues[0].m_TouchID != -1) {
				moveValue = touchValues[0].m_X - touchValues[0].m_PreX;
			} else if (touchValues[1].m_TouchID != -1) {
				moveValue = touchValues[1].m_X - touchValues[1].m_PreX;
			}
			break;
		}

		if(moveValue == 0f)
			return;

		if (Mathf.Abs(moveValue) < 0.5f) {
			m_StartListTouchTime = System.DateTime.Now.ToBinary();
			switch(m_ScrollType)
			{
			case ScrType.eScrVerticality:
				m_StartValue = posY;
				break;
			case ScrType.eScrHorizontal:
				m_StartValue = posX;
				break;
			}
		}

		MoveScrItemData(moveValue);
	}

	public override void OnTouchScale(){}

	public override void OnTouchEvent(CACurGamePlayTouchData playTouchData, float posX, float posY){}
	public override void OnTouchRelease(int touchID, float posX, float posY)
	{
		if (AddRecOutMoveAni ()) {
			m_ListRectOutState = true;
		} else {
			AddScrollMoveAni ();
		}

		m_StartListTouchTime = 0;
	}

	#endregion

	#region CAScrollMoveAniObserver

	void CAScrollMoveAniObserver.OnCompleteListMoveAni(CAScrollMoveAni listMoveAniData)
	{
		switch(listMoveAniData.eListMoveAniType)
		{
		case CAScrollMoveAni.ListMoveAniType.eRectOutAniType:
			m_ListRectOutState = false;
			break;
		case CAScrollMoveAni.ListMoveAniType.eScrollMoveAniType:
			break;
		}
	}

	void CAScrollMoveAniObserver.OnCompleteScrollMoveAni()
	{
		if(AddRecOutMoveAni())
			m_ListRectOutState = true;
	}

	#endregion

	#region Properties

	public Camera uiCamera
	{
		get{ return m_UICamera; }
		set{ m_UICamera = value; }
	}

	public int scrollDepth
	{
		get{ return m_ScrollDepth; }
		set{ m_ScrollDepth = value; }
	}

	public float itemWidth
	{
		get{ return m_ItemWidth; }
	}

	public float itemHeight
	{
		get{ return m_ItemHeight; }
	}

	public float itemGapX
	{
		get{ return m_ItemGapX; }
	}

	public float itemGapY
	{
		get{ return m_ItemGapY; }
	}

	public float scrStartX
	{
		get{ return m_ScrStartX; }
	}

	public float scrStartY
	{
		get{ return m_ScrStartY; }
	}

	public float scrEndX
	{
		get{ return m_ScrEndX; }
	}

	public float scrEndY
	{
		get{ return m_ScrEndY; }
	}

	public List<CAScrItemData> lScrItemData
	{
		get{ return m_lScrItemData; }
	}

	public float scrItemEndValue
	{
		get{ return m_ScrItemEndValue; }
	}

	#endregion

	#region Variables

	[SerializeField] Camera m_UICamera = null;
	[SerializeField] int m_ScrollDepth;
	[SerializeField] BoxCollider m_ScrBoxCollider;
	[SerializeField] ScrType m_ScrollType;
	[SerializeField] float m_ItemWidth;
	[SerializeField] float m_ItemHeight;
	[SerializeField] float m_ItemGapX;
	[SerializeField] float m_ItemGapY;
	[SerializeField] float m_ScrStartX;
	[SerializeField] float m_ScrStartY;
	[SerializeField] float m_ScrEndX;
	[SerializeField] float m_ScrEndY;

	float m_ScrAreaStartX;
	float m_ScrAreaEndX;
	float m_ScrAreaStartY;
	float m_ScrAreaEndY;

	float m_ScrItemEndValue;

	List<CAScrItemData> m_lScrItemData = new List<CAScrItemData>();
	List<CASCrollObserver> m_lScrObserver = new List<CASCrollObserver>();
	CAScrollMoveAniManager m_ScrMoveAniManager = new CAScrollMoveAniManager();

	bool m_ListRectOutState = false;
	long m_StartListTouchTime = 0;
	float m_StartValue = 0f;

	long m_SysTimeToSecond = 10000000;

	#endregion
}
