﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScrollCenterFocusManager : MonoBehaviour 
{
    #region Serialize Variables

#pragma warning disable 649

    [SerializeField] Transform _objListTrans = default(Transform);
    [SerializeField] ScrollDefinitions.MoveType _moveType = default(ScrollDefinitions.MoveType);
    [SerializeField] Camera _scrollCamera = default(Camera);
    [SerializeField] BoxCollider _scrollTouchArea = default(BoxCollider);
    [SerializeField] float _topGapX = default(float);
    [SerializeField] float _bottomGapX = default(float);
    [SerializeField] float _topGapY = default(float);
    [SerializeField] float _bottomGapY = default(float);
    [SerializeField] float _objGapValue = default(float);
    [SerializeField] float _scaleObjGapValue;

#pragma warning restore 649

    #endregion

    #region Const Variables

    const int _outLineMotionID = 1;
    const int _scrollMotionID = 2;
    const int _centerFocusMotionID = 3;

    #endregion

    #region Variables

    bool _isUpdate = true;
    bool _isTouch = true;

    float _widthRatio = 1f;
    float _heightRatio = 1f;

    protected ScreenTouchValue[] _screenTouchValues = new ScreenTouchValue[2];
    private float _firstStartX;
    private float _firstStartY;
    private int _touchCount;

    bool _isScrollMoveMotion = false;
    bool _isCollideOutLine = false;
    bool _isCenterFocusMotion = false;

    int _curFocusScrollIndex = -1;
    ScrollDefinitions.MoveDirection _moveDir = ScrollDefinitions.MoveDirection.None;

    ObjectScrollMotionManager _objectMotionManager = new ObjectScrollMotionManager();

    Action<float, float> _onScrollObject = null;
    Rect _scrollAreaRect;
    float _startRectY = -10000f;
    float _curStartRectY = 0f;
    float _endRectY;

    float _centerFocusStartX;
    float _centerFocusY;

    float _curCenterX;
    float _curCenterY;

    float _startTouchTime;
    Vector2 _startTouchValue;

    List<ScrollBaseObject> _scrollObjectList = new List<ScrollBaseObject>();
    //ScrollBaseObject _focusScrollObject = null;
    int _focusScrollIndex = -1;

    float _minScale = 0.5f;
    Action<int> _onFocusScrollIndex = null;
    //Action<int> _onSelectButton = null;

    #endregion

    #region Properties

    public bool IsTouch
    {
        get { return _isTouch; }
        set { _isTouch = value; }
    }

    public List<ScrollBaseObject> ScrollObjectList
    {
        get { return _scrollObjectList; }
    }

    public ScrollBaseObject FocusScrollObject
    {
        get
        {
            if(_focusScrollIndex != -1 && _scrollObjectList.Count > _focusScrollIndex)
            {
                return _scrollObjectList[_focusScrollIndex];
            }

            return null;
        }
    }

    public int FocusScrollIndex
    {
        get { return _focusScrollIndex; }
    }

    public Action<int> OnFocusScrollIndex
    {
        get { return _onFocusScrollIndex; }
        set { _onFocusScrollIndex = value; }
    }

    //public Action<int> OnSelectButton
    //{
    //    get { return _onSelectButton; }
    //    set { _onSelectButton = value; }
    //}

    #endregion

    #region MonoBehaviour Methods

    private void Awake()
    {
        InitScrollData();
    }

    void OnEnable()
    {
        InitTouchValue();

        _isUpdate = true;
        StartCoroutine(UpdateObjScroll());
    }

    void OnDisable()
    {
        StopCoroutine(UpdateObjScroll());
        _isUpdate = false;
    }

    #endregion

    #region Methods

    public void InitScrollData()
    {
        Vector3 listScreenPos = _scrollCamera.WorldToScreenPoint(_objListTrans.transform.position);
        _centerFocusStartX = _objListTrans.transform.localPosition.x;

        _scrollAreaRect = new Rect(listScreenPos.x - (_scrollTouchArea.size.x * 0.5f), 
            listScreenPos.y - (_scrollTouchArea.size.y * 0.5f),
            _scrollTouchArea.size.x, _scrollTouchArea.size.y);
    }

    public void SetCenterX(float posX)
    {
        _curCenterX = posX;
    }

    public void InitTouchValue()
    {
        for (int i = 0; i < _screenTouchValues.Length; i++)
        {
            _screenTouchValues[i] = new ScreenTouchValue();
            _screenTouchValues[i].touchID = -1;
            _screenTouchValues[i].curPosition = Vector3.zero;
            _screenTouchValues[i].prePosition = Vector3.zero;
            _screenTouchValues[i].firstDragState = false;
        }

        _firstStartX = 0f;
        _firstStartY = 0f;
        _touchCount = 0;
        _curFocusScrollIndex = -1;
    }

    public void ResetScrollData()
    {
        for(int i = 0;i< _scrollObjectList.Count; i++)
        {
            if(_scrollObjectList[i].ScrollObjInfo.ScrollGameObject != null)
            {
                Destroy(_scrollObjectList[i].ScrollObjInfo.ScrollGameObject);
            }
        }

        _scrollObjectList.Clear();

        _objListTrans.transform.localPosition = new Vector3(_centerFocusStartX, 0f, 0f);
    }

    void UpdateEditor()
    {
        if (Input.GetMouseButtonDown(0) && _screenTouchValues[0].touchID == -1)
        {
            _firstStartX = Input.mousePosition.x;
            _firstStartY = Input.mousePosition.y;

            //          Debug.Log (string.Format ("UpdateEditor _firstStartX : {0}, _firstStartY : {1}", _firstStartX, _firstStartY));

            if (!CheckValidTouchArea(_firstStartX, _firstStartY))
            {
                return;
            }

            //          _firstDragState = false;

            _screenTouchValues[0].touchID = 0;
            _screenTouchValues[0].curPosition = Input.mousePosition;
            _screenTouchValues[0].prePosition = Input.mousePosition;
            _screenTouchValues[0].firstDragState = false;

            if (_isScrollMoveMotion)
            {
                ReleaseScrollMoveMotion();
            }

        }
        else if (Input.GetMouseButton(0) && _screenTouchValues[0].touchID == 0)
        {
            _screenTouchValues[0].prePosition = _screenTouchValues[0].curPosition;
            _screenTouchValues[0].curPosition = Input.mousePosition;

            if (_screenTouchValues[0].firstDragState)
            {

                MoveScroll(Input.mousePosition, _screenTouchValues[0].curPosition.x - _screenTouchValues[0].prePosition.x, _screenTouchValues[0].curPosition.y - _screenTouchValues[0].prePosition.y);
            }
            else
            {
                if (Mathf.Abs(_firstStartX - Input.mousePosition.x) > (6f) || Mathf.Abs(_firstStartY - Input.mousePosition.y) > (6f))
                {
                    SetFirstDragData(Input.mousePosition);
                }
            }
        }
        else if (Input.GetMouseButtonUp(0) && _screenTouchValues[0].touchID == 0)
        {
            _screenTouchValues[0].touchID = -1;

            int releaseIndex = -1;
            if (_curFocusScrollIndex != -1)
            {
                releaseIndex = _curFocusScrollIndex;
            }

            bool isOutLineMotion = false;

            switch (_moveType)
            {
                case ScrollDefinitions.MoveType.HorizontalScroll:
                    isOutLineMotion = CheckHorizontalScrollMotion();
                    break;
                case ScrollDefinitions.MoveType.VerticalScroll:
                    isOutLineMotion = CheckVerticalScrollMotion();
                    break;
            }

            if (!isOutLineMotion && _screenTouchValues[0].firstDragState)
            {
                if (releaseIndex != -1)
                {
                    CheckMoveScrollMotion(releaseIndex, Input.mousePosition);
                }
            } else if (!isOutLineMotion)
            {
                if (releaseIndex != -1)
                {
                    switch (_moveType)
                    {
                        case ScrollDefinitions.MoveType.HorizontalScroll:
                            CheckHorizontalCenterFocusMotion();
                            break;
                        case ScrollDefinitions.MoveType.VerticalScroll:
                            CheckVerticalCenterFocusMotion();
                            break;
                    }
                }
            }
        }
    }

    void UpdateDevice()
    {
        if (Input.touchCount > 0)
        {
            for (int i = 0; i < Input.touchCount; i++)
            {
                Touch curTouch = Input.GetTouch(i);

                if (curTouch.phase == TouchPhase.Began)
                {
                    if (!CheckValidTouchArea(curTouch.position.x, curTouch.position.y))
                    {
                        return;
                    }

                    if (_touchCount == 0)
                    {
                        //                      _firstDragState = false;

                        _firstStartX = curTouch.position.x;
                        _firstStartY = curTouch.position.y;
                    }

                    if (_screenTouchValues[0].touchID == -1)
                    {
                        _touchCount++;

                        _screenTouchValues[0].touchID = curTouch.fingerId;
                        _screenTouchValues[0].curPosition = curTouch.position;
                        _screenTouchValues[0].prePosition = curTouch.position;
                        _screenTouchValues[0].firstDragState = false;

                        if (_isScrollMoveMotion)
                        {
                            ReleaseScrollMoveMotion();
                        }
                    }
                    else if (_screenTouchValues[1].touchID == -1)
                    {
                        _touchCount++;

                        _screenTouchValues[1].touchID = curTouch.fingerId;
                        _screenTouchValues[1].curPosition = curTouch.position;
                        _screenTouchValues[1].prePosition = curTouch.position;
                        _screenTouchValues[1].firstDragState = false;

                        if (_isScrollMoveMotion)
                        {
                            ReleaseScrollMoveMotion();
                        }
                    }
                }
                else if (curTouch.phase == TouchPhase.Moved)
                {
                    if (_screenTouchValues[0].touchID == curTouch.fingerId)
                    {
                        _screenTouchValues[0].prePosition = _screenTouchValues[0].curPosition;
                        _screenTouchValues[0].curPosition = curTouch.position;

                        //                      if (_touchCount == 1) {
                        //                          
                        //                      }

                        if (_screenTouchValues[0].firstDragState)
                        {

                            MoveScroll(curTouch.position, _screenTouchValues[0].curPosition.x - _screenTouchValues[0].prePosition.x, _screenTouchValues[0].curPosition.y - _screenTouchValues[0].prePosition.y, 0);
                        }
                        else
                        {
                            if (Mathf.Abs(_firstStartX - curTouch.position.x) > 6 || Mathf.Abs(_firstStartY - curTouch.position.y) > 6)
                            {
                                SetFirstDragData(curTouch.position, 0);
                            }
                        }
                    }
                    else if (_screenTouchValues[1].touchID == curTouch.fingerId)
                    {
                        _screenTouchValues[1].prePosition = _screenTouchValues[1].curPosition;
                        _screenTouchValues[1].curPosition = curTouch.position;

                        //                      if (_touchCount == 1) {
                        //                          
                        //                      }

                        if (_screenTouchValues[1].firstDragState)
                        {

                            MoveScroll(curTouch.position, _screenTouchValues[1].curPosition.x - _screenTouchValues[1].prePosition.x, _screenTouchValues[1].curPosition.y - _screenTouchValues[1].prePosition.y, 1);
                        }
                        else
                        {
                            if (Mathf.Abs(_firstStartX - curTouch.position.x) > 6 || Mathf.Abs(_firstStartY - curTouch.position.y) > 6)
                            {
                                SetFirstDragData(curTouch.position, 1);
                            }
                        }
                    }
                }
                else if (curTouch.phase == TouchPhase.Ended)
                {
                    int releaseIndex = -1;

                    bool isScrollMotion = false;
                    if (_screenTouchValues[0].touchID == curTouch.fingerId)
                    {
                        _screenTouchValues[0].touchID = -1;
                        if (_screenTouchValues[0].firstDragState)
                            isScrollMotion = true;
                        _touchCount--;
                        if (_curFocusScrollIndex == 0)
                        {
                            releaseIndex = 0;
                            _curFocusScrollIndex = -1;
                        }
                    }
                    else if (_screenTouchValues[1].touchID == curTouch.fingerId)
                    {
                        _screenTouchValues[1].touchID = -1;
                        if (_screenTouchValues[1].firstDragState)
                            isScrollMotion = true;
                        _touchCount--;
                        if (_curFocusScrollIndex == 1)
                        {
                            releaseIndex = 1;
                            _curFocusScrollIndex = -1;
                        }
                    }

                    bool isOutLineMotion = false;

                    if (_touchCount == 0)
                    {
                        switch (_moveType)
                        {
                            case ScrollDefinitions.MoveType.HorizontalScroll:
                                isOutLineMotion = CheckHorizontalScrollMotion();
                                break;
                            case ScrollDefinitions.MoveType.VerticalScroll:
                                isOutLineMotion = CheckVerticalScrollMotion();
                                break;
                        }
                    }

                    if (!isOutLineMotion && isScrollMotion)
                    {
                        if (releaseIndex != -1)
                        {
                            CheckMoveScrollMotion(releaseIndex, curTouch.position);
                        }
                    }
                    else if (!isOutLineMotion)
                    {
                        if (releaseIndex != -1)
                        {
                            switch (_moveType)
                            {
                                case ScrollDefinitions.MoveType.HorizontalScroll:
                                    CheckHorizontalCenterFocusMotion();
                                    break;
                                case ScrollDefinitions.MoveType.VerticalScroll:
                                    CheckVerticalCenterFocusMotion();
                                    break;
                            }
                        }
                    }

                }
                else if (curTouch.phase == TouchPhase.Canceled)
                {
                    int releaseIndex = -1;
                    bool isScrollMotion = false;
                    if (_screenTouchValues[0].touchID == curTouch.fingerId)
                    {
                        _screenTouchValues[0].touchID = -1;
                        if (_screenTouchValues[0].firstDragState)
                            isScrollMotion = true;
                        _touchCount--;
                        if (_curFocusScrollIndex == 0)
                        {
                            releaseIndex = 0;
                            _curFocusScrollIndex = -1;
                        }
                    }
                    else if (_screenTouchValues[1].touchID == curTouch.fingerId)
                    {
                        _screenTouchValues[1].touchID = -1;
                        if (_screenTouchValues[1].firstDragState)
                            isScrollMotion = true;
                        _touchCount--;
                        if (_curFocusScrollIndex == 1)
                        {
                            releaseIndex = 1;
                            _curFocusScrollIndex = -1;
                        }
                    }

                    bool isOutLineMotion = false;

                    if (_touchCount == 0)
                    {
                        switch (_moveType)
                        {
                            case ScrollDefinitions.MoveType.HorizontalScroll:
                                isOutLineMotion = CheckHorizontalScrollMotion();
                                break;
                            case ScrollDefinitions.MoveType.VerticalScroll:
                                isOutLineMotion = CheckVerticalScrollMotion();
                                break;
                        }
                    }

                    if (!isOutLineMotion && isScrollMotion)
                    {
                        if (releaseIndex != -1)
                        {
                            CheckMoveScrollMotion(releaseIndex, curTouch.position);
                        }
                    }
                    else if (!isOutLineMotion)
                    {
                        if (releaseIndex != -1)
                        {
                            switch (_moveType)
                            {
                                case ScrollDefinitions.MoveType.HorizontalScroll:
                                    CheckHorizontalCenterFocusMotion();
                                    break;
                                case ScrollDefinitions.MoveType.VerticalScroll:
                                    CheckVerticalCenterFocusMotion();
                                    break;
                            }
                        }
                    }
                }
            }
        }
    }

    public void AddScrollObject(IScrollObjectInfo scrollObjInfo)
    {
        ScrollBaseObject scrollObject = new ScrollBaseObject();
        scrollObject.ScrollObjInfo = scrollObjInfo;
        scrollObject.ScrollObjInfo.ScrollGameObject.transform.SetParent(_objListTrans);
        //scrollObject.ScrollObjInfo.ScrollGameObject.transform.localScale = Vector3.one;

        float calcPosX = 0f;
        float calcPosY = 0f;

        if (_scrollObjectList.Count == 0)
        {
            switch (_moveType)
            {
                case ScrollDefinitions.MoveType.HorizontalScroll:
                    {
                        calcPosX = _centerFocusStartX;
                    }
                    break;
                case ScrollDefinitions.MoveType.VerticalScroll:
                    {
                        calcPosY = _endRectY - _topGapY - (scrollObject.ScrollObjInfo.ObjectHeight * 0.5f);
                        //                  Debug.Log (string.Format ("AddScrollObject _endRectY : {0}", _endRectY));
                        _curStartRectY = calcPosY - (scrollObject.ScrollObjInfo.ObjectHeight * 0.5f);
                    }
                    break;
            }
        }
        else
        {
            ScrollBaseObject lastScrollObject = _scrollObjectList[_scrollObjectList.Count - 1];

            switch (_moveType)
            {
                case ScrollDefinitions.MoveType.HorizontalScroll:
                    calcPosX = lastScrollObject.ObjPosX + (lastScrollObject.ScrollObjInfo.ObjectWidth * 0.5f) + _objGapValue + (scrollObject.ScrollObjInfo.ObjectWidth * 0.5f);

                    break;
                case ScrollDefinitions.MoveType.VerticalScroll:
                    {
                        calcPosY = lastScrollObject.ObjPosY - (lastScrollObject.ScrollObjInfo.ObjectHeight * 0.5f) - _objGapValue - (scrollObject.ScrollObjInfo.ObjectHeight * 0.5f);
                        _curStartRectY = calcPosY - (scrollObject.ScrollObjInfo.ObjectHeight * 0.5f);
                        if (_curStartRectY < _startRectY + _bottomGapY)
                        {
                            _curStartRectY = _startRectY + _bottomGapY;
                        }

                        float curBottomObjPosY = calcPosY - lastScrollObject.ScrollObjInfo.ObjectHeight * 0.5f + _objListTrans.transform.localPosition.y;

                        if (curBottomObjPosY < _curStartRectY)
                        {
                            float gapRectYValue = _curStartRectY - curBottomObjPosY;
                            _objListTrans.transform.localPosition = new Vector3(_objListTrans.transform.localPosition.x,
                                _objListTrans.transform.localPosition.y + gapRectYValue,
                                _objListTrans.transform.localPosition.z);
                        }
                    }
                    break;
            }
        }

        scrollObject.SetScrollObjectPos(calcPosX, calcPosY);

        _scrollObjectList.Add(scrollObject);

        //float scaleObjValue = 1f;
        if (_moveType == ScrollDefinitions.MoveType.HorizontalScroll)
        {
            SetHorizontalObjectScale(_scrollObjectList.Count - 1);
            //float curCenterPosX = scrollObject.GetCenterPosX(_objListTrans.localPosition.x);
            //float curGapObj = Mathf.Abs(_curCenterX - curCenterPosX);
            //if (_scaleObjGapValue < curGapObj)
            //{
            //    scaleObjValue = _minScale;
            //}
            //else
            //{
            //    float calcScale = (curGapObj / _scaleObjGapValue) * 0.5f;
            //    scaleObjValue = 1f - calcScale;
            //}
        }
        else if(_moveType == ScrollDefinitions.MoveType.VerticalScroll)
        {
            SetVerticalObjectScale(_scrollObjectList.Count - 1);
        }

        //scrollObject.ScrollObjInfo.ScrollGameObject.transform.localScale = new Vector3(scaleObjValue, scaleObjValue, 1f);
    }

    public void SetStartFocusObj()
    {
        //_focusScrollObject = _scrollObjectList[0];
        SetFocusScrollIndex(0);
    }

    void SetFocusScrollIndex(int scollIndex)
    {
        _focusScrollIndex = scollIndex;
        if (_onFocusScrollIndex != null)
            _onFocusScrollIndex(_focusScrollIndex);
    }

    bool CheckValidTouchArea(float posX, float posY)
    {
        //Debug.Log(string.Format("CheckValidTouchArea posX : {0}, posY : {1}, XMin : {2}, xMax : {3}, yMin : {4}, yMax : {5}", 
            //posX, posY, _scrollAreaRect.xMin, _scrollAreaRect.xMax, _scrollAreaRect.yMin, _scrollAreaRect.yMax));
        if ((_scrollAreaRect.xMin <= posX && _scrollAreaRect.xMax >= posX) &&
           (_scrollAreaRect.yMin <= posY && _scrollAreaRect.yMax >= posY))
        {
                      //Debug.Log (string.Format (" CheckValidTouchArea True"));
            return true;
        }

        //      Debug.Log (string.Format (" CheckValidTouchArea false"));
        return false;
    }

    void CheckMoveScrollMotion(int focusIndex, Vector2 posValue)
    {
        float gapTime = Time.time - _startTouchTime;
        float gapMove = 0f;

        switch (_moveType)
        {
            case ScrollDefinitions.MoveType.HorizontalScroll:
                gapMove = posValue.x - _startTouchValue.x;
                break;
            case ScrollDefinitions.MoveType.VerticalScroll:
                gapMove = posValue.y - _startTouchValue.y;
                break;
        }

        if (_objectMotionManager.AddAccelerationListMove(_moveType, gapTime, gapMove, OnChangeMoveMotionValue, _scrollMotionID, OnCompletedMoveMotionScroll))
        {
            _isScrollMoveMotion = true;
        }

        if (!_isScrollMoveMotion)
        {
            switch (_moveType)
            {
                case ScrollDefinitions.MoveType.HorizontalScroll:
                    CheckHorizontalCenterFocusMotion();
                    break;
                case ScrollDefinitions.MoveType.VerticalScroll:
                    CheckVerticalCenterFocusMotion();
                    break;
            }
        }
    }

    protected void MoveScroll(Vector2 touchPos, float moveX, float moveY, int focusIndex = 0)
    {
        //Debug.Log(string.Format("MoveScroll moveX : {0}", moveX));
        int isPlusState = -1; // 0 : Minus, 1 : Plus
        switch (_moveType)
        {
            case ScrollDefinitions.MoveType.HorizontalScroll:
                moveY = 0f;

                if (moveX != 0f)
                {
                    moveX *= _widthRatio;
                    if (moveX < 0f)
                    {
                        isPlusState = 0;
                    }
                    else
                    {
                        isPlusState = 1;
                    }
                }
                else
                {
                    SetFirstDragData(touchPos);
                    _moveDir = ScrollDefinitions.MoveDirection.None;
                }

                break;
            case ScrollDefinitions.MoveType.VerticalScroll:
                moveX = 0f;

                if (moveY != 0f)
                {
                    moveY *= _heightRatio;
                    if (moveY < 0f)
                    {
                        isPlusState = 0;
                    }
                    else
                    {
                        isPlusState = 1;
                    }
                }
                else
                {
                    SetFirstDragData(touchPos);
                    _moveDir = ScrollDefinitions.MoveDirection.None;
                }
                break;
        }

        if (isPlusState != -1)
        {
            switch (_moveDir)
            {
                case ScrollDefinitions.MoveDirection.None:
                    if (isPlusState == 0)
                    {
                        _moveDir = ScrollDefinitions.MoveDirection.MinusMove;
                    }
                    else if (isPlusState == 1)
                    {
                        _moveDir = ScrollDefinitions.MoveDirection.PlusMove;
                    }
                    SetFirstDragData(touchPos);
                    break;
                case ScrollDefinitions.MoveDirection.PlusMove:
                    if (isPlusState == 0)
                    {
                        _moveDir = ScrollDefinitions.MoveDirection.MinusMove;
                        SetFirstDragData(touchPos);
                    }
                    break;
                case ScrollDefinitions.MoveDirection.MinusMove:
                    if (isPlusState == 1)
                    {
                        _moveDir = ScrollDefinitions.MoveDirection.PlusMove;
                        SetFirstDragData(touchPos);
                    }
                    break;
            }
        }

        //Debug.Log (string.Format ("MoveScroll 2 moveX : {0}, moveY : {1}", moveX, moveY));

        _objListTrans.localPosition = new Vector3(GetObjCalcMovePosX(moveX), GetObjCalcMovePosY(moveY), _objListTrans.localPosition.z);

        if (_onScrollObject != null)
        {
            _onScrollObject(moveX, moveY);
        }

        CheckCenterFocusObject();
    }

    void CheckCenterFocusObject()
    {
        switch (_moveType)
        {
            case ScrollDefinitions.MoveType.HorizontalScroll:
                {
                    float curCenterPosX = _scrollObjectList[_focusScrollIndex].GetCenterPosX(_objListTrans.localPosition.x);

                    float preGap = 10000f;
                    float nextGap = 10000f;

                    if (_focusScrollIndex > 0)
                    {
                        float preCenterPosX = _scrollObjectList[_focusScrollIndex - 1].GetCenterPosX(_objListTrans.localPosition.x);
                        preGap = Mathf.Abs(_curCenterX - preCenterPosX);
                    }

                    if(_focusScrollIndex < _scrollObjectList.Count - 1)
                    {
                        float nextCenterPosX = _scrollObjectList[_focusScrollIndex + 1].GetCenterPosX(_objListTrans.localPosition.x);
                        nextGap = Mathf.Abs(_curCenterX - nextCenterPosX);
                    }

                    float curGap = Mathf.Abs(_curCenterX - curCenterPosX);
                    
                    if(preGap < nextGap)
                    {
                        if(preGap < curGap)
                        {
                            SetFocusScrollIndex(_focusScrollIndex - 1);
                        }
                    }
                    else
                    {
                        if (nextGap < curGap)
                        {
                            SetFocusScrollIndex(_focusScrollIndex + 1);
                        }
                    }

                    //Debug.Log(string.Format("CheckCenterFocusObject _focusScrollIndex : {0}", _focusScrollIndex));
                }
                break;
            case ScrollDefinitions.MoveType.VerticalScroll:
                break;
        }

        int preIndex = -1;
        int nextIndex = -1;
        if (_focusScrollIndex > 0)
        {
            preIndex = _focusScrollIndex - 1;
        }

        if (_focusScrollIndex < _scrollObjectList.Count - 1)
        {
            nextIndex = _focusScrollIndex + 1;
        }

        if (_moveType == ScrollDefinitions.MoveType.HorizontalScroll)
        {
            SetHorizontalObjectScale(preIndex);
            SetHorizontalObjectScale(nextIndex);
            SetHorizontalObjectScale(_focusScrollIndex);
        }
        else if (_moveType == ScrollDefinitions.MoveType.VerticalScroll)
        {
            SetVerticalObjectScale(preIndex);
            SetVerticalObjectScale(nextIndex);
            SetVerticalObjectScale(_focusScrollIndex);
        }
    }

    void SetHorizontalObjectScale(int objIndex)
    {
        if (objIndex == -1)
            return;

        float scaleObjValue = 1f;
        float curCenterPosX = _scrollObjectList[objIndex].GetCenterPosX(_objListTrans.localPosition.x);
        float curGapObj = Mathf.Abs(_curCenterX - curCenterPosX);
        if (_scaleObjGapValue < curGapObj)
        {
            scaleObjValue = _minScale;
        }
        else
        {
            float calcScale = (curGapObj / _scaleObjGapValue) * 0.5f;
            scaleObjValue = 1f - calcScale;
        }

        _scrollObjectList[objIndex].ScrollObjInfo.ScrollGameObject.transform.localScale = new Vector3(scaleObjValue, scaleObjValue, 1f);
        _scrollObjectList[objIndex].ScrollObjInfo.SetAlphaObject(scaleObjValue);
    }

    void SetVerticalObjectScale(int objIndex)
    {
        if (objIndex == -1)
            return;

        float scaleObjValue = 1f;
        float curCenterPosY = _scrollObjectList[objIndex].GetCenterPosY(_objListTrans.localPosition.y);
        float curGapObj = Mathf.Abs(_curCenterY - curCenterPosY);
        if (_scaleObjGapValue < curGapObj)
        {
            scaleObjValue = _minScale;
        }
        else
        {
            float calcScale = (curGapObj / _scaleObjGapValue) * 0.5f;
            scaleObjValue = 1f - calcScale;
        }

        _scrollObjectList[objIndex].ScrollObjInfo.ScrollGameObject.transform.localScale = new Vector3(scaleObjValue, scaleObjValue, 1f);
    }

    float GetObjCalcMovePosX(float moveX)
    {
        if (moveX > 0f)
        {
            float compareObjX = _scrollObjectList[0].GetCenterPosX(_objListTrans.localPosition.x);
            if (compareObjX + moveX >= _curCenterX)
            {
                //              moveY = _curStartRectY - compareObjY;
                moveX = moveX * 0.5f;
            }
        }
        else if (moveX < 0f)
        {
            float compareObjX = _scrollObjectList[_scrollObjectList.Count - 1].GetCenterPosX(_objListTrans.localPosition.x);
            if (compareObjX + moveX <= _curCenterX)
            {
                //              moveY = (_endRectY - _topGapY) - compareObjY;
                moveX = moveX * 0.5f;
            }
        }

        return _objListTrans.localPosition.x + moveX;
    }

    float GetObjCalcMovePosY(float moveY)
    {
        if (moveY > 0f)
        {
            float compareObjY = _scrollObjectList[_scrollObjectList.Count - 1].GetBottomPosY(_objListTrans.localPosition.y);
            if (compareObjY + moveY > _curStartRectY)
            {
                //              moveY = _curStartRectY - compareObjY;
                moveY = moveY * 0.5f;
            }
        }
        else if (moveY < 0f)
        {
            float compareObjY = _scrollObjectList[0].GetTopPosY(_objListTrans.localPosition.y);
            if (compareObjY + moveY < _endRectY - _topGapY)
            {
                //              moveY = (_endRectY - _topGapY) - compareObjY;
                moveY = moveY * 0.5f;
            }
        }

        return _objListTrans.localPosition.y + moveY;
    }

    bool CheckHorizontalScrollMotion()
    {
        bool retValue = false;
        float compareLastObjX = _scrollObjectList[_scrollObjectList.Count - 1].GetCenterPosX(_objListTrans.localPosition.x);
        if (compareLastObjX < _curCenterX)
        {
            float gapValue = _curCenterX - compareLastObjX;
            Vector3 startPos = new Vector3(_objListTrans.localPosition.x, 0f, 0f);
            Vector3 destPos = new Vector3(_objListTrans.localPosition.x + gapValue, 0f, 0f);

            _objectMotionManager.AddOutLineMovement(startPos, destPos, OnChangeMoveMotionValue, _outLineMotionID, OnCompletedOutLineMotionAni);

            retValue = true;
        }
        else
        {
            float compareFirstObjX = _scrollObjectList[0].GetCenterPosX(_objListTrans.localPosition.x);
            if (compareFirstObjX > _curCenterX)
            {
                float gapValue = _curCenterX - compareFirstObjX;
                Vector3 startPos = new Vector3(_objListTrans.localPosition.x, 0f, 0f);
                Vector3 destPos = new Vector3(_objListTrans.localPosition.x + gapValue, 0f, 0f);

                _objectMotionManager.AddOutLineMovement(startPos, destPos, OnChangeMoveMotionValue, _outLineMotionID, OnCompletedOutLineMotionAni);

                retValue = true;
            }
        }

        return retValue;
    }

    bool CheckVerticalScrollMotion()
    {
        bool retValue = false;
        float compareLastObjY = _scrollObjectList[_scrollObjectList.Count - 1].GetBottomPosY(_objListTrans.localPosition.y);
        if (compareLastObjY > _curStartRectY)
        {
            float gapValue = _curStartRectY - compareLastObjY;
            Vector3 startPos = new Vector3(0f, _objListTrans.localPosition.y, 0f);
            Vector3 destPos = new Vector3(0f, _objListTrans.localPosition.y + gapValue, 0f);

            _objectMotionManager.AddOutLineMovement(startPos, destPos, OnChangeMoveMotionValue, _outLineMotionID, OnCompletedOutLineMotionAni);

            retValue = true;
        }
        else
        {
            float compareFirstObjY = _scrollObjectList[0].GetTopPosY(_objListTrans.localPosition.y);
            if (compareFirstObjY < _endRectY - _topGapY)
            {
                float gapValue = (_endRectY - _topGapY) - compareFirstObjY;
                Vector3 startPos = new Vector3(0f, _objListTrans.localPosition.y, 0f);
                Vector3 destPos = new Vector3(0f, _objListTrans.localPosition.y + gapValue, 0f);

                _objectMotionManager.AddOutLineMovement(startPos, destPos, OnChangeMoveMotionValue, _outLineMotionID, OnCompletedOutLineMotionAni);

                retValue = true;
            }
        }

        return retValue;
    }

    void CheckHorizontalCenterFocusMotion()
    {
        float curObjPosX = _scrollObjectList[_focusScrollIndex].GetCenterPosX(_objListTrans.localPosition.x);
        float gapValue = _curCenterX - curObjPosX;
        Vector3 startPos = new Vector3(_objListTrans.localPosition.x, 0f, 0f);
        Vector3 destPos = new Vector3(_objListTrans.localPosition.x + gapValue, 0f, 0f);

        if(Mathf.Abs(gapValue) > 0f)
        {
            _isCenterFocusMotion = true;
        }

        _objectMotionManager.AddOutLineMovement(startPos, destPos, OnChangeMoveMotionValue, _outLineMotionID, OnCompletedCenterFocusMotionAni);
    }

    void CheckVerticalCenterFocusMotion()
    {

    }

    void SetFirstDragData(Vector2 touchValue, int focusIndex = 0)
    {
        //      _firstDragState = true;

        if (focusIndex == 0)
        {
            if (_screenTouchValues[1].firstDragState)
            {
                _screenTouchValues[1].firstDragState = false;
            }
        }
        else
        {
            if (_screenTouchValues[0].firstDragState)
            {
                _screenTouchValues[0].firstDragState = false;
            }
        }

        _screenTouchValues[focusIndex].firstDragState = true;

        SetStartDragTouchValues(touchValue);

        _curFocusScrollIndex = focusIndex;
    }

    void SetStartDragTouchValues(Vector2 touchValue)
    {
        _startTouchTime = Time.time;
        _startTouchValue = touchValue;
    }

    bool CheckVerticalOutLinePos()
    {
        bool retValue = false;
        float compareLastObjY = _scrollObjectList[_scrollObjectList.Count - 1].GetBottomPosY(_objListTrans.localPosition.y);
        if (compareLastObjY > _curStartRectY)
        {
            retValue = true;
        }
        else
        {
            float compareFirstObjY = _scrollObjectList[0].GetTopPosY(_objListTrans.localPosition.y);
            if (compareFirstObjY < _endRectY - _topGapY)
            {
                retValue = true;
            }
        }

        return retValue;
    }

    bool CheckHorizontalOutLinePos()
    {
        bool retValue = false;
        float compareLastObjX = _scrollObjectList[_scrollObjectList.Count - 1].GetCenterPosX(_objListTrans.localPosition.x);
        if (compareLastObjX < _curCenterX)
        {
            retValue = true;
        }
        else
        {
            float compareFirstObjX = _scrollObjectList[0].GetCenterPosX(_objListTrans.localPosition.x);
            if (compareFirstObjX > _curCenterX)
            {
                retValue = true;
            }
        }

        return retValue;
    }

    public bool GetFocusObjectState()
    {
        if (!_isScrollMoveMotion && !_isCollideOutLine && !_isCenterFocusMotion)
            return true;

        return false;
    }

    void ReleaseScrollMoveMotion()
    {
        _objectMotionManager.ReleaseObjectMovement(_scrollMotionID, false);
        _isScrollMoveMotion = false;
        _isCollideOutLine = false;
        _isCenterFocusMotion = false;
    }

    #endregion

    #region Coroutine Methods

    IEnumerator UpdateObjScroll()
    {
        while (_isUpdate)
        {
            if (_scrollObjectList.Count > 0 && _isTouch)
            {
#if UNITY_EDITOR
                UpdateEditor();
#else
                UpdateDevice ();
#endif

                _objectMotionManager.Update();
            }

            yield return null;
        }
    }

    #endregion

    #region CallBack Methods

    void OnChangeMoveMotionValue(ObjectScrollMotionManager.MotionType motionType, Vector2 moveValue, int motionID)
    {
        switch (_moveType)
        {
            case ScrollDefinitions.MoveType.HorizontalScroll:
                _objListTrans.localPosition = new Vector3(_objListTrans.localPosition.x + moveValue.x, _objListTrans.localPosition.y, _objListTrans.localPosition.z);

                if (motionID == _scrollMotionID && !_isCollideOutLine)
                {
                    if (CheckHorizontalOutLinePos())
                    {
                        _objectMotionManager.ChangeSpeedValue(_scrollMotionID, 500f);
                        _isCollideOutLine = true;
                    }
                }
                break;
            case ScrollDefinitions.MoveType.VerticalScroll:
                _objListTrans.localPosition = new Vector3(_objListTrans.localPosition.x, _objListTrans.localPosition.y + moveValue.y, _objListTrans.localPosition.z);

                if (motionID == _scrollMotionID && !_isCollideOutLine)
                {
                    if (CheckVerticalOutLinePos())
                    {
                        _objectMotionManager.ChangeSpeedValue(_scrollMotionID, 500f);
                        _isCollideOutLine = true;
                    }
                }
                break;
        }

        CheckCenterFocusObject();
    }

    void OnCompletedOutLineMotionAni()
    {

    }

    void OnCompletedCenterFocusMotionAni()
    {
        _isCenterFocusMotion = false;
    }

    void OnCompletedMoveMotionScroll()
    {
        bool isOutLineMotion = false;
        switch (_moveType)
        {
            case ScrollDefinitions.MoveType.HorizontalScroll:
                isOutLineMotion = CheckHorizontalScrollMotion();
                break;
            case ScrollDefinitions.MoveType.VerticalScroll:
                isOutLineMotion = CheckVerticalScrollMotion();
                break;
        }

        _isScrollMoveMotion = false;
        _isCollideOutLine = false;
        _isCenterFocusMotion = false;

        if (!isOutLineMotion)
        {
            switch (_moveType)             {                 case ScrollDefinitions.MoveType.HorizontalScroll:                     CheckHorizontalCenterFocusMotion();                     break;                 case ScrollDefinitions.MoveType.VerticalScroll:                     CheckVerticalCenterFocusMotion();                     break;             }
        }
    }

    #endregion
}
