﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IScrollObjectInfo
{
    GameObject ScrollGameObject { get; }
    float ObjectMaxWidth { get; set; }
    float ObjectWidth { get; }
    float ObjectHeight { get; }
    void SetAlphaObject(float alpha);
}
