﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public interface CAScrollMoveAniObserver
{
	void OnCompleteListMoveAni(CAScrollMoveAni listMoveAniData);
	void OnCompleteScrollMoveAni();
}

public class CAScrollMoveAniManager
{
	#region Methods

	public void UpdateListMoveAni()
	{
		if(m_lScrollMoveAni.Count == 0)
			return;

		List<CAScrollMoveAni> lDeleteListMoveAni = null;
		for(int i = 0;i<m_lScrollMoveAni.Count;i++)
			
		{
			int completeValue = -1;
			switch(m_lScrollMoveAni[i].eListMoveAniType)
			{
			case CAScrollMoveAni.ListMoveAniType.eRectOutAniType:
				completeValue = UpdateRectOutAni(m_lScrollMoveAni[i]);
				break;
			case CAScrollMoveAni.ListMoveAniType.eScrollMoveAniType:
				completeValue = UpdateScrollMoveAni(m_lScrollMoveAni[i]);
				break;
			}

			if(completeValue == 1)
			{
				if(lDeleteListMoveAni == null)
					lDeleteListMoveAni = new List<CAScrollMoveAni>();
				lDeleteListMoveAni.Add(m_lScrollMoveAni[i]);
			}

		}

		if(lDeleteListMoveAni != null)
		{
			for(int i = 0;i<lDeleteListMoveAni.Count;i++)
			{
				m_lScrollMoveAni.Remove(lDeleteListMoveAni[i]);
				if(lDeleteListMoveAni[i].eListMoveAniType == CAScrollMoveAni.ListMoveAniType.eScrollMoveAniType)
				{
					NotifyCompleteScrollMoveAni();
				}
			}
		}
	}

	int UpdateRectOutAni(CAScrollMoveAni scrMoveAni)
	{
		int retValue = -1;

		float calcGapValue = scrMoveAni.gapPosValue*5f;
		if(scrMoveAni.plusNumState)
		{
			if(calcGapValue < m_MinSpeedValue)
				calcGapValue = m_MinSpeedValue;
		}
		else
		{
			if(calcGapValue > -m_MinSpeedValue)
				calcGapValue = -m_MinSpeedValue;
		}

		float calcMoveValue = calcGapValue*Time.deltaTime;

		if((Mathf.Abs(scrMoveAni.gapPosValue) - Mathf.Abs(calcMoveValue)) <= 0f)
		{
			calcMoveValue = scrMoveAni.gapPosValue;
			NotifyCompleteMoveAni(scrMoveAni);
			retValue = 1;
		}

		scrMoveAni.gapPosValue -= calcMoveValue;

		//Transform thiefOranTransform = scrMoveAni.moveTransform;
		List<CAScrItemData> lMoveItemData = scrMoveAni.lMoveScrItemData;
		switch(scrMoveAni.eListDirection)
		{
		case CAScroll.ScrType.eScrVerticality:
			//thiefOranTransform.localPosition = new Vector3(thiefOranTransform.localPosition.x, thiefOranTransform.localPosition.y + calcMoveValue, 0f);
			for (int i = 0; i < lMoveItemData.Count; i++) {
				lMoveItemData [i].SetScrItemPos (lMoveItemData [i].posX, lMoveItemData [i].posY + calcMoveValue);
			}
			break;
		case CAScroll.ScrType.eScrHorizontal:
			//thiefOranTransform.localPosition = new Vector3(thiefOranTransform.localPosition.x + calcMoveValue, thiefOranTransform.localPosition.y, 0f);
			for (int i = 0; i < lMoveItemData.Count; i++) {
				lMoveItemData [i].SetScrItemPos (lMoveItemData [i].posX + calcMoveValue, lMoveItemData [i].posY);
			}
			break;
		}

		return retValue;
	}

	int UpdateScrollMoveAni(CAScrollMoveAni scrMoveAni)
	{
		if (scrMoveAni.areaMeetState) {
			scrMoveAni.moveSpeed -= scrMoveAni.minusSpeed*5f;
		} else {
			scrMoveAni.moveSpeed -= scrMoveAni.minusSpeed;
		}
		
		if(scrMoveAni.plusNumState)
		{
			if(scrMoveAni.moveSpeed <= 0f)
			{
				scrMoveAni.moveSpeed = 0f;
				return  1;
			}
		}
		else
		{
			if(scrMoveAni.moveSpeed >= 0f)
			{
				return 1;
			}
		}

		scrMoveAni.CalcCurMinusSpeed ();

		float calcMoveValue = scrMoveAni.moveSpeed*Time.deltaTime;

		//Transform thiefOranTransform = scrMoveAni.moveTransform;
		List<CAScrItemData> lMoveItemData = scrMoveAni.lMoveScrItemData;
		switch(scrMoveAni.eListDirection)
		{
		case CAScroll.ScrType.eScrVerticality:
			//thiefOranTransform.localPosition = new Vector3(thiefOranTransform.localPosition.x, thiefOranTransform.localPosition.y + calcMoveValue, 0f);
			for (int i = 0; i < lMoveItemData.Count; i++) {
				lMoveItemData [i].SetScrItemPos (lMoveItemData [i].posX, lMoveItemData [i].posY + calcMoveValue);
			}
			break;
		case CAScroll.ScrType.eScrHorizontal:
			//thiefOranTransform.localPosition = new Vector3(thiefOranTransform.localPosition.x + calcMoveValue, thiefOranTransform.localPosition.y, 0f);
			for (int i = 0; i < lMoveItemData.Count; i++) {
				lMoveItemData [i].SetScrItemPos (lMoveItemData [i].posX + calcMoveValue, lMoveItemData [i].posY);
			}
			break;
		}

		if (!scrMoveAni.areaMeetState) {
			if (scrMoveAni.plusNumState) {
				switch (scrMoveAni.eListDirection) {
				case CAScroll.ScrType.eScrVerticality:
					if (scrMoveAni.startPosValue > lMoveItemData [0].posY) {
						scrMoveAni.areaMeetState = true;
					}
					break;
				case CAScroll.ScrType.eScrHorizontal:
					if (scrMoveAni.startPosValue < lMoveItemData [0].posX) {
						scrMoveAni.areaMeetState = true;
					}
					break;
				}
			} else {
				switch (scrMoveAni.eListDirection) {
				case CAScroll.ScrType.eScrVerticality:
					if (scrMoveAni.endPosValue < lMoveItemData [lMoveItemData.Count - 1].posY) {
						scrMoveAni.areaMeetState = true;
					}
					break;
				case CAScroll.ScrType.eScrHorizontal:
					if (scrMoveAni.endPosValue > lMoveItemData [lMoveItemData.Count - 1].posX) {
						scrMoveAni.areaMeetState = true;
					}
					break;
				}
			}
		}



//		if(!scrMoveAni.areaMeetState)
//		{
//			if(scrMoveAni.plusNumState)
//			{
//				switch(scrMoveAni.eListDirection)
//				{
//				case CAScroll.ScrType.eScrVerticality:
//					if(lMoveItemData[0].posY >= m_ListTopValue)
//					{
//						scrMoveAni.areaMeetState = true;
//					}
//					break;
//				case CAScroll.ScrType.eScrHorizontal:
//					if(lMoveItemData[0].posX >= m_ListTopValue)
//					{
//						scrMoveAni.areaMeetState = true;
//					}
//					break;
//				}
//			}
//			else
//			{
//				switch(scrMoveAni.eListDirection)
//				{
//				case CAScroll.ScrType.eScrVerticality:
//					if(lMoveItemData[0].posY <= m_ListBottomValue)
//					{
//						scrMoveAni.areaMeetState = true;
//					}
//					break;
//				case CAScroll.ScrType.eScrHorizontal:
//					if(lMoveItemData[0].posX <= m_ListBottomValue)
//					{
//						scrMoveAni.areaMeetState = true;
//					}
//					break;
//				}
//			}
//		}

		return -1;
	}

	public void AddRectOutListMoveAni(List<CAScrItemData> lScrItemData, float startPosValue, float endPosValue, CAScroll.ScrType listDir, CAScrollMoveAni.ListMoveAniType moveAniType)
	{
		CAScrollMoveAni inputOrganListMoveAni = new CAScrollMoveAni();
		inputOrganListMoveAni.lMoveScrItemData = lScrItemData;
		inputOrganListMoveAni.startPosValue = startPosValue;
		inputOrganListMoveAni.endPosValue = endPosValue;
		inputOrganListMoveAni.eListDirection = listDir;
		inputOrganListMoveAni.eListMoveAniType = moveAniType;
		inputOrganListMoveAni.CalcMoveAniData();
		m_lScrollMoveAni.Add(inputOrganListMoveAni);
	}

	public void AddScrollListMoveDragAni(List<CAScrItemData> lScrItemData, float moveSpeed, CAScroll.ScrType listDir)
	{
		CAScrollMoveAni inputOrganListMoveAni = new CAScrollMoveAni();
		inputOrganListMoveAni.lMoveScrItemData = lScrItemData;
		inputOrganListMoveAni.moveSpeed = moveSpeed;
		inputOrganListMoveAni.eListDirection = listDir;
		inputOrganListMoveAni.eListMoveAniType = CAScrollMoveAni.ListMoveAniType.eScrollMoveAniType;
		inputOrganListMoveAni.CalcScrollMoveAniData();
		m_lScrollMoveAni.Add(inputOrganListMoveAni);
	}

	public void AttachMoveAniObserver(CAScrollMoveAniObserver inputMoveAniOb)
	{
		if(m_lMoveAniObserver.Contains(inputMoveAniOb))
			return;

		m_lMoveAniObserver.Add(inputMoveAniOb);
	}

	void NotifyCompleteMoveAni(CAScrollMoveAni listMoveAniData)
	{
		for(int i = 0;i<m_lMoveAniObserver.Count;i++)
		{
			m_lMoveAniObserver[i].OnCompleteListMoveAni(listMoveAniData);
		}
	}

	void NotifyCompleteScrollMoveAni()
	{
		for(int i = 0;i<m_lMoveAniObserver.Count;i++)
		{
			m_lMoveAniObserver[i].OnCompleteScrollMoveAni();
		}
	}

	public bool CheckAniList()
	{
		if(m_lScrollMoveAni.Count > 0)
			return true;
		return false;
	}

	public void StopListMoveAni()
	{
		if(m_lScrollMoveAni.Count == 0)
			return;

		m_lScrollMoveAni.Clear();
	}

	#endregion

	#region Properties

	public float listTopValue
	{
		get{ return m_ListTopValue; }
		set{ m_ListTopValue = value; }
	}

	public float listBottomValue
	{
		get{ return m_ListBottomValue; }
		set{ m_ListBottomValue = value; }
	}

	#endregion

	#region Variables

	List<CAScrollMoveAni> m_lScrollMoveAni = new List<CAScrollMoveAni>();
	List<CAScrollMoveAniObserver> m_lMoveAniObserver = new List<CAScrollMoveAniObserver>();
	float m_MinSpeedValue = 30f;
	float m_ListTopValue;
	float m_ListBottomValue;

	#endregion
}
