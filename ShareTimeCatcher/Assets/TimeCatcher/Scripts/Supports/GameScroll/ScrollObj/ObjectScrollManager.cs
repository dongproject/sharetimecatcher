using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;

public class ObjectScrollManager : MonoBehaviour 
{
	#region Definitions

	//public enum ScrollMoveType
	//{
	//	HorizontalScroll 	= 1,
	//	VerticalScroll		= 2,
	//}

	//public enum MoveDirection
	//{
	//	None 				= 0,
	//	PlusMove			= 1,
	//	MinusMove			= 2,
	//}

    #endregion

    #region Serialze Variables

#pragma warning disable 649

    [SerializeField] Vector2 _referenceResolution = default(Vector2);
//	[SerializeField] ObjAnchorType _anchorType = default(ObjAnchorType);
	[SerializeField] Transform _objListTrans = default(Transform);
	[SerializeField] ScrollDefinitions.MoveType _moveType = default(ScrollDefinitions.MoveType);
	[SerializeField] Camera _scrollCamera = default(Camera);
	[SerializeField] BoxCollider _scrollTouchArea = default(BoxCollider);
	[SerializeField] float _topGapY = default(float);
	[SerializeField] float _bottomGapY = default(float);
	[SerializeField] float _objGapValue = default(float);
	[SerializeField] RectTransform _viewObjMaskRectTrans = default(RectTransform);

#pragma warning restore 649

    #endregion

    #region Variables

    bool _isUpdate = true;

	protected ScreenTouchValue[] _screenTouchValues = new ScreenTouchValue[2];
//	protected bool _firstDragState;
	private float _firstStartX;
	private float _firstStartY;
	private int _touchCount;

	Action<float, float> _onScrollObject = null;
	Rect _scrollAreaRect;
	float _startRectY = -10000f;
	float _curStartRectY = 0f;
	float _endRectY;

	List<ScrollBaseObject> _scrollObjectList = new List<ScrollBaseObject>();

	float _startViewRectTop;
	float _startBoxCollideHeight;
	bool _isTouch = true;

	ObjectScrollMotionManager _objectMotionManager = new ObjectScrollMotionManager();

	int _outLineMotionID = 1;
	int _scrollMotionID = 2;

	float _widthRatio = 1f;
	float _heightRatio = 1f;

	float _startTouchTime;
	Vector2 _startTouchValue;

	bool _isScrollMoveMotion = false;
	bool _isCollideOutLine = false;

	int _curFocusScrollIndex = -1;
    ScrollDefinitions.MoveDirection _moveDir = ScrollDefinitions.MoveDirection.None;

	#endregion

	#region Properties

	public Transform ObjListTrans
	{
		get{ return _objListTrans; }
	}

	public Action<float, float> OnScrollObject
	{
		get{ return _onScrollObject; }
		set{ _onScrollObject = value; }
	}

	public List<ScrollBaseObject> ScrollObjectList
	{
		get{ return _scrollObjectList; }
	}

	public bool IsTouch
	{
		get{ return _isTouch; }
		set{ _isTouch = value; }
	}

	#endregion

	#region MonoBehaviour Methods

	void Awake()
	{
	}

	void Start()
	{
		InitScrollData ();
	}

	void OnEnable()
	{
		InitTouchValue ();

		_isUpdate = true;
		StartCoroutine (UpdateObjScroll ());
	}

	void OnDisable()
	{
		StopCoroutine (UpdateObjScroll ());
		_isUpdate = false;
	}

	#endregion

	#region Methods

	public void InitScrollData()
	{
		if (_startRectY != -10000f)
			return;

		_widthRatio = _referenceResolution.x / _scrollCamera.pixelWidth;
		float calcHeight = _scrollCamera.pixelHeight * _widthRatio;
		_heightRatio = calcHeight / _scrollCamera.pixelHeight;
		float gapHeight = calcHeight - _referenceResolution.y;
		_scrollTouchArea.size = new Vector3 (_scrollTouchArea.size.x, _scrollTouchArea.size.y + gapHeight, _scrollTouchArea.size.z);

		_startRectY = -(_scrollTouchArea.size.y * 0.5f);
		_endRectY = _scrollTouchArea.size.y * 0.5f;

		_curStartRectY = 0f;

		Vector2 touchScrPos = _scrollCamera.WorldToScreenPoint (_scrollTouchArea.transform.position);
		float realWidth = _scrollTouchArea.size.x / _widthRatio;
		float realHeight = _scrollTouchArea.size.y / _heightRatio;
		float halfWidth = realWidth * 0.5f;
		float halfHeight = realHeight * 0.5f;
//		Debug.Log (string.Format ("touchScrPos x : {0}, y : {1}, halfWidth : {2}, halfHeight : {3}", touchScrPos.x, touchScrPos.y, halfWidth, halfHeight));
		_scrollAreaRect = new Rect (touchScrPos.x - halfWidth, touchScrPos.y - halfHeight, 
			realWidth, realHeight);

		_startViewRectTop = _viewObjMaskRectTrans.offsetMax.y;
		_startBoxCollideHeight = _scrollTouchArea.size.y;
//		Debug.Log (string.Format ("InitScrollData _startViewRectTop : {0}, _startBoxCollideHeight : {1}", _startViewRectTop, _startBoxCollideHeight));
//		Debug.Log (string.Format ("_scrollAreaRect xMin : {0}, xMax : {1}, yMin : {2}, yMax : {3}", _scrollAreaRect.xMin, _scrollAreaRect.xMax, _scrollAreaRect.yMin, _scrollAreaRect.yMax));
	}

	public void SetRectGapHeightValue(float gapValue)
	{
		if (gapValue == 0f)
			return;
		
		_viewObjMaskRectTrans.offsetMax = new Vector2 (_viewObjMaskRectTrans.offsetMax.x, _viewObjMaskRectTrans.offsetMax.y - gapValue);
		_scrollTouchArea.size = new Vector3 (_scrollTouchArea.size.x, _scrollTouchArea.size.y - gapValue, _scrollTouchArea.size.z);

		_startRectY = -(_scrollTouchArea.size.y * 0.5f);
		_endRectY = _scrollTouchArea.size.y * 0.5f;

//		Debug.Log (string.Format ("SetRectGapHeightValue gapValue : {0}, _startRectY : {1}, _endRectY : {2}", gapValue, _startRectY, _endRectY));
	}

	public void ReleaseScrollData(bool isResize)
	{
		if (isResize) {
			if (_viewObjMaskRectTrans.offsetMax.y != _startViewRectTop) {
				_viewObjMaskRectTrans.offsetMax = new Vector2 (_viewObjMaskRectTrans.offsetMax.x, _startViewRectTop);
				_scrollTouchArea.size = new Vector3 (_scrollTouchArea.size.x, _startBoxCollideHeight, _scrollTouchArea.size.z);

				_startRectY = -(_scrollTouchArea.size.y * 0.5f);
				_endRectY = _scrollTouchArea.size.y * 0.5f;
			}
		}

		_scrollObjectList.Clear ();

		_objListTrans.localPosition = Vector3.zero;

		InitTouchValue ();

		ReleaseAllScrollMotion ();
	}

	void UpdateEditor()
	{
		if (Input.GetMouseButtonDown (0) && _screenTouchValues [0].touchID == -1) {
			_firstStartX = Input.mousePosition.x;
			_firstStartY = Input.mousePosition.y;

//			Debug.Log (string.Format ("UpdateEditor _firstStartX : {0}, _firstStartY : {1}", _firstStartX, _firstStartY));

			if (!CheckValidTouchArea(_firstStartX, _firstStartY)) {
				return;
			}

//			_firstDragState = false;

			_screenTouchValues [0].touchID = 0;
			_screenTouchValues [0].curPosition = Input.mousePosition;
			_screenTouchValues [0].prePosition = Input.mousePosition;
			_screenTouchValues [0].firstDragState = false;

			if (_isScrollMoveMotion) {
				ReleaseScrollMoveMotion ();
			}

		} else if (Input.GetMouseButton (0) && _screenTouchValues [0].touchID == 0) {
			_screenTouchValues [0].prePosition = _screenTouchValues [0].curPosition;
			_screenTouchValues [0].curPosition = Input.mousePosition;

			if (_screenTouchValues [0].firstDragState) {

				MoveScroll (Input.mousePosition, _screenTouchValues [0].curPosition.x - _screenTouchValues [0].prePosition.x, _screenTouchValues [0].curPosition.y - _screenTouchValues [0].prePosition.y);
			} else {
				if (Mathf.Abs (_firstStartX - Input.mousePosition.x) > (6f) || Mathf.Abs (_firstStartY - Input.mousePosition.y) > (6f)) {
					SetFirstDragData (Input.mousePosition);
				}
			}
		} else if (Input.GetMouseButtonUp (0) && _screenTouchValues [0].touchID == 0) {
			_screenTouchValues [0].touchID = -1;

			int releaseIndex = -1;
			if (_curFocusScrollIndex != -1) {
				releaseIndex = _curFocusScrollIndex;
			}

			bool isOutLineMotion = false;

            switch (_moveType) {
		    case ScrollDefinitions.MoveType.HorizontalScroll:
	            isOutLineMotion = CheckHorizontalScrollMotion ();
				break;
		    case ScrollDefinitions.MoveType.VerticalScroll:
		        isOutLineMotion = CheckVerticalScrollMotion ();
				break;
			}

			if (!isOutLineMotion && _screenTouchValues [0].firstDragState) {
				if (releaseIndex != -1) {
					CheckMoveScrollMotion (releaseIndex, Input.mousePosition);
				}
			}
		}
	}

	void UpdateDevice()
	{
		if (Input.touchCount > 0) {
			for (int i = 0; i < Input.touchCount; i++) {
				Touch curTouch = Input.GetTouch(i);

				if (curTouch.phase == TouchPhase.Began) {
					if (!CheckValidTouchArea(curTouch.position.x, curTouch.position.y)) {
						return;
					}

					if (_touchCount == 0) {
//						_firstDragState = false;

						_firstStartX = curTouch.position.x;
						_firstStartY = curTouch.position.y;
					}

					if (_screenTouchValues [0].touchID == -1) {
						_touchCount++;

						_screenTouchValues [0].touchID = curTouch.fingerId;
						_screenTouchValues [0].curPosition = curTouch.position;
						_screenTouchValues [0].prePosition = curTouch.position;
						_screenTouchValues [0].firstDragState = false;

						if (_isScrollMoveMotion) {
							ReleaseScrollMoveMotion ();
						}
					} else if (_screenTouchValues [1].touchID == -1) {
						_touchCount++;

						_screenTouchValues [1].touchID = curTouch.fingerId;
						_screenTouchValues [1].curPosition = curTouch.position;
						_screenTouchValues [1].prePosition = curTouch.position;
						_screenTouchValues [1].firstDragState = false;

						if (_isScrollMoveMotion) {
							ReleaseScrollMoveMotion ();
						}
					}
				} else if (curTouch.phase == TouchPhase.Moved) {
					if (_screenTouchValues [0].touchID == curTouch.fingerId) {
						_screenTouchValues [0].prePosition = _screenTouchValues [0].curPosition;
						_screenTouchValues [0].curPosition = curTouch.position;

//						if (_touchCount == 1) {
//							
//						}

						if (_screenTouchValues [0].firstDragState) {

							MoveScroll (curTouch.position, _screenTouchValues [0].curPosition.x - _screenTouchValues [0].prePosition.x, _screenTouchValues [0].curPosition.y - _screenTouchValues [0].prePosition.y, 0);
						} else {
							if (Mathf.Abs (_firstStartX - curTouch.position.x) > 6 || Mathf.Abs (_firstStartY - curTouch.position.y) > 6) {
								SetFirstDragData (curTouch.position, 0);
							}
						}
					} else if (_screenTouchValues [1].touchID == curTouch.fingerId) {
						_screenTouchValues [1].prePosition = _screenTouchValues [1].curPosition;
						_screenTouchValues [1].curPosition = curTouch.position;

//						if (_touchCount == 1) {
//							
//						}

						if (_screenTouchValues [1].firstDragState) {

							MoveScroll (curTouch.position, _screenTouchValues [1].curPosition.x - _screenTouchValues [1].prePosition.x, _screenTouchValues [1].curPosition.y - _screenTouchValues [1].prePosition.y, 1);
						} else {
							if (Mathf.Abs (_firstStartX - curTouch.position.x) > 6 || Mathf.Abs (_firstStartY - curTouch.position.y) > 6) {
								SetFirstDragData (curTouch.position, 1);
							}
						}
					}
				} else if (curTouch.phase == TouchPhase.Ended) {
					int releaseIndex = -1;

                    bool isScrollMotion = false;
					if (_screenTouchValues [0].touchID == curTouch.fingerId) {
						_screenTouchValues [0].touchID = -1;
                        if(_screenTouchValues [0].firstDragState)
                            isScrollMotion = true;
						_touchCount--;
						if (_curFocusScrollIndex == 0) {
							releaseIndex = 0;
							_curFocusScrollIndex = -1;
						}
					} else if (_screenTouchValues [1].touchID == curTouch.fingerId) {
						_screenTouchValues [1].touchID = -1;
                        if(_screenTouchValues [1].firstDragState)
                            isScrollMotion = true;
						_touchCount--;
						if (_curFocusScrollIndex == 1) {
							releaseIndex = 1;
							_curFocusScrollIndex = -1;
						}
					}

					bool isOutLineMotion = false;

					if (_touchCount == 0) {
						switch (_moveType) {
						case ScrollDefinitions.MoveType.HorizontalScroll:
							isOutLineMotion = CheckHorizontalScrollMotion ();
							break;
						case ScrollDefinitions.MoveType.VerticalScroll:
							isOutLineMotion = CheckVerticalScrollMotion ();
							break;
						}
					}

					if (!isOutLineMotion && isScrollMotion) {
						if (releaseIndex != -1) {
							CheckMoveScrollMotion (releaseIndex, curTouch.position);
						}
					}

				} else if (curTouch.phase == TouchPhase.Canceled) {
					int releaseIndex = -1;
                    bool isScrollMotion = false;
					if (_screenTouchValues [0].touchID == curTouch.fingerId) {
						_screenTouchValues [0].touchID = -1;
                        if(_screenTouchValues [0].firstDragState)
                            isScrollMotion = true;
						_touchCount--;
						if (_curFocusScrollIndex == 0) {
							releaseIndex = 0;
							_curFocusScrollIndex = -1;
						}
					} else if (_screenTouchValues [1].touchID == curTouch.fingerId) {
						_screenTouchValues [1].touchID = -1;
                        if(_screenTouchValues [1].firstDragState)
                            isScrollMotion = true;
						_touchCount--;
						if (_curFocusScrollIndex == 1) {
							releaseIndex = 1;
							_curFocusScrollIndex = -1;
						}
					}

					bool isOutLineMotion = false;

					if (_touchCount == 0) {
						switch (_moveType) {
						case ScrollDefinitions.MoveType.HorizontalScroll:
							isOutLineMotion = CheckHorizontalScrollMotion ();
							break;
						case ScrollDefinitions.MoveType.VerticalScroll:
							isOutLineMotion = CheckVerticalScrollMotion ();
							break;
						}
					}

                    if (!isOutLineMotion && isScrollMotion) {
						if (releaseIndex != -1) {
							CheckMoveScrollMotion (releaseIndex, curTouch.position);
						}
					}
				}
			}
		}
	}

	public void InitTouchValue()
	{
		for (int i = 0; i < _screenTouchValues.Length; i++) {
			_screenTouchValues [i] = new ScreenTouchValue ();
			_screenTouchValues [i].touchID = -1;
			_screenTouchValues [i].curPosition = Vector3.zero;
			_screenTouchValues [i].prePosition = Vector3.zero;
			_screenTouchValues [i].firstDragState = false;
		}

		_firstStartX = 0f;
		_firstStartY = 0f;
		_touchCount = 0;
		_curFocusScrollIndex = -1;
	}

	public void ResetTouchValue()
	{
		if (_screenTouchValues == null || _screenTouchValues.Length == 0)
			return;

		for (int i = 0; i < _screenTouchValues.Length; i++) {
			_screenTouchValues [i].touchID = -1;
			_screenTouchValues [i].curPosition = Vector3.zero;
			_screenTouchValues [i].prePosition = Vector3.zero;
			_screenTouchValues [i].firstDragState = false;
		}

//		_firstDragState = false;
		_firstStartX = 0f;
		_firstStartY = 0f;
		_touchCount = 0;
		_curFocusScrollIndex = -1;
	}

	bool CheckValidTouchArea(float posX, float posY)
	{
		if ((_scrollAreaRect.xMin <= posX && _scrollAreaRect.xMax >= posX) &&
		   (_scrollAreaRect.yMin <= posY && _scrollAreaRect.yMax >= posY)) {
//			Debug.Log (string.Format (" CheckValidTouchArea True"));
			return true;
		}

//		Debug.Log (string.Format (" CheckValidTouchArea false"));
		return false;
	}

	protected void MoveScroll(Vector2 touchPos, float moveX, float moveY, int focusIndex = 0)
	{
		int isPlusState = -1; // 0 : Minus, 1 : Plus
		switch (_moveType) {
		case ScrollDefinitions.MoveType.HorizontalScroll:
			moveY = 0f;

			if (moveX != 0f) {
				moveX *= _widthRatio;
				if (moveX < 0f) {
					isPlusState = 0;
				} else {
					isPlusState = 1;
				}
			} else {
				SetFirstDragData (touchPos);
				_moveDir = ScrollDefinitions.MoveDirection.None;
			}

			break;
		case ScrollDefinitions.MoveType.VerticalScroll:
			moveX = 0f;

			if (moveY != 0f) {
				moveY *= _heightRatio;
				if (moveY < 0f) {
					isPlusState = 0;
				} else {
					isPlusState = 1;
				}
			} else {
				SetFirstDragData (touchPos);
				_moveDir = ScrollDefinitions.MoveDirection.None;
			}
			break;
		}

		if (isPlusState != -1) {
			switch (_moveDir) {
			case ScrollDefinitions.MoveDirection.None:
				if (isPlusState == 0) {
					_moveDir = ScrollDefinitions.MoveDirection.MinusMove;
				} else if (isPlusState == 1){
					_moveDir = ScrollDefinitions.MoveDirection.PlusMove;
				}
				SetFirstDragData (touchPos);
				break;
			case ScrollDefinitions.MoveDirection.PlusMove:
				if (isPlusState == 0) {
					_moveDir = ScrollDefinitions.MoveDirection.MinusMove;
					SetFirstDragData (touchPos);
				}
				break;
			case ScrollDefinitions.MoveDirection.MinusMove:
				if (isPlusState == 1) {
					_moveDir = ScrollDefinitions.MoveDirection.PlusMove;
					SetFirstDragData (touchPos);
				}
				break;
			}
		}

//		Debug.Log (string.Format ("MoveScroll moveX : {0}, moveY : {1}", moveX, moveY));

		_objListTrans.localPosition = new Vector3 (GetObjCalcMovePosX(moveX), GetObjCalcMovePosY(moveY), _objListTrans.localPosition.z);

		if (_onScrollObject != null) {
			_onScrollObject (moveX, moveY);
		}
	}

	float GetObjCalcMovePosX(float moveX)
	{
		return _objListTrans.localPosition.x + moveX;
	}

	float GetObjCalcMovePosY(float moveY)
	{
		if (moveY > 0f) {
			float compareObjY = _scrollObjectList [_scrollObjectList.Count - 1].GetBottomPosY (_objListTrans.localPosition.y);
			if (compareObjY + moveY > _curStartRectY) {
//				moveY = _curStartRectY - compareObjY;
				moveY = moveY * 0.5f;
			}
		} else if(moveY < 0f){
			float compareObjY = _scrollObjectList [0].GetTopPosY (_objListTrans.localPosition.y);
			if (compareObjY + moveY < _endRectY - _topGapY) {
//				moveY = (_endRectY - _topGapY) - compareObjY;
				moveY = moveY * 0.5f;
			}
		}

		return _objListTrans.localPosition.y + moveY;
	}

	bool CheckHorizontalScrollMotion()
	{
		return false;
	}

	bool CheckVerticalScrollMotion()
	{
		bool retValue = false;
		float compareLastObjY = _scrollObjectList [_scrollObjectList.Count - 1].GetBottomPosY (_objListTrans.localPosition.y);
		if (compareLastObjY > _curStartRectY) {
			float gapValue = _curStartRectY - compareLastObjY;
			Vector3 startPos = new Vector3 (0f, _objListTrans.localPosition.y, 0f);
			Vector3 destPos = new Vector3 (0f, _objListTrans.localPosition.y + gapValue, 0f);

			_objectMotionManager.AddOutLineMovement (startPos, destPos, OnChangeMoveMotionValue, _outLineMotionID, OnCompletedOutLineMotionAni);

			retValue = true;
		} else {
			float compareFirstObjY = _scrollObjectList [0].GetTopPosY (_objListTrans.localPosition.y);
			if (compareFirstObjY < _endRectY - _topGapY) {
				float gapValue = (_endRectY - _topGapY) - compareFirstObjY;
				Vector3 startPos = new Vector3 (0f, _objListTrans.localPosition.y, 0f);
				Vector3 destPos = new Vector3 (0f, _objListTrans.localPosition.y + gapValue, 0f);

				_objectMotionManager.AddOutLineMovement (startPos, destPos, OnChangeMoveMotionValue, _outLineMotionID, OnCompletedOutLineMotionAni);

				retValue = true;
			}
		}

		return retValue;
	}

	bool CheckVerticalOutLinePos()
	{
		bool retValue = false;
		float compareLastObjY = _scrollObjectList [_scrollObjectList.Count - 1].GetBottomPosY (_objListTrans.localPosition.y);
		if (compareLastObjY > _curStartRectY) {
			retValue = true;
		} else {
			float compareFirstObjY = _scrollObjectList [0].GetTopPosY (_objListTrans.localPosition.y);
			if (compareFirstObjY < _endRectY - _topGapY) {
				retValue = true;
			}
		}

		return retValue;
	}

	void CheckMoveScrollMotion(int focusIndex, Vector2 posValue)
	{
		float gapTime = Time.time - _startTouchTime;
		float gapMove = 0f;

		switch (_moveType) {
		case ScrollDefinitions.MoveType.HorizontalScroll:
			gapMove = posValue.x - _startTouchValue.x;
			break;
		case ScrollDefinitions.MoveType.VerticalScroll:
			gapMove = posValue.y - _startTouchValue.y;
			break;
		}

		if (_objectMotionManager.AddAccelerationListMove (_moveType, gapTime, gapMove, OnChangeMoveMotionValue, _scrollMotionID, OnCompletedMoveMotionScroll)) {
			_isScrollMoveMotion = true;
		}
	}

	public void AddScrollObject(IScrollObjectInfo scrollObjInfo)
	{
		ScrollBaseObject scrollObject = new ScrollBaseObject ();
		scrollObject.ScrollObjInfo = scrollObjInfo;

		scrollObject.ScrollObjInfo.ScrollGameObject.transform.SetParent (_objListTrans);
		scrollObject.ScrollObjInfo.ScrollGameObject.transform.localScale = Vector3.one;

		float calcPosX = 0f;
		float calcPosY = 0f;
		if (_scrollObjectList.Count == 0) {
			switch (_moveType) {
			case ScrollDefinitions.MoveType.HorizontalScroll:
				break;
			case ScrollDefinitions.MoveType.VerticalScroll:
				{
					calcPosY = _endRectY - _topGapY - (scrollObject.ScrollObjInfo.ObjectHeight * 0.5f);
//					Debug.Log (string.Format ("AddScrollObject _endRectY : {0}", _endRectY));
					_curStartRectY = calcPosY - (scrollObject.ScrollObjInfo.ObjectHeight * 0.5f);
				}
				break;
			}
		} else {
			ScrollBaseObject lastScrollObject = _scrollObjectList[_scrollObjectList.Count - 1];

			switch (_moveType) {
			case ScrollDefinitions.MoveType.HorizontalScroll:
				break;
			case ScrollDefinitions.MoveType.VerticalScroll:
				{
					calcPosY = lastScrollObject.ObjPosY - (lastScrollObject.ScrollObjInfo.ObjectHeight * 0.5f) - _objGapValue - (scrollObject.ScrollObjInfo.ObjectHeight * 0.5f);
					_curStartRectY = calcPosY - (scrollObject.ScrollObjInfo.ObjectHeight * 0.5f);
					if (_curStartRectY < _startRectY + _bottomGapY) {
						_curStartRectY = _startRectY + _bottomGapY;
					}

					float curBottomObjPosY = calcPosY - lastScrollObject.ScrollObjInfo.ObjectHeight * 0.5f + _objListTrans.transform.localPosition.y;

					if (curBottomObjPosY < _curStartRectY) {
						float gapRectYValue = _curStartRectY - curBottomObjPosY;
						_objListTrans.transform.localPosition = new Vector3 (_objListTrans.transform.localPosition.x, 
							_objListTrans.transform.localPosition.y + gapRectYValue, 
							_objListTrans.transform.localPosition.z);
					}
				}
				break;
			}
		}

//		Debug.Log (string.Format ("AddScrollObject calcPosX : {0}, calcPosY : {1}, _endRectY : {2}, _topGapY : {3}, scrollObject.ScrollObjInfo.ObjectHeight {4}", 
//			calcPosX, calcPosY, _endRectY, _topGapY, scrollObject.ScrollObjInfo.ObjectHeight));

		scrollObject.SetScrollObjectPos (calcPosX, calcPosY);

		_scrollObjectList.Add (scrollObject);
	}

	public ScrollBaseObject GetScrollObj(int objIndex)
	{
		if (_scrollObjectList.Count <= objIndex)
			return null;

		return _scrollObjectList [objIndex];
	}

	public void RemoveScrollObj(int objIndex)
	{
		if (_scrollObjectList.Count > objIndex) {
			_scrollObjectList.RemoveAt (objIndex);
		}
	}

	void SetFirstDragData(Vector2 touchValue, int focusIndex = 0)
	{
//		_firstDragState = true;

		if (focusIndex == 0) {
			if (_screenTouchValues [1].firstDragState) {
				_screenTouchValues [1].firstDragState = false;
			}
		} else {
			if (_screenTouchValues [0].firstDragState) {
				_screenTouchValues [0].firstDragState = false;
			}
		}

		_screenTouchValues[focusIndex].firstDragState = true;

		SetStartDragTouchValues (touchValue);

		_curFocusScrollIndex = focusIndex;
	}

	void SetStartDragTouchValues(Vector2 touchValue)
	{
		_startTouchTime = Time.time;
		_startTouchValue = touchValue;
	}

	void ReleaseScrollMoveMotion()
	{
		_objectMotionManager.ReleaseObjectMovement (_scrollMotionID, false);
		_isScrollMoveMotion = false;
		_isCollideOutLine = false;
	}

	void ReleaseAllScrollMotion()
	{
		_objectMotionManager.ReleaseAllMotion ();
		_isScrollMoveMotion = false;
		_isCollideOutLine = false;
	}

	#endregion

	#region Coroutine Methods

	IEnumerator UpdateObjScroll()
	{
		while (_isUpdate) {
			if (_scrollObjectList.Count > 0 && _isTouch){
				#if UNITY_EDITOR
				UpdateEditor ();
				#else
				UpdateDevice ();
				#endif

				_objectMotionManager.Update ();
			}

			yield return null;
		}
	}

	#endregion

	#region CallBack Methods

	void OnChangeMoveMotionValue(ObjectScrollMotionManager.MotionType motionType, Vector2 moveValue, int motionID)
	{
		switch (_moveType) {
		case ScrollDefinitions.MoveType.HorizontalScroll:
			_objListTrans.localPosition = new Vector3 (_objListTrans.localPosition.x + moveValue.x, _objListTrans.localPosition.y, _objListTrans.localPosition.z);
			break;
		case ScrollDefinitions.MoveType.VerticalScroll:
			_objListTrans.localPosition = new Vector3 (_objListTrans.localPosition.x, _objListTrans.localPosition.y + moveValue.y, _objListTrans.localPosition.z);

			if (motionID == _scrollMotionID && !_isCollideOutLine) {
				if (CheckVerticalOutLinePos ()) {
					_objectMotionManager.ChangeSpeedValue (_scrollMotionID, 500f);
					_isCollideOutLine = true;
				}		
			}
			break;
		}
	}

	void OnCompletedOutLineMotionAni()
	{
		
	}

	void OnCompletedMoveMotionScroll()
	{
		switch (_moveType) {
		case ScrollDefinitions.MoveType.HorizontalScroll:
			CheckHorizontalScrollMotion ();
			break;
		case ScrollDefinitions.MoveType.VerticalScroll:
			CheckVerticalScrollMotion ();
			break;
		}

		_isScrollMoveMotion = false;
		_isCollideOutLine = false;
	}

	#endregion
}
