﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UGUITextUtil
{
//	public static int GetWidthOfMessage(string message, Text text)
//	{
//		int totalLength = 0;
//
//		Font font = text.font;
//		CharacterInfo characterInfo = new CharacterInfo();
//		
//		char[] arr = message.ToCharArray();
//
//		Debug.Log(string.Format("GetWidthOfMessage text : {0}, text.pixelsPerUnit : {1}, text.preferredWidth : {2}", text.text, text.pixelsPerUnit, text.preferredWidth));
//
//		foreach (char c in arr)
//		{
//			font.GetCharacterInfo(c, out characterInfo, text.fontSize);
//			totalLength += characterInfo.advance;
////			Debug.Log (string.Format ("GetWidthOfMessage text.fontSize : {0}, c : {1}, characterInfo.advance : {2}, characterInfo.glyphWidth : {3}, characterInfo.size : {4}", text.fontSize,
////				c, characterInfo.advance, characterInfo.glyphWidth, characterInfo.size));
//			
//		}
//
//		return totalLength;
//	}

	public static int GetCharByteSize(byte ch)
	{
		int charByte = 1;

		if ((ch & 0x80) == 0) { //1 byte
			charByte=1;
		}else if( (ch&0xE0) == 0xC0){//2byte
			charByte=2;
		}else if( (ch&0xF0) == 0xE0){//3byte
			charByte=3;
		}else if( (ch&0xF8) == 0xF0){//4byte
			charByte=4;
		}else if( (ch&0xFC) == 0xF8){//5byte
			charByte=5;
		}else if( (ch&0xFE) == 0xFC){//6byte
			charByte=6;
		}

		return charByte;
	}
}
