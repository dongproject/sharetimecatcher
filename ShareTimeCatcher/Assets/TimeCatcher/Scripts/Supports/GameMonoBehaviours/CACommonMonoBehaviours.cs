﻿using UnityEngine;
using System.Collections;

public enum CAGamePlayTouchType
{
	eNone                       = -1,
	StageSelect                 = 0,
	StopTimeButton              = 1,
	StopTimeOtherButton         = 2,
    InfiniteSelectButton        = 3,
}

public class CACurGamePlayTouchData
{
	public CACurGamePlayTouchData(CAGamePlayTouchType touchType, object touchObject)
	{
		playTouchType = touchType;
		curTouchObject = touchObject;
	}

	#region Properties

	public CAGamePlayTouchType playTouchType
	{
		get{ return m_PlayTouchType; }
		set{ m_PlayTouchType = value; }
	}

	public object curTouchObject
	{
		get{ return m_CurTouchObject; }
		set{ m_CurTouchObject = value; }
	}

	#endregion

	#region Variables

	CAGamePlayTouchType m_PlayTouchType;
	object m_CurTouchObject;

	#endregion
}

public struct CAGamePlayTouchValue
{
	public int m_TouchID;
	public float m_X;
	public float m_Y;
	public float m_PreX;
	public float m_PreY;
}

public class CACommonMonoBehaviours : MonoBehaviour 
{
	#region MonoBehaviour

	public virtual void Awake()
	{
		if (CAResolutionCtl.Instance == null) {
			CAResolutionCtl.InitResolutionCtl ();
			CAResolutionCtl.Instance.InitResolution (Screen.width, Screen.height, false);
			StartCoroutine(OnSetScreenResolution());
		}

		InitTouchValueOnce();
		InitTouchValue();
	}

	public virtual void OnDestroy()
	{
		
	}

	public virtual void OnEnable()
	{

	}

	public virtual void OnDisable()
	{

	}

	public virtual void Start()
	{
		
	}

	public virtual void OnApplicationPause(bool tempValue)
	{
		if(tempValue)
		{
			InitTouchValue();
		}
	}

	public virtual void OnApplicationFocus(bool tempValue)
	{

	}

	public virtual void OnApplicationQuit()
	{
		ReleaseCommonPlanData ();
	}

	#endregion

	#region Methods

	void InitTouchValueOnce()
	{
		m_FirstStartX = 0f;
		m_FirstStartY = 0f;
		m_ScaleStartX = 0f;
		m_ScaleStartY = 0f;
		m_ScaleP = 1f;
	}

	void InitTouchValue()
	{
		for(int i=0;i<2;i++)
		{
			m_TouchValues[i].m_TouchID = -1;
			m_TouchValues[i].m_X = 0f;
			m_TouchValues[i].m_Y = 0f;
			m_TouchValues[i].m_PreX = 0f;
			m_TouchValues[i].m_PreY = 0f;
		}

		m_TouchCount = 0;
		m_PreDisValue = 0;
		m_FirstDragState = false;
		m_TouchData = null;
	}

	public IEnumerator UpdateTouchHandle()
	{
		while(curTouchState)
		{
			if(Input.touchCount > 0)
			{
				for(int i=0;i<Input.touchCount;i++)
				{
					Touch tempTouch = Input.GetTouch(i);
					float inputTouchX = tempTouch.position.x;
					float inputTouchY = tempTouch.position.y;

					if(tempTouch.phase == TouchPhase.Began && OnGetTouchState(tempTouch.fingerId, inputTouchX, inputTouchY))
					{
						OnTouchPress (tempTouch.fingerId, inputTouchX, inputTouchY);
						if(m_TouchCount == 0)
						{
							m_FirstDragState = false;
							m_FirstStartX = inputTouchX;
							m_FirstStartY = inputTouchY;
							m_TouchData = OnGetTouchData(tempTouch.fingerId, inputTouchX, inputTouchY, true);
						}

						if(m_TouchValues[0].m_TouchID == -1)
						{
							m_TouchValues[0].m_TouchID = tempTouch.fingerId;
							m_TouchValues[0].m_X = inputTouchX;
							m_TouchValues[0].m_Y = inputTouchY;
							m_TouchValues[0].m_PreX = inputTouchX;
							m_TouchValues[0].m_PreY = inputTouchY;
							m_TouchCount++;
							if(m_TouchCount == 2)
							{
								m_ScaleStartX = (m_TouchValues[0].m_X + m_TouchValues[1].m_X)*0.5f;
								m_ScaleStartY = (m_TouchValues[0].m_Y + m_TouchValues[1].m_Y)*0.5f;
								OnMultiTouchPosSet(m_ScaleStartX, m_ScaleStartY);
								m_TouchBGGapX = (m_ScaleStartX - m_PictureBGCalcX)/m_ScaleP;
								m_TouchBGGapY = (m_ScaleStartY - m_PictureBGCalcY)/m_ScaleP;
							}
						}
						else if(m_TouchValues[1].m_TouchID == -1)
						{
							m_TouchValues[1].m_TouchID = tempTouch.fingerId;
							m_TouchValues[1].m_X = inputTouchX;
							m_TouchValues[1].m_Y = inputTouchY;
							m_TouchValues[1].m_PreX = inputTouchX;
							m_TouchValues[1].m_PreY = inputTouchY;
							m_TouchCount++;
							if(m_TouchCount == 2)
							{
								m_ScaleStartX = (m_TouchValues[0].m_X + m_TouchValues[1].m_X)*0.5f;
								m_ScaleStartY = (m_TouchValues[0].m_Y + m_TouchValues[1].m_Y)*0.5f;
								OnMultiTouchPosSet(m_ScaleStartX, m_ScaleStartY);
								m_TouchBGGapX = (m_ScaleStartX - m_PictureBGCalcX)/m_ScaleP;
								m_TouchBGGapY = (m_ScaleStartY - m_PictureBGCalcY)/m_ScaleP;
							}
						}
					}
					else if(tempTouch.phase == TouchPhase.Moved && m_TouchCount > 0)
					{
						if(m_TouchValues[0].m_TouchID == tempTouch.fingerId)
						{
							m_TouchValues[0].m_PreX = m_TouchValues[0].m_X;
							m_TouchValues[0].m_PreY = m_TouchValues[0].m_Y;
							m_TouchValues[0].m_X = inputTouchX;
							m_TouchValues[0].m_Y = inputTouchY;
						}
						else if(m_TouchValues[1].m_TouchID == tempTouch.fingerId)
						{
							m_TouchValues[1].m_PreX = m_TouchValues[1].m_X;
							m_TouchValues[1].m_PreY = m_TouchValues[1].m_Y;
							m_TouchValues[1].m_X = inputTouchX;
							m_TouchValues[1].m_Y = inputTouchY;
						}

						if(m_TouchCount == 1)
						{
							if(m_FirstDragState)
							{
//								m_TouchData = null;
								OnTouchDrag(tempTouch.fingerId, inputTouchX, inputTouchY);
							}
							else
							{
								if(Mathf.Abs (m_FirstStartX - inputTouchX) > 6 || Mathf.Abs(m_FirstStartY - inputTouchY) > 6)
								{
									m_FirstDragState = true;
									OnFirstTouchDrag (tempTouch.fingerId, inputTouchX, inputTouchY);
								}
							}
						}
						else if(m_TouchCount == 2)
						{
							m_TouchData = null;
							OnTouchScale();
						}
					}
					else if(tempTouch.phase == TouchPhase.Ended && m_TouchCount > 0)
					{
						if(m_TouchCount == 1)
						{
							CACurGamePlayTouchData curPlayTouchData = OnGetTouchData (tempTouch.fingerId, inputTouchX, inputTouchY, false);
							if(m_TouchData != null && curPlayTouchData != null && curPlayTouchData.playTouchType == m_TouchData.playTouchType)
							{
								OnTouchEvent(m_TouchData, inputTouchX, inputTouchY);
							}
						}

						if(m_TouchValues[0].m_TouchID == tempTouch.fingerId)
						{
							m_TouchValues[0].m_TouchID = -1;
							m_TouchCount--;
						}
						else if(m_TouchValues[1].m_TouchID == tempTouch.fingerId)
						{
							m_TouchValues[1].m_TouchID = -1;
							m_TouchCount--;
						}

						if(m_TouchCount == 0 || m_TouchCount == 1)
						{
							m_PreDisValue = 0;
						}

						if (m_TouchData != null)
							m_TouchData = null;

						OnTouchRelease (tempTouch.fingerId, inputTouchX, inputTouchY);

					}
					else if(tempTouch.phase == TouchPhase.Canceled && m_TouchCount > 0)
					{
						if(m_TouchValues[0].m_TouchID == tempTouch.fingerId)
						{
							m_TouchValues[0].m_TouchID = -1;
							m_TouchCount--;
						}
						else if(m_TouchValues[1].m_TouchID == tempTouch.fingerId)
						{
							m_TouchValues[1].m_TouchID = -1;
							m_TouchCount--;
						}

						if(m_TouchCount == 0 || m_TouchCount == 1)
						{
							m_PreDisValue = 0;
						}
					}
				} // for(i=0;i<Input.touchCount;i++)
			} // if(Input.touchCount > 0)
			yield return null;
		}
	}

	IEnumerator UpdateOSXEditorTouch()
	{
		while(curTouchState)
		{
			float inputTouchX = Input.mousePosition.x;
			float inputTouchY = Input.mousePosition.y;

			if(Input.GetMouseButtonDown(0) && OnGetTouchState(0, inputTouchX, inputTouchY))
			{
				m_FirstDragState = false;
				m_FirstStartX = inputTouchX;
				m_FirstStartY = inputTouchY;

				//m_TouchData = OnGetTouchData(0, inputTouchX, inputTouchY);
				m_TouchValues[0].m_TouchID = 0;
				m_TouchValues[0].m_X = inputTouchX;
				m_TouchValues[0].m_Y = inputTouchY;
				m_TouchValues[0].m_PreX = inputTouchX;
				m_TouchValues[0].m_PreY = inputTouchY;
				OnTouchPress (0, inputTouchX, inputTouchY);
                m_TouchData = OnGetTouchData(0, inputTouchX, inputTouchY, true);
            }
			else if(Input.GetMouseButton(0) && m_TouchValues[0].m_TouchID != -1)
			{
				m_TouchValues[0].m_PreX = m_TouchValues[0].m_X;
				m_TouchValues[0].m_PreY = m_TouchValues[0].m_Y;
				m_TouchValues[0].m_X = inputTouchX;
				m_TouchValues[0].m_Y = inputTouchY;

				if(m_FirstDragState)
				{
//					if (m_TouchData != null)
//						m_TouchData = null;
					OnTouchDrag(0, inputTouchX, inputTouchY);
				}
				else
				{
					if(Mathf.Abs (m_FirstStartX - inputTouchX) > 6 || Mathf.Abs(m_FirstStartY - inputTouchY) > 6)
					{
						m_FirstDragState = true;
						OnFirstTouchDrag (0, inputTouchX, inputTouchY);
					}
				}
			}
			else if(Input.GetMouseButtonUp(0) && m_TouchValues[0].m_TouchID != -1)
			{
				CACurGamePlayTouchData curPlayTouchData = OnGetTouchData (0, inputTouchX, inputTouchY, false);
				if(m_TouchData != null && curPlayTouchData != null && curPlayTouchData.playTouchType == m_TouchData.playTouchType)
				{
					OnTouchEvent(m_TouchData, inputTouchX, inputTouchY);
				}
				m_TouchValues[0].m_TouchID = -1;
				m_TouchCount--;
				m_TouchData = null;
				OnTouchRelease(0, inputTouchX, inputTouchY);
			}

			yield return null;
		}

	}

	protected virtual void LoadCommonPlanData()
	{
		GamePlanDataManager.Instance.LoadStopTimeStagePlanData ();
	}

	protected virtual void ReleaseCommonPlanData()
	{
		GamePlanDataManager.Instance.ReleaseStopTimeStagePlanData ();
	}

	#endregion

	#region Touch Handler

	public virtual CACurGamePlayTouchData OnGetTouchData(int touchID, float posX, float posY, bool isTouch)
	{
		return null;
	}

	public virtual bool OnGetTouchState(int touchID, float posX, float posY)
	{
		return true;
	}

	public virtual void OnMultiTouchPosSet(float posX, float posY){}
	public virtual void OnFirstTouchDrag(int touchID, float posX, float posY){}
	public virtual void OnTouchDrag(int touchID, float posX, float posY){}
	public virtual void OnTouchScale(){}

	public virtual void OnTouchEvent(CACurGamePlayTouchData playTouchData, float posX, float posY){}
	public virtual void OnTouchPress(int touchID, float posX, float posY){}
	public virtual void OnTouchRelease(int touchID, float posX, float posY){}

	#endregion

	#region Properties

	public float scaleP
	{
		get{ return m_ScaleP; }
		set{ m_ScaleP = value; }
	}

	public CAGamePlayTouchValue[] touchValues
	{
		get{ return m_TouchValues; }
		set{ m_TouchValues = value; }
	}

	public bool curTouchState
	{
		get{ return m_CurTouchState; }
		set
		{
			if(m_CurTouchState == value)
				return;
			m_CurTouchState = value;
			if(m_CurTouchState)
			{
				if(Application.platform == RuntimePlatform.OSXEditor || Application.platform == RuntimePlatform.WindowsEditor)
				{
					StartCoroutine(UpdateOSXEditorTouch());
				}
				else
				{
					StartCoroutine(UpdateTouchHandle());
				}
			}
            else
            {
                if (Application.platform == RuntimePlatform.OSXEditor || Application.platform == RuntimePlatform.WindowsEditor)
                {
                    StopCoroutine(UpdateOSXEditorTouch());
                }
                else
                {
                    StopCoroutine(UpdateTouchHandle());
                }
            }
        }
	}

	public IEnumerator OnSetScreenResolution()
	{
		Screen.SetResolution ((int)CAResolutionCtl.Instance.GetResolutionWidth (), (int)CAResolutionCtl.Instance.GetResolutionHeight (), true);
		yield return null;
	}

	#endregion

	#region Variables

	int m_TouchCount;
	int m_PreDisValue;
	bool m_FirstDragState;
	float m_FirstStartX;
	float m_FirstStartY;
	float m_ScaleStartX;
	float m_ScaleStartY;
	float m_TouchBGGapX;
	float m_TouchBGGapY;
	float m_PictureBGCalcX;
	float m_PictureBGCalcY;
	float m_ScaleP;
	CAGamePlayTouchValue[] m_TouchValues = new CAGamePlayTouchValue[2];
	CACurGamePlayTouchData m_TouchData = null;
	bool m_CurTouchState = false;

    #endregion

    #region CallBack Methods

    public virtual void OnBackEvent()
    {

    }

    #endregion
}
