﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public enum StepAniType
{
	EnableType,
	CallBackType,
}

public enum StepAniEnableType
{
	None,
	GameObjectType,
	AnimatorType,

}

public class StepAniInfo
{
	#region Variables



	#endregion

	#region Properties

	#endregion

	#region Methods

	#endregion
}

public class StepAnimationManager : MonoBehaviour
{
	#region Sub Class

	[System.Serializable]
	public class StepAniEnableInfo
	{
		#region Serialize Variables

		[SerializeField] StepAniType _aniType;
		[SerializeField] StepAniEnableType _aniEnableType;
		[SerializeField] GameObject _enableObject;
		[SerializeField] float _startEnableTime;
		[SerializeField] EventDelegate _onEnableEvent;

		#endregion

		#region Variables

		bool _isActionState = false;

		#endregion

		#region Properties

		public bool IsActionState
		{
			get{ return _isActionState; }
			set{ _isActionState = value; }
		}

		public StepAniType AniType
		{
			get{ return _aniType; }
		}

		public StepAniEnableType AniEnableType
		{
			get{ return _aniEnableType; }
		}

		public GameObject EnableObject
		{
			get{ return _enableObject; }
		}

		public float StartEnableTime
		{
			get{ return _startEnableTime; }
		}

		public EventDelegate OnEnableEvent
		{
			get{ return _onEnableEvent; }
		}

		#endregion
	}

	#endregion

	#region Serialize Variables

	[SerializeField] StepAniEnableInfo[] _stepAniEnableInfos;

	#endregion

	#region Variables

	float _curAniTime;
	int _curAniIndex;
	bool _isAniStepState = true;

	#endregion

	#region Properties


	#endregion

	#region MonoBehaviour Methods

	void Awake()
	{
		StartCoroutine (OnUpdateStepAni());
//		List<EventDelegate> evenList = new List<EventDelegate> ();
//		evenList.Add (_stepAniEnableInfos [0].onEnableEvent);
//		EventDelegate.Execute (evenList);

		_curAniTime = 0f;
		_curAniIndex = 0;
	}

	// Use this for initialization
	void Start () {
		
	}

	public void OnUpdate()
	{
		
	}

	#endregion

	#region Coroutine Methods

	IEnumerator OnUpdateStepAni()
	{
		while(_isAniStepState)
		{
			OnAniStep ();

			yield return null;
		}
	}

	#endregion

	#region Methods

	void OnAniStep()
	{
		_curAniTime += Time.deltaTime;
		bool isLeftFinishStep = true;
		for (int i = 0; i < _stepAniEnableInfos.Length; i++) {
			if (_stepAniEnableInfos [i].IsActionState)
				continue;

			if (isLeftFinishStep)
				isLeftFinishStep = false;
			if (_stepAniEnableInfos [i].StartEnableTime <= _curAniTime) {
				_stepAniEnableInfos [i].IsActionState = true;
				switch (_stepAniEnableInfos [i].AniType) {
				case StepAniType.EnableType:
					switch (_stepAniEnableInfos [i].AniEnableType) {
					case StepAniEnableType.GameObjectType:
						_stepAniEnableInfos [i].EnableObject.SetActive (true);
						break;
					case StepAniEnableType.AnimatorType:
						Animator enableAnimator = _stepAniEnableInfos [i].EnableObject.GetComponent<Animator> ();
						enableAnimator.enabled = true;
						break;
					}
					break;
				case StepAniType.CallBackType:
					List<EventDelegate> evenList = new List<EventDelegate> ();
					evenList.Add (_stepAniEnableInfos [i].OnEnableEvent);
					EventDelegate.Execute (evenList);
					break;
				}
			}
		}

		if (isLeftFinishStep) {
			_isAniStepState = false;
		}
	}

	#endregion
}
