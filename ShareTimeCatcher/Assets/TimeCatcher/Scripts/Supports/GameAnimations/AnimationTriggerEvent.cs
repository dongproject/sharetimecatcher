﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class AnimationTriggerEvent : MonoBehaviour 
{
	#region Variables

	Action<int> _onTriggerEvent;

	#endregion

	#region Properties

	public Action<int> OnTriggerEvent
	{
		get{ return _onTriggerEvent; }
		set{ _onTriggerEvent = value; }
	}

	#endregion

	#region CallBack Methods

	public void OnAniTriggerEvent(int animType)
	{
		if (_onTriggerEvent != null)
			_onTriggerEvent (animType);
	}

	#endregion
}
