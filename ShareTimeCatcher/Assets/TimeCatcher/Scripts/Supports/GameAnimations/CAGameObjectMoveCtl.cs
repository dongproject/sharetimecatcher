﻿using UnityEngine;
using System.Collections;

public class CAGameObjectMoveCtl
{
	#region Methods

	public void CalcDisAndAngle(float startX, float startY, float destX, float destY, float startScale, float destScale)
	{
		float gapX = destX - startX;
		float gapY = destY - startY;
		m_GapScale = destScale - startScale;
		m_Distance = Mathf.Sqrt((float)(Mathf.Pow(gapX, 2f) + Mathf.Pow(gapY, 2f)));
		float tempDistance = m_Distance/startScale;
		m_Distance = tempDistance*destScale;
		m_MoveValue = m_Distance*5f;

		if(m_MoveValue > 4000f)
		{
			m_MoveValue = 4000f;
		}

		m_SinA = gapY/m_Distance;
		m_CosA = gapX/m_Distance;
		m_MoveTotal = 0.0f;
		m_ScaleSpd = m_GapScale/1.3f;
		m_ScaleTotal = 0.0f;
		if(m_GapScale != 0.0f)
			m_ScaleState = true;
		else
			m_ScaleState = false;

		if(m_Distance == 0f)
		{
			m_MoveState = false;
		}
		else
		{
			m_MoveState = true;
		}
	}

	public void CalcDisAndAngleEx(float startX, float startY, float destX, float destY, float startScale = 1f, float destScale = 1f, float speedValue = 5f, float maxSpeedValue = 6000f)
	{
		float gapX = destX - startX;
		float gapY = destY - startY;
		m_GapScale = destScale - startScale;
		m_Distance = Mathf.Sqrt((float)(Mathf.Pow(gapX, 2f) + Mathf.Pow(gapY, 2f)));
		float tempDistance = m_Distance/startScale;
		m_Distance = tempDistance*destScale;
		m_MoveValue = m_Distance*speedValue;
		if(m_MoveValue > maxSpeedValue)
		{
			m_MoveValue = maxSpeedValue;
		}

		m_SinA = gapY/m_Distance;
		m_CosA = gapX/m_Distance;
		m_MoveTotal = 0.0f;
		m_ScaleSpd = m_GapScale/9.0f;
		m_ScaleTotal = 0.0f;
		if(m_GapScale != 0.0f)
			m_ScaleState = true;
		else
			m_ScaleState = false;
	}

	public void CalcDisAngleFixedSpd(float startX, float startY, float destX, float destY, float moveSpd = 500f)
	{
		float gapX = destX - startX;
		float gapY = destY - startY;
		m_Distance = Mathf.Sqrt((float)(Mathf.Pow(gapX, 2f) + Mathf.Pow(gapY, 2f)));
		m_MoveValue = moveSpd;

		m_SinA = gapY/m_Distance;
		m_CosA = gapX/m_Distance;
		m_MoveTotal = 0.0f;

	}

	public void CalcDisAngleSpd(float startX, float startY, float destX, float destY, float moveTime, float acceleration)
	{
		float gapX = destX - startX;
		float gapY = destY - startY;
		m_Distance = Mathf.Sqrt((float)(Mathf.Pow(gapX, 2f) + Mathf.Pow(gapY, 2f)));
		m_SinA = gapY/m_Distance;
		m_CosA = gapX/m_Distance;
		m_StartX = startX;
		m_StartY = startY;

		m_DestMoveTime = moveTime;
		m_Acceleration = acceleration;

		m_MoveSpeed = (m_Distance - 0.5f*acceleration*(moveTime*moveTime))/moveTime;
		m_CurMoveTime = 0f;
		m_MoveTotal = 0.0f;

		Debug.Log(string.Format("CalcDisAngleSpd m_MoveSpeed : {0}, m_Distance : {1}", m_MoveSpeed, m_Distance));
	}

	public void CalcArcDisAndAngle(float startX, float startY, float destX, float destY, float tempGravity = 0.98f, float tempPowerValue = 10.0f)
	{
		m_Gravity = tempGravity;
		m_PowerValue = tempPowerValue;
		m_StartX = startX;
		m_StartY = startY;
		m_DestX = destX;
		m_DestY = destY;
		float gapX = destX - startX;
		float gapY = destY - startY;

		m_Distance = Mathf.Sqrt((float)(Mathf.Pow(gapX, 2f) + Mathf.Pow(gapY, 2f)));

		m_SinA = gapY/m_Distance;
		m_CosA = gapX/m_Distance;
		m_MoveTotal = 0.0f;

		float temp_A = 0.5f*m_Gravity;
		float temp_B = m_SinA*m_PowerValue;
		float temp_C = Mathf.Abs(gapY);
		float tempValue_1 = -temp_B + Mathf.Sqrt(temp_B*temp_B - 4*temp_A*temp_C);
		float tempValue_2 = -temp_B - Mathf.Sqrt(temp_B*temp_B - 4*temp_A*temp_C);
		if(tempValue_1 > tempValue_2)
		{
			m_DestTimeCount = tempValue_1/(2*temp_A);
		}
		else
		{
			m_DestTimeCount = tempValue_2/(2*temp_A);
		}

		Debug.Log(string.Format("m_StartX : {0}, m_StartY : {1}, m_DestX : {2}, m_DestY : {3}, timeTotalY : {4}", m_StartX, m_StartY, m_DestX, m_DestY, m_DestTimeCount));
		m_SpeedX = gapX/(m_CosA*m_DestTimeCount);
		m_TimeCount = 0;
	}

	public void CalcCurveUpMoveData(float startX, float startY, float destX, float destY, float tempGravity = 0.98f, float tempPowerValue = 10.0f)
	{
		m_Gravity = tempGravity;
		m_PowerValue = tempPowerValue;
		m_StartX = startX;
		m_StartY = startY;
		m_DestX = destX;
		m_DestY = destY;
		float gapX = destX - startX;
		float gapY = destY - startY;

		m_Distance = Mathf.Sqrt((float)(Mathf.Pow(gapX, 2f) + Mathf.Pow(gapY, 2f)));

		m_SinA = gapY/m_Distance;
		m_CosA = gapX/m_Distance;
		m_MoveTotal = 0.0f;

		float temp_A = 0.5f*m_Gravity;
		float temp_B = m_SinA*m_PowerValue;
		float temp_C = -gapY;
		float tempValue_1 = -temp_B + Mathf.Sqrt(temp_B*temp_B - 4*temp_A*temp_C);
		float tempValue_2 = -temp_B - Mathf.Sqrt(temp_B*temp_B - 4*temp_A*temp_C);
		if(tempValue_1 > tempValue_2)
		{
			m_DestTimeCount = tempValue_1/(2*temp_A);
		}
		else
		{
			m_DestTimeCount = tempValue_2/(2*temp_A);
		}
		#if !REAL_MODE && _CHEAT
		Debug.Log(string.Format("CalcCurveUpMoveData m_StartX : {0}, m_StartY : {1}, m_DestX : {2}, m_DestY : {3}, timeTotalY : {4}", m_StartX, m_StartY, m_DestX, m_DestY, m_DestTimeCount));
		#endif
		m_SpeedX = gapX/(m_CosA*m_DestTimeCount);
		m_TimeCount = 0;
	}

	public void CalcCurveDownMoveData(float startX, float startY, float destX, float destY, float tempGravity = 0.98f, float tempPowerValue = 10.0f)
	{
		m_Gravity = tempGravity;
		m_PowerValue = tempPowerValue;
		m_StartX = startX;
		m_StartY = startY;
		m_DestX = destX;
		m_DestY = destY;
		float gapX = destX - startX;
		float gapY = destY - startY;

		m_Distance = Mathf.Sqrt((float)(Mathf.Pow(gapX, 2f) + Mathf.Pow(gapY, 2f)));

		m_SinA = gapY/m_Distance;
		m_CosA = gapX/m_Distance;
		m_MoveTotal = 0.0f;

		float temp_A = 0.5f*m_Gravity;
		float temp_B = m_SinA*m_PowerValue;
		float temp_C = gapY;
		float tempValue_3 = temp_B*temp_B - 4*temp_A*temp_C;
		float tempValue_1 = -temp_B + Mathf.Sqrt(tempValue_3);
		float tempValue_2 = -temp_B - Mathf.Sqrt(tempValue_3);
		if(tempValue_1 > tempValue_2)
		{
			m_DestTimeCount = tempValue_1/(2*temp_A);
		}
		else
		{
			m_DestTimeCount = tempValue_2/(2*temp_A);
		}
		#if !REAL_MODE && _CHEAT
		Debug.Log(string.Format("CalcCurveDownMoveData m_StartX : {0}, m_StartY : {1}, m_DestX : {2}, m_DestY : {3}, timeTotalY : {4}", m_StartX, m_StartY, m_DestX, m_DestY, m_DestTimeCount));
		#endif
		m_SpeedX = gapX/(m_CosA*m_DestTimeCount);
		m_TimeCount = 0;
	}

	#endregion

	#region Variables

	public float m_StartX;
	public float m_StartY;
	public float m_DestX;
	public float m_DestY;
	public float m_MoveValue;
	public float m_Distance;
	public float m_MoveTotal;
	public float m_SinA;
	public float m_CosA;
	public float m_StartScale;
	public float m_DestScale;
	public float m_GapScale;
	public float m_ScaleSpd;
	public float m_ScaleTotal;
	public bool m_ScaleState = true;
	public float m_Gravity;
	public float m_SpeedX;
	public float m_PowerValue;
	public float m_DestTimeCount;
	public float m_TimeCount;
	public bool m_MoveState;
	public float m_MoveSpeed;
	public float m_Acceleration;
	public float m_DestMoveTime;
	public float m_CurMoveTime;

	#endregion
}
