﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CAQueueObjMoveData
{
	#region Properties

	public List<CAGameObjMoveData> lGameObjMoveData
	{
		get{ return m_lGameObjMoveData; }
	}

	#endregion

	#region Variables

	List<CAGameObjMoveData> m_lGameObjMoveData = new List<CAGameObjMoveData>();

	#endregion
}

public class CAGameObjMoveData
{
	#region Properties

	public CAGameObjMoveManager.GameObjMoveType objMoveType
	{
		get{ return m_ObjMoveType; }
		set{ m_ObjMoveType = value; }
	}

	public CAGameObjectMoveCtl gameObjMoveCtl
	{
		get{ return m_GameObjMoveCtl; }
	}

	public GameObject gameMoveObject
	{
		get{ return m_GameMoveObject; }
		set{ m_GameMoveObject = value; }
	}

	public object moveObjData
	{
		get{ return m_MoveObjData; }
		set{ m_MoveObjData = value; }
	}

	public Vector3 startObjPos
	{
		get{ return m_StartObjPos; }
		set{ m_StartObjPos = value; }
	}

	public Vector3 destObjPos
	{
		get{ return m_DestObjPos; }
		set{ m_DestObjPos = value; }
	}

	#endregion

	#region Variables

	CAGameObjMoveManager.GameObjMoveType m_ObjMoveType = CAGameObjMoveManager.GameObjMoveType.eMoveNormal;
	CAGameObjectMoveCtl m_GameObjMoveCtl = new CAGameObjectMoveCtl();
	GameObject m_GameMoveObject = null;
	object m_MoveObjData = null;
	Vector3 m_StartObjPos;
	Vector3 m_DestObjPos;

	#endregion
}

public interface CAGameObjMoveObserver
{
	void OnCompleteGameObjMove (CAGameObjMoveData gameMoveData);
	void OnIntervalGameObjMove (CAGameObjMoveData gameMoveData);
}

public class CAGameObjMoveManager : ScriptableObject 
{
	#region Definitions

	public enum GameObjMoveType
	{
		eMoveNormal,
		eMoveLoop,
	}

	#endregion

	#region Methods

	public void UpdateGameObjMove()
	{
		if (lGameObjMoveData.Count == 0)
			return;
		
		List<CAGameObjMoveData> ldelGameObjMoveData = null;
		for (int i = 0; i < lGameObjMoveData.Count; i++) {
			if(!CalcCurObjMoveData(lGameObjMoveData[i])){
				if (ldelGameObjMoveData == null)
					ldelGameObjMoveData = new List<CAGameObjMoveData> ();
				ldelGameObjMoveData.Add (lGameObjMoveData [i]);
			}
		}

		if (ldelGameObjMoveData != null) {
			for (int i = 0; i < ldelGameObjMoveData.Count; i++) {
				NotifyCompleteGameObjMove (ldelGameObjMoveData [i]);
				lGameObjMoveData.Remove (ldelGameObjMoveData [i]);
			}
		}
	}

	public void UpdateQueueObjMove()
	{
		if (lQueueObjMoveData.Count == 0)
			return;

		List<CAQueueObjMoveData> ldelQueueObjMoveData = null;
		for (int i = 0; i < lQueueObjMoveData.Count; i++) {
			if(!CalcCurQueueObjMoveData(lQueueObjMoveData[i])){
				if (ldelQueueObjMoveData == null)
					ldelQueueObjMoveData = new List<CAQueueObjMoveData> ();
				ldelQueueObjMoveData.Add (lQueueObjMoveData [i]);
			}
		}

		if (ldelQueueObjMoveData != null) {
			for (int i = 0; i < ldelQueueObjMoveData.Count; i++) {
				NotifyCompleteGameObjMove (ldelQueueObjMoveData [i].lGameObjMoveData[0]);
				lQueueObjMoveData.Remove (ldelQueueObjMoveData [i]);
			}
		}
	}

	bool CalcCurObjMoveData(CAGameObjMoveData gameObjMoveData)
	{
		if (gameObjMoveData == null)
			return false;
		
		bool objMoveState = true;

		float speedTemp = gameObjMoveData.gameObjMoveCtl.m_MoveValue*Time.deltaTime;
		if(gameObjMoveData.gameObjMoveCtl.m_MoveTotal + speedTemp > gameObjMoveData.gameObjMoveCtl.m_Distance)
		{
			speedTemp = gameObjMoveData.gameObjMoveCtl.m_Distance - gameObjMoveData.gameObjMoveCtl.m_MoveTotal;

			objMoveState = false;
		}
		else
		{
			gameObjMoveData.gameObjMoveCtl.m_MoveTotal += speedTemp;
		}

		float moveX = gameObjMoveData.gameObjMoveCtl.m_CosA*speedTemp;
		float moveY = gameObjMoveData.gameObjMoveCtl.m_SinA*speedTemp;

		Vector3 moveObjVec = gameObjMoveData.gameMoveObject.transform.localPosition;
		gameObjMoveData.gameMoveObject.transform.localPosition = new Vector3 (moveObjVec.x + moveX, 
			moveObjVec.y + moveY, moveObjVec.z);

		return objMoveState;
	}

	bool CalcCurObjMoveInterval(CAGameObjMoveData gameObjMoveData)
	{
		if (gameObjMoveData == null)
			return false;

		bool objMoveState = true;

		float speedTemp = gameObjMoveData.gameObjMoveCtl.m_MoveValue*Time.deltaTime;
		if(gameObjMoveData.gameObjMoveCtl.m_MoveTotal + speedTemp > gameObjMoveData.gameObjMoveCtl.m_Distance)
		{
			speedTemp = gameObjMoveData.gameObjMoveCtl.m_Distance - gameObjMoveData.gameObjMoveCtl.m_MoveTotal;

			objMoveState = false;
		}
		else
		{
			gameObjMoveData.gameObjMoveCtl.m_MoveTotal += speedTemp;
		}

		curMoveValue += speedTemp;
		if (curMoveValue >= objIntervalValue) {
			NotifyIntervalGameObjMove (gameObjMoveData);
			curMoveValue = 0f;
		}

		float moveX = gameObjMoveData.gameObjMoveCtl.m_CosA*speedTemp;
		float moveY = gameObjMoveData.gameObjMoveCtl.m_SinA*speedTemp;

		Vector3 moveObjVec = gameObjMoveData.gameMoveObject.transform.localPosition;
		gameObjMoveData.gameMoveObject.transform.localPosition = new Vector3 (moveObjVec.x + moveX, 
			moveObjVec.y + moveY, moveObjVec.z);

		return objMoveState;
	}

	bool CalcCurQueueObjMoveData(CAQueueObjMoveData queueObjMoveData)
	{
		if (queueObjMoveData == null || queueObjMoveData.lGameObjMoveData.Count == 0)
			return false;

		if (!CalcCurObjMoveInterval (queueObjMoveData.lGameObjMoveData [0])) {
			if (queueObjMoveData.lGameObjMoveData.Count == 1) {
				return false;
			} else {
				queueObjMoveData.lGameObjMoveData.RemoveAt (0);
				return true;
			}
		} else {
			return true;
		}
	}

	public void AttachGameObjMoveOb(CAGameObjMoveObserver gameObjMoveOb)
	{
		if (m_lGameObjMoveOb.Contains (gameObjMoveOb))
			return;

		m_lGameObjMoveOb.Add (gameObjMoveOb);
	}

	public void DetachGameObjMoveOb(CAGameObjMoveObserver gameObjMoveOb)
	{
		if (!m_lGameObjMoveOb.Contains (gameObjMoveOb))
			return;

		m_lGameObjMoveOb.Remove (gameObjMoveOb);
	}

	void NotifyCompleteGameObjMove (CAGameObjMoveData gameMoveData)
	{
		for (int i = 0; i < m_lGameObjMoveOb.Count; i++) {
			m_lGameObjMoveOb [i].OnCompleteGameObjMove (gameMoveData);
		}
	}

	void NotifyIntervalGameObjMove (CAGameObjMoveData gameMoveData)
	{
		for (int i = 0; i < m_lGameObjMoveOb.Count; i++) {
			m_lGameObjMoveOb [i].OnIntervalGameObjMove (gameMoveData);
		}
	}

	public void AddGameObjMoveData(CAGameObjMoveData inputObjMoveData){
		if (inputObjMoveData == null || m_lGameObjMoveData.Contains (inputObjMoveData))
			return;

		m_lGameObjMoveData.Add (inputObjMoveData);
	}

	public void AddQueueObjMoveData(CAQueueObjMoveData inputQueueObjMoveData, float intervalValue = 0f){
		if (inputQueueObjMoveData == null || m_lQueueObjMoveData.Contains (inputQueueObjMoveData))
			return;

		objIntervalValue = intervalValue;
		m_lQueueObjMoveData.Add (inputQueueObjMoveData);
	}

	public void RemoveGameObjMoveDataByObject(GameObject curMoveObject)
	{
		if (curMoveObject == null || m_lGameObjMoveData.Count == 0)
			return;

		for (int i = 0; i < m_lGameObjMoveData.Count; i++) {
			if (m_lGameObjMoveData [i].gameMoveObject == curMoveObject) {
				m_lGameObjMoveData.RemoveAt (i);
				break;
			}
		}
	}

	public void RemoveQueueObjMoveDataByObject(GameObject curMoveObject)
	{
		if (curMoveObject == null || lQueueObjMoveData.Count == 0)
			return;

		for (int i = 0; i < lQueueObjMoveData.Count; i++) {
			if (lQueueObjMoveData [i].lGameObjMoveData[0].gameMoveObject == curMoveObject) {
				lQueueObjMoveData.RemoveAt (i);
				break;
			}
		}
	}

	#endregion

	#region Properties

	public List<CAGameObjMoveData> lGameObjMoveData
	{
		get{ return m_lGameObjMoveData; }
	}

	public List<CAQueueObjMoveData> lQueueObjMoveData
	{
		get{ return m_lQueueObjMoveData; }
	}

	public float curMoveValue
	{
		get{ return m_CurMoveValue; }
		set{ m_CurMoveValue = value; }
	}

	public float objIntervalValue
	{
		get{ return m_ObjIntervalValue; }
		set{ m_ObjIntervalValue = value; }
	}

	#endregion

	#region Variables

	List<CAGameObjMoveData> m_lGameObjMoveData = new List<CAGameObjMoveData>();
	List<CAGameObjMoveObserver> m_lGameObjMoveOb = new List<CAGameObjMoveObserver>();
	List<CAQueueObjMoveData> m_lQueueObjMoveData = new List<CAQueueObjMoveData>();
	float m_CurMoveValue = 0f;
	float m_ObjIntervalValue = 0f;

	#endregion
}
