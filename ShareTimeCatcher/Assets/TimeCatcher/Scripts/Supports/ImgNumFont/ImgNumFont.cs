﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ImgNumFont : MonoBehaviour 
{
    #region Serialize Variables

#pragma warning disable 649

    //[SerializeField] int _numberFont;
    [SerializeField] GameObject _imgRootObj;
    [SerializeField] float _imgScale = 1f;
    [SerializeField] float _fontGap = 0f;
    [SerializeField] ImgNumberSprite _imgSprite;
    [SerializeField] Color _imgFontColor;

    [SerializeField] string _numberString;
    //[SerializeField] string _saveNumber;

#pragma warning restore 649

    #endregion

    #region Variables

    int _numberValue;
    float _scaleValue = -1f;
    //string _saveNumber = "";
    List<GameObject> _numberObjects = new List<GameObject>();
    Color _saveColor = Color.white;

    #endregion

    #region Properties

    public int NumberValue
    {
        get { return _numberValue; }
        set { _numberValue = value; }
    }

    public string NumberString
    {
        get { return _numberString; }
        set { _numberString = value; }
    }

    #endregion

    #region Methods

    public void SetNumberStr(string numStr)
    {
        if (string.IsNullOrEmpty(numStr))
        {
            ReleaseNumberObjects();
            return;
        }

        if (_numberString == numStr)
            return;

        if (numStr.Length > 10)
            return;

        if(!int.TryParse(numStr, out _numberValue))
        {
            return;
        }

        ReleaseNumberObjects();

        _numberString = numStr;
        //_saveNumber = numStr;

        float fontTotalWidth = 0f;
        float preWidth = 0f;
        for (int i = 0;i< numStr.Length; i++)
        {
            int curNum = int.Parse(numStr[i].ToString());
            Sprite numSprite = _imgSprite.ImgNumSprites[curNum];

            GameObject addNumObj = new GameObject(curNum.ToString());
            addNumObj.transform.SetParent(_imgRootObj.transform);
            addNumObj.transform.localScale = Vector3.one;

            RectTransform numRectTrans = addNumObj.AddComponent<RectTransform>();
            //addNumObj.transform.localPosition = new Vector3(fontTotalWidth, 0f, 0f);

            Image curImage = addNumObj.AddComponent<Image>();
            curImage.sprite = numSprite;
            curImage.color = _imgFontColor;
            curImage.SetNativeSize();
            float curImgWidth = curImage.preferredWidth;

            if (i != 0)
            {
                fontTotalWidth += (preWidth * 0.5f) + (curImgWidth * 0.5f) + _fontGap;
            }

            numRectTrans.anchoredPosition = new Vector2(fontTotalWidth, 0f);
            preWidth = curImgWidth;
            //Debug.Log(string.Format("curImage.preferredWidth : {0}", curImage.preferredWidth));

            _numberObjects.Add(addNumObj);
        }

        float halfWidth = fontTotalWidth * 0.5f;
        for (int i = 0;i< _numberObjects.Count; i++)
        {
            RectTransform numRectTrans = _numberObjects[i].GetComponent<RectTransform>();
            Vector2 anPos = numRectTrans.anchoredPosition;
            numRectTrans.anchoredPosition = new Vector2(anPos.x - halfWidth, anPos.y);
        }
    }

    public void RefreshValue()
    {
        if(_scaleValue != _imgScale)
        {
            _imgRootObj.transform.localScale = new Vector3(_imgScale, _imgScale, 1f);
            _scaleValue = _imgScale;
        }

        ResetNumObjects();

        if (_saveColor != _imgFontColor)
        {
            SetNumImageColor(_imgFontColor);
            //for (int i = 0; i < _numberObjects.Count; i++)
            //{
            //    if(_numberObjects[i] != null)
            //    {
            //        Image numImage = _numberObjects[i].GetComponent<Image>();
            //        if(numImage != null)
            //            numImage.color = _imgFontColor;
            //    }
            //}
        }
    }

    public void SetNumImageColor(Color fontColor)
    {
        _imgFontColor = fontColor;
        _saveColor = fontColor;
        for (int i = 0; i < _numberObjects.Count; i++) {
            if (_numberObjects[i] != null) {
                Image numImage = _numberObjects[i].GetComponent<Image>();
                if (numImage != null)
                    numImage.color = fontColor;
            }
        }
    }

    void ResetNumObjects()
    {
        if (_imgRootObj == null)
            return;

        if (_numberObjects.Count == 0)
        {
            Transform[] numTrans = _imgRootObj.transform.GetComponentsInChildren<Transform>();
            if(numTrans != null && numTrans.Length > 0)
            {
                for(int i = 0;i< numTrans.Length; i++)
                {
                    if (_imgRootObj.transform == numTrans[i]) continue;

                    _numberObjects.Add(numTrans[i].gameObject);
                }
            }
        }
    }

    public void SetImageScaleValue(float scale)
    {
        if (_scaleValue != scale)
        {
            _imgScale = scale;
            _imgRootObj.transform.localScale = new Vector3(_imgScale, _imgScale, 1f);
            _scaleValue = _imgScale;
        }
    }

    void ReleaseNumberObjects()
    {
        ResetNumObjects();

        for (int i = 0;i< _numberObjects.Count; i++)
        {
            if (_numberObjects[i] == null) continue;

            DestroyImmediate(_numberObjects[i]);
        }

        _numberObjects.Clear();
    }

    #endregion
}
