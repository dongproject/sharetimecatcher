﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ImgNumObjects : MonoBehaviour 
{
    #region Serialize Variables

#pragma warning disable 649

    [SerializeField] GameObject[] _imgNumObjs;

#pragma warning restore 649

    #endregion

    #region Properties

    public GameObject[] ImgNumObjs
    {
        get { return _imgNumObjs; }
    }

    #endregion
}
