﻿
#if UNITY_EDITOR
using UnityEditor;

[CustomEditor(typeof(ImgNumFont))]
public class ImgNumFontEditor : Editor 
{
    SerializedProperty imgRootObj;
    SerializedProperty imgScale;
    SerializedProperty fontGap;
    SerializedProperty imageSprite;
    SerializedProperty imgFontColor;

    void OnEnable()
    {
        imgRootObj = serializedObject.FindProperty("_imgRootObj");
        imgScale = serializedObject.FindProperty("_imgScale");
        fontGap = serializedObject.FindProperty("_fontGap");
        imageSprite = serializedObject.FindProperty("_imgSprite");
        imgFontColor = serializedObject.FindProperty("_imgFontColor");
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();
        EditorGUILayout.PropertyField(imgRootObj);
        EditorGUILayout.PropertyField(imgScale);
        EditorGUILayout.PropertyField(fontGap);
        EditorGUILayout.PropertyField(imageSprite);
        EditorGUILayout.PropertyField(imgFontColor);
        serializedObject.ApplyModifiedProperties();

        ImgNumFont imgFont = (ImgNumFont)target;

        imgFont.SetNumberStr(EditorGUILayout.TextField("Input Number", imgFont.NumberString));

        imgFont.RefreshValue();
    }
}

#endif