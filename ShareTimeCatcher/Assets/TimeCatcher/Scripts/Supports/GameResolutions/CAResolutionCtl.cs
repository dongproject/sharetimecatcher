﻿using UnityEngine;
using System.Collections;

public class CAResolutionCtl
{
	private static CAResolutionCtl _instance = null;
	public static CAResolutionCtl Instance
	{
		get
		{
//			if(_instance == null)
//			{
//				_instance = new CAResolutionCtl();
//			}
			return _instance;
		}
	}
	
	#region Variables

	float m_AppOriginalWidth;
	float m_AppOriginalHeight;
	float m_DeviceRealWidth;
	float m_DeviceRealHeight;
	float m_ResolutionWidth;
	float m_ResolutionHeight;
	float m_ResolutionHalfWidth;
	float m_ResolutionHalfHeight;
//	bool _isInitState = false;

	#endregion

	#region Properties

//	public bool IsInitState
//	{
//		get{ return _isInitState; }
//		set{ _isInitState = value; }
//	}

	#endregion
	
	#region Methods

	public static void InitResolutionCtl()
	{
		if (_instance != null)
			return;

		_instance = new CAResolutionCtl ();
	}

	public void InitResolution(float deviceWidth, float deviceHeight, bool isWidth = true)
	{
		m_DeviceRealWidth = deviceWidth;
		m_DeviceRealHeight = deviceHeight;

		if (isWidth) {
			m_AppOriginalWidth = 1136f;
			m_AppOriginalHeight = 640f;
			if (m_DeviceRealWidth / m_DeviceRealHeight > m_AppOriginalWidth / m_AppOriginalHeight) {
				m_ResolutionWidth = m_DeviceRealWidth * (m_AppOriginalHeight / m_DeviceRealHeight);
				m_ResolutionHeight = m_AppOriginalHeight;
			} else if (m_DeviceRealWidth / m_DeviceRealHeight == m_AppOriginalWidth / m_AppOriginalHeight) {
				m_ResolutionWidth = m_AppOriginalWidth;
				m_ResolutionHeight = m_AppOriginalHeight;
			} else {
				m_ResolutionWidth = m_AppOriginalWidth;
				m_ResolutionHeight = m_DeviceRealHeight * (m_AppOriginalWidth/m_DeviceRealWidth);
			}
		} else {
			m_AppOriginalWidth = 640f;
			m_AppOriginalHeight = 1136f;
            if(m_DeviceRealWidth/m_DeviceRealHeight > m_AppOriginalWidth/m_AppOriginalHeight)
            {
            	m_ResolutionWidth = m_DeviceRealWidth * (m_AppOriginalHeight/m_DeviceRealHeight);
            	m_ResolutionHeight = m_AppOriginalHeight;
            } else if (m_DeviceRealWidth / m_DeviceRealHeight == m_AppOriginalWidth / m_AppOriginalHeight) {
            	m_ResolutionWidth = m_AppOriginalWidth;
            	m_ResolutionHeight = m_AppOriginalHeight;
            } else {
            	m_ResolutionWidth = m_AppOriginalWidth;
            	m_ResolutionHeight = m_DeviceRealHeight * (m_AppOriginalWidth/m_DeviceRealWidth);
            }
        }

		m_ResolutionHalfWidth = m_ResolutionWidth * 0.5f;
		m_ResolutionHalfHeight = m_ResolutionHeight * 0.5f;

//		_isInitState = true;
	}
	
	public float GetResolutionWidth()
	{
		return m_ResolutionWidth;
	}
	
	public float GetResolutionHeight()
	{
		return m_ResolutionHeight;
	}
	
	public float GetResolutionHalfWidth()
	{
		return m_ResolutionHalfWidth;
	}
	
	public float GetResolutionHalfHeight()
	{
		return m_ResolutionHalfHeight;
	}
	
	public float CalcResolutionWidth()
	{
		return (m_ResolutionWidth - m_AppOriginalWidth)*0.5f;
	}
	
	public float CalcResolutionHeight()
	{
		return (m_ResolutionHeight - m_AppOriginalHeight)*0.5f;
	}
	
	public float CalcResolutionWidth2X()
	{
		return m_ResolutionWidth - m_AppOriginalWidth;
	}
	
	public float CalcResolutionHeight2Y()
	{
		return m_ResolutionHeight - m_AppOriginalHeight;
	}
	
	public float GetRatioWidth()
	{
		return m_ResolutionWidth/m_AppOriginalWidth;
	}
	
	public float GetRatioHeight()
	{
		return m_ResolutionHeight/m_AppOriginalHeight;
	}
	
	public float GetMaintenanceRatio()
	{
		float retMainRatio = 0f;
		if(GetRatioWidth() / GetRatioHeight() > 0f)
			retMainRatio = GetRatioWidth();
		else
			retMainRatio = GetRatioHeight();
		
		return retMainRatio;
	}
	
	public float GetResolutionXWithRealX(float realX)
	{
		float retX;
		retX = (realX*GetRatioWidth())-realX;
		return retX;
	}
	
	public float GetResolutionYWithRealY(float realY)
	{
		float retY;
		retY = (realY*GetRatioHeight())-realY;
		return retY;
	}
	
	public float GetResolutionX(float x)
	{
		float retX;
		retX = (x*GetRatioWidth());
		return retX;
	}
	
	public float GetResolutionY(float y)
	{
		float retY;
		retY = (y*GetRatioHeight());
		return retY;
	}
	
	public float BackImgStartPositionX()
	{
		float retPositionX = 0f;
		if(GetRatioWidth() < GetRatioHeight())
			retPositionX = m_ResolutionWidth - (m_ResolutionWidth*GetMaintenanceRatio())*0.5f;
		return retPositionX;
	}
	
	public float BackImgStartPositionY()
	{
		float retPositionY = 0f;
		if(GetRatioWidth() > GetRatioHeight())
			retPositionY = m_ResolutionHeight - (m_ResolutionHeight*GetMaintenanceRatio())*0.5f;
		return retPositionY;
	}
	
	public Vector3 GetDefaultImgScale()
	{
		Vector3 tempScale;
		
		tempScale = new Vector3(GetRatioWidth(), GetRatioWidth(), 1f);
		
		return tempScale;
	}

	#endregion
	
	#region Screen Position Methods

	public float GetImageToScreenPosX(float tempX)
	{
//		float retValue = 0f;
//		
//		retValue = (float)GetResolutionHalfWidth() + tempX;
//		
//		return retValue;

		return tempX + (float)GetResolutionHalfWidth();
	}
	
	public float GetImageToScreenPosY(float tempY)
	{
//		float retValue = 0f;
//		
//		retValue = (float)GetResolutionHalfHeight() + tempY;
//		
//		return retValue;

		return tempY + (float)GetResolutionHalfHeight();
	}
	
	public float GetScreenToImagePosX(float tempX)
	{
//		float retValue = 0f;
//		
//		retValue = tempX - (float)GetResolutionHalfWidth();
//		
//		return retValue;

		return tempX - (float)GetResolutionHalfWidth();
	}
	
	public float GetScreenToImagePosY(float tempY)
	{
//		float retValue = 0f;
//		
//		retValue = tempY - (float)GetResolutionHalfHeight();
//		
//		return retValue;

		return tempY - (float)GetResolutionHalfHeight();
	}

	public Vector3 GetScreenToImagePos(Vector3 scrPos)
	{
		return new Vector3 (GetScreenToImagePosX(scrPos.x), GetScreenToImagePosY(scrPos.y), scrPos.z);
	}
	
	public float GetScreenInputWidthPosX(float tempX, float tempWidth)
	{
		float retValue = 0f;
		
		retValue = tempWidth + tempX;
		
		return retValue;
	}

	#endregion
}


















