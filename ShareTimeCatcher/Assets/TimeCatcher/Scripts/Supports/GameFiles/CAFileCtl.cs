﻿using UnityEngine;
using System.Collections;
using System.IO;
using System.Text;

public class CAFileCtl
{
	private static CAFileCtl _instance = null;
	public static CAFileCtl Instance
	{
		get
		{
			if(_instance == null)
			{
				_instance = new CAFileCtl();
			}
			return _instance;
		}
	}
	
	private CAFileCtl()
	{
	}
	
	public void WriteStringToFile( string str, string filename )
	{
		#if !WEB_BUILD
		string path = PathForDocumentsFile( filename );
		File.WriteAllBytes(path, Encoding.UTF8.GetBytes(str));
        //FileStream file = new FileStream (path, FileMode.Create, FileAccess.Write);
        //
        //StreamWriter sw = new StreamWriter( file );
        //sw.WriteLine( str );

        //sw.Close();
        //file.Close();
#endif
    }
	
	
	public string ReadStringFromFile( string filename)
	{
		#if !WEB_BUILD
		string path = PathForDocumentsFile( filename );
		#if _DEBUG
		Debug.Log ("path : " + path);
		#endif
		string str;
		str = null;
		
		if (File.Exists(path))
		{
			str = File.ReadAllText(path, Encoding.UTF8);
			//FileStream file = new FileStream (path, FileMode.Open, FileAccess.Read);
			//StreamReader sr = new StreamReader( file );
			//
			//while(sr.EndOfStream == false)
			//{
			//	str += sr.ReadLine();
			//}
			
			//sr.Close();
			//file.Close();
			
			return str;
		}
		else
		{
			return null;
		}
		#else
		return null;
		#endif 
		
	}
	
	public void DeleteFile(string filename)
	{
		string path = PathForDocumentsFile( filename );
		if(File.Exists(path))
		{
			File.Delete(path);
		}
	}
	
	public string PathForDocumentsFile( string filename ) 
	{
		if (Application.platform == RuntimePlatform.IPhonePlayer)
		{
//			string path = Application.dataPath.Substring( 0, Application.dataPath.Length - 5 );
//			path = path.Substring( 0, path.LastIndexOf( '/' ) );
//			return Path.Combine( Path.Combine( path, "Documents" ), filename );
			return string.Format("{0}/{1}", Application.temporaryCachePath, filename);
		}
		else if(Application.platform == RuntimePlatform.Android)
		{
			string path = Application.persistentDataPath; 
			path = path.Substring(0, path.LastIndexOf( '/' ) ); 
			return Path.Combine (path, filename);
		}
		else 
		{
			string path = Application.dataPath; 
			path = path.Substring(0, path.LastIndexOf( '/' ) );
			return Path.Combine (path, filename);
		}
	}

	//public string PathNameForDocumentsFile () {
	//	string path = Application.dataPath;
	//	if (Application.platform == RuntimePlatform.IPhonePlayer) {
	//		path = Application.dataPath.Substring( 0, Application.dataPath.Length - 5 );
	//		return path.Substring (0, path.LastIndexOf( '/' ) + 1);
	//	}		
	//	else if(Application.platform == RuntimePlatform.Android) {
	//		path = Application.persistentDataPath; 
	//		return path.Substring (0, path.LastIndexOf( '/' ) + 1); 
	//	}
	//	return path.Substring (0, path.LastIndexOf( '/' ) + 1);
	//}
}