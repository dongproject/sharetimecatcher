﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using SettlersEngine;

public enum PathNodeDir
{
	Up 			= 0,
	UpRight		= 1,
	Right		= 2,
	DownRight	= 3,
	Down		= 4,
	DownLeft	= 5,
	Left		= 6,
	UpLeft		= 7,
	Max			= 8,
}

public class CAAStarOpenCloseMap
{
	public CAAStarOpenCloseMap(int width, int height)
	{
		Width = width;
		Height = height;
		Count = 0;

		m_PathNodes = new CAAStarPathNode[width, height];
	}

	public CAAStarPathNode this[int indexX, int indexY]
	{
		get{ return m_PathNodes [indexX, indexY]; }
	}

	public CAAStarPathNode this[CAAStarPathNode pathNode]
	{
		get{ return m_PathNodes [pathNode.IndexX, pathNode.IndexY]; }
	}

	#region Variables

	CAAStarPathNode[,] m_PathNodes;

	#endregion

	#region Properties

	public int Width { get; private set; }
	public int Height { get; private set; }
	public int Count { get; private set; }

	public bool IsEmpty
	{
		get
		{
			return Count == 0;
		}
	}

	#endregion

	#region Methods

	public void InitPathNode(int width, int height)
	{
		Width = width;
		Height = height;
		Count = 0;

		m_PathNodes = new CAAStarPathNode[width, height];
	}

	public void AddPathNode(CAAStarPathNode inputPathNode)
	{
		CAAStarPathNode pathNode = m_PathNodes[inputPathNode.IndexX, inputPathNode.IndexY];

#if DEBUG
		if (pathNode != null)
			throw new ApplicationException();
		#endif

		Count++;
		m_PathNodes[inputPathNode.IndexX, inputPathNode.IndexY] = inputPathNode;
	}

	public void RemovePathNode(CAAStarPathNode pathNode)
	{
		m_PathNodes [pathNode.IndexX, pathNode.IndexY] = null;
		Count--;
	}

	public void RemovePathNode(int indexX, int indexY)
	{
		m_PathNodes [indexX, indexY] = null;
		Count--;
	}

	public bool Contains(CAAStarPathNode inValue)
	{
		CAAStarPathNode item = m_PathNodes[inValue.IndexX, inValue.IndexY];

		if (item == null)
			return false;

		#if DEBUG
		if (!inValue.Equals(item))
			throw new ApplicationException();
		#endif

		return true;
	}

	public void Clear()
	{
		Count = 0;

		for (int x = 0; x < Width; x++)
		{
			for (int y = 0; y < Height; y++)
			{
				m_PathNodes[x, y] = null;
			}
		}
	}

	#endregion
}

public class CAAStarPathFinding
{
	public CAAStarPathFinding(int width, int height, IAStarPathNode[,] aStarPathNodes)
	{
		Width = width;
		Height = height;
		SearchSpace = aStarPathNodes;

		m_SearchSpace = new CAAStarPathNode[Width, Height];

		m_OpenMapInfos = new CAAStarOpenCloseMap (width, height);
		m_CloseMapInfos = new CAAStarOpenCloseMap (width, height);
		m_RuntimeGrid = new CAAStarOpenCloseMap(Width, Height);

		m_CameFrom = new CAAStarPathNode[Width, Height];
		m_OrderedOpenSet = new PriorityQueue<CAAStarPathNode>(CAAStarPathNode.Comparer);

		for (int x = 0; x < Width; x++)
		{
			for (int y = 0; y < Height; y++)
			{

				m_SearchSpace[x, y] = new CAAStarPathNode(x, y, aStarPathNodes[x, y]);
			}
		}
	}

	#region Variables

	CAAStarOpenCloseMap m_OpenMapInfos;
	CAAStarOpenCloseMap m_CloseMapInfos;

	private CAAStarOpenCloseMap m_RuntimeGrid;

	private CAAStarPathNode[,] m_CameFrom;

	private CAAStarPathNode[,] m_SearchSpace;

	private PriorityQueue<CAAStarPathNode> m_OrderedOpenSet;

	int m_GapCost = 10;
	int m_DiagonalCost = 14;

//	List<CAAStarPathNode> m_OpenPathList = new List<CAAStarPathNode>();
//	List<CAAStarPathNode> m_ClosedPathList = new List<CAAStarPathNode>();

	#endregion

	#region Properties

	public int Width { get; private set; }
	public int Height { get; private set; }

	public IAStarPathNode[,] SearchSpace { get; private set; }

	#endregion

	#region Methods

	public List<CA2DIndexValue> GetValidPathList(CA2DIndexValue inStartNode, CA2DIndexValue inEndNode)
	{
		Debug.Log (string.Format ("GetValidPathList end IndexX : {0}, IndexY : {1}", inEndNode.m_IndexX, inEndNode.m_IndexY));
		CAAStarPathNode startNode = m_SearchSpace[inStartNode.m_IndexX, inStartNode.m_IndexY];
		CAAStarPathNode endNode = m_SearchSpace[inEndNode.m_IndexX, inEndNode.m_IndexY];

		if (startNode == endNode)
			return null;

		CAAStarPathNode[] neighborNodes = new CAAStarPathNode[8];

		m_CloseMapInfos.Clear();
		m_OpenMapInfos.Clear();
		m_RuntimeGrid.Clear();
		m_OrderedOpenSet.Clear();

		for (int x = 0; x < Width; x++)
		{
			for (int y = 0; y < Height; y++)
			{
				m_CameFrom[x, y] = null;
			}
		}

		startNode.GValue = 0;
		startNode.HValue = Heuristic(startNode, endNode);
		startNode.FValue = startNode.HValue;

		m_OpenMapInfos.AddPathNode(startNode);
		m_OrderedOpenSet.Push(startNode);

		m_RuntimeGrid.AddPathNode(startNode);

		int nodes = 0;

		while (!m_OpenMapInfos.IsEmpty)
		{
			CAAStarPathNode x = m_OrderedOpenSet.Pop();

			if (x == endNode)
			{
				List<CA2DIndexValue> result = ReconstructPath(m_CameFrom, m_CameFrom[endNode.IndexX, endNode.IndexY]);

				result.Add(new CA2DIndexValue(endNode.IndexX, endNode.IndexY));

				return result;
			}

			m_OpenMapInfos.RemovePathNode(x);
			m_CloseMapInfos.AddPathNode(x);

			StoreNeighborNodes(x, neighborNodes);

			for (int i = 0; i < neighborNodes.Length; i++)
			{
				CAAStarPathNode y = neighborNodes[i];
				Boolean tentative_is_better;

				if (y == null)
					continue;

				if (!y.UserContext.IsAccessable())
					continue;

				if (m_CloseMapInfos.Contains(y))
					continue;

				nodes++;

				int tentative_g_score = m_RuntimeGrid[x].GValue + NeighborDistance(x, y);
				Boolean wasAdded = false;

				if (!m_OpenMapInfos.Contains(y))
				{
					m_OpenMapInfos.AddPathNode(y);
					tentative_is_better = true;
					wasAdded = true;
				}
				else if (tentative_g_score < m_RuntimeGrid[y].GValue)
				{
					tentative_is_better = true;
				}
				else
				{
					tentative_is_better = false;
				}

				if (tentative_is_better)
				{
					m_CameFrom[y.IndexX, y.IndexY] = x;

					if (!m_RuntimeGrid.Contains(y))
						m_RuntimeGrid.AddPathNode(y);

					m_RuntimeGrid[y].GValue = tentative_g_score;
					m_RuntimeGrid[y].HValue = Heuristic(y, endNode);
					m_RuntimeGrid[y].FValue = m_RuntimeGrid[y].GValue + m_RuntimeGrid[y].HValue;

					if (wasAdded)
						m_OrderedOpenSet.Push(y);
					else
						m_OrderedOpenSet.Update(y);
				}
			}
		}

		return null;
	}

	public List<CA2DIndexValue> MakeValidPathData(int width, int height, IAStarPathNode[,] aStarPathNodes, CA2DIndexValue startTileValue, CA2DIndexValue destTileValue, Vector2 startVec, Vector2 destVec)
	{

		CAAStarPathNode inputOpenPathNode = new CAAStarPathNode (startTileValue.m_IndexX, startTileValue.m_IndexY,
			SearchSpace[startTileValue.m_IndexX, startTileValue.m_IndexY]);
		inputOpenPathNode.IndexX = startTileValue.m_IndexX;
		inputOpenPathNode.IndexY = startTileValue.m_IndexY;

		int horizonGap = Mathf.Abs(destTileValue.m_IndexX - inputOpenPathNode.IndexX);
		int verticalGap = Mathf.Abs(destTileValue.m_IndexY - inputOpenPathNode.IndexY);
		inputOpenPathNode.HValue = horizonGap * 10 + verticalGap * 10;
		inputOpenPathNode.GValue = 0;

		m_OpenMapInfos.AddPathNode (inputOpenPathNode);
		StoreNeighborNodes_2 (inputOpenPathNode, destTileValue, aStarPathNodes);

//		int exceptionCount = 0;
//		while (CheckArriveDestNode ()) {
//			exceptionCount++;
//			if (exceptionCount > 10000) {
//				Debug.Log (string.Format ("MakeValidPathData Over Exception Count!!!"));
//				break;
//			}
//		}

		return null;
	}

	bool CheckArriveDestNode()
	{
		return true;
	}

	void MakeOpenList()
	{
		
	}

	public int Heuristic(CAAStarPathNode startNode, CAAStarPathNode endNode)
	{
		int horizonGap = Mathf.Abs(endNode.IndexX - startNode.IndexX);
		int verticalGap = Mathf.Abs(endNode.IndexY - startNode.IndexY);
		return horizonGap * 10 + verticalGap * 10;
	}

	protected virtual int NeighborDistance(CAAStarPathNode inStart, CAAStarPathNode inEnd)
	{
		int diffX = Math.Abs(inStart.IndexX - inEnd.IndexX);
		int diffY = Math.Abs(inStart.IndexY - inEnd.IndexY);

		switch (diffX + diffY)
		{
		case 1: return 10;
		case 2: return 14;
		case 0: return 0;
		default:
			throw new ApplicationException();
		}
	}

	private void StoreNeighborNodes(CAAStarPathNode inAround, CAAStarPathNode[] inNeighbors)
	{
		int x = inAround.IndexX;
		int y = inAround.IndexY;

		if ((x > 0) && (y > 0))
			inNeighbors[0] = m_SearchSpace[x - 1, y - 1];
		else
			inNeighbors[0] = null;

		if (y > 0)
			inNeighbors[1] = m_SearchSpace[x, y - 1];
		else
			inNeighbors[1] = null;

		if ((x < Width - 1) && (y > 0))
			inNeighbors[2] = m_SearchSpace[x + 1, y - 1];
		else
			inNeighbors[2] = null;

		if (x > 0)
			inNeighbors[3] = m_SearchSpace[x - 1, y];
		else
			inNeighbors[3] = null;

		if (x < Width - 1)
			inNeighbors[4] = m_SearchSpace[x + 1, y];
		else
			inNeighbors[4] = null;

		if ((x > 0) && (y < Height - 1))
			inNeighbors[5] = m_SearchSpace[x - 1, y + 1];
		else
			inNeighbors[5] = null;

		if (y < Height - 1)
			inNeighbors[6] = m_SearchSpace[x, y + 1];
		else
			inNeighbors[6] = null;

		if ((x < Width - 1) && (y < Height - 1))
			inNeighbors[7] = m_SearchSpace[x + 1, y + 1];
		else
			inNeighbors[7] = null;
	}


	void StoreNeighborNodes_2(CAAStarPathNode pathNode, CA2DIndexValue destNodeIndex, IAStarPathNode[,] aStarPathNodes)
	{
		int curIndexX = 0;
		int curIndexY = 0;
		int gValue = 0;

		for (int i = 0; i < (int)PathNodeDir.Max; i++) {
			switch ((PathNodeDir)i) {
			case PathNodeDir.Up:
				curIndexX = pathNode.IndexX;
				curIndexY = pathNode.IndexY + 1;
				gValue = 10;
				break;
			case PathNodeDir.UpRight:
				curIndexX = pathNode.IndexX + 1;
				curIndexY = pathNode.IndexY + 1;
				gValue = 14;
				break;
			case PathNodeDir.Right:
				curIndexX = pathNode.IndexX + 1;
				curIndexY = pathNode.IndexY;
				gValue = 10;
				break;
			case PathNodeDir.DownRight:
				curIndexX = pathNode.IndexX + 1;
				curIndexY = pathNode.IndexY - 1;
				gValue = 14;
				break;
			case PathNodeDir.Down:
				curIndexX = pathNode.IndexX;
				curIndexY = pathNode.IndexY - 1;
				gValue = 10;
				break;
			case PathNodeDir.DownLeft:
				curIndexX = pathNode.IndexX - 1;
				curIndexY = pathNode.IndexY - 1;
				gValue = 14;
				break;
			case PathNodeDir.Left:
				curIndexX = pathNode.IndexX - 1;
				curIndexY = pathNode.IndexY;
				gValue = 10;
				break;
			case PathNodeDir.UpLeft:
				curIndexX = pathNode.IndexX - 1;
				curIndexY = pathNode.IndexY + 1;
				gValue = 14;
				break;
			}

			if (curIndexX < 0 || curIndexX >= m_OpenMapInfos.Width || curIndexY < 0 || curIndexY >= m_OpenMapInfos.Height) {
				continue;
			}

			if (m_CloseMapInfos [curIndexX, curIndexY] != null)
				continue;

			if (aStarPathNodes [curIndexX, curIndexY] == null || !aStarPathNodes [curIndexX, curIndexY].IsAccessable ()) {
				CAAStarPathNode inputClosePathNode = new CAAStarPathNode (curIndexX, curIndexY, SearchSpace[curIndexX, curIndexY]);
				m_CloseMapInfos.AddPathNode (inputClosePathNode);
				continue;
			}

			int horizonGap = Mathf.Abs(destNodeIndex.m_IndexX - curIndexX);
			int verticalGap = Mathf.Abs(destNodeIndex.m_IndexY - curIndexY);

			CAAStarPathNode inputOpenPathNode = new CAAStarPathNode (curIndexX, curIndexY, SearchSpace[curIndexX, curIndexY]);
			inputOpenPathNode.IndexX = curIndexX;
			inputOpenPathNode.IndexY = curIndexY;
			inputOpenPathNode.HValue = horizonGap * 10 + verticalGap * 10;
			inputOpenPathNode.GValue = gValue;
			m_OpenMapInfos.AddPathNode (inputOpenPathNode);
		}
	}

	void CalcPathScores(CAAStarPathNode pathNode, CAAStarPathNode neighborNode, CA2DIndexValue destNodeIndex)
	{
		int horizonGap = Mathf.Abs(destNodeIndex.m_IndexX - neighborNode.IndexX);
		int verticalGap = Mathf.Abs(destNodeIndex.m_IndexY - neighborNode.IndexY);
		
		neighborNode.HValue = horizonGap * 10 + verticalGap * 10;
	}

	private List<CA2DIndexValue> ReconstructPath(CAAStarPathNode[,] came_from, CAAStarPathNode current_node)
	{
		List<CA2DIndexValue> result = new List<CA2DIndexValue> ();

		ReconstructPathRecursive(came_from, current_node, result);

		return result;
	}

	private void ReconstructPathRecursive(CAAStarPathNode[,] came_from, CAAStarPathNode current_node, List<CA2DIndexValue> result)
	{
		CAAStarPathNode item = came_from[current_node.IndexX, current_node.IndexY];

		if (item != null)
		{
			ReconstructPathRecursive(came_from, item, result);

			result.Add(new CA2DIndexValue(current_node.IndexX, current_node.IndexY));
		}
		else
			result.Add(new CA2DIndexValue(current_node.IndexX, current_node.IndexY));
	}

	#endregion
}
