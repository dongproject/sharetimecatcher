﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SettlersEngine;

public interface IAStarPathNode
{
	bool IsAccessable();
}

public class CAAStarPathNode : IAStarPathNode, IComparer<CAAStarPathNode>, IIndexedObject
{
	public CAAStarPathNode(int inX, int inY, IAStarPathNode inUserContext)
	{
		m_IndexX = inX;
		m_IndexY = inY;
		UserContext = inUserContext;
	}

	public static readonly CAAStarPathNode Comparer = new CAAStarPathNode(0, 0, default(IAStarPathNode));

	public IAStarPathNode UserContext { get; internal set; }

	#region Variables

	int m_IndexX;
	int m_IndexY;
	int m_GValue;
	int m_HValue;
	int m_FValue;

	#endregion

	#region Properties

	public int Index { get; set; }

	public int IndexX
	{
		get{ return m_IndexX; }
		set{ m_IndexX = value; }
	}

	public int IndexY
	{
		get{ return m_IndexY; }
		set{ m_IndexY = value; }
	}

	public int GValue
	{
		get{ return m_GValue; }
		set{ m_GValue = value; }
	}

	public int HValue
	{
		get{ return m_HValue; }
		set{ m_HValue = value; }
	}

	public int FValue
	{
		get{ return m_FValue; }
		set{ m_FValue = value; }
	}

	#endregion

	#region Methods

	public bool IsAccessable()
	{
		if (UserContext == null)
			return false;

		return UserContext.IsAccessable();
	}

	public int Compare(CAAStarPathNode x, CAAStarPathNode y)
	{
		if (x.FValue < y.FValue)
			return -1;
		else if (x.FValue > y.FValue)
			return 1;

		return 0;
	}

	#endregion
}
