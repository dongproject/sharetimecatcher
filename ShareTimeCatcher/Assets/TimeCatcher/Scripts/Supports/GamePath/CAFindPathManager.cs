﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CAFindPathManager 
{
	#region Definitions

	public enum COMPARE_TILE_DIR
	{
		ePlusHorizontal,
		eMinusHorizontal,
		ePlusVertical,
		eMinusVertical,
	}

	#endregion

	#region Methods

	public List<CAGameObjMoveData> MakeValidPathData(CA2DIndexValue startTileValue, CA2DIndexValue destTileValue, GameObject moveObject, Vector2 startVec, Vector2 destVec, CAGameObjMoveManager.GameObjMoveType objMoveType = CAGameObjMoveManager.GameObjMoveType.eMoveNormal)
	{
		List<CAGameObjMoveData> retGameObjMoveData = null;

		if (startTileValue.m_IndexX == destTileValue.m_IndexX && startTileValue.m_IndexY == destTileValue.m_IndexY) {
			retGameObjMoveData = new List<CAGameObjMoveData> ();
			CAGameObjMoveData inputGameObjMoveData = new CAGameObjMoveData ();
			inputGameObjMoveData.objMoveType = objMoveType;
			inputGameObjMoveData.gameMoveObject = moveObject;
			inputGameObjMoveData.startObjPos = startVec;
			inputGameObjMoveData.destObjPos = destVec;
			inputGameObjMoveData.gameObjMoveCtl.CalcDisAngleFixedSpd (startVec.x, startVec.y, destVec.x, destVec.y);

			retGameObjMoveData.Add (inputGameObjMoveData);
			return retGameObjMoveData;
		}

		CAFindPathData findPathData = new CAFindPathData ();
		findPathData.angleChangeValue = CAMathCtl.Instance.GetAngleChangeValue (startVec.x, startVec.y, 
			destVec.x, destVec.y);
		findPathData.compareTileDir = GetCompareTileDir (findPathData.angleChangeValue);
		findPathData.startTileValue = startTileValue;
		findPathData.destTileValue = destTileValue;
		findPathData.moveObject = moveObject;
		findPathData.startVec = startVec;
		findPathData.destVec = destVec;

		findPathData.curFindPathStep = CAFindPathData.FIND_PATH_STEP.eSearchDirectPath;
		SetCurPathData (findPathData);
		if (findPathData.curFindPathStep == CAFindPathData.FIND_PATH_STEP.eCompleteFindPath) {
			retGameObjMoveData = GetObjPathList (findPathData, moveObject, objMoveType);
		}

		return retGameObjMoveData;
	}

	void SetCurPathData(CAFindPathData findPathData)
	{
		switch (findPathData.curFindPathStep) {
		case CAFindPathData.FIND_PATH_STEP.eSearchDirectPath:
			SetSearchDirectPath (findPathData);
			break;
		case CAFindPathData.FIND_PATH_STEP.eMeetObstacle:
			SetMeetObstaclePathData (findPathData);
			break;
		case CAFindPathData.FIND_PATH_STEP.eFindValidPath:
			break;
		}
	}

	List<CAGameObjMoveData> GetObjPathList(CAFindPathData findPathData, GameObject moveObject, CAGameObjMoveManager.GameObjMoveType objMoveType = CAGameObjMoveManager.GameObjMoveType.eMoveNormal)
	{
		List<CAGameObjMoveData> retlObjPathData = new List<CAGameObjMoveData> ();
		CAGameObjMoveData inputObjMoveData = null;

		if (findPathData.lRouteTileIndex.Count == 1) {
			inputObjMoveData = new CAGameObjMoveData ();
			inputObjMoveData.objMoveType = objMoveType;
			inputObjMoveData.gameMoveObject = moveObject;
			inputObjMoveData.startObjPos = findPathData.startVec;
			inputObjMoveData.destObjPos = findPathData.destVec;
			inputObjMoveData.gameObjMoveCtl.CalcDisAngleFixedSpd (findPathData.startVec.x, findPathData.startVec.y, findPathData.destVec.x, findPathData.destVec.y);
			retlObjPathData.Add (inputObjMoveData);
		} else {
			Vector2 startObjVec = findPathData.startVec;
			Vector2 destObjVec = Vector2.zero;
			for (int i = 0; i < findPathData.lRouteTileIndex.Count; i++) {
				if (findPathData.lRouteTileIndex.Count - 1 == i) {
					destObjVec = findPathData.destVec;
				} else {
//					destObjVec = mapStructureCtrl.GetTilePosByIndex ((short)findPathData.lRouteTileIndex [i].m_IndexX, (short)findPathData.lRouteTileIndex [i].m_IndexY);
				}

				inputObjMoveData = new CAGameObjMoveData ();
				inputObjMoveData.objMoveType = objMoveType;
				inputObjMoveData.gameMoveObject = moveObject;
				inputObjMoveData.startObjPos = startObjVec;
				inputObjMoveData.destObjPos = destObjVec;
				inputObjMoveData.gameObjMoveCtl.CalcDisAngleFixedSpd (startObjVec.x, startObjVec.y, destObjVec.x, destObjVec.y);
				retlObjPathData.Add (inputObjMoveData);

				startObjVec = destObjVec;
			}
		}

		return retlObjPathData;
	}

	void SetSearchDirectPath(CAFindPathData findPathData)
	{
		switch (findPathData.compareTileDir) {
		case COMPARE_TILE_DIR.ePlusHorizontal:
			ComparePlusHorizontal (findPathData, findPathData.angleChangeValue);
			break;
		case COMPARE_TILE_DIR.eMinusHorizontal:
			break;
		case COMPARE_TILE_DIR.ePlusVertical:
			break;
		case COMPARE_TILE_DIR.eMinusVertical:
			break;
		}
	}

	COMPARE_TILE_DIR GetCompareTileDir(CA2DAngleChangeValue angleChangeValue)
	{
		COMPARE_TILE_DIR comTileDir = COMPARE_TILE_DIR.ePlusHorizontal;

		if (Mathf.Abs (angleChangeValue.m_SinA) > Mathf.Abs (angleChangeValue.m_CosA)) {
			if(angleChangeValue.m_SinA > 0)
				comTileDir = COMPARE_TILE_DIR.ePlusVertical;
			else
				comTileDir = COMPARE_TILE_DIR.eMinusVertical;
		} else {
			if(angleChangeValue.m_CosA > 0)
				comTileDir = COMPARE_TILE_DIR.ePlusHorizontal;
			else
				comTileDir = COMPARE_TILE_DIR.eMinusHorizontal;
		}

		return comTileDir;
	}

	void ComparePlusHorizontal(CAFindPathData findPathData, CA2DAngleChangeValue angleChangeValue)
	{
//		float curPosX = findPathData.startVec.x;
//		float curPosY = findPathData.startVec.y;
//
//		float speedValue = mapStructureCtrl.mapGapX / angleChangeValue.m_CosA;
//
//		CA2DIndexValue curTileValue = new CA2DIndexValue (0, 0);
//		int prePosIndexX = findPathData.startTileValue.m_IndexX;
//		int prePosIndexY = findPathData.startTileValue.m_IndexY;
//		int exceptionCount = 0;
//
//		CAMapTileSceneData mapTileSceneData = null;
//		while (true) {
//			if (curPosX + mapStructureCtrl.mapGapX >= findPathData.destVec.x) {
//				speedValue = (findPathData.destVec.x - curPosX) / angleChangeValue.m_CosA;
//				curPosX = findPathData.destVec.x;
//			} else {
//				curPosX += mapStructureCtrl.mapGapX;
//			}
//
//			curPosY += angleChangeValue.m_SinA * speedValue;
//
//			curTileValue = mapStructureCtrl.GetTileValueByPos (curPosX, curPosY);
//			mapTileSceneData = mapStructureCtrl.arrayMapTileData [curTileValue.m_IndexX, curTileValue.m_IndexY];
//
//			if (mapTileSceneData != null && (mapTileSceneData.mapSceneBaseBuilding != null || mapTileSceneData.mapSceneBaseTile != null)) {
//				findPathData.lRouteTileIndex.Add (new CA2DIndexValue(prePosIndexX, prePosIndexY));
//				findPathData.curFindPathStep = CAFindPathData.FIND_PATH_STEP.eMeetObstacle;
//				findPathData.curObstacleTile = curTileValue; 
//				break;
//			}
//
//			if (prePosIndexX == curTileValue.m_IndexX && prePosIndexY == curTileValue.m_IndexY) {
//				findPathData.lRouteTileIndex.Add (curTileValue);
//				findPathData.curFindPathStep = CAFindPathData.FIND_PATH_STEP.eCompleteFindPath;
//				break;
//			}
//
//			prePosIndexX = curTileValue.m_IndexX;
//			prePosIndexY = curTileValue.m_IndexY;
//
//			Debug.Log (string.Format ("curTileValue x : {0}, y : {1}", curTileValue.m_IndexX, curTileValue.m_IndexY));
//
//			if (curPosX == findPathData.destVec.x) {
//				findPathData.lRouteTileIndex.Add (curTileValue);
//				break;
//			}
//
//			exceptionCount++;
//			if (exceptionCount > 5000) {
//				Debug.Log (string.Format ("Compare Over ComparePlusHorizontal !!!!"));
//				break;
//			}
//		}
//
//		if (findPathData.lRouteTileIndex [0].m_IndexX == findPathData.destTileValue.m_IndexX &&
//		   findPathData.lRouteTileIndex [0].m_IndexY == findPathData.destTileValue.m_IndexY) {
//			findPathData.curFindPathStep = CAFindPathData.FIND_PATH_STEP.eCompleteFindPath;
//		}
//
//		if(findPathData.curFindPathStep != CAFindPathData.FIND_PATH_STEP.eCompleteFindPath)
//			SetCurPathData (findPathData);
	}

	void SetMeetObstaclePathData(CAFindPathData findPathData)
	{
		switch (findPathData.compareTileDir) {
		case COMPARE_TILE_DIR.ePlusHorizontal:
			SearchValidPathPlusHorizontal (findPathData);
			break;
		case COMPARE_TILE_DIR.eMinusHorizontal:
			break;
		case COMPARE_TILE_DIR.ePlusVertical:
			break;
		case COMPARE_TILE_DIR.eMinusVertical:
			break;
		}
	}

	void SearchValidPathPlusHorizontal(CAFindPathData findPathData)
	{
//		int curTileX = findPathData.curObstacleTile.m_IndexX;
//		int curTileY = findPathData.curObstacleTile.m_IndexY;
//		int calcTileX = 0;
//		int exceptionCount = 0;
//		int addTileX = 1;
//		CAMapTileSceneData mapTileSceneData = null;
//		CA2DIndexValue upComIndexValue = new CA2DIndexValue (0, 0);
//
//		// Up Compare
//		if (findPathData.curObstacleTile.m_IndexY + 1 < mapStructureCtrl.loadMapGameData.mapArraySizeY) {
//			curTileY++;
//			while (true) {
//				mapTileSceneData = mapStructureCtrl.arrayMapTileData [curTileX, curTileY];
//
//				if (mapTileSceneData.mapSceneBaseBuilding == null && mapTileSceneData.mapSceneBaseTile == null) {
//					upComIndexValue = new CA2DIndexValue (curTileX, curTileY);
//					break;
//				}
//
//				addTileX = 1;
//				calcTileX = curTileX - addTileX;
//				if (calcTileX - addTileX >= 0) {
//					mapTileSceneData = mapStructureCtrl.arrayMapTileData [calcTileX, curTileY];
//
//					if (mapTileSceneData != null) {
//						if (mapTileSceneData.mapSceneBaseBuilding == null && mapTileSceneData.mapSceneBaseTile == null) {
//							while (true) {
//								addTileX++;
//								calcTileX = curTileX - addTileX;
//								mapTileSceneData = mapStructureCtrl.arrayMapTileData [calcTileX, curTileY];
//
//								if (mapTileSceneData.mapSceneBaseBuilding == null && mapTileSceneData.mapSceneBaseTile == null) {
//									upComIndexValue = new CA2DIndexValue (calcTileX, curTileY);
//									break;
//								}
//							}
//						}
//					}
//				}
//
//				exceptionCount++;
//				if (exceptionCount > 5000) {
//					Debug.Log(string.Format("Over SearchValidPathPlusHorizontal While !!!!!"));
//					break;
//				}
//			}
//		}
	}

	#endregion

	#region Properties

//	public CAMapStructureController mapStructureCtrl
//	{
//		get{ return m_MapStructureCtrl; }
//		set{ m_MapStructureCtrl = value; }
//	}

	#endregion

	#region Variables

//	CAMapStructureController m_MapStructureCtrl = null;

	#endregion
}
