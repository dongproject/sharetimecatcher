﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CAFindPathData 
{
	#region Definitions

	public enum FIND_PATH_STEP
	{
		eSearchDirectPath = 0,
		eMeetObstacle,
		eFindValidPath,
		eCompleteFindPath,
	}

	#endregion

	#region Properties

	public FIND_PATH_STEP curFindPathStep
	{
		get{ return m_CurFindPathStep; }
		set{ m_CurFindPathStep = value; }
	}

	public CA2DAngleChangeValue angleChangeValue
	{
		get{ return m_AngleChangeValue; }
		set{ m_AngleChangeValue = value; }
	}

	public CAFindPathManager.COMPARE_TILE_DIR compareTileDir
	{
		get{ return m_CompareTileDir; }
		set{ m_CompareTileDir = value; }
	}

	public CA2DIndexValue startTileValue
	{
		get{ return m_StartTileValue; }
		set{ m_StartTileValue = value; }
	}

	public CA2DIndexValue destTileValue
	{
		get{ return m_DestTileValue; }
		set{ m_DestTileValue = value; }
	}

	public CA2DIndexValue curObstacleTile
	{
		get{ return m_CurObstacleTile; }
		set{ m_CurObstacleTile = value; }
	}

	public GameObject moveObject
	{
		get{ return m_MoveObject; }
		set{ m_MoveObject = value; }
	}

	public Vector2 startVec
	{
		get{ return m_StartVec; }
		set{ m_StartVec = value; }
	}

	public Vector2 destVec
	{
		get{ return m_DestVec; }
		set{ m_DestVec = value; }
	}

	public List<CA2DIndexValue> lRouteTileIndex
	{
		get{ return m_lRouteTileIndex; }
	}

	public List<CA2DIndexValue> lReserveRouteIndex
	{
		get{ return m_lReserveRouteIndex; }
	}

	#endregion

	#region Variables

	FIND_PATH_STEP m_CurFindPathStep = FIND_PATH_STEP.eSearchDirectPath;
	CA2DAngleChangeValue m_AngleChangeValue;
	CAFindPathManager.COMPARE_TILE_DIR m_CompareTileDir;
	CA2DIndexValue m_StartTileValue;
	CA2DIndexValue m_DestTileValue;
	CA2DIndexValue m_CurObstacleTile;
	GameObject m_MoveObject;
	Vector2 m_StartVec;
	Vector2 m_DestVec;
	List<CA2DIndexValue> m_lRouteTileIndex = new List<CA2DIndexValue>();
	List<CA2DIndexValue> m_lReserveRouteIndex = new List<CA2DIndexValue>();

	#endregion
}
