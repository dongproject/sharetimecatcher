﻿Shader "ProjectA/MaskTex" {
	Properties {
		_MainTex ("Base (RGB)", 2D) = "white" {}
		_MaskTex ("Mask (A)", 2D) = "white" {}
		_Color ("Overlay Color", Color) = (1,1,1,1)		
	}
	SubShader {
		Tags { "Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent" }
		Pass {
			BindChannels {
			   Bind "Vertex", vertex
			   Bind "texcoord", texcoord0
			   Bind "texcoord1", texcoord1
			}
			
			Blend SrcAlpha OneMinusSrcAlpha	
			Lighting Off
			ZWrite Off
			Cull Back
			Fog { Mode Off }
			SetTexture[_MainTex] { 
				constantColor [_Color]
				Combine constant * texture
			}
			SetTexture[_MaskTex] { Combine previous * texture }			
		}		
	} 	
}
