using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Tween the object's alpha. Works with both UI widgets as well as renderers.
/// </summary>

[AddComponentMenu("UGUI/Tween/UGUI Tween Alpha")]
public class UGUITweenAlpha : UGUITweener
{
	[Range(0f, 1f)] public float from = 1f;
	[Range(0f, 1f)] public float to = 1f;

	bool mCached = false;

	Graphic mGraphic;
	SpriteRenderer mSr;
	CanvasGroup mCanvasGroup;
    tk2dSprite mtk2dSprite;

	[System.Obsolete("Use 'value' instead")]
	public float alpha { get { return this.value; } set { this.value = value; } }

	void Cache ()
	{
		mCached = true;

		mGraphic = GetComponent<Graphic>();
		mSr = GetComponent<SpriteRenderer>();
		mCanvasGroup = GetComponent<CanvasGroup>();
        mtk2dSprite = GetComponent<tk2dSprite>();
    }

	/// <summary>
	/// Tween's current value.
	/// </summary>

	public float value
	{
		get
		{
			if (!mCached) Cache();
			if(mGraphic != null) return mGraphic.color.a;
			return mGraphic != null ? mGraphic.color.a : 1f;
		}
		set
		{
			if (!mCached) Cache();

			if (mCanvasGroup != null)
			{
				mCanvasGroup.alpha = value;
			}
			else if(mGraphic != null)
			{
				Color c = mGraphic.color;
				c.a = value;
				mGraphic.color = c;
				return;
			} 
			else if (mSr != null)
			{
				Color c = mSr.color;
				c.a = value;
				mSr.color = c;
			}
            else if(mtk2dSprite != null)
            {
                Color c = mtk2dSprite.color;
                c.a = value;
                mtk2dSprite.color = c;
            }
		}
	}

	/// <summary>
	/// Tween the value.
	/// </summary>

	protected override void OnUpdate (float factor, bool isFinished) { value = Mathf.Lerp(from, to, factor); }

	/// <summary>
	/// Start the tweening operation.
	/// </summary>

	static public UGUITweenAlpha Begin (GameObject go, float duration, float alpha)
	{
		UGUITweenAlpha comp = UGUITweener.Begin<UGUITweenAlpha>(go, duration);
		comp.from = comp.value;
		comp.to = alpha;

		if (duration <= 0f)
		{
			comp.Sample(1f, true);
			comp.enabled = false;
		}
		return comp;
	}

	public override void SetStartToCurrentValue () { from = value; }
	public override void SetEndToCurrentValue () { to = value; }
}
