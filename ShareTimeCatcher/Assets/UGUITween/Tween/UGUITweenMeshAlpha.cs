using UnityEngine;
using UnityEngine.UI;

public class UGUITweenMeshAlpha : UGUITweener
{
	[Range(0f, 1f)] public float from = 1f;
	[Range(0f, 1f)] public float to = 1f;

	bool mCached = false;

	Material mMat;

	[System.Obsolete("Use 'value' instead")]
	public float alpha { get { return this.value; } set { this.value = value; } }

	void Cache ()
	{
		mCached = true;

		Renderer render = GetComponentInChildren<Renderer>();
		if( render != null )
		{
			mMat = render.material;
		}
	}

	public float value
	{
		get
		{
			if (!mCached) Cache();

			if (mMat != null) return mMat.color.a;
			return 1.0f;
		}
		set
		{
			if (!mCached) Cache();

			if (mMat != null)
			{
				Color c = mMat.color;
				c.a = value;
				mMat.color = c;
			}
		}
	}

	/// <summary>
	/// Tween the value.
	/// </summary>

	protected override void OnUpdate (float factor, bool isFinished) { value = Mathf.Lerp(from, to, factor); }

	/// <summary>
	/// Start the tweening operation.
	/// </summary>

	static public UGUITweenMeshAlpha Begin (GameObject go, float duration, float alpha)
	{
#if UNITY_EDITOR
		if (!Application.isPlaying) return null;
#endif		
		UGUITweenMeshAlpha comp = UGUITweenMeshAlpha.Begin<UGUITweenMeshAlpha>(go, duration);
		comp.from = comp.value;
		comp.to = alpha;

		if (duration <= 0f)
		{
			comp.Sample(1f, true);
			comp.enabled = false;
		}
		return comp;
	}

	public override void SetStartToCurrentValue () { from = value; }
	public override void SetEndToCurrentValue () { to = value; }
}
