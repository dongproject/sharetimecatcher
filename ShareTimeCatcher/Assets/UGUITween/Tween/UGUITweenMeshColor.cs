using UnityEngine;
using UnityEngine.UI;

public class UGUITweenMeshColor : UGUITweener
{
	public Color from = Color.white;
	public Color to = Color.white;

	bool mCached = false;

	Material mMat;

	[System.Obsolete("Use 'value' instead")]
	public Color color { get { return this.value; } set { this.value = value; } }

	void Cache ()
	{
		mCached = true;

		Renderer render = GetComponentInChildren<Renderer>();
		if( render != null )
		{
			mMat = render.material;
		}
	}

	public Color value
	{
		get
		{
			if (!mCached) Cache();

			if (mMat != null) return mMat.color;
			return Color.black;
		}
		set
		{
			if (!mCached) Cache();

			if (mMat != null)
			{
				mMat.color = value;
				return;
			}
		}
	}

	/// <summary>
	/// Tween the value.
	/// </summary>

	protected override void OnUpdate (float factor, bool isFinished) { value = Color.Lerp(from, to, factor); }

	/// <summary>
	/// Start the tweening operation.
	/// </summary>

	static public UGUITweenMeshColor Begin (GameObject go, float duration, Color color)
	{
#if UNITY_EDITOR
		if (!Application.isPlaying) return null;
#endif		
		UGUITweenMeshColor comp = UGUITweenMeshColor.Begin<UGUITweenMeshColor>(go, duration);
		comp.from = comp.value;
		comp.to = color;

		if (duration <= 0f)
		{
			comp.Sample(1f, true);
			comp.enabled = false;
		}
		return comp;
	}

	public override void SetStartToCurrentValue () { from = value; }
	public override void SetEndToCurrentValue () { to = value; }
}
