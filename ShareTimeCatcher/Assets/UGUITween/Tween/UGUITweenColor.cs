using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Tween the object's color.
/// </summary>

[AddComponentMenu("UGUI/Tween/UGUI Tween Color")]
public class UGUITweenColor : UGUITweener
{
	public Color from = Color.white;
	public Color to = Color.white;

	bool mCached = false;

	Graphic mGraphic;
	SpriteRenderer mSr;

	void Cache ()
	{
		mCached = true;

		mGraphic = GetComponent<Graphic>();
		mSr = GetComponent<SpriteRenderer>();

		if(mGraphic != null)
			return;
	}

	[System.Obsolete("Use 'value' instead")]
	public Color color { get { return this.value; } set { this.value = value; } }

	/// <summary>
	/// Tween's current value.
	/// </summary>

	public Color value
	{
		get
		{
			if (!mCached) Cache();
			if (mGraphic != null) return mGraphic.color;
			return Color.black;
		}
		set
		{
			if (!mCached) Cache();

			if(mGraphic != null)
			{
				mGraphic.color = value;
				return;
			}
			else if(mSr != null)
			{
				mSr.color = value;
				return;
			}
		}
	}

	/// <summary>
	/// Tween the value.
	/// </summary>

	protected override void OnUpdate (float factor, bool isFinished) { value = Color.Lerp(from, to, factor); }

	/// <summary>
	/// Start the tweening operation.
	/// </summary>

	static public UGUITweenColor Begin (GameObject go, float duration, Color color)
	{
#if UNITY_EDITOR
		if (!Application.isPlaying) return null;
#endif
		UGUITweenColor comp = UGUITweener.Begin<UGUITweenColor>(go, duration);
		comp.from = comp.value;
		comp.to = color;

		if (duration <= 0f)
		{
			comp.Sample(1f, true);
			comp.enabled = false;
		}
		return comp;
	}

	[ContextMenu("Set 'From' to current value")]
	public override void SetStartToCurrentValue () { from = value; }

	[ContextMenu("Set 'To' to current value")]
	public override void SetEndToCurrentValue () { to = value; }

	[ContextMenu("Assume value of 'From'")]
	void SetCurrentValueToStart () { value = from; }

	[ContextMenu("Assume value of 'To'")]
	void SetCurrentValueToEnd () { value = to; }
}
