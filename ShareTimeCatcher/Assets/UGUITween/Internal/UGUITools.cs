﻿using UnityEngine;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;

public struct SpeedEntry
{
	public int startIndex;
	public int endIndex;
	public int speed;
}

static public class UGUITools 
{

	static public T FindInParents<T> (Transform trans) where T : Component
	{
		if (trans == null) return null;
		#if UNITY_4_3
		#if UNITY_FLASH
		object comp = trans.GetComponent<T>();
		#else
		T comp = trans.GetComponent<T>();
		#endif
		if (comp == null)
		{
			Transform t = trans.transform.parent;

			while (t != null && comp == null)
			{
				comp = t.gameObject.GetComponent<T>();
				t = t.parent;
			}
		}
		#if UNITY_FLASH
		return (T)comp;
		#else
		return comp;
		#endif
		#else
		return trans.GetComponentInParent<T>();
		#endif
	}

	static public T AddMissingComponent<T> (this GameObject go) where T : Component
	{
		#if UNITY_FLASH
		object comp = go.GetComponent<T>();
		#else
		T comp = go.GetComponent<T>();
		#endif
		if (comp == null)
		{
			#if UNITY_EDITOR
			if (!Application.isPlaying)
				RegisterUndo(go, "Add " + typeof(T));
			#endif
			comp = go.AddComponent<T>();
		}
		#if UNITY_FLASH
		return (T)comp;
		#else
		return comp;
		#endif
	}

	static public void RegisterUndo (UnityEngine.Object obj, string name)
	{
		#if UNITY_EDITOR
		UnityEditor.Undo.RecordObject(obj, name);
		if (obj)
		{
			UnityEditor.EditorUtility.SetDirty(obj);
		}
		#endif
	}

	static public GameObject AddChild (GameObject parent) { return AddChild(parent, true); }

	/// <summary>
	/// Add a new child game object.
	/// </summary>

	static public GameObject AddChild (GameObject parent, bool undo)
	{
		GameObject go = new GameObject();
		#if UNITY_EDITOR
		if (undo) UnityEditor.Undo.RegisterCreatedObjectUndo(go, "Create Object");
		#endif
		if (parent != null)
		{
			Transform t = go.transform;
			t.parent = parent.transform;
			t.localPosition = Vector3.zero;
			t.localRotation = Quaternion.identity;
			t.localScale = Vector3.one;
			go.layer = parent.layer;
		}
		return go;
	}

	/// <summary>
	/// Instantiate an object and add it to the specified parent.
	/// </summary>

	static public GameObject AddChild (GameObject parent, GameObject prefab)
	{
		GameObject go = GameObject.Instantiate(prefab) as GameObject;
		#if UNITY_EDITOR
		UnityEditor.Undo.RegisterCreatedObjectUndo(go, "Create Object");
		#endif
		if (go != null && parent != null)
		{
			Transform t = go.transform;
			t.parent = parent.transform;
			t.localPosition = Vector3.zero;
			t.localRotation = Quaternion.identity;
			t.localScale = Vector3.one;
			go.layer = parent.layer;
		}
		return go;
	}

	static public string GetFuncName (object obj, string method)
	{
		if (obj == null) return "<null>";
		string type = obj.GetType().ToString();
		int period = type.LastIndexOf('/');
		if (period > 0) type = type.Substring(period + 1);
		return string.IsNullOrEmpty(method) ? type : type + "/" + method;
	}
}
