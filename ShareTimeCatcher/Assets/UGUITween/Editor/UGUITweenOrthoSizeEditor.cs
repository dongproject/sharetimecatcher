using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(UGUITweenOrthoSize))]
public class UGUITweenOrthoSizeEditor : UGUITweenerEditor
{
	public override void OnInspectorGUI ()
	{
		GUILayout.Space(6f);
		UGUIEditorTools.SetLabelWidth(120f);

		UGUITweenOrthoSize tw = target as UGUITweenOrthoSize;
		GUI.changed = false;

		float from = EditorGUILayout.FloatField("From", tw.from);
		float to = EditorGUILayout.FloatField("To", tw.to);

		if (from < 0f) from = 0f;
		if (to < 0f) to = 0f;

		if (GUI.changed)
		{
			UGUIEditorTools.RegisterUndo("Tween Change", tw);
			tw.from = from;
			tw.to = to;
			UGUIEditorTools.SetDirty(tw);
		}

		DrawCommonProperties();
	}
}
