//----------------------------------------------
//            NGUI: Next-Gen UI kit
// Copyright © 2011-2015 Tasharen Entertainment
//----------------------------------------------

using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(UGUITweenColor))]
public class UGUITweenColorEditor : UGUITweenerEditor
{
	public override void OnInspectorGUI ()
	{
		GUILayout.Space(6f);
		UGUIEditorTools.SetLabelWidth(120f);

		UGUITweenColor tw = target as UGUITweenColor;
		GUI.changed = false;

		Color from = EditorGUILayout.ColorField("From", tw.from);
		Color to = EditorGUILayout.ColorField("To", tw.to);

		if (GUI.changed)
		{
			UGUIEditorTools.RegisterUndo("Tween Change", tw);
			tw.from = from;
			tw.to = to;
			UGUIEditorTools.SetDirty(tw);
		}

		DrawCommonProperties();
	}
}
