using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(UGUITweenFOV))]
public class UGUITweenFOVEditor : UGUITweenerEditor
{
	public override void OnInspectorGUI ()
	{
		GUILayout.Space(6f);
		UGUIEditorTools.SetLabelWidth(120f);

		UGUITweenFOV tw = target as UGUITweenFOV;
		GUI.changed = false;

		float from = EditorGUILayout.Slider("From", tw.from, 1f, 180f);
		float to = EditorGUILayout.Slider("To", tw.to, 1f, 180f);

		if (GUI.changed)
		{
			UGUIEditorTools.RegisterUndo("Tween Change", tw);
			tw.from = from;
			tw.to = to;
			UGUIEditorTools.SetDirty(tw);
		}

		DrawCommonProperties();
	}
}
